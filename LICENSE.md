# LICENSE

This project **be.seeld.seeld2-pwa** is licensed under a **GNU General Public License 3 (GPL-3.0)** (see https://opensource.org/licenses/GPL-3.0 for details).

## Dependencies to other licenses

* Angular 5.2.11: MIT - https://mit-license.org/
* Ionic 4.20.0: MIT - https://mit-license.org/
* @ionic/storage: MIT - https://mit-license.org/
* @stomp/rx-stomp 0.3.5: MIT - https://mit-license.org/
* angular2-uuid 1.1.1: MIT - https://mit-license.org/
* autolinker 3.11.0: MIT - https://mit-license.org/
* Cordova Android 8.1.0: Apache-2.0 - https://www.apache.org/licenses/LICENSE-2.0
* ionic-angular 3.9.9: MIT - https://mit-license.org/
* ionicons 3.0.0: MIT - https://mit-license.org/
* lodash 4.17.21: MIT - https://mit-license.org/
* luxon 3.3.0: MIT - https://mit-license.org/
* moment 2.29.4: MIT - https://mit-license.org/
* ngx-avatar 3.7.0: MIT - https://mit-license.org/
* ngx-clipboard 10.0.0: MIT - https://mit-license.org/
* ngx-linky 2.2.0:MIT - https://mit-license.org/
* ngx-webstorage-service 3.1.3: MIT - https://mit-license.org/
* rxjs 5.5.11: Apache-2.0 - https://www.apache.org/licenses/LICENSE-2.0
* scrypt-async 2.0.1: MIT - https://mit-license.org/
* sockjs-client 1.4.0: MIT - https://mit-license.org/
* sw-toolbox 3.6.0: Apache-2.0 - https://www.apache.org/licenses/LICENSE-2.0
* text-encoding-utf-8 1.0.2: Public Domain 
* zone.js 0.8.29: MIT - https://mit-license.org/
