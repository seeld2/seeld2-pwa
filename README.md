# seeld2-pwa

This module implements Seeld's main client as a PWA (Progressive Web Application) in Ionic.

# Release Notes


## 2.21.0

* Increase security of user's Display Name storage by saving it directly in the encrypted payload
* Attempt to speed up loading and display of unread conversations for the mobile user
* Fix minor bug when deleting a profile, which would prevent a user that still has a deleted contact in his list to delete his profile

### 2.20.3

* Speed up Conversation message display when accessing a conversation's messages
* Fix unread message refresh when resuming (or logging into) Seeld

### 2.20.2

* Fix bug where, in some cases, the conversation would not be set as "unread" when receiving a new message
* Fix bug with the "battery optimized" alert
* Fix bug related to Android App detection

### 2.20.1

* Add refresh gesture on Conversation page for mobile app

## 2.20.0

* Update the version to a "production ready" version
* Notify user when mobile version exposed by the back end differs from the current version, so that users know they have to update their App
* Fix bug where the client would attempt to delete the processed user event twice

## 0.19.0

* Rewrote conversations and messages syncing
* Improved conversations and messages' display speed by first displaying stored items, and only afterwards syncing with the server
* Fixed "load more" scroll in Conversations page
* Updated the external link to Passphrase definition, which was broken

### 0.18.3

* Fix refresh bug when receiving a new message while in Conversation Page
* Fix positioning of the "battery optimization" message that invites the user to disable battery optimization for Seeld
* Fix disappearance of "battery optimization" message once the user has tapped on it

### 0.18.2

* Do not close app notification if application is in the background

### 0.18.1

* Fix bug of "double app notification" when unread message poller pulls new messages
* Fix "resume" listener so that it is created once (at App level) and sends refresh notifications at resume. This will prevent memory issue caused by registering multiple "resume" listeners every time the ConversationPage is opened.

## 0.18.0

* Ring and notify when polling new messages
* Refresh view when bringing Seeld back from background to foreground
* Propose user to whitelist Seeld in battery optimizations for it to work correctly

## 0.17.0

* Incorporate new seeld2-cordova-plugin-android

### 0.15.1

* Add IDs for e2e tests
* Various technical cleanups

## 0.15.0

* Reactivate user's account deletion
* Disable returning to Conversations after sending a message from the Contacts page: an attempt to prevent a conversations refresh issue that seems to happen only in production...
* Rewrite conversations' services and classes to bypass in-memory caching and directly use storage instead
* Add missing letter-based avatars at contact request

## 0.13.0

* Add letter-based avatars in Conversations, Conversation and Contacts pages
* Fix Logs page's title and improve the information text on it
* Add widget on Logs page to select the logging level
* Add button on Logs page to copy the current logs to the clipboard
* Fix bug that would not redirect user to conversations when sending a new message from the Contacts page 

### 0.12.2

* Remove some verbose logs related to conversations' remote accesses
* Attempt to completely disable HTML pages' caching

### 0.12.1

* Fix missing "refresh" button in conversations page when web app is opened on mobile browsers
* Fix bug that would not refresh conversations when sending a new message from the contacts page 
* Rework app logging statements to comply with documented LoggingLevels

## 0.12.0

* Perform some minor cleanup

### 0.11.2

* Implement an Executor to execute events (or any other task) using a specific priority.
* Implement conversation and message deletion
* Improve anonymity of some API calls
* Fix ugly bug at login when client is attempting to fetch events

### 0.11.1

* Reactivate "delete contact" feature
* Implement tracking of deleted contacts so that one can keep on fetching messages from the old box addresses 
* Fix bug where you could try to send a message without recipients using Ctrl+Enter shortcut
* Mark sync'ed messages locally as unread when requesting a refresh

## 0.11.0

* Adapt remote services to leverage the new security field when querying the API, to thwart data harvesting
* Add "secret key" to user's payload to anonymously fetch data from system box address 
* Fix fetching "new messages" minor error when user has no contacts yet 
* Fix minor warning at validation of passphrase fields in registration page
* Temporarily disable account and contact deletion, because the handling of those cases needs to be completely rewritten

### 0.10.1

* Add "View App Logs" to view client app logs: for debugging and transparency!
* Multiple minor UI improvements: registration page improvements ("eye" icon, etc.), faster "spinner" disappearance once messages have been loaded, "first time" message appearing in contacts' page, etc.
* Rework object/json serialization for more stability
* Multiple technical improvements: transforming classes into decent entities, code restructuring, etc.

## 0.10.0

* Add generation of shared contact HMAC secret key at connection request. This is to be used later to improve anonymity.
* Add post-login process to add a generated HMAC secret key for existing contacts
* Allow refreshing (reloading) contacts
* Add profile deletion menu option, which also send "profile deleted" event to contacts of the deleted profile
* Implement handling of "contact deletion" events so when persons delete their profile then they disappear from their contacts' list of contacts
* Fix bug that would reject a contact request when switching between tabs
* Check user events at each tab switch
* Add AES-encryption of some query data to improve anonymity on some web API exchanges

### 0.9.1

* Add the notion of message "order", which has priority over the date on the ordering of messages
* Implement service which partially syncs with server to fix a client's bad time setup 

## 0.9.0

* Add conversations' and messages' pagination
* Improve synchronization of conversations and messages
* Rethink function of "refresh" buttons on conversations' and messages' pages
* Add unified "loading" modal when network or processing takes too much time
* Restore authentication for fetching messages, to prevent harvesting
* Close "new messages" notification on device (mobile, tablet...) when the Seeld app opens

### 0.8.1

* Make App notification ring when Seeld is open and a new message is received
* Open Seeld when tapping the "new messages" notification
* Improve the notification a bit, adding Seeld's logo on it
* Align "Toast" styles between mobile App and web app
* Show precise timestamp in web app when hovering over a message's timestamp 

## 0.8.0

* Fix login screen to make it work on Android 10 devices
* Show/Hide login password to help mobile users in typing it correctly
* Improve conversations display in Conversations page
* Improve messages display in Conversation page
* Add poll-like ("slow") notifications for App, which works when app is closed or dozing
* Adapt registration and login logic to handle usernames in a case-insensitive way 
* Fix App keystore storage strategy to circumvent limitations on Android API <= 26
* Update logos and improve splash screen
* Add App native Toast notifications

### 0.7.3

* Upgrade to Cordova 8.1 and update plugins accordingly, move to WebView 4
* Switch to custom AndroidKeystore to securely store App session and device storage key
* Fix Platforms for correct device detection
* Fix mobile splash screen

### 0.7.2

* Refactor Session and session-related components to allow device-specific sessions
* Implement device-compatible session objects
* Fix PushNotifications to be compatible with App
* Fix bottom-tabs display to appear only when the session is initialized
* Fix platform.ready() not firing on App
* Fix logic to detect whether we're running in a browser or as an App

### 0.7.1

* Fix bug with push notifications not requesting access to show messages on device
* Adapt polling interval times depending on whether the connection to live notifications was successful or not
* Display an on-screen message if connection to live notifications failed   
* Add some logs to NotificationsListener to inform when connection is failing
* Increased delay to 1 sec before executing post-login conversations sync

## 0.7.0

* Implement receiving notifications from a WebSocket channel
* Change REST API to point to new endpoint "/api/registrations/{username}/availability"
* Fix style to correctly show when pseudo is not available in "registration" page 
* Remove CSRF handling, since cookies usage has been completely removed
* Improve lock handling to correctly receive feedback in case of exception

### 0.6.1

* Fix glitch in contacts page, when accessing it from a new conversation through the "select participants" button

## 0.6.0

* Style the user interface! 
* Display some help notifications when starting Seeld for the first time
* Set Seeld logo for favicon, app icon and splash screens (Android-only)
* Sort contacts by their description in the Contacts page
* Add the conversation initiator to the conversation title generated by default
* Fix minor issue "No element named username could be found in the DOM (validation.ts)" at registration

### 0.5.1

* Fix bug that would keep a recipient in the group conversation even after excluding it
* Fix minor display bug in group conversation's message recipients display

## 0.5.0

* Group discussions
* User selection (for dialogue or group)
* Improve display of messages' senders / recipients
* Fill new conversation title with participants description by default
* Fix refresh bug that happens on first exchange, where the reply would not be received if the initial sender had not closed and reopened the conversation
* Add websql and localstorage as storage drivers to attempt fixing the web app on iOS

### 0.4.1

* Fix bug when sync'ing a conversation's messages (Bad Request due to wrong query object's format)
* Add disclaimer page
* Add "back" button on registration window
* Prepare mobile app by leveraging "tap" events instead of "clicks"

## 0.4.0 (first beta release)

* Improve system to fetch eventually-missing messages when displaying a conversation
* Warn user when registration limit has been reached

### 0.3.2

* When opening a conversation, fetch missing messages (this happens if a conversation is fueled on multiple devices: this ensures other devices end up with all the messages)
* Disable browser's autocomplete on register page's pseudo and passphrase
* Fetch unread messages right after receiving an event of contact acceptance
* Don't show popup when sync'ing conversations right after logging in
* Focus on main fields in login, message editor and connect forms  

### 0.3.1

* Prevent receiving multiple contact requests from the same person
* fix minor bug when sync'ing a conversation's list of messages: the first message would sometimes appear at the bottom instead of the top
* Attempt to disable capitalization of the login field on mobile browsers
* In Ansible playbook, use the correct rfc config js for SRP keys when deploying to production
* Show a "Incorrect pseudo or passphrase... Please check your credentials and try again" when entering wrong credentials at login
* Set the list of participants as the conversation title when no title is specified 
* Enable using Ctrl+Enter to send a message
* At login, sync conversations right after fetching new ones

## 0.3.0

* Adopt secure remote password (SRP) for authentication, so that the user password is never sent over the web! 
* Device storage key, exchange key and user box address are now securely generated on the server side and sent back to client at login, encrypted using SRP's session key
* SRP's M2, JWT and user profile payload are sent at login, along with the user's profile's credentials (which are encrypted using SRP's session key)
* Sensitive data between client and server is now AES-encrypted using the exchange key
* Credentials for resuming a session are now encrypted with the device storage key and stored in session storage for browsers (for security reasons, they are persisted only as far as the tab is open)
* Change the remotely stored (and PGP-encrypted) contacts' structure in the user profile's encrypted payload, to prepare for multiple key types in the future
* Likewise, change the way keys are stored in the encrypted user profile's payload so that multiple types of keys can eventually be stored
* Switch completely to ECC keys for all PGP encrypting work (for increased speed and security)
* Contact requests to other persons, as well as all user events, are fully PGP-encrypted and encrypted with the exchange key before being sent to the server
* The user events' "return payload" is encrypted with the PGP sender's key, so that the system cannot access its data
* Conversation (messages) logic is now rewritten to adopt the same strategy than ConversationS, therefore increasing its reliability

### 0.2.2

* Fix big [#11] not all unread messages appear if multiple messages sent to same conversation

### 0.2.1

* Fix bug [#10] related to unread message fetching when conversations' storage is not initialized yet
* Fix [#7] for displaying a warning when searched pseudo does not exist
* Fix [#7] for displaying a warning when searched pseudo does not exist
* Navigate back to conversations after sending first message of a conversation
* Switch "username" for "pseudo", which better conveys anonymity of users

## 0.2

* Fix bug happening when user logs in and has unread messages waiting
* Upgrade OpenPGP.js to 4.5.1
* Redirect to https if user tries to access app without
* Add hint paragraphs when registering
* Textually warn user at registration when username is not available

### 0.1.5

* Check for unread messages as soon as user is logged in
* Indicate number of unread conversations in browser title
* Improve message sending process' information to the user (notifications and message's check mark -> done all mark)
* Add browser push notifications to inform user of new messages
* Add app-closing prevention on browser if user wants to close the app before message is sent
* Temporarily enable console out for logger in production, for better troubleshooting

### 0.1.4

* Avoid sending credentials when fetching messages / conversations (prevents identification)
* Rewrite login process for the better
* Delete persisted session before registering a new user and when uploading new keys
* Close configuration popup after registering and having the keys downloaded

### 0.1.3

* Fix many issues with the update and refresh of conversations in the Conversations Page
* Add the front-end client version in the sliding menu

### 0.1.2

* Make text in messages selectable and links clickable / tappable
* Remove telling the server to mark conversations / messages as "read"
* Implement synchronization of conversations, which allows keeping multiple devices in sync with the server
* Rewrite the whole conversations' fetching code to improve its efficiency
* Implement a decent logging system
* Various bug fixes

### 0.1.1

* Test square "chat" icons to try and make them clearer in the header band (to switch to conversations)
* In Contacts change the paper plane icon to something better, that conveys the idea of starting a conversation (use the chat bubbles or search the ionic icons using the "start" word)
* Add "title" attributes to buttons for browser versions
* Prevent user from searching one of his contacts or... himself!
* Keep new messages' notifications for longer on screen
* Save generated keys's file by naming it according to the username.
* Try and auto-save the keys
* Try to intercept browser's "back" button and map it to application's "back", otherwise the user might get confused and exit the application
* Disable caching on index.html page and enable cache busting with ansible (generate timestamp, append it to "build" and "assets" folders and modify index.html to update those folders' names)

## 0.1.0

First production release
