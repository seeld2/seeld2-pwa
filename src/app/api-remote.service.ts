import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {config} from "./app.config";
import {Versions} from "./versions";

@Injectable()
export class ApiRemote {

  constructor(private httpClient: HttpClient) {
  }

  versions(): Observable<Versions | undefined> {
    return this.httpClient.get(`${config.api.root}/versions`)
      .pipe(
        map(response => response ? Versions.from(response) : undefined)
      );
  }
}
