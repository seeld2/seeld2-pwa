import {Observable} from "rxjs/Observable";
import {Contact} from "./user/contact/contact";
import {forkJoin} from "rxjs/observable/forkJoin";
import {catchError, mergeMap} from "rxjs/operators";
import {AlertController, Events} from "ionic-angular";
import {SessionService} from "./sessions/session.service";
import {ContactsService} from "./contacts/contacts.service";
import {ProfilesService} from "./profiles/profiles.service";
import {LogoutFunction} from "./logout-function";
import {Injectable} from "@angular/core";
import {AppEvent} from "./app.event";
import {of} from "rxjs/observable/of";

@Injectable()
export class AppDialogsService {

  constructor(private readonly alertController: AlertController,
              private readonly contactsService: ContactsService,
              private readonly events: Events,
              private readonly profilesService: ProfilesService,
              private readonly sessionService: SessionService) {
  }

  deleteProfileTapped(logoutFunction: LogoutFunction) {
    this.alertController.create({
      buttons: [
        {
          cssClass: 'danger',
          handler: () => this.displaySecondDeleteProfileAlert(logoutFunction),
          text: "Yes, delete it"
        },
        {text: "No wait, go back"}
      ],
      enableBackdropDismiss: false,
      message: "You are about to DELETE YOUR SEELD PROFILE. ARE YOU SURE ?",
      title: "DELETE PROFILE"
    }).present();
  }

  private displaySecondDeleteProfileAlert = (logoutFunction: LogoutFunction) => {
    this.alertController.create({
      buttons: [
        {
          cssClass: 'danger',
          handler: () => {
            this.events.publish(AppEvent.ONGOING_PROCESS, true);

            const contacts: Contact[] = this.sessionService.session().contactsAsArray();
            const removeContactsObs: Observable<any>[] = contacts && contacts.length > 0
              ? contacts.map((contact: Contact) => this.contactsService.deleteContact(contact)
                .pipe(
                  catchError(err => {
                    // Do nothing
                    return of(true);
                  }),
                ))
              : [of({})];

            forkJoin(removeContactsObs)
              .pipe(
                mergeMap(() => this.profilesService.deleteProfile())
              )
              .subscribe(
                () => this.displayByeByeAlert(logoutFunction),
                () => {},
                () => this.events.publish(AppEvent.ONGOING_PROCESS, false)
              );
          },
          text: "YES, delete everything forever!!!"
        },
        {text: "Abort, let me think this twice"}
      ],
      enableBackdropDismiss: false,
      message: "This is SERIOUS ! If you confirm, you won't be able to access your messages anymore." +
        " This is IRREVERSIBLE ! ARE YOU SURE ?",
      title: "OMG! DELETE PROFILE!"
    }).present();
  }

  private displayByeByeAlert(logoutFunction: LogoutFunction) {
    this.alertController.create({
      buttons: [{text: 'Logout', handler: () => logoutFunction()}],
      enableBackdropDismiss: false,
      message: "Your profile has fully been deleted now: as far as we're concerned, you've never existed on Seeld. " +
        "Click on the button below to log out, and don't forget to delete the Seeld app and any browser bookmarks that " +
        "might betray you as a former user of Seeld.",
      title: "PROFILE DELETED!"
    }).present();
  }
}
