import {config} from "./app.config";

describe("app.config", () => {

  it("should define vbox and production as secure environments", () => {
    expect(config.environments.secure.indexOf('production')).toBeGreaterThan(-1);
    expect(config.environments.secure.indexOf('vbox')).toBeGreaterThan(-1);
  });
});
