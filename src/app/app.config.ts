import {environment} from "../environments/environment";

const apiServerAddress = `${environment.api.server.address}`;
const apiRoot = `${apiServerAddress}/api`;
const wsApiServerAddress = `${environment.wsapi.server.address}`;

// TODO Split this config file by module. For example, config.ui.notifications should be in "uinotifications" module

export const config = {
  api: {
    conversations: `${apiRoot}/conversations`,
    events: `${apiRoot}/events`,
    credentials: {
      exceptions: [
        // Api
        // {url: '/api/versions', method}
        // Conversations
        {url: '/api/conversations', method: 'PUT'},
        {url: '/api/conversations/ids', method: 'PUT'},
        {url: '/api/conversations/*/messages', method: 'PUT'},
        {url: '/api/conversations/*/state/deleted', method: 'PUT'},
        {url: '/api/conversations/*/messages/*/state/deleted', method: 'PUT'},
        // Events
        {url: '/api/events', method: 'PUT'},
        {url: '/api/events/*/state/deleted', method: 'PUT'},
        // Login
        {url: '/api/login', method: 'POST'},
        {url: '/api/login/challenge/*', method: 'GET'},
        // Messages
        {url: '/api/messages/new', method: 'PUT'},
        {url: '/api/messages/new/timeline/latest', method: 'PUT'},
        // Profiles
        {url: '/api/profiles/*', method: 'GET'},
        // Registration
        {url: '/api/registrations', method: 'POST'},
        {url: '/api/registrations/availability', method: 'GET'},
        {url: '/api/registrations/*/availability', method: 'GET'},
      ],
      methods: ['DELETE', 'GET', 'POST', 'PUT']
    },
    login: `${apiRoot}/login`,
    messages: `${apiRoot}/messages`,
    profiles: `${apiRoot}/profiles`,
    registrations: `${apiRoot}/registrations`,
    root: apiRoot,
    version: 1
  },
  conversations: {
    pageSize: 20
  },
  environments: {
    secure: ['production', 'vbox']
  },
  kdf: {
    N: 32768,
    r: 8,
    p: 2,
    dkLen: 64,
    encoding: 'hex'
  },
  keystore: {
    alias: 'seeld'
  },
  messages: {
    pageSize: 20
  },
  moment: {
    format: {
      date: 'DD/MM/YYYY',
      dateTime: 'DD/MM/YYYY HH:mm',
      dateTimeMillis: 'DD/MM/YYYY HH:mm:ss.SSS'
    }
  },
  pgp: {
    ecc: {
      curve: 'ed25519'
    },
    userIdDomain: '@seeld.io'
  },
  storage: {
    name: 'seeld',
    storeName: 'seeldStore'
  },
  ui: {
    loading: {
      delayBeforeShowingInMillis: 1000
    },
    notifications: {
      duration: 6000,
      textLength: 160,
      title: 'Seeld'
    }
  },
  wsapi: {
    connectionUrl: `${wsApiServerAddress}/wsapi`,
    destination: {
      notification: '/user/notification'
    },
    endpoint: {
      notifications: {
        register: `/wsapi/notifications/register`
      }
    }
  },
  version: '2.22.0'
};
