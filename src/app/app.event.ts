/**
 * App-related events' constants
 */
export enum AppEvent {
  /**
   * Event to indicate whether a process is currently being performed or not. For instance, is a message being encrypted
   * and sent, etc.
   * The data emitted is a boolean that indicates whether a process is ongoing (true) or not (false).
   */
  ONGOING_PROCESS = 'AppEvent:ONGOING_PROCESS',
  /**
   * Event used for requesting to switch to a different tab.
   * The data emitted is one of the constants declared in the **Tab** class.
   */
  TABS_SWITCH = 'AppEvent:TABS_SWITCH'
}
