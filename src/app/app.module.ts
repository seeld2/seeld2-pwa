import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Device} from "@ionic-native/device";
import {File} from "@ionic-native/file";
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from "@ionic/storage";
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {AvatarModule} from "ngx-avatar";
import {ClipboardModule} from "ngx-clipboard";
import {LinkyModule} from "ngx-linky";
import {CoreModule} from "../core/core.module";
import {ApiRemote} from "./api-remote.service";
import {App} from './app';
import {AppDialogsService} from "./app-dialogs.service";
import {ContactsModule} from "./contacts/contacts.module";
import {ConversationsModule} from "./conversations/conversations.module";
import {LoginModule} from "./login/login.module";
import {MobileModule} from "./mobile/mobile.module";
import {NotificationsModule} from "./notifications/notifications.module";
import {AccountKeysPage} from "./pages/accountkeys/account-keys-page";
import {ImportAccountKeys} from "./pages/accountkeys/import-account-keys";
import {ConnectPage} from "./pages/connect/connect-page";
import {ContactActionsPage} from "./pages/contacts/contact-actions.page";
import {ContactsPage} from "./pages/contacts/contacts-page";
import {ConversationActionsPage} from "./pages/conversations/conversation-actions.page";
import {ConversationPage} from "./pages/conversations/conversation/conversation-page";
import {ConversationsPage} from "./pages/conversations/conversations-page";
import {MessageActionsPage} from "./pages/conversations/conversation/message-actions.page";
import {LoginPage} from "./pages/login/login-page";
import {LogsPage} from "./pages/logs/logs-page";
import {RegisterPage} from "./pages/register/register-page";
import {UsernameIsAvailableValidator} from "./pages/register/username-is-available-validator";
import {MainTabsPage} from "./pages/tabs/main-tabs-page";
import {ProfilesModule} from "./profiles/profiles.module";
import {RegistrationsModule} from "./registrations/registrations.module";
import {RequestsInterceptor} from "./requests-interceptor";
import {SessionsModule} from "./sessions/sessions.module";
import {TimeModule} from "./time/time.module";
import {UiNotificationsModule} from "./uinotifications/ui-notifications.module";
import {UserEventsModule} from "./userevents/user-events.module";
import {UtilsModule} from "./utils/utils.module";

@NgModule({
  imports: [
    IonicModule.forRoot(App),
    IonicStorageModule.forRoot({ // TODO Try and move these to app config
      name: 'seeld',
      storeName: 'seeldStore'
    }),
    AvatarModule,
    ClipboardModule,
    ContactsModule,
    ConversationsModule,
    CoreModule,
    LinkyModule,
    LoginModule,
    MobileModule,
    NotificationsModule,
    ProfilesModule,
    RegistrationsModule,
    SessionsModule,
    TimeModule,
    UiNotificationsModule,
    UserEventsModule,
    UtilsModule
  ],
  declarations: [
    App,
    // Pages
    AccountKeysPage,
    ConnectPage,
    ContactActionsPage,
    ContactsPage,
    ConversationActionsPage,
    ConversationPage,
    ConversationsPage,
    LoginPage,
    LogsPage,
    MainTabsPage,
    MessageActionsPage,
    RegisterPage,
    // Components
    ImportAccountKeys,
    // Directives
    UsernameIsAvailableValidator
  ],
  exports: [
    UsernameIsAvailableValidator
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    App,
    AccountKeysPage,
    ConnectPage,
    ContactActionsPage,
    ContactsPage,
    ConversationActionsPage,
    ConversationPage,
    ConversationsPage,
    LoginPage,
    LogsPage,
    MainTabsPage,
    MessageActionsPage,
    RegisterPage
  ],
  providers: [
    // Error Handler
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    // Modules
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // Native
    StatusBar,
    SplashScreen,
    Device,
    File,
    FileChooser,
    FilePath,
    // Requests Interceptor
    {useClass: RequestsInterceptor, provide: HTTP_INTERCEPTORS, multi: true},
    // Services
    AppDialogsService,
    ApiRemote,
  ]
})
export class AppModule {
}
