import {Component, OnInit, ViewChild} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {Events, MenuController, Modal, ModalController, Nav} from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {map, mergeMap, tap} from "rxjs/operators";
import {environment} from "../environments/environment";
import {AppDialogsService} from "./app-dialogs.service";
import {config} from "./app.config";
import {AppEvent} from "./app.event";
import {ConversationsEvent} from "./conversations/conversations-event";
import {ConversationsRefreshParams} from "./conversations/conversations-refresh-params";
import {LoginParams} from "./login/login-params";
import {LoginService} from "./login/login.service";
import {MobileEvent} from "./mobile/mobile.event";
import {PlatformService} from "./mobile/platform.service";
import {RemoteNotificationsServiceProvider} from "./notifications/remote-notifications-service-provider.service";
import {RemoteNotificationsService} from "./notifications/remote-notifications.service";
import {AccountKeysPage} from "./pages/accountkeys/account-keys-page";
import {LoginPage} from "./pages/login/login-page";
import {LogsPage} from "./pages/logs/logs-page";
import {Page} from "./pages/page";
import {RegisterPage} from "./pages/register/register-page";
import {MainTabsPage} from "./pages/tabs/main-tabs-page";
import {SessionEvent} from "./sessions/session-event";
import {SessionService} from "./sessions/session.service";
import {Tab} from "./tab";
import {TimeService} from "./time/time.service";
import {UiNotifications} from "./uinotifications/ui-notifications";
import {Browser} from "./utils/browser";
import {Logger} from "./utils/logging/logger";

/**
 * The App class is the root application class.
 *
 * It is responsible of:<br/>
 * <ul>
 * <li>The application's main navigation point</li>
 * <li>The application's main menu</li>
 * <li>Triggering the modals that are global to the application</li>
 * <li>Starting background processes</li>
 * </ul>
 * It also detects at initialization what the user's session state is, redirecting him/her to the login page as needed.
 */
@Component({
  templateUrl: 'app.html'
})
export class App implements OnInit {

  @ViewChild(Nav) nav: Nav;

  rootPage = MainTabsPage;
  version: string;

  private readonly logger = Logger.for("App");

  private ongoingProcess = false;
  private remoteNotificationsService: RemoteNotificationsService;

  constructor(readonly splashScreen: SplashScreen,
              readonly statusBar: StatusBar,
              private readonly appDialogsService: AppDialogsService,
              private readonly events: Events,
              private readonly loginService: LoginService,
              private readonly menuCtrl: MenuController,
              private readonly modalCtrl: ModalController,
              private readonly notifications: UiNotifications,
              private readonly platformService: PlatformService,
              private readonly remoteNotificationsServiceProvider: RemoteNotificationsServiceProvider,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService,
              private readonly title: Title) {

    platformService.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      if (this.platformService.isBrowser() && config.environments.secure.indexOf(environment.name) > -1) {
        Browser.enforceHttps();
      }

      Logger.setLevel(environment.logging.level);
      Logger.setConsoleOut(environment.logging.consoleOut);

      this.version = config.version;

      this.logger.info(`Version: ${config.version}`);
      this.logger.info(`Platforms: ${platformService.getPlatformProperties()}`);
      this.logger.info(`Is browser: ${platformService.isBrowser()}`);
      this.logger.info(`Is app: ${platformService.isAndroidApp()}`);
      this.logger.info(`Document URL: ${document.URL}`);

      setTimeout(() => {
        this.loginService.restoreSession()
          .subscribe((result: boolean) => {
            if (result) {
              this.switchToConversationsTab();
            } else {
              this.openLoginModal({forceLogin: true});
            }
          });
      }, 1);

      platformService.watchIfAppIsRunningInBackground(() => {
        this.events.publish(MobileEvent.RESUMED_FROM_BACKGROUND, <ConversationsRefreshParams>{});
      });
    });
  }

  ngOnInit(): void {

    this.events.subscribe(AppEvent.ONGOING_PROCESS, (state) => {
      return this.ongoingProcess = state;
    });

    this.events.subscribe(ConversationsEvent.REFRESH_NEW_CONVERSATIONS_COUNT, (count) => {
      this.title.setTitle(count > 0 ? '[' + count + '] Seeld' : 'Seeld');
    });

    this.events.subscribe(SessionEvent.INITIALIZED, () => {
      this.initRemoteNotificationsService()
        .pipe(
          tap(() => {
            this.remoteNotificationsService.registerBoxAddressesInSession();
            this.timeService.adjust();
          })
        )
        .subscribe();
    });

    this.notifications.initialize();

    this.initRemoteNotificationsService()
      .pipe(
        tap(() => this.remoteNotificationsService.initialize())
      )
      .subscribe();

    if (this.platformService.isBrowser()) {
      Browser.disableBackButton();
      Browser.beforeBrowserCloses((e) => {
        if (this.ongoingProcess) {
          e.preventDefault();
          e.returnValue = '';
        } else {
          // Do whatever needs to be done before closing the browser
        }
      });
    }
  }

  // ACTIONS

  accountKeysTapped() {
    this.menuCtrl.close();
    this.openAccountKeysModal();
  }

  deleteProfileTapped() {
    this.appDialogsService.deleteProfileTapped(() => {
      this.logoutTapped();
    });
  }

  logoutTapped() {
    this.menuCtrl.close()
      .then(() => {

        this.loginService.performLogout()
          .pipe(
            mergeMap(() => {
              return this.remoteNotificationsService.stopListening();
            })
          )
          .subscribe(() => {
            location.reload();
          });
      });
  }

  logsTapped() {
    this.modalCtrl.create(LogsPage).present();
  }

  // PUBLIC

  getMenuTitle() {
    return this.sessionService.isSessionInitialized() ? this.sessionService.session().pseudo() : 'Menu';
  }

  // PRIVATE

  private initRemoteNotificationsService(): Observable<RemoteNotificationsService> {
    // TODO This is not needed: RemoteNotificationsServiceProvider already caches the RemoteNotificationsService
    return this.remoteNotificationsService
      ? of(this.remoteNotificationsService)
      : this.remoteNotificationsServiceProvider.obtain()
        .pipe(
          tap((instance: RemoteNotificationsService) => this.remoteNotificationsService = instance),
          map(() => this.remoteNotificationsService)
        );
  }

  private openAccountKeysModal(params: any = {}) {
    const accountKeysModal: Modal = this.modalCtrl.create(AccountKeysPage, params);
    // accountKeysModal.onDidDismiss((armoredKeyPair: ArmoredKeyPair) => {
      //
      // if (armoredKeyPair) {
      //   this.openLoginModal({armoredKeyPair: armoredKeyPair, forceLogin: true});
      // }
    // });

    accountKeysModal.present();
  }

  private openLoginModal(loginParams: LoginParams = {}) {

    const loginModal: Modal = this.modalCtrl.create(LoginPage, {loginParams: loginParams}, {enableBackdropDismiss: false});
    loginModal.onDidDismiss(page => {

      if (page === Page.CONVERSATIONS) {
        this.switchToConversationsTab();
      }

      if (page === Page.REGISTER) {
        this.openRegisterModal();
      }
    });
    loginModal.present();
  }

  private openRegisterModal() {
    const registerModal: Modal = this.modalCtrl.create(RegisterPage, null, {enableBackdropDismiss: false});
    registerModal.onDidDismiss(page => {

      if (page === Page.CONVERSATIONS) {
        this.switchToConversationsTab();
      }

      if (page === Page.LOGIN) {
        this.openLoginModal({forceLogin: true});
      }
    });

    registerModal.present();
  }

  private switchToConversationsTab() {
    this.events.publish(AppEvent.TABS_SWITCH, Tab.CONVERSATIONS);
  }
}
