import {Objects} from "../utils/objects";
import {ContactsService} from "./contacts.service";

export class ContactsJasmine {

  static contactsService() {
    return jasmine.createSpyObj('contactsService', Objects
      .propertyNamesOf(new ContactsService(null, null, null, null, null)));
  }
}
