import {NgModule} from "@angular/core";
import {MobileModule} from "../mobile/mobile.module";
import {UtilsModule} from "../utils/utils.module";
import {ContactsService} from "./contacts.service";
import {ProfilesModule} from "../profiles/profiles.module";
import {UserModule} from "../user/user.module";
import {SessionsModule} from "../sessions/sessions.module";
import {UserEventsModule} from "../userevents/user-events.module";
import {HttpClientModule} from "@angular/common/http";
import {TimeModule} from "../time/time.module";
import {CryptoModule} from "../crypto/crypto.module";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    MobileModule,
    ProfilesModule,
    SessionsModule,
    TimeModule,
    UserEventsModule,
    UserModule,
    UtilsModule
  ],
  providers: [
    ContactsService
  ],
})
export class ContactsModule {
}
