import {of} from "rxjs/observable/of";
import {CoreJasmine} from "../../core/core-jasmine";
import {RemoteNotificationsServiceProvider} from "../notifications/remote-notifications-service-provider.service";
import {WebRemoteNotificationsService} from "../notifications/web-remote-notifications.service";
import {Profile} from "../profiles/profile";
import {ProfilesJasmine} from "../profiles/profiles.jasmine";
import {ProfilesService} from "../profiles/profiles.service";
import {SessionTestBuilder as aSession} from "../sessions/session.test-builder";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {TimeJasmine} from "../time/time.jasmine";
import {TimeService} from "../time/time.service";
import {Contact} from "../user/contact/contact";
import {ContactKey} from "../user/contact/contact-key";
import {Contacts} from "../user/contact/contacts";
import {KeyPair} from "../user/key/key-pair";
import {Keys} from "../user/key/keys";
import {UserEvent} from "../userevents/user-event";
import {UserEventReturn} from "../userevents/user-event-return";
import {UserEventType} from "../userevents/user-event-type";
import {UserEventsJasmine} from "../userevents/user-events.jasmine";
import {UserEventsService} from "../userevents/user-events.service";
import {ContactsService} from "./contacts.service";
import {ContactRemove} from "./remove/contact-remove";
import {ContactRemoveEvent} from "./remove/contact-remove-event";
import {ContactRequest} from "./request/contact-request";
import {ContactRequestEvent} from "./request/contact-request-event";
import {ContactRequestResponse} from "./request/contact-request-response";
import {ContactRequestResponseEvent} from "./request/contact-request-response-event";
import {ContactRequestResponseHandlingResult} from "./request/contact-request-response-handling-result";

describe("ContactsService", () => {

  const ALICE_BOBBY_SECRET_KEY = "aliceBobbySecretKey"

  const ALICE_DISPLAY_NAME: string = "Alice";
  const ALICE_PSEUDO: string = "alice";
  const ALICE_SYSTEM_PUBLIC_KEYS: string = "aliceSystemPublicKeys";
  const ALICE_WRITE_TO: string = "aliceWriteTo";

  const BOBBY_BOX_ADDRESS: string = "bobbyBoxAddress";
  const BOBBY_DISPLAY_NAME: string = "Bobby";
  const BOBBY_MESSAGE: string = "Hello Alice! Want to connect?";
  const BOBBY_PSEUDO: string = "bobbyPseudo";
  const BOBBY_SYSTEM_PUBLIC_KEYS: string = "bobbySystemPublicKeys";
  const BOBBY_WRITE_TO: string = "bobbyWriteTo";

  const CONTACT_TO_REMOVE_ARMORED_PUBLIC_KEYS: string = "armored public keys";
  const CONTACT_TO_REMOVE_BOX_ADDRESS: string = "box address";
  const CONTACT_TO_REMOVE_PSEUDO: string = "alice";
  const CONTACT_TO_REMOVE_WRITE_TO: string = "write to box address";

  const EVENT_ID: string = "eventId";

  const NOW: number = 123456789;

  const PROFILE_BOX_ADDRESS: string = "profile box address";
  const PROFILE_PSEUDO: string = "profile pseudo";
  const PROFILE_SYSTEM_PUBLIC_KEYS: string = "profile system public keys";

  let profilesService: ProfilesService;
  let remoteNotificationsServiceProvider: RemoteNotificationsServiceProvider;
  let sessionService: SessionService;
  let timeService: TimeService;
  let userEventsService: UserEventsService;

  let contactsService: ContactsService;

  let remoteNotificationsService: WebRemoteNotificationsService;
  beforeEach(() => {
    profilesService = ProfilesJasmine.profilesService();
    remoteNotificationsServiceProvider = CoreJasmine.remoteNotificationsServiceProvider();
    sessionService = SessionsJasmine.sessionService();
    timeService = TimeJasmine.timeService();
    userEventsService = UserEventsJasmine.userEventsService();

    contactsService = new ContactsService(profilesService,
      remoteNotificationsServiceProvider, sessionService, timeService, userEventsService);

    remoteNotificationsService = CoreJasmine.remoteNotificationsService();
    (<jasmine.Spy>remoteNotificationsServiceProvider.obtain).and.returnValue(of(remoteNotificationsService));

    // session = Session.build(null, null,
    //   Contacts.fromArrayOfContacts([aContact(BOBBY_PSEUDO, BOBBY_WRITE_TO, BOBBY_SYSTEM_PUBLIC_KEYS)]), null,
    //   ALICE_DISPLAY_NAME, null, systemKeys(ALICE_SYSTEM_PUBLIC_KEYS), null, ALICE_PSEUDO);
    (<jasmine.Spy>sessionService.session).and.returnValue(aSession.with()
      .contacts(Contacts.fromArrayOfContacts([aContact(BOBBY_PSEUDO, BOBBY_WRITE_TO, BOBBY_SYSTEM_PUBLIC_KEYS)]))
      .displayName(ALICE_DISPLAY_NAME).keys(systemKeys(ALICE_SYSTEM_PUBLIC_KEYS)).pseudo(ALICE_PSEUDO).asIs());

    (<jasmine.Spy>timeService.nowAdjusted).and.returnValue(NOW);
  });

  it("it should accept a contact request from another person", (done: DoneFn) => {

    const requesterProfile = Profile.build(BOBBY_BOX_ADDRESS, BOBBY_SYSTEM_PUBLIC_KEYS, BOBBY_PSEUDO);
    (<jasmine.Spy>profilesService.fetchDetails).and.returnValue(of(requesterProfile));
    (<jasmine.Spy>userEventsService.postEventToProfile).and.returnValue(of({}));

    (<jasmine.Spy>sessionService.addContact).and.returnValue(of(true));

    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    const contactRequestEvent: ContactRequestEvent = {
      eventId: EVENT_ID,
      contactRequest: ContactRequest.fromUserEvent(UserEvent.build({
        requesterUsername: BOBBY_PSEUDO,
        requesterDisplayName: BOBBY_DISPLAY_NAME,
        requesterPublicKeys: BOBBY_SYSTEM_PUBLIC_KEYS,
        requesterWriteTo: BOBBY_WRITE_TO,
        requesterMessage: BOBBY_MESSAGE,
        secretKey: ALICE_BOBBY_SECRET_KEY,
      }, EVENT_ID, UserEventType.CONTACT_REQUEST))
    };
    contactsService.acceptContactRequest(contactRequestEvent).subscribe(() => {

      expect(profilesService.fetchDetails).toHaveBeenCalledWith(BOBBY_PSEUDO);
      expect(userEventsService.postEventToProfile).toHaveBeenCalledWith(
        jasmine.any(ContactRequestResponse),
        jasmine.any(UserEventReturn),
        requesterProfile
      );

      expect(remoteNotificationsService.registerBoxAddresses).toHaveBeenCalled();
      expect(sessionService.addContact).toHaveBeenCalled();

      expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(EVENT_ID);

      done();
    });
  });

  it("should handle a contact remove event", (done: DoneFn) => {

    (<jasmine.Spy>sessionService.deleteContact).and.returnValue(of({}));
    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    const event: ContactRemoveEvent = {contactRemove: ContactRemove.for(BOBBY_PSEUDO), eventId: EVENT_ID};
    contactsService.handleContactRemove(event).subscribe((result: boolean) => {

      expect(sessionService.deleteContact).toHaveBeenCalled();
      expect(profilesService.updatePayload).toHaveBeenCalled();
      expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(EVENT_ID);

      expect(result).toBe(true);

      done();
    });
  });

  it("should handle a contact request response where contact was accepted", (done: DoneFn) => {

    (<jasmine.Spy>sessionService.addContact).and.returnValue(of(true));

    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    const contactRequestResponseEvent: ContactRequestResponseEvent = {
      contactRequestResponse: ContactRequestResponse.for(
        ALICE_DISPLAY_NAME,
        ALICE_SYSTEM_PUBLIC_KEYS,
        ALICE_PSEUDO,
        ALICE_WRITE_TO,
        BOBBY_WRITE_TO,
        ALICE_BOBBY_SECRET_KEY
      ),
      eventId: EVENT_ID
    };

    contactsService.handleContactRequestResponse(contactRequestResponseEvent)
      .subscribe((handlingResult: ContactRequestResponseHandlingResult) => {

        expect(remoteNotificationsService.registerBoxAddresses).toHaveBeenCalledWith([BOBBY_WRITE_TO]);
        expect(sessionService.addContact).toHaveBeenCalledTimes(1);

        expect(profilesService.updatePayload).toHaveBeenCalled();
        expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(EVENT_ID);

        expect(handlingResult.hasAcceptedContactRequest).toEqual(true);
        expect(handlingResult.profileContactDescription).toEqual(ALICE_DISPLAY_NAME);

        done();
      });
  });

  it("should handle a contact request response where contact was rejected", (done: DoneFn) => {

    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    const contactRequestResponseEvent: ContactRequestResponseEvent = {
      contactRequestResponse: ContactRequestResponse.for(
        ALICE_DISPLAY_NAME,
        ALICE_SYSTEM_PUBLIC_KEYS,
        ALICE_PSEUDO,
        ALICE_WRITE_TO,
        ALICE_WRITE_TO,
        ALICE_BOBBY_SECRET_KEY
      ),
      eventId: EVENT_ID
    };

    contactsService.handleContactRequestResponse(contactRequestResponseEvent)
      .subscribe((handlingResult: ContactRequestResponseHandlingResult) => {

        expect(remoteNotificationsService.registerBoxAddresses).not.toHaveBeenCalled();
        expect(sessionService.addContact).not.toHaveBeenCalled();

        expect(profilesService.updatePayload).toHaveBeenCalled();
        expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(EVENT_ID);

        expect(handlingResult.hasAcceptedContactRequest).toEqual(false);

        done();
      });
  });

  it("should reject a contact request from another person", (done: DoneFn) => {

    const requesterProfile: Profile = Profile.build(BOBBY_BOX_ADDRESS, BOBBY_SYSTEM_PUBLIC_KEYS, BOBBY_PSEUDO);
    (<jasmine.Spy>profilesService.fetchDetails).and
      .returnValue(of(requesterProfile));
    (<jasmine.Spy>userEventsService.postEventToProfile).and.returnValue(of({}));

    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    const contactRequestEvent: ContactRequestEvent = {
      eventId: EVENT_ID,
      contactRequest: ContactRequest.fromUserEvent(UserEvent.build({
        requesterUsername: BOBBY_PSEUDO,
        requesterDisplayName: BOBBY_DISPLAY_NAME,
        requesterPublicKeys: BOBBY_SYSTEM_PUBLIC_KEYS,
        requesterWriteTo: BOBBY_WRITE_TO,
        requesterMessage: BOBBY_MESSAGE,
        secretKey: ALICE_BOBBY_SECRET_KEY,
      }, EVENT_ID, UserEventType.CONTACT_REQUEST))
    };

    contactsService.rejectContactRequest(contactRequestEvent).subscribe(() => {

      expect(profilesService.fetchDetails).toHaveBeenCalledWith(BOBBY_PSEUDO);
      expect(userEventsService.postEventToProfile).toHaveBeenCalledWith(
        jasmine.any(ContactRequestResponse),
        jasmine.any(UserEventReturn),
        requesterProfile
      );

      expect(remoteNotificationsService.registerBoxAddresses).not.toHaveBeenCalled();
      expect(sessionService.addContact).not.toHaveBeenCalled();

      expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(EVENT_ID);

      done();
    });
  });

  it("should remove a contact from the user's list of contacts", (done: DoneFn) => {

    const profileToRemove: Profile = aProfile(CONTACT_TO_REMOVE_BOX_ADDRESS,
      CONTACT_TO_REMOVE_ARMORED_PUBLIC_KEYS, CONTACT_TO_REMOVE_PSEUDO);
    (<jasmine.Spy>profilesService.fetchDetails).and.returnValue(of(profileToRemove));
    (<jasmine.Spy>sessionService.deleteContact).and.returnValue(of({}));

    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));
    (<jasmine.Spy>userEventsService.postEventToContact).and.returnValue(of({}));

    const contact: Contact = aContact(CONTACT_TO_REMOVE_PSEUDO, CONTACT_TO_REMOVE_WRITE_TO, CONTACT_TO_REMOVE_ARMORED_PUBLIC_KEYS);
    contactsService.deleteContact(contact).subscribe(() => {

      expect(profilesService.fetchDetails).toHaveBeenCalledWith(CONTACT_TO_REMOVE_PSEUDO);
      expect(sessionService.deleteContact).toHaveBeenCalledWith(contact);
      expect(profilesService.updatePayload).toHaveBeenCalled();
      expect(userEventsService.postEventToContact).toHaveBeenCalled();

      const removedContactEvent = (<jasmine.Spy>(userEventsService.postEventToContact)).calls.argsFor(0)[0];
      expect(removedContactEvent['_pseudo']).toEqual(ALICE_PSEUDO);

      const userEventReturn = (<jasmine.Spy>(userEventsService.postEventToContact)).calls.argsFor(0)[1];
      expect(userEventReturn['_pseudo']).toEqual(ALICE_PSEUDO);
      expect(userEventReturn['_userEventType']).toEqual(UserEventType.CONTACT_REMOVE_RETURN);

      done();
    });
  });

  it("should request a contact with a given profile", (done: DoneFn) => {

    const profile: Profile = aProfile(PROFILE_BOX_ADDRESS, PROFILE_SYSTEM_PUBLIC_KEYS, PROFILE_PSEUDO);

    (<jasmine.Spy>userEventsService.postEventToProfile).and.returnValue(of({}));

    contactsService.requestContactWith(profile).subscribe(() => {

      const contactRequest: ContactRequest = (<jasmine.Spy>(userEventsService.postEventToProfile)).calls.argsFor(0)[0];
      expect(contactRequest['_requesterPseudo']).toEqual(ALICE_PSEUDO);

      const userEventReturn: UserEventReturn = (<jasmine.Spy>(userEventsService.postEventToProfile)).calls.argsFor(0)[1];
      expect(userEventReturn['_pseudo']).toEqual(PROFILE_PSEUDO);
      expect(userEventReturn['_userEventType']).toEqual(UserEventType.CONTACT_REQUEST_RETURN);

      const targetedProfile: Profile = (<jasmine.Spy>(userEventsService.postEventToProfile)).calls.argsFor(0)[2];
      expect(targetedProfile).toEqual(profile);

      done();
    });
  });

  function aContact(pseudo: string, writeTo: string, publicKeys: string): Contact {
    return Contact.with(pseudo, null, null, writeTo, ContactKey.with(null, null, publicKeys), null);
  }

  function aProfile(boxAddress: string, systemPublicKeys: string, pseudo: string): Profile {
    return Profile.build(boxAddress, systemPublicKeys, pseudo);
  }

  function systemKeys(publicKeys: string) {
    const keyPairsByKeyId: any = {};
    keyPairsByKeyId[Keys.systemKeysId()] = KeyPair.with(Keys.SYSTEM_KEYS_TYPE, publicKeys, null);
    return Keys.with(keyPairsByKeyId);
  }
});
