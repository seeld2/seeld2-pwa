import {Injectable} from "@angular/core";
import {SessionService} from "../sessions/session.service";
import {Observable} from "rxjs";
import {Contact} from "../user/contact/contact";
import {ProfilesService} from "../profiles/profiles.service";
import {UserEventsService} from "../userevents/user-events.service";
import {UserEventReturn} from "../userevents/user-event-return";
import {TimeService} from "../time/time.service";
import {forkJoin} from "rxjs/observable/forkJoin";
import {UserEventType} from "../userevents/user-event-type";
import {Profile} from "../profiles/profile";
import {ContactRequest} from "./request/contact-request";
import {Session} from "../sessions/session";
import {ContactRequestEvent} from "./request/contact-request-event";
import {ContactRequestResponseEvent} from "./request/contact-request-response-event";
import {ContactRequestResponse} from "./request/contact-request-response";
import {UUID} from "angular2-uuid";
import {map, mergeMap} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import {RemoteNotificationsServiceProvider} from "../notifications/remote-notifications-service-provider.service";
import {RemoteNotificationsService} from "../notifications/remote-notifications.service";
import {ContactRequestResponseHandlingResult} from "./request/contact-request-response-handling-result";
import {ContactRemove} from "./remove/contact-remove";
import {ContactRemoveEvent} from "./remove/contact-remove-event";
import {ContactKey} from "../user/contact/contact-key";
import {Keys} from "../user/key/keys";

@Injectable()
export class ContactsService {

  constructor(private readonly profilesService: ProfilesService,
              private readonly remoteNotificationsServiceProvider: RemoteNotificationsServiceProvider,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService,
              private readonly userEventsService: UserEventsService) {
  }

  /**
   * Accepts a contact request from another person.
   */
  acceptContactRequest(contactRequestEvent: ContactRequestEvent): Observable<string> {
    const session: Session = this.sessionService.session();

    const accepterWriteTo: string = UUID.UUID();
    const contactRequest: ContactRequest = contactRequestEvent.contactRequest;
    const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.for(
      session.displayName(),
      session.systemPublicKeys(),
      session.pseudo(),
      accepterWriteTo,
      contactRequest.requesterWriteTo(),
      contactRequest.secretKey());

    return this.processContactRequestResponse(contactRequestEvent, contactRequestResponse);
  }

  /**
   * Removes the specified contact from the logged pseudo's list of contacts.<br>
   * The payload containing the list of contacts is also updated remotely, and the removed contact is informed that
   * he/she has been removed, by user event.
   */
  deleteContact(contact: Contact): Observable<any> { // TODO Re-test after events are fetched by box address
    const loggedUserPseudo: string = this.sessionService.session().pseudo();

    return this.profilesService.fetchDetails(contact.pseudo())
      .pipe(
        mergeMap((contactProfile: Profile) => {
          return this.userEventsService.postEventToContact(
            ContactRemove.for(loggedUserPseudo),
            UserEventReturn.for(UserEventType.CONTACT_REMOVE_RETURN, loggedUserPseudo, this.timeService.nowAdjusted()),
            contact,
            contactProfile.boxAddress() // TODO For the moment this event is sent to the system box address. Afterwards it will need to use the contact's implementing HMAC and having events fetched on each contact's writeTo box address
          )
        }),
        mergeMap(() => {
          return this.sessionService.deleteContact(contact);
        }),
        mergeMap(() => {
          return this.profilesService.updatePayload();
        }) // We make sure to update the payload AFTER the contact has been removed from the session service
      );
  }

  /**
   * Handles the removal of the logged person as part of one of its friends contacts. Consequently, the contact who
   * removed this logged person must also be removed from this person's list on contacts
   */
  handleContactRemove(contactRemoveEvent: ContactRemoveEvent): Observable<boolean> {
    const contactToRemove: Contact = this.sessionService.session()
      .contact(contactRemoveEvent.contactRemove.pseudo());
    if (contactToRemove) {
      return this.sessionService.deleteContact(contactToRemove)
        .pipe(
          mergeMap(() => {
            return forkJoin(
              this.profilesService.updatePayload(),
              this.userEventsService.deleteEventFromSystemBoxAddress(contactRemoveEvent.eventId)
            );
          }),
          mergeMap(() => of(true))
        );
    } else {
      return this.userEventsService.deleteEventFromSystemBoxAddress(contactRemoveEvent.eventId)
        .pipe(
          mergeMap(() => of(false))
        );
    }
  }

  /**
   * Handles a contact request response.<br>
   * If the response is not silent, the logged person will be notified on whether his or her contact request was
   * accepted or not.
   */
  handleContactRequestResponse(contactRequestResponseEvent: ContactRequestResponseEvent): Observable<ContactRequestResponseHandlingResult> {
    const contactRequestResponse: ContactRequestResponse = contactRequestResponseEvent.contactRequestResponse;

    const contact: Contact = Contact.with(
      contactRequestResponse.accepterPseudo(),
      contactRequestResponse.accepterDisplayName(),
      contactRequestResponse.requesterWriteTo(),
      contactRequestResponse.accepterWriteTo(),
      ContactKey.with(
        Keys.systemKeysId(), // TODO This should actually be extracted from the contact request's username and key type (to be added to the contact request object)
        Keys.SYSTEM_KEYS_TYPE, // TODO This should actually be extracted from the contact request's username and key type (to be added to the contact request object)
        contactRequestResponse.accepterPublicKeys()
      ),
      contactRequestResponse.secretKey(),
      this.timeService.nowAdjusted());

    let updateContactsObs: Observable<any> = of({});
    if (contactRequestResponse.isAccepted()) {
      updateContactsObs = this.remoteNotificationsServiceProvider.obtain()
        .pipe(
          mergeMap((remoteNotificationService: RemoteNotificationsService) => {
            remoteNotificationService.registerBoxAddresses([contactRequestResponse.requesterWriteTo()]);

            return this.sessionService.addContact(contact);
          })
        );
    }

    return updateContactsObs
      .pipe(
        mergeMap(() => {
          return forkJoin(
            this.profilesService.updatePayload(),
            this.userEventsService.deleteEventFromSystemBoxAddress(contactRequestResponseEvent.eventId)
          );
        }),

        map(() => <ContactRequestResponseHandlingResult>{
          hasAcceptedContactRequest: contactRequestResponse.isAccepted(),
          profileContactDescription: contact.description(),
          notify: !contactRequestResponse.isSilent()
        })
      );
  }

  syncContactsToRemote(): Observable<any> {
    return this.profilesService.syncSessionContactsToRemote();
  }

  /**
   * Rejects the contact request. If the <em>silently</em> parameter is set to true, the requester won't see the
   * rejection.
   */
  rejectContactRequest(contactRequestEvent: ContactRequestEvent, silently: boolean = false): Observable<any> {
    const session: Session = this.sessionService.session();
    const contactRequest: ContactRequest = contactRequestEvent.contactRequest;

    const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.for(
      contactRequest.requesterDisplayName(), // We send the requester's display name instead of the rejecter's, because we don't want divulge that if a request is rejected
      contactRequest.requesterPublicKeys(), // We send the rejecter's public keys instead of the requester's, because we don't want divulge that if a request is rejected
      session.pseudo(),
      contactRequest.requesterWriteTo(),
      contactRequest.requesterWriteTo(), // By sending the same values for accepterWriteTo and requesterWriteTo, we subtly imply that the response is a rejection (but seen from outside it's just a contact response, no clue on its result
      contactRequest.secretKey(),
      silently);

    return this.processContactRequestResponse(contactRequestEvent, contactRequestResponse);
  }

  /**
   * Rejects the contact request in a way that the requester will not be notified of that rejection.
   */
  rejectContactRequestSilently(contactRequestEvent: ContactRequestEvent): Observable<any> {
    return this.rejectContactRequest(contactRequestEvent, true);
  }

  /**
   * Sends a contact request to connect with the specified profile.
   */
  requestContactWith(profile: Profile): Observable<any> {

    const session: Session = this.sessionService.session();
    const contactRequest: ContactRequest = ContactRequest
      .for(session.pseudo(), session.displayName(), session.systemPublicKeys());

    const userEventReturn: UserEventReturn = UserEventReturn
      .for(UserEventType.CONTACT_REQUEST_RETURN, profile.pseudo(), this.timeService.nowAdjusted());

    return this.userEventsService.postEventToProfile(contactRequest, userEventReturn, profile);
  }

  // PRIVATE

  private processContactRequestResponse(contactRequestEvent: ContactRequestEvent,
                                        contactRequestResponse: ContactRequestResponse): Observable<string> {
    const contactRequest: ContactRequest = contactRequestEvent.contactRequest;
    const now: number = this.timeService.nowAdjusted();
    const session: Session = this.sessionService.session();

    const userEventReturn: UserEventReturn = UserEventReturn
      .for(UserEventType.CONTACT_REQUEST_RESPONSE, session.pseudo(), now);

    return this.profilesService.fetchDetails(contactRequest.requesterPseudo())
      .pipe(
        mergeMap((requesterProfile: Profile) => {
          return this.userEventsService.postEventToProfile(contactRequestResponse, userEventReturn, requesterProfile);
        }),

        mergeMap(() => {
          if (contactRequestResponse.isAccepted()) {
            return this.remoteNotificationsServiceProvider.obtain()
              .pipe(
                mergeMap((remoteNotificationService: RemoteNotificationsService) => {
                  remoteNotificationService.registerBoxAddresses([contactRequestResponse.accepterWriteTo()]);

                  return this.sessionService.addContact(Contact.with(
                    contactRequest.requesterPseudo(),
                    contactRequest.requesterDisplayName(),
                    contactRequestResponse.accepterWriteTo(), // Read from the box address sent to the requester for writing to this user
                    contactRequest.requesterWriteTo(),
                    ContactKey.with(
                      Keys.systemKeysId(), // TODO This should actually be extracted from the contact request's username and key type (to be added to the contact request object)
                      Keys.SYSTEM_KEYS_TYPE, // TODO This should actually be extracted from the contact request key type (to be added to the contact request object)
                      contactRequest.requesterPublicKeys()
                    ),
                    contactRequest.secretKey(),
                    now
                  ));
                })
              );
          } else {
            return of({});
          }
        }),

        mergeMap(() => {
          return this.profilesService.updatePayload();
        }),

        mergeMap(() => {
          return this.userEventsService.deleteEventFromSystemBoxAddress(contactRequestEvent.eventId);
        })
      );
  }
}
