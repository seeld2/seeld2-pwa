/**
 * A ContactRemoveEvent is a ContactRemove coupled with its event ID.
 */
import {ContactRemove} from "./contact-remove";

export interface ContactRemoveEvent {
  contactRemove: ContactRemove,
  eventId: string
}
