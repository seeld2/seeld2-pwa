import {ContactRemove} from "./contact-remove";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEvent} from "../../userevents/user-event";

describe("ContactRemove", () => {

  const EVENT_ID: string = 'eventId';
  const PSEUDO: string = "pseudo";

  it("should provide an instance having all fields completed as expected", () => {
    const contactRemove: ContactRemove = ContactRemove.for(PSEUDO);
    expect(contactRemove.pseudo()).toEqual(PSEUDO);
  });

  describe(", when building an instance from a user event,", () => {

    it("should provide an instance build from that user event", () => {
      const contactRemove: ContactRemove = ContactRemove
        .fromUserEvent(UserEvent.build({pseudo: PSEUDO}, EVENT_ID, UserEventType.CONTACT_REMOVE));
      expect(contactRemove['_pseudo']).toEqual(PSEUDO);
    });

    it("should raise an exception if the user event is not of the right type", () => {
      const wrongUserEvent: UserEvent = UserEvent.build({}, EVENT_ID, UserEventType.CONTACT_REQUEST);
      expect(() => ContactRemove.fromUserEvent(wrongUserEvent)).toThrowError();
    });
  });

  it("should return a ContactRemove as a JavaScript object", () => {
    const contactRemove: ContactRemove = ContactRemove.for(PSEUDO);
    const obj: any = contactRemove.asObject();
    expect(obj['pseudo']).toEqual(PSEUDO);
    expect(obj['type']).toEqual(UserEventType.CONTACT_REMOVE);
  });
});
