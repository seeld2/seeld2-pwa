import {UserEventType} from "../../userevents/user-event-type";
import {UserEvent} from "../../userevents/user-event";
import {Logger} from "../../utils/logging/logger";
import {UserEventContent} from "../../userevents/user-event-content";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class ContactRemove implements Serializable, UserEventContent {

  private static readonly LOGGER: Logger = Logger.for('ContactRemove');

  static for(pseudo: string): ContactRemove {
    return new ContactRemove(pseudo);
  }

  static fromUserEvent(userEvent: UserEvent): ContactRemove {
    if (userEvent.isNotOfType(UserEventType.CONTACT_REMOVE)) {
      ContactRemove.LOGGER.error(`The passed UserEvent is not of type ${UserEventType.CONTACT_REMOVE}`);
      throw new Error(Logger.lastEntry().getEntry());
    }

    return new ContactRemove((userEvent.content())['pseudo']);
  }

  private constructor(private readonly _pseudo: string) {
  }

  asObject(): any {
    return {
      pseudo: this._pseudo,
      type: UserEventType.CONTACT_REMOVE
    };
  }

  pseudo(): string {
    return this._pseudo;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
