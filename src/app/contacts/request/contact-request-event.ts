/**
 * A ContactRequestEvent is a ContactRequest coupled with its event ID.
 */
import {ContactRequest} from "./contact-request";

export class ContactRequestEvent {
  contactRequest: ContactRequest;
  eventId: string;
}
