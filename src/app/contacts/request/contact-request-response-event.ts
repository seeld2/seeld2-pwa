/**
 * A ContactRequestResponseEvent is a ContactRequestResponse coupled with its event ID.
 */
import {ContactRequestResponse} from "./contact-request-response";

export interface ContactRequestResponseEvent {
  contactRequestResponse: ContactRequestResponse,
  eventId: string
}
