export interface ContactRequestResponseHandlingResult {
  hasAcceptedContactRequest: boolean;
  notify: boolean;
  profileContactDescription: string;
}
