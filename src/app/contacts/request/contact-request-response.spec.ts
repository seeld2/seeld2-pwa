import {ContactRequestResponse} from "./contact-request-response";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEvent} from "../../userevents/user-event";

describe("ContactRequestResponse", () => {

  const EVENT_ID: string = "eventid";

  const ACCEPTER_DISPLAY_NAME: string = "accepterDisplayName";
  const ACCEPTER_PUBLIC_KEYS: string = "accepterPublicKeys";
  const ACCEPTER_PSEUDO: string = "accepterPseudo";
  const ACCEPTER_WRITE_TO: string = "accepterWriteTo";
  const REQUESTER_WRITE_TO: string = "requesterWriteTo";
  const SECRET_KEY: string = "secretKey";
  const SILENTLY: boolean = true;

  describe(", when building an instance,", () => {

    it("should provide a ContactRequestResponse with all fields completed as expected", () => {
      const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.for(
        ACCEPTER_DISPLAY_NAME, ACCEPTER_PUBLIC_KEYS, ACCEPTER_PSEUDO,
        ACCEPTER_WRITE_TO, REQUESTER_WRITE_TO, SECRET_KEY, SILENTLY);
      contactRequestResponseCommonExpectations(contactRequestResponse);
      expect(contactRequestResponse['_silently']).toEqual(SILENTLY);
    });

    it("should provide a ContactRequestResponse with its 'silently' field set to false if not specified", () => {
      const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.for(
        ACCEPTER_DISPLAY_NAME, ACCEPTER_PUBLIC_KEYS, ACCEPTER_PSEUDO,
        ACCEPTER_WRITE_TO, REQUESTER_WRITE_TO, SECRET_KEY);
      contactRequestResponseCommonExpectations(contactRequestResponse);
      expect(contactRequestResponse['_silently']).toEqual(false);
    });

    function contactRequestResponseCommonExpectations(contactRequestResponse: ContactRequestResponse) {
      expect(contactRequestResponse).toBeDefined();
      expect(contactRequestResponse['_accepterDisplayName']).toEqual(ACCEPTER_DISPLAY_NAME);
      expect(contactRequestResponse['_accepterPublicKeys']).toEqual(ACCEPTER_PUBLIC_KEYS);
      expect(contactRequestResponse['_accepterPseudo']).toEqual(ACCEPTER_PSEUDO);
      expect(contactRequestResponse['_accepterWriteTo']).toEqual(ACCEPTER_WRITE_TO);
      expect(contactRequestResponse['_requesterWriteTo']).toEqual(REQUESTER_WRITE_TO);
      expect(contactRequestResponse['_secretKey']).toEqual(SECRET_KEY);
    }
  });

  describe(", when building an instance from a user event,", () => {

    it("should provide a ContactRequestResponse built from that user event", () => {

      const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.fromUserEvent(UserEvent.build({
        accepterDisplayName: ACCEPTER_DISPLAY_NAME,
        accepterPublicKeys: ACCEPTER_PUBLIC_KEYS,
        accepterUsername: ACCEPTER_PSEUDO,
        accepterWriteTo: ACCEPTER_WRITE_TO,
        requesterWriteTo: REQUESTER_WRITE_TO,
        silently: SILENTLY,
        secretKey: SECRET_KEY,
      }, EVENT_ID, UserEventType.CONTACT_REQUEST_RESPONSE));

      expect(contactRequestResponse).toBeDefined();
      expect(contactRequestResponse['_accepterDisplayName']).toEqual(ACCEPTER_DISPLAY_NAME);
      expect(contactRequestResponse['_accepterPublicKeys']).toEqual(ACCEPTER_PUBLIC_KEYS);
      expect(contactRequestResponse['_accepterPseudo']).toEqual(ACCEPTER_PSEUDO);
      expect(contactRequestResponse['_accepterWriteTo']).toEqual(ACCEPTER_WRITE_TO);
      expect(contactRequestResponse['_requesterWriteTo']).toEqual(REQUESTER_WRITE_TO);
      expect(contactRequestResponse['_silently']).toEqual(SILENTLY);
      expect(contactRequestResponse['_secretKey']).toEqual(SECRET_KEY);
    });

    it("should raise an exception if the user event is not of the right type", () => {
      const wrongUserEvent: UserEvent = UserEvent.build({}, EVENT_ID, UserEventType.CONTACT_REQUEST);
      expect(() => ContactRequestResponse.fromUserEvent(wrongUserEvent)).toThrowError();
    });
  });

  it("should return a ContactRequestResponse as a JavaScript object", () => {
    const contactRequestResponse: ContactRequestResponse = ContactRequestResponse.for(
      ACCEPTER_DISPLAY_NAME, ACCEPTER_PUBLIC_KEYS, ACCEPTER_PSEUDO,
      ACCEPTER_WRITE_TO, REQUESTER_WRITE_TO, SECRET_KEY, SILENTLY);

    const obj: any = contactRequestResponse.asObject();

    expect(obj['accepterDisplayName']).toEqual(ACCEPTER_DISPLAY_NAME);
    expect(obj['accepterPublicKeys']).toEqual(ACCEPTER_PUBLIC_KEYS);
    expect(obj['accepterUsername']).toEqual(ACCEPTER_PSEUDO);
    expect(obj['accepterWriteTo']).toEqual(ACCEPTER_WRITE_TO);
    expect(obj['requesterWriteTo']).toEqual(REQUESTER_WRITE_TO);
    expect(obj['silently']).toEqual(SILENTLY);
    expect(obj['secretKey']).toEqual(SECRET_KEY);
    expect(obj['type']).toEqual(UserEventType.CONTACT_REQUEST_RESPONSE);
  });

  it("should determine if a contact request response is accepted or rejected", () => {
    const accepted: ContactRequestResponse = ContactRequestResponse.for(
      ACCEPTER_DISPLAY_NAME, ACCEPTER_PUBLIC_KEYS, ACCEPTER_PSEUDO,
      ACCEPTER_WRITE_TO, REQUESTER_WRITE_TO, SECRET_KEY, SILENTLY);
    expect(accepted.isAccepted()).toBe(true);

    const rejected: ContactRequestResponse = ContactRequestResponse.for(
      ACCEPTER_DISPLAY_NAME, ACCEPTER_PUBLIC_KEYS, ACCEPTER_PSEUDO,
      REQUESTER_WRITE_TO, REQUESTER_WRITE_TO, SECRET_KEY, SILENTLY);
    expect(rejected.isAccepted()).toBe(false);
  });
});
