import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";
import {Logger} from "../../utils/logging/logger";
import {UserEventContent} from "../../userevents/user-event-content";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class ContactRequestResponse implements Serializable, UserEventContent {

  private static readonly LOGGER: Logger = Logger.for('ContactRequestResponse');

  static for(accepterDisplayName: string,
             accepterPublicKeys: string,
             accepterPseudo: string,
             accepterWriteTo: string,
             requesterWriteTo: string,
             secretKey: string,
             silently: boolean = false) {

    return new ContactRequestResponse(
      accepterDisplayName,
      accepterPublicKeys,
      accepterPseudo,
      accepterWriteTo,
      requesterWriteTo,
      secretKey,
      silently);
  }

  static fromUserEvent(userEvent: UserEvent) {
    if (userEvent.isNotOfType(UserEventType.CONTACT_REQUEST_RESPONSE)) {
      ContactRequestResponse.LOGGER
        .error(`The passed UserEvent is not of type ${UserEventType.CONTACT_REQUEST_RESPONSE}`);
      throw new Error(Logger.lastEntry().getEntry());
    }

    const content: any = userEvent.content();
    return new ContactRequestResponse(
      content['accepterDisplayName'],
      content['accepterPublicKeys'],
      content['accepterUsername'],
      content['accepterWriteTo'],
      content['requesterWriteTo'],
      content['secretKey'],
      content['silently']
    );
  }

  private constructor(private readonly _accepterDisplayName: string,
                      private readonly _accepterPublicKeys: string,
                      private readonly _accepterPseudo: string,
                      private readonly _accepterWriteTo: string,
                      private readonly _requesterWriteTo: string,
                      private readonly _secretKey: string,
                      private readonly _silently: boolean = false) {
  }

  accepterDisplayName(): string {
    return this._accepterDisplayName;
  }

  accepterPseudo(): string {
    return this._accepterPseudo;
  }

  accepterPublicKeys(): string {
    return this._accepterPublicKeys;
  }

  accepterWriteTo(): string {
    return this._accepterWriteTo;
  }

  asObject(): any {
    return {
      accepterDisplayName: this._accepterDisplayName,
      accepterPublicKeys: this._accepterPublicKeys,
      accepterUsername: this._accepterPseudo,
      accepterWriteTo: this._accepterWriteTo,
      requesterWriteTo: this._requesterWriteTo,
      silently: this._silently,
      secretKey: this._secretKey,
      type: UserEventType.CONTACT_REQUEST_RESPONSE
    }
  }

  /**
   * Returns whether this response event indicates a contact request acceptance or a contact request rejection.
   */
  isAccepted(): boolean {
    return this._accepterWriteTo !== this._requesterWriteTo;
  }

  isSilent(): boolean {
    return this._silently;
  }

  requesterWriteTo(): string {
    return this._requesterWriteTo;
  }

  secretKey(): string {
    return this._secretKey;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
