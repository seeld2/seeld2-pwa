import {ContactRequest} from "./contact-request";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEvent} from "../../userevents/user-event";

describe("ContactRequest", () => {

  const EVENT_ID: string = 'eventid';

  const REQUESTER_DISPLAY_NAME: string = "requesterDisplayName";
  const REQUESTER_MESSAGE: string = "requesterMessage";
  const REQUESTER_PSEUDO: string = "requesterPseudo";
  const REQUESTER_PUBLIC_KEYS: string = "requesterPublicKeys";
  const REQUESTER_WRITE_TO: string = "requesterWriteTo";
  const SECRET_KEY: string = "secretKey";

  describe(", when building an instance,", () => {

    it("should provide a ContactRequest having all fields completed as expected", () => {
      const contactRequest: ContactRequest = ContactRequest
        .for(REQUESTER_PSEUDO, REQUESTER_DISPLAY_NAME, REQUESTER_PUBLIC_KEYS, REQUESTER_MESSAGE);
      contactRequestCommonExpectations(contactRequest);
      expect(contactRequest['_requesterMessage']).toEqual(REQUESTER_MESSAGE);
    });

    it("should provide a ContactRequest with a default message if that message hasn't been specified", () => {
      const contactRequest: ContactRequest = ContactRequest
        .for(REQUESTER_PSEUDO, REQUESTER_DISPLAY_NAME, REQUESTER_PUBLIC_KEYS);
      contactRequestCommonExpectations(contactRequest);
      expect(contactRequest['_requesterMessage'].indexOf(REQUESTER_PSEUDO)).toBeGreaterThan(-1);
    });

    function contactRequestCommonExpectations(contactRequest: ContactRequest) {
      expect(contactRequest).toBeDefined();
      expect(contactRequest['_requesterDisplayName']).toEqual(REQUESTER_DISPLAY_NAME);
      expect(contactRequest['_requesterPseudo']).toEqual(REQUESTER_PSEUDO);
      expect(contactRequest['_requesterPublicKeys']).toEqual(REQUESTER_PUBLIC_KEYS);
      expect(contactRequest['_requesterWriteTo']).toBeDefined();
      expect(contactRequest['_secretKey']).toBeDefined();
    }
  });

  describe(", when building an instance from a user event,", () => {

    it("should provide a ContactRequest built from a user event", () => {

      const contactRequest: ContactRequest = ContactRequest.fromUserEvent(UserEvent.build({
        requesterUsername: REQUESTER_PSEUDO,
        requesterDisplayName: REQUESTER_DISPLAY_NAME,
        requesterPublicKeys: REQUESTER_PUBLIC_KEYS,
        requesterWriteTo: REQUESTER_WRITE_TO,
        requesterMessage: REQUESTER_MESSAGE,
        secretKey: SECRET_KEY
      }, EVENT_ID, UserEventType.CONTACT_REQUEST));

      expect(contactRequest).toBeDefined();
      expect(contactRequest['_requesterDisplayName']).toEqual(REQUESTER_DISPLAY_NAME);
      expect(contactRequest['_requesterPseudo']).toEqual(REQUESTER_PSEUDO);
      expect(contactRequest['_requesterPublicKeys']).toEqual(REQUESTER_PUBLIC_KEYS);
      expect(contactRequest['_requesterWriteTo']).toEqual(REQUESTER_WRITE_TO);
      expect(contactRequest['_requesterMessage']).toEqual(REQUESTER_MESSAGE);
      expect(contactRequest['_secretKey']).toEqual(SECRET_KEY);
    });

    it("should raise an exception if the user event is not of the right type", () => {
      const wrongUserEvent: UserEvent = UserEvent.build({}, EVENT_ID, UserEventType.CONTACT_REQUEST_RETURN);
      expect(() => ContactRequest.fromUserEvent(wrongUserEvent)).toThrowError();
    });
  });

  it("should return a ContactRequest as a JavaScript object", () => {
    const contactRequest: ContactRequest = ContactRequest
      .for(REQUESTER_PSEUDO, REQUESTER_DISPLAY_NAME, REQUESTER_PUBLIC_KEYS, REQUESTER_MESSAGE);

    const obj: any = contactRequest.asObject();

    expect(obj['requesterDisplayName']).toEqual(REQUESTER_DISPLAY_NAME);
    expect(obj['requesterMessage']).toEqual(REQUESTER_MESSAGE);
    expect(obj['requesterPublicKeys']).toEqual(REQUESTER_PUBLIC_KEYS);
    expect(obj['requesterUsername']).toEqual(REQUESTER_PSEUDO);
    expect(obj['requesterWriteTo']).toBeDefined();
    expect(obj['secretKey']).toBeDefined();
    expect(obj['type']).toEqual(UserEventType.CONTACT_REQUEST);
  });
});
