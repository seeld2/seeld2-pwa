import {UUID} from "angular2-uuid";
import {Hmac} from "../../crypto/hmac/hmac";
import {UserEvent} from "../../userevents/user-event";
import {UserEventContent} from "../../userevents/user-event-content";
import {UserEventType} from "../../userevents/user-event-type";
import {Logger} from "../../utils/logging/logger";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class ContactRequest implements Serializable, UserEventContent {

  private static readonly LOGGER: Logger = Logger.for('ContactRequest');

  static for(requesterPseudo: string,
             requesterDisplayName: string,
             requesterPublicKeys: string,
             requesterMessage?: string) {

    if (!requesterMessage) {
      requesterMessage = `${requesterPseudo} wants to connect with you!`;
    }

    const requesterWriteTo: string = UUID.UUID();

    const secretKey: string = Hmac.generateSecretKey();

    return new ContactRequest(
      requesterPseudo,
      requesterDisplayName,
      requesterPublicKeys,
      requesterWriteTo,
      requesterMessage,
      secretKey);
  }

  static fromUserEvent(userEvent: UserEvent) {
    if (userEvent.isNotOfType(UserEventType.CONTACT_REQUEST)) {
      ContactRequest.LOGGER.error(`The passed UserEvent is not of type ${UserEventType.CONTACT_REQUEST}`);
      throw new Error(Logger.lastEntry().getEntry());
    }

    const content: any = userEvent.content();
    return new ContactRequest(
      content['requesterUsername'],
      content['requesterDisplayName'],
      content['requesterPublicKeys'],
      content['requesterWriteTo'],
      content['requesterMessage'],
      content['secretKey']
    );
  }

  private constructor(private readonly _requesterPseudo: string,
                      private readonly _requesterDisplayName: string,
                      private readonly _requesterPublicKeys: string,
                      private readonly _requesterWriteTo: string,
                      private readonly _requesterMessage: string,
                      private readonly _secretKey: string) {
  }

  asObject(): any {
    return {
      requesterUsername: this._requesterPseudo,
      requesterDisplayName: this._requesterDisplayName,
      requesterPublicKeys: this._requesterPublicKeys,
      requesterWriteTo: this._requesterWriteTo,
      requesterMessage: this._requesterMessage,
      secretKey: this._secretKey,
      type: UserEventType.CONTACT_REQUEST
    };
  }

  describe(): string {
    return this._requesterDisplayName ? this._requesterDisplayName : this._requesterPseudo;
  }

  requesterDisplayName(): string {
    return this._requesterDisplayName;
  }

  requesterPseudo(): string {
    return this._requesterPseudo;
  }

  requesterPublicKeys(): string {
    return this._requesterPublicKeys;
  }

  requesterWriteTo(): string {
    return this._requesterWriteTo;
  }

  secretKey(): string {
    return this._secretKey;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
