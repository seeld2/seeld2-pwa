import {ContactSecretKey} from "./contact-secret-key";

/**
 * A ContactSecretKeyEvent is a ContactSecretKey coupled with its event ID.
 */
export interface ContactSecretKeyEvent {
  contactSecretKey: ContactSecretKey,
  eventId: string
}
