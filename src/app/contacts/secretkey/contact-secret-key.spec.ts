import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";
import {ContactSecretKey} from "./contact-secret-key";

describe("ContactSecretKey", () => {

  const EVENT_ID: string = "eventId";
  const KEY: string = "key";
  const PSEUDO: string = "pseudo";

  describe(", when creating an instance,", () => {

    it("should build an instance from a pseudo and a key", () => {
      const contactSecretKey: ContactSecretKey = ContactSecretKey.for(PSEUDO, KEY);
      expect(contactSecretKey.asObject()).toEqual({
        key: KEY,
        pseudo: PSEUDO,
        type: UserEventType.CONTACT_SECRET_KEY
      });
    });

    it("should build the instance from a UserEvent", () => {
      const userEvent: UserEvent = UserEvent.build({key: KEY, pseudo: PSEUDO}, EVENT_ID, UserEventType.CONTACT_SECRET_KEY);

      const contactSecretKey: ContactSecretKey = ContactSecretKey.fromUserEvent(userEvent);

      expect(contactSecretKey.key()).toEqual(KEY);
      expect(contactSecretKey.pseudo()).toEqual(PSEUDO);
    });
  });
});
