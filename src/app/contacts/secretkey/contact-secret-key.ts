import {UserEventContent} from "../../userevents/user-event-content";
import {Logger} from "../../utils/logging/logger";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEvent} from "../../userevents/user-event";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class ContactSecretKey implements Serializable, UserEventContent {

  private static readonly LOGGER: Logger = Logger.for('ContactSecretKey');

  static for(pseudo: string, key: string): ContactSecretKey {
    return new ContactSecretKey(pseudo, key);
  }

  static fromUserEvent(userEvent: UserEvent): ContactSecretKey {
    if (userEvent.isNotOfType(UserEventType.CONTACT_SECRET_KEY)) {
      ContactSecretKey.LOGGER.error(`The passed UserEvent is not of type ${UserEventType.CONTACT_SECRET_KEY}`);
      throw new Error(Logger.lastEntry().getEntry());
    }

    const object: any = userEvent.content();
    return new ContactSecretKey(object['pseudo'], object['key']);
  }

  private constructor(private readonly _pseudo: string,
                      private readonly _key: string) {
  }

  asObject(): any {
    return {
      key: this._key,
      pseudo: this._pseudo,
      type: UserEventType.CONTACT_SECRET_KEY
    }
  }

  key(): string {
    return this._key;
  }

  pseudo(): string {
    return this._pseudo;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
