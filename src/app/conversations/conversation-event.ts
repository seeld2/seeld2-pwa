/**
 * Conversation-related events' constants.
 */
export enum ConversationEvent {
  UPDATE_RECIPIENTS = 'ConversationEvent:UPDATE_RECIPIENTS'
}
