import {ConversationIds} from "./conversation-ids";
import {UUID} from "angular2-uuid";

describe('ConversationIds', () => {

  it("should build an instance from a map of existing conversation IDs by pseudo", () => {

    const conversationIds: ConversationIds = new ConversationIds({'a': UUID.UUID()});

    const pseudos: string[] = Object.keys(conversationIds.getMap());
    expect(pseudos.length).toEqual(1);
    expect(pseudos).toContain('a');
    expect(conversationIds.getMap()['a']).toBeDefined();
  });

  it("should add a new pseudo, generating a new conversation ID for it", () => {

    const conversationIds: ConversationIds = new ConversationIds({'a': UUID.UUID()});

    conversationIds.addNew('b');

    const pseudos: string[] = Object.keys(conversationIds.getMap());
    expect(pseudos.length).toEqual(2);
    expect(pseudos).toContain('a');
    expect(pseudos).toContain('b');
    expect(conversationIds.getMap()['a']).toBeDefined();
    expect(conversationIds.getMap()['b']).toBeDefined();
  });

  it("should NOT overwrite a pseudo's conversation ID when adding a pseudo", () => {

    const conversationId = UUID.UUID();
    const conversationIds: ConversationIds = new ConversationIds({'a': conversationId});

    conversationIds.addNew('a');

    const pseudos: string[] = Object.keys(conversationIds.getMap());
    expect(pseudos.length).toEqual(1);
    expect(pseudos).toContain('a');
    expect(conversationIds.getMap()['a']).toEqual(conversationId);
  });

  it("should add an array of new pseudos, generating new conversation IDs for them", () => {

    const conversationIds: ConversationIds = new ConversationIds({'a': UUID.UUID()});

    conversationIds.addAllNew(['b', 'c']);

    const pseudos: string[] = Object.keys(conversationIds.getMap());
    expect(pseudos.length).toEqual(3);
    expect(pseudos).toContain('a');
    expect(pseudos).toContain('b');
    expect(pseudos).toContain('c');
    expect(conversationIds.getMap()['a']).toBeDefined();
    expect(conversationIds.getMap()['b']).toBeDefined();
    expect(conversationIds.getMap()['c']).toBeDefined();
  });

  it("should NOT overwrite the pseudos' conversation IDs when adding an array of pseudos", () => {

    const conversationId = UUID.UUID();
    const conversationIds: ConversationIds = new ConversationIds({'a': conversationId});

    conversationIds.addAllNew(['a', 'b']);

    const pseudos: string[] = Object.keys(conversationIds.getMap());
    expect(pseudos.length).toEqual(2);
    expect(pseudos).toContain('a');
    expect(pseudos).toContain('b');
    expect(conversationIds.getMap()['a']).toEqual(conversationId);
    expect(conversationIds.getMap()['b']).toBeDefined();
  });
});
