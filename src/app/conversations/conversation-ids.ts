import {UUID} from "angular2-uuid";

/**
 * A map of conversation IDs (value) by their matching pseudo (key).
 */
export class ConversationIds {

  constructor(private conversationIdsMap: any = {}) {
  }

  /**
   * Adds a pseudo, generating a new conversation ID for it ONLY IF the pseudo is not already registered for this
   * ConversationIds' instance.
   */
  addNew(pseudo: string): ConversationIds {
    if (!this.conversationIdsMap[pseudo]) {
      this.conversationIdsMap[pseudo] = UUID.UUID();
    }
    return this;
  }

  /**
   * Adds the specified array of pseudos, generating conversation IDs for them ONLY IF they are already registered for
   * this ConversationIds' instance.
   */
  addAllNew(pseudos: string[]): ConversationIds {
    pseudos.forEach((pseudo: string) => {
      if (!this.conversationIdsMap[pseudo]) {
        this.addNew(pseudo);
      }
    });
    return this;
  }

  getMap() {
    return this.conversationIdsMap;
  }
}
