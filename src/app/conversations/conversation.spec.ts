import {Message} from "./messages/message";
import {Contact} from "../user/contact/contact";
import {Conversation} from "./conversation";

describe("Conversation", () => {

  const READ: boolean = true;
  const UNREAD: boolean = false;

  it("should generate a reply message stemming from this conversation", () => {

    const conversation: Conversation = Conversation.with(
      'bobbyConversationId',
      Message.newMessage('title', 'content', 'alice', Date.now(), [aContact('bobby')])
    );

    const reply: Message = conversation.reply('reply', 'bobby', Date.now(), [aContact('alice')]);

    expect(reply.title()).toEqual('title');
    expect(reply.content()).toEqual('reply');
    expect(reply.sender()).toEqual('bobby');
    expect(reply.sendDate()).toBeDefined();
    expect(Object.keys(reply.participants()).length).toEqual(2);
    expect(Object.keys(reply.participants())).toContain('alice');
    expect(reply.participants()['alice']).toEqual(conversation.message().participants()['alice']);
    expect(Object.keys(reply.participants())).toContain('bobby');
    expect(reply.participants()['bobby']).toEqual(conversation.message().participants()['bobby']);
    expect(reply.conversationId()).toEqual(conversation.message().participants()['bobby']);
    expect(reply.order()).toEqual(2);
    expect(reply.recipients().length).toEqual(2);
    expect(reply.recipients()).toContain('alice');
    expect(reply.recipients()).toContain('bobby');
  });

  describe(", when comparing this conversation with another,", () => {

    it("should sort the earliest conversation first", () => {
      expect(aConversation(1000).compareTo(aConversation(2000))).toBeLessThan(0);
    });

    it("should give even more precedence to conversations marked as unread", () => {
      expect(aConversation(1000, UNREAD).compareTo(aConversation(2000, READ))).toBeGreaterThan(0);
    });

    it("should consider two conversations with same timestamp and read flag to be equivalent", () => {
      expect(aConversation(2000, READ).compareTo(aConversation(2000, READ))).toEqual(0);
    });
  });

  it("should indicate whether a collection is equal to another collection or not", () => {
    let thisConversation: Conversation, thatConversation: Conversation;

    thisConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    thatConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    expect(thisConversation.equals(thatConversation)).toEqual(true);

    thisConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    thatConversation = Conversation.with("changed", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    expect(thisConversation.equals(thatConversation)).toEqual(false);

    thisConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    thatConversation = Conversation.with("conversationId", Message.with("changed", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    expect(thisConversation.equals(thatConversation)).toEqual(false);

    thisConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), false);
    thatConversation = Conversation.with("conversationId", Message.with("title", "content", "sender", 1, null, null, null, 1, "conversationId", "messageId"), true);
    expect(thisConversation.equals(thatConversation)).toEqual(false);
  });

  function aContact(pseudo: string): Contact {
    return Contact.with(pseudo, null, null, null, null, null);
  }

  function aConversation(sendDate: number, read: boolean = true): Conversation {
    const message: Message = Message.with(null, null, null, sendDate, null, null, null);
    return Conversation.with(null, message, read);
  }
});
