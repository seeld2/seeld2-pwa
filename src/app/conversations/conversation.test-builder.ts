import {Message} from "./messages/message";
import {Conversation} from "./conversation";

export class ConversationTestBuilder {

  public static readonly CONVERSATION_ID: string = "conversation id";
  public static readonly MESSAGE: Message = Message.with(null, null, null, null, null, null, {});
  public static readonly READ: boolean = true;

  static with(): ConversationTestBuilder {
    return new ConversationTestBuilder();
  }

  and = this;

  private _conversationId: string;
  private _message: Message;
  private _read: boolean;

  private constructor() {
  }

  asIs(): Conversation {
    return Conversation.with(this._conversationId, this._message, this._read);
  }

  noBlanks(): Conversation {
    return Conversation.with(
      this._conversationId !== undefined ? this._conversationId : ConversationTestBuilder.CONVERSATION_ID,
      this._message !== undefined ? this._message : ConversationTestBuilder.MESSAGE,
      this._read !== undefined ? this._read : ConversationTestBuilder.READ
    );
  }

  conversationId(conversationId: string): ConversationTestBuilder {
    this._conversationId = conversationId;
    return this;
  }

  message(message: Message): ConversationTestBuilder {
    this._message = message;
    return this;
  }

  read(read: boolean): ConversationTestBuilder {
    this._read = read;
    return this;
  }
}
