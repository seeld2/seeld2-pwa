import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";
import {Message} from "./messages/message";
import {Contact} from "../user/contact/contact";
import {ConversationIds} from "./conversation-ids";
import {Contacts} from "../user/contact/contacts";

export class Conversation implements Serializable {

  static fromJson(json: string): Conversation {
    return Conversation.fromObject(JSON.parse(json));
  }

  static fromObject(object: any): Conversation {
    return new Conversation(
      object.conversationId,
      Message.fromObject(object.message),
      object.read
    );
  }

  static with(conversationId: string, message: Message, read: boolean = true): Conversation {
    return new Conversation(conversationId, message, read);
  }

  private constructor(private readonly _conversationId: string,
                      private _message: Message,
                      private _read: boolean) {
  }

  asObject(): any {
    return {
      conversationId: this._conversationId,
      message: this._message,
      read: this._read
    };
  }

  /**
   * Compares this conversation with another. The conversations with the lowest send date and that are "read"
   * have precedence.
   * @param {Conversation} other The other conversation to compare with this
   * @returns {number} 0 if both conversations are equal, >0 if this conversation has precedence and <0 if the other
   * conversation has precedence
   */
  compareTo(other: Conversation): number {
    let thisWeight: number = this._read ? 10 : 0;
    let otherWeight: number = other._read ? 10 : 0;
    thisWeight += this._message.sendDate() < other._message.sendDate() ? 1 : 0;
    otherWeight += other._message.sendDate() < this._message.sendDate() ? 1 : 0;
    return otherWeight - thisWeight;
  }

  conversationId(): string {
    return this._conversationId;
  }

  equals(other: Conversation): boolean { // TODO Test
    return this._conversationId === other._conversationId
      && this._message.equals(other._message)
      && this._read === other._read;
  }

  message(): Message {
    return this._message;
  }

  participantsPseudos(): string[] {
    return this._message.participantsPseudos();
  }

  read(): boolean {
    return this._read;
  }

  reply(content: string, sender: string, now: number, tos: Contact[]): Message {

    const toPseudos = Contacts.fromArrayOfContacts(tos).pseudos();

    const participants: ConversationIds = new ConversationIds(this._message.participants());
    participants.addAllNew(toPseudos);

    const recipients: string[] = [sender].concat(toPseudos);

    const conversationId: string = participants.getMap()[sender];

    return Message.with(
      this._message.title(),
      content,
      sender,
      now,
      undefined,
      undefined,
      participants.getMap(),
      this._message.order() ? this._message.order() + 1 : 1,
      conversationId,
      undefined,
      recipients);
  }

  setMessage(message: Message) {
    this._message = message
  }

  setRead(read: boolean) {
    this._read = read;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
