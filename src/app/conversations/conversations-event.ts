/**
 * Conversations-related events' constants.
 */
export enum ConversationsEvent {
  REFRESH_FULLY = 'ConversationsEvent:REFRESH_FULLY',
  REFRESH_NEW_CONVERSATIONS_COUNT = 'ConversationsEvent:REFRESH_NEW_CONVERSATIONS_COUNT',
}
