import {ConversationFixture} from "../../core/conversations/fixtures/conversation-fixture";
import {Conversation} from "./conversation";
import {ConversationTestBuilder as aConversation} from "./conversation.test-builder";
import {ConversationsMap} from "./conversations-map";

describe("ConversationsMap", () => {

  const CONVERSATION_ID_1: string = "conversationId1";
  const CONVERSATION_ID_2: string = "conversationId2";

  const CONVERSATION_1: Conversation = ConversationFixture.withId('1');
  const CONVERSATION_2: Conversation = ConversationFixture.withId('2');

  it("should instantiate an empty ConversationsMap", () => {
    const conversationsMap = ConversationsMap.empty();

    expect(Object.keys(conversationsMap['map']).length).toBe(0);
  });

  it("should delete a conversation from the map", () => {

    const conversationsMap: ConversationsMap = ConversationsMap.for([
      aConversation.with().conversationId(CONVERSATION_ID_1).noBlanks(),
      aConversation.with().conversationId(CONVERSATION_ID_2).noBlanks()]);

    conversationsMap.deleteConversation(CONVERSATION_ID_1);

    const storedConversationIds: string[] = Object.keys(conversationsMap['map']);
    expect(storedConversationIds).toContain(CONVERSATION_ID_2);
    expect(storedConversationIds).not.toContain(CONVERSATION_ID_1);
  });

  it("should indicate whether it's empty or not", () => {
    let conversationsMap: ConversationsMap = ConversationsMap.for([CONVERSATION_1]);

    expect(conversationsMap.isEmpty()).toBe(false);

    conversationsMap = ConversationsMap.empty();

    expect(conversationsMap.isEmpty()).toBe(true);
  });

  it("should return all conversations in the map", () => {
    const conversationsMap: ConversationsMap = ConversationsMap.for([CONVERSATION_1, CONVERSATION_2]);

    const conversations: Conversation[] = conversationsMap.getAllConversations();

    expect(conversations).toContain(CONVERSATION_1);
    expect(conversations).toContain(CONVERSATION_2);
  });

  it("should return a conversation with a given ID", () => {
    const conversationsMap: ConversationsMap = ConversationsMap.for([CONVERSATION_1]);

    const returnedConversation: Conversation = conversationsMap.getConversationWithId(CONVERSATION_1.conversationId());

    expect(returnedConversation).toBeDefined();
    expect(returnedConversation).not.toBeNull();
    expect(returnedConversation).toEqual(CONVERSATION_1);
  });

  it("should put a conversation into the map", () => {
    const conversationsMap: ConversationsMap = ConversationsMap.empty();

    conversationsMap.putConversation(CONVERSATION_1);

    expect(conversationsMap['map'][CONVERSATION_1.conversationId()]).toEqual(CONVERSATION_1);
  });

  it("should return the amount of conversations in the map", () => {
    const conversationsMap: ConversationsMap = ConversationsMap.for([CONVERSATION_1, CONVERSATION_2]);

    expect(conversationsMap.size()).toBe(2);
  });
});
