import {Conversation} from "./conversation";

/**
 * A Map specifically for conversations, which keeps a dictionary of conversations by their ID.
 */
export class ConversationsMap {

  private readonly map: any; // Conversations mapped by their ID.

  static empty(): ConversationsMap {
    return new ConversationsMap({});
  }

  static for(conversations: Conversation[] = []) {
    const map: any = {};
    conversations.forEach((conversation: Conversation) => map[conversation.conversationId()] = conversation);
    return new ConversationsMap(map);
  }

  private constructor(map: any) {
    this.map = map;
  }

  deleteConversation(conversationId: string) {
    delete this.map[conversationId];
  }

  isEmpty(): boolean {
    return Object.keys(this.map).length === 0;
  }

  // TODO Watch out, this returns values that are not correctly encapsulated (it should return copies of Messages)
  getAllConversations(): Conversation[] {
    return Object.keys(this.map).map(key => this.map[key]);
  }

  // TODO Watch out, this returns a value that is not correctly encapsulated (it should return a copy)
  getConversationWithId(conversationId: string): Conversation {
    return this.map[conversationId];
  }

  // TODO Watch out, this uses a value without encapsulating it correctly (it should set a copy of conversation)
  putConversation(conversation: Conversation): Conversation {
    this.map[conversation.conversationId()] = conversation;
    return conversation;
  }

  size(): number {
    return Object.keys(this.map).length;
  }
}
