export interface ConversationsRefreshParams {
  markNewAsUnread?:boolean;
  notify?: boolean; // TODO Likely not used
  poll?: boolean;
  scrollToBottom?: boolean; // TODO Used on ConversationPage, not ConversationsPage. Should be part of ConversationRefreshParam
}
