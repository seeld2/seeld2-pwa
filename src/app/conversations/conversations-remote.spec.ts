import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {AngularJasmine} from "../../shared/angular-jasmine";
import {config} from "../app.config";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {Contact} from "../user/contact/contact";
import {ContactKey} from "../user/contact/contact-key";
import {Contacts} from "../user/contact/contacts";
import {KeyPair} from "../user/key/key-pair";
import {Keys} from "../user/key/keys";
import {ConversationsRemote, ConversationsResponseConversation, MessageDeleteRequest} from "./conversations-remote";
import {Message} from "./messages/message";
import {MessageFixture} from "./messages/message-fixture";

describe("ConversationsRemote", () => {

  const ALICE_BOB_HMAC_HASH: string = "alice and bob's mutual hmac hash";

  const ALICE_BOX_ADDRESS: string = '7b58f8fe-95f6-4b03-bed8-5c5857bb5392';
  const ALICE_PSEUDO: string = 'alice';
  const ALICE_PUBLIC_KEY: string = 'alice public key';

  const BOB_BOX_ADDRESS: string = 'af7d58d4-f28d-435c-afe4-69239651855a';
  const BOB_PSEUDO: string = 'bob';
  const BOB_PUBLIC_KEY: string = 'bob public key';

  const CONVERSATION_ID_1: string = 'e9d1f432-43d2-413c-a69b-29b0411f764c';
  const CONVERSATION_ID_2: string = 'ad466105-e975-4616-9deb-292e2de63c3a';
  const MESSAGE_ID_1: string = '390d7277-ce95-4d8d-97bf-3d2d54d1c8da';

  const PAYLOAD: string = 'payload';
  const MESSAGE: Message = MessageFixture.build();

  const PAGE_ONE = 0;
  const PAGE_SIZE = 25;

  let openPgp: OpenPgp;

  let httpClient: HttpClient;
  let sessionService: SessionService;

  let conversationsRemote: ConversationsRemote;

  beforeEach(() => {

    httpClient = AngularJasmine.httpClient();
    sessionService = SessionsJasmine.sessionService();

    conversationsRemote = new ConversationsRemote(httpClient, sessionService);

    (<jasmine.Spy>sessionService.session).and.returnValue(
      aSession(ALICE_BOX_ADDRESS, [aContact(BOB_PSEUDO, BOB_BOX_ADDRESS, BOB_PUBLIC_KEY)], ALICE_PUBLIC_KEY));

    openPgp = CryptoJasmine.openPgp();
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);
  });

  // it("should delete a conversation", (done: DoneFn) => {
  //
  //   const hmacHashesByBoxAddress: HmacHashesByBoxAddress = aHmacHashesByBoxAddress
  //     .with().entry(ALICE_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs();
  //
  //   (<jasmine.Spy>httpClient.put).and.returnValue(of({}));
  //
  //   const conversationDeleteRequestObject: any = {hmacHashesByBoxAddress: {}};
  //   conversationDeleteRequestObject.hmacHashesByBoxAddress[ALICE_BOX_ADDRESS] = ALICE_BOB_HMAC_HASH;
  //   conversationsRemote.deleteConversation(CONVERSATION_ID_1, hmacHashesByBoxAddress).subscribe(() => {
  //     expect(httpClient.put).toHaveBeenCalledWith(
  //       `${config.api.conversations}/${CONVERSATION_ID_1}/state/deleted`,
  //       conversationDeleteRequestObject);
  //     done();
  //   });
  // });

  it("should delete a conversation's message", (done: DoneFn) => {

    (<jasmine.Spy>httpClient.put).and.returnValue(of({}));

    conversationsRemote.deleteConversationMessage(BOB_BOX_ADDRESS, CONVERSATION_ID_1, MESSAGE_ID_1, ALICE_BOB_HMAC_HASH)
      .subscribe(() => {
        expect(httpClient.put).toHaveBeenCalledWith(
          `${config.api.conversations}/${CONVERSATION_ID_1}/messages/${MESSAGE_ID_1}/state/deleted`,
          <MessageDeleteRequest>{boxAddress: BOB_BOX_ADDRESS, hmacHash: ALICE_BOB_HMAC_HASH}
        );
        done();
      });
  });

  // describe(", when fetching conversations by ID,", () => {
  //
  //   let session: Session;
  //
  //   beforeEach(() => {
  //     session = SessionsJasmine.session();
  //     (<jasmine.Spy>sessionService.session).and.returnValue(session);
  //   });
  //
  //   it("should fetch the conversations matching the specified IDs", (done: DoneFn) => {
  //
  //     const hmacHashesByBoxAddress: HmacHashesByBoxAddress = aHmacHashesByBoxAddress
  //       .with().entry(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs();
  //     (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and.returnValue(of(hmacHashesByBoxAddress));
  //     (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
  //       .returnValue(of(HmacHashesByBoxAddress.empty()));
  //     (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_BOB_HMAC_HASH));
  //     (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
  //
  //     (httpClient.put as jasmine.Spy).and.returnValue(of(<ConversationsResponse>{
  //       conversations: [
  //         aConversationResponseConversation(CONVERSATION_ID_1, ALICE_BOX_ADDRESS),
  //         aConversationResponseConversation(CONVERSATION_ID_2, BOB_BOX_ADDRESS)
  //       ]
  //     }));
  //     (<jasmine.Spy>sessionService.contactWithReadFromBoxAddress).and
  //       .returnValues(
  //         aContact(ALICE_PSEUDO, ALICE_BOX_ADDRESS, ALICE_PUBLIC_KEY),
  //         aContact(BOB_PSEUDO, BOB_BOX_ADDRESS, BOB_PUBLIC_KEY)
  //       );
  //     (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(MESSAGE.toJson()));
  //
  //     const conversations: Conversation[] = [];
  //     conversationsRemote.fetchConversationsByIds([CONVERSATION_ID_1, CONVERSATION_ID_2])
  //       .subscribe((conversation: Conversation) => {
  //         conversations.push(conversation);
  //         if (conversations.length === 2) {
  //
  //           expect(session.hmacHashesByReadFromBoxAddresses).toHaveBeenCalled();
  //           expect(httpClient.put).toHaveBeenCalledWith(
  //             `${config.api.conversations}`,
  //             `{"conversationIds":["${CONVERSATION_ID_1}","${CONVERSATION_ID_2}"],"hmacHashesByBoxAddress":{"${BOB_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}","${ALICE_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}"}}`);
  //           expect((<jasmine.Spy>(sessionService.contactWithReadFromBoxAddress)).calls.argsFor(0)[0])
  //             .toEqual(ALICE_BOX_ADDRESS);
  //           expect((<jasmine.Spy>(sessionService.contactWithReadFromBoxAddress)).calls.argsFor(1)[0])
  //             .toEqual(BOB_BOX_ADDRESS);
  //           expect(openPgp.decrypt).toHaveBeenCalledTimes(2);
  //           expect((<jasmine.Spy>(openPgp.decrypt)).calls.allArgs()[0]).toEqual([PAYLOAD, ALICE_PUBLIC_KEY]);
  //           expect((<jasmine.Spy>(openPgp.decrypt)).calls.allArgs()[1]).toEqual([PAYLOAD, BOB_PUBLIC_KEY]);
  //
  //           done();
  //         }
  //       });
  //   });
  // });

  // describe(", when fetching missing conversations,", () => {
  //
  //   let session: Session;
  //
  //   beforeEach(() => {
  //     session = SessionsJasmine.session();
  //     (<jasmine.Spy>sessionService.session).and.returnValue(session);
  //   });
  //
  //   it("should fetch those whose ID is missing from the stored passed conversation IDs", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
  //       .returnValue(of(aHmacHashesByBoxAddress.with().entry(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs()));
  //     (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
  //       .returnValue(of(HmacHashesByBoxAddress.empty()));
  //       (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_BOB_HMAC_HASH));
  //     (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
  //
  //     (<jasmine.Spy>httpClient.put).and.returnValues(
  //       of(<ConversationIdsResponse>{conversationIds: [CONVERSATION_ID_1, CONVERSATION_ID_2]}),
  //       of(<ConversationsResponse>{conversations: [aConversationResponseConversation(CONVERSATION_ID_2, BOB_BOX_ADDRESS)]})
  //     );
  //
  //     (<jasmine.Spy>sessionService.contactWithReadFromBoxAddress).and
  //       .returnValue(aContact(BOB_PSEUDO, BOB_BOX_ADDRESS, BOB_PUBLIC_KEY));
  //
  //     (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(MESSAGE.toJson()));
  //
  //     conversationsRemote.fetchMissingConversations([CONVERSATION_ID_1], PAGE_ONE, PAGE_SIZE)
  //       .subscribe((conversation: Conversation) => {
  //
  //         expect(httpClient.put).toHaveBeenCalledTimes(2);
  //         expect((<jasmine.Spy>httpClient.put).calls.argsFor(0)[0]).toEqual(`${config.api.conversations}/ids`);
  //         expect((<jasmine.Spy>httpClient.put).calls.argsFor(0)[1])
  //           .toEqual(`{"hmacHashesByBoxAddress":{"${BOB_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}","${ALICE_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}"},"page":${PAGE_ONE},"pageSize":${PAGE_SIZE}}`);
  //         expect((<jasmine.Spy>httpClient.put).calls.argsFor(1)[0]).toEqual(`${config.api.conversations}`);
  //         expect((<jasmine.Spy>httpClient.put).calls.argsFor(1)[1])
  //           .toEqual(`{"conversationIds":["${CONVERSATION_ID_2}"],"hmacHashesByBoxAddress":{"${BOB_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}","${ALICE_BOX_ADDRESS}":"${ALICE_BOB_HMAC_HASH}"}}`);
  //
  //         expect(sessionService.contactWithReadFromBoxAddress).toHaveBeenCalledWith(BOB_BOX_ADDRESS);
  //         expect(openPgp.decrypt).toHaveBeenCalledTimes(1);
  //         expect(openPgp.decrypt).toHaveBeenCalledWith(PAYLOAD, BOB_PUBLIC_KEY);
  //
  //         expect(conversation.conversationId()).toEqual(CONVERSATION_ID_2);
  //
  //         done();
  //       });
  //   });
  //
  //   it("should not emit anything if no conversations are stored on server", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
  //       .returnValue(of(aHmacHashesByBoxAddress.with().entry(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs()));
  //     (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
  //       .returnValue(of(HmacHashesByBoxAddress.empty()));
  //     (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_BOB_HMAC_HASH));
  //     (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
  //
  //     (<jasmine.Spy>httpClient.put).and.returnValue(of(<ConversationIdsResponse>{conversationIds: []}));
  //
  //     conversationsRemote.fetchMissingConversations([CONVERSATION_ID_1, CONVERSATION_ID_2], PAGE_ONE, PAGE_SIZE)
  //       .subscribe(() => {
  //         done.fail("Unexpected conversation emitted");
  //       });
  //
  //     setTimeout(() => done(), 100);
  //   });
  //
  //   it("should not emit anything if stored and remote conversations are identical", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
  //       .returnValue(of(aHmacHashesByBoxAddress.with().entry(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs()));
  //     (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
  //       .returnValue(of(HmacHashesByBoxAddress.empty()));
  //     (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_BOB_HMAC_HASH));
  //     (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
  //     (<jasmine.Spy>httpClient.put).and
  //       .returnValue(of(<ConversationIdsResponse>{conversationIds: [CONVERSATION_ID_1, CONVERSATION_ID_2]}));
  //
  //     conversationsRemote.fetchMissingConversations([CONVERSATION_ID_1, CONVERSATION_ID_2], PAGE_ONE, PAGE_SIZE)
  //       .subscribe(() => {
  //         done.fail("Unexpected conversation emitted");
  //       });
  //
  //     setTimeout(() => done(), 100);
  //   });
  //
  //   it("should not emit anything when no server conversations are missing in local storage", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
  //       .returnValue(of(aHmacHashesByBoxAddress.with().entry(BOB_BOX_ADDRESS, ALICE_BOB_HMAC_HASH).asIs()));
  //     (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
  //       .returnValue(of(HmacHashesByBoxAddress.empty()));
  //     (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_BOB_HMAC_HASH));
  //     (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
  //     (<jasmine.Spy>httpClient.put).and
  //       .returnValue(of(<ConversationIdsResponse>{conversationIds: [CONVERSATION_ID_2]}));
  //
  //     conversationsRemote.fetchMissingConversations([CONVERSATION_ID_1, CONVERSATION_ID_2], PAGE_ONE, PAGE_SIZE)
  //       .subscribe(() => {
  //         done.fail("Unexpected conversation emitted");
  //       });
  //
  //     setTimeout(() => done(), 100);
  //   });
  // });

  function aContact(pseudo: string, readFrom: string, publicKey?: string) {
    const writeWith: ContactKey = pseudo
      ? ContactKey.with(Keys.SYSTEM_KEYS_TYPE, Keys.SYSTEM_KEYS_TYPE, publicKey)
      : null;
    return Contact.with(pseudo, null, readFrom, null, writeWith, null);
  }

  function aConversationResponseConversation(conversationId: string, readFromBoxAddress: string) {
    return <ConversationsResponseConversation>{
      conversationId: conversationId,
      payload: PAYLOAD,
      readFromBoxAddress: readFromBoxAddress
    };
  }

  function aSession(boxAddress: string, contacts: Contact[], publicKeys: string) {
    const keyPairsByKeyId: any = {};
    keyPairsByKeyId[Keys.systemKeysId()] = KeyPair.with(Keys.SYSTEM_KEYS_TYPE, publicKeys, null);
    return Session.build(null, boxAddress, Contacts.fromArrayOfContacts(contacts),
      null, null, null, null, Keys.with(keyPairsByKeyId), null, null, null);
  }
});
