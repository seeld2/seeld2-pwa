import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../app.config";
import {HmacHashesByBoxAddress} from "../sessions/hmac-hashes-by-box-address";
import {SessionService} from "../sessions/session.service";
import {Contact} from "../user/contact/contact";
import {Logger} from "../utils/logging/logger";
import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";
import {Conversation} from "./conversation";
import {Message} from "./messages/message";

@Injectable()
export class ConversationsRemote {

  private readonly logger: Logger = Logger.for("ConversationsRemote");

  constructor(private httpClient: HttpClient,
              private sessionService: SessionService) {
  }

  // TODO Rename to deleteConversation() and test. Add logger trace
  deleteConversation2(conversation: Conversation, hmacHashesByBoxAddress: HmacHashesByBoxAddress): Observable<any> {
    return this.httpClient.put(
      `${config.api.conversations}/${conversation.conversationId()}/state/deleted`,
      <ConversationDeleteRequest>{hmacHashesByBoxAddress: hmacHashesByBoxAddress.toObject()}
    )
  }

  deleteConversationMessage(boxAddress: string, conversationId: string, messageId: string, hmacHash: string):
    Observable<any> {

    return this.httpClient.put(
      `${config.api.conversations}/${conversationId}/messages/${messageId}/state/deleted`,
      <MessageDeleteRequest>{boxAddress: boxAddress, hmacHash: hmacHash});
  }

  // TODO Test
  fetchPage(page: number, pageSize: number): Observable<Conversation> {
    this.logger.trace(`fetchPage(${page},${pageSize})`);

    return forkJoin(
      this.sessionService.session().hmacHashesByReadFromBoxAddresses(),
      this.sessionService.session().hmacHashesByDeletedReadFromBoxAddresses(),
      this.sessionService.session().hmacHashOfSystemBoxAddress())
      .pipe(
        mergeMap(([hmacHashesByBoxAddress, hmacHashesByDeletedBoxAddresses, hmacHashOfSystemBoxAddress]) => {
          const allHmacHashesByBoxAddress: HmacHashesByBoxAddress = hmacHashesByBoxAddress.mergeWith(hmacHashesByDeletedBoxAddresses);
          allHmacHashesByBoxAddress.set(this.sessionService.session().systemBoxAddress(), hmacHashOfSystemBoxAddress);

          const request: ConversationsRequest = ConversationsRequest.for(allHmacHashesByBoxAddress, page, pageSize);
          return this.httpClient.put(`${config.api.conversations}`, request.toJson());
        }),
        // We process each conversation one at a time... hence that from() function!
        mergeMap((conversationsResponse: ConversationsResponse) => {
          return from(conversationsResponse.conversations);
        }),
        mergeMap((conversationsResponseConversation: ConversationsResponseConversation) => {
          return this.buildConversationFromResponse(conversationsResponseConversation);
        })
      );
  }

  // PRIVATE

  private buildConversationFromResponse(conversation: ConversationsResponseConversation):
    Observable<Conversation> {

    const sender: Contact = this.sessionService.contactWithReadFromBoxAddress(conversation.readFromBoxAddress);
    const senderArmoredPublicKey: string = sender
      ? sender.writeWith().publicKeys()
      : this.sessionService.session().systemPublicKeys(); // This is the logged user's public keys

    return this.sessionService.openPgp()
      .decrypt(conversation.payload, senderArmoredPublicKey)
      .pipe(
        map((messageJson: string) => {
          return Conversation.with(conversation.conversationId, Message.fromJson(messageJson));
        })
      );
  }
}

export class ConversationIdsRequest implements Serializable {

  static for(hmacHashesByBoxAddress: HmacHashesByBoxAddress, page: number, pageSize: number): ConversationIdsRequest {
    return new ConversationIdsRequest(hmacHashesByBoxAddress, page, pageSize);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: HmacHashesByBoxAddress,
                      private readonly _page: number,
                      private readonly _pageSize: number) {
  }

  asObject(): any {
    return {
      hmacHashesByBoxAddress: this._hmacHashesByBoxAddress,
      page: this._page,
      pageSize: this._pageSize
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export interface ConversationDeleteRequest {
  hmacHashesByBoxAddress: HmacHashesByBoxAddress;
}

export interface ConversationIdsResponse {
  conversationIds: string[]
}

export class ConversationsRequest implements Serializable {

  static for(hmacHashesForBoxAddress: HmacHashesByBoxAddress, page: number, pageSize: number): ConversationsRequest {
    return new ConversationsRequest(hmacHashesForBoxAddress, page, pageSize);
  }

  constructor(private readonly _hmacHashesByBoxAddress: HmacHashesByBoxAddress,
              private readonly _page: number,
              private readonly _pageSize: number) {
  }

  asObject(): any {
    return {
      hmacHashesByBoxAddress: this._hmacHashesByBoxAddress,
      page: this._page,
      pageSize: this._pageSize
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export interface ConversationsResponse {
  conversations: ConversationsResponseConversation[];
}

export interface ConversationsResponseConversation {
  conversationId: string;
  messageId: string;
  payload: string;
  read: boolean;
  readFromBoxAddress: string;
}

export interface MessageDeleteRequest {
  boxAddress: string;
  hmacHash: string;
}
