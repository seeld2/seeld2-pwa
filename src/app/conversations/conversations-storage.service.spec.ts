import {Storage} from "@ionic/storage";
import {of} from "rxjs/observable/of";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {AES} from "../crypto/aes/aes";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {HmacHashesByBoxAddressTestBuilder as aHmacHashesByBoxAddress} from "../sessions/hmac-hashes-by-box-address.test-builder";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {SessionStorageService} from "../sessions/storage/session-storage.service";
import {Conversation} from "./conversation";
import {ConversationTestBuilder as aConversation} from "./conversation.test-builder";
import {ConversationsRemote} from "./conversations-remote";
import {ConversationsStorageService} from "./conversations-storage.service";
import {ConversationsTimeMap} from "./conversations-time-map";
import {ConversationsJasmine} from "./conversations.jasmine";
import {Message} from "./messages/message";
import {MessageTestBuilder as aMessage} from "./messages/message.test-builder";

describe("ConversationsStorageService", () => {

  const ALICE_BOBBY_READ_FROM: string = "aliceBobbyReadFrom";
  const ALICE_BOBBY_HMAC_HASH: string = "aliceBobbyHmacHash";
  const ALICE_CHARLES_READ_FROM: string = "aliceCharlesReadFrom";
  const ALICE_CHARLES_HMAC_HASH: string = "aliceCharlesHmacHash";
  const ALICE_PSEUDO: string = "alice";
  const ALICE_SYSTEM_BOX_ADDRESS: string = "aliceSystemBoxAddress";
  const ALICE_SYSTEM_BOX_ADDRESS_HMAC_HASH: string = "aliceSystemBoxAddressHmacHash";

  const BOBBY_PSEUDO: string = "bobby";

  const CHARLES_PSEUDO: string = "charles";

  const CONVERSATION_ID_1: string = '1';
  const CONVERSATION_ID_2: string = '2';
  const CONVERSATION_ID_3: string = '3';

  const CONVERSATION_AES: Uint8Array = new Uint8Array([1, 2, 3]);
  const CONVERSATIONS_TIME_MAP_AES: Uint8Array = new Uint8Array([1, 4, 5, 6, 3]);

  const PAGE = 0;
  const PAGE_SIZE = 100;
  const READ: boolean = true;
  const UNREAD: boolean = false;

  let conversationsRemote: ConversationsRemote;
  let session: Session;
  let sessionService: SessionService;
  let storage: Storage;

  let conversationsStorageService: ConversationsStorageService;

  let sessionStorageService: SessionStorageService;
  let storageAes: AES;

  beforeEach(() => {
    conversationsRemote = ConversationsJasmine.conversationsRemote();
    sessionService = SessionsJasmine.sessionService();
    storage = IonicJasmine.storage();

    conversationsStorageService = new ConversationsStorageService(conversationsRemote, sessionService, storage);

    sessionStorageService = SessionsJasmine.sessionWebStorageService();
    (<jasmine.Spy>sessionService.sessionStorageService).and.returnValue(sessionStorageService);
    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);

    storageAes = CryptoJasmine.storageAes();
    (<jasmine.Spy>sessionStorageService.storageAes).and.returnValue(of(storageAes));
  });

  it("should delete a conversation", (done: DoneFn) => {

    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.empty();
    conversationsTimeMap.put(aConversation.with().conversationId(CONVERSATION_ID_3).noBlanks());
    conversationsTimeMap.put(aConversation.with().conversationId(CONVERSATION_ID_2).noBlanks());
    conversationsTimeMap.put(aConversation.with().conversationId(CONVERSATION_ID_1).noBlanks());
    conversationsStorageService['_conversationsTimeMap'] = conversationsTimeMap;

    const participants: any = {};
    participants[ALICE_PSEUDO] = CONVERSATION_ID_1;
    participants[BOBBY_PSEUDO] = CONVERSATION_ID_2;
    participants[CHARLES_PSEUDO] = CONVERSATION_ID_3;
    const message: Message = aMessage.with().participants(participants).noBlanks();
    const conversation: Conversation = aConversation.with()
      .conversationId(CONVERSATION_ID_1)
      .and.message(message)
      .noBlanks();

    (<jasmine.Spy>sessionService.pseudo).and.returnValue(ALICE_PSEUDO);
    (<jasmine.Spy>session.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_SYSTEM_BOX_ADDRESS_HMAC_HASH));
    (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
      .returnValue(of(aHmacHashesByBoxAddress.with().entry(ALICE_BOBBY_READ_FROM, ALICE_BOBBY_HMAC_HASH).asIs()));
    (<jasmine.Spy>session.hmacHashesByDeletedReadFromBoxAddresses).and
      .returnValue(of(aHmacHashesByBoxAddress.with().entry(ALICE_CHARLES_READ_FROM, ALICE_CHARLES_HMAC_HASH).asIs()));
    (<jasmine.Spy>session.systemBoxAddress).and.returnValue(ALICE_SYSTEM_BOX_ADDRESS);
    (<jasmine.Spy>conversationsRemote.deleteConversation2).and.returnValue(of({}));
    (<jasmine.Spy>storage.remove).and.returnValue(Promise.resolve());
    // setConversationsTimeMapInStorage
    (<jasmine.Spy>storageAes.encrypt).and.returnValue(of(CONVERSATIONS_TIME_MAP_AES));
    (<jasmine.Spy>storage.set).and.returnValue(of({}));

    conversationsStorageService.deleteConversation(conversation).subscribe(
      () => {
        expect(session.hmacHashesByReadFromBoxAddresses).toHaveBeenCalledWith([BOBBY_PSEUDO, CHARLES_PSEUDO]);
        expect(session.hmacHashesByDeletedReadFromBoxAddresses).toHaveBeenCalledWith([BOBBY_PSEUDO, CHARLES_PSEUDO]);
        expect(storageAes.encrypt).toHaveBeenCalledTimes(1);
        expect(storage.set).toHaveBeenCalledTimes(1);
        expect(conversationsRemote.deleteConversation2)
          .toHaveBeenCalledWith(conversation, aHmacHashesByBoxAddress.with()
            .entry(ALICE_SYSTEM_BOX_ADDRESS, ALICE_SYSTEM_BOX_ADDRESS_HMAC_HASH)
            .entry(ALICE_BOBBY_READ_FROM, ALICE_BOBBY_HMAC_HASH)
            .entry(ALICE_CHARLES_READ_FROM, ALICE_CHARLES_HMAC_HASH)
            .asIs());

        done();
      }
    );
  });

  it("should list unread conversations", (done: DoneFn) => {

    const conversation1: Conversation = aConversation.with().conversationId(CONVERSATION_ID_1).and.read(UNREAD).noBlanks();
    const conversation2: Conversation = aConversation.with().conversationId(CONVERSATION_ID_2).and.read(READ).noBlanks();
    const conversation3: Conversation = aConversation.with().conversationId(CONVERSATION_ID_3).and.read(UNREAD).noBlanks();
    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.empty();
    conversationsTimeMap.put(conversation3);
    conversationsTimeMap.put(conversation2);
    conversationsTimeMap.put(conversation1);
    conversationsStorageService['_conversationsTimeMap'] = conversationsTimeMap;

    (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(CONVERSATION_AES));
    (<jasmine.Spy>storageAes.decrypt).and.returnValues(of(conversation3.toJson()), of(conversation2.toJson()), of(conversation1.toJson()));

    conversationsStorageService.fetchPageOfUnreadConversations(PAGE, PAGE_SIZE).subscribe(
      (conversations: Conversation[]) => {
        expect(conversations.length).toEqual(2);
        expect(conversations[0].read()).toBe(false);
        expect(conversations[1].read()).toBe(false);
        done();
      }
    );
  });

  describe(", when storing a conversation in storage", () => {

    const CONVERSATION_1: Conversation = aConversationWithIdAndSendDate('1', 1);
    const CONVERSATION_2: Conversation = aConversationWithIdAndSendDate('2', 2);
    const CONVERSATION_3: Conversation = aConversationWithIdAndSendDate('3', 3);
    const CONVERSATION_4: Conversation = aConversationWithIdAndSendDate('4', 4);

    beforeEach(() => {
      conversationsStorageService['_conversationsTimeMap'] = ConversationsTimeMap
        .for([CONVERSATION_1, CONVERSATION_2, CONVERSATION_3]);
    });

    it("should store a new conversation", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(undefined));

      (<jasmine.Spy>storageAes.encrypt).and.returnValues(of(CONVERSATION_AES), of(CONVERSATIONS_TIME_MAP_AES));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      conversationsStorageService.storeConversation(CONVERSATION_4).subscribe(
        (storedConversation: Conversation) => {
          expect(storageAes.encrypt).toHaveBeenCalledTimes(2);
          expect(storage.set).toHaveBeenCalledTimes(2);
          expect(storedConversation).toEqual(CONVERSATION_4);
          expect(conversationsStorageService['_conversationsTimeMap'].size()).toEqual(4);
          done();
        }
      );
    });

    it("should update an existing conversation", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(CONVERSATION_1));
      (<jasmine.Spy>storageAes.decrypt).and.returnValue(of(CONVERSATION_1.toJson()));

      (<jasmine.Spy>storageAes.encrypt).and.returnValues(of(CONVERSATION_AES), of(CONVERSATIONS_TIME_MAP_AES));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      const conversationToUpdate: Conversation = aConversationWithIdAndSendDate('1', 5);
      conversationsStorageService.storeConversation(conversationToUpdate).subscribe(
        (storedConversation: Conversation) => {
          expect(storageAes.encrypt).toHaveBeenCalledTimes(2);
          expect(storage.set).toHaveBeenCalledTimes(2);
          expect(storedConversation).toEqual(conversationToUpdate);
          expect(conversationsStorageService['_conversationsTimeMap'].size()).toEqual(3);
          done();
        }
      );
    });
  });

  function aConversationWithIdAndSendDate(conversationId: string, sendDate: number) {
    const message: Message = aMessage.with().sendDate(sendDate).noBlanks();
    return aConversation.with().conversationId(conversationId).and.message(message).noBlanks();
  }
});
