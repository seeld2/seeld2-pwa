import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {Observable} from "rxjs/Observable";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {filter, map, mergeMap, toArray} from "rxjs/operators";
import {config} from "../app.config";
import {AES} from "../crypto/aes/aes";
import {HmacHashesByBoxAddress} from "../sessions/hmac-hashes-by-box-address";
import {SessionService} from "../sessions/session.service";
import {Logger} from "../utils/logging/logger";
import {Conversation} from "./conversation";
import {ConversationsRemote} from "./conversations-remote";
import {ConversationsTimeMap} from "./conversations-time-map";
import {Message} from "./messages/message";

@Injectable()
export class ConversationsStorageService {

  private static readonly CONVERSATION_KEY: string = 'CONVERSATION';
  private static readonly CONVERSATIONS_TIME_MAP_KEY: string = 'CONVERSATIONS_TIME_MAP';

  private static conversationKey(conversationId: string): string {
    return ConversationsStorageService.CONVERSATION_KEY + '(' + conversationId + ')';
  }

  private readonly logger: Logger = Logger.for("ConversationsStorageService");

  private _storageAes: AES;
  private _conversationsTimeMap: ConversationsTimeMap;

  constructor(private readonly _conversationsRemote: ConversationsRemote,
              private readonly _sessionService: SessionService,
              private readonly _storage: Storage,) {
  }

  deleteConversation(conversation: Conversation): Observable<any> {
    this.logger.trace(`deleteConversation(${conversation.conversationId()})`);

    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.initConversationTimeMap()),
        mergeMap(() => {
          const message: Message = conversation.message();
          const loggedUserPseudo: string = this._sessionService.pseudo();
          const participantsPseudosWithoutLoggedUser: string[] = message.participantsPseudos([loggedUserPseudo]);

          const systemBoxAddressHmacHashObs: Observable<string> = this._sessionService.session()
            .hmacHashOfSystemBoxAddress();
          const hmacHashesByBoxAddressObs: Observable<HmacHashesByBoxAddress> = this._sessionService.session()
            .hmacHashesByReadFromBoxAddresses(participantsPseudosWithoutLoggedUser);
          const hmacHashesByDeletedBoxAddressObs: Observable<HmacHashesByBoxAddress> = this._sessionService.session()
            .hmacHashesByDeletedReadFromBoxAddresses(participantsPseudosWithoutLoggedUser);
          return forkJoin(systemBoxAddressHmacHashObs, hmacHashesByBoxAddressObs, hmacHashesByDeletedBoxAddressObs)
        }),
        mergeMap(([systemBoxAddressHmacHash, hmacHashesByBoxAddress, hmacHashesByDeletedBoxAddress]) => {
          const allHmacHashesByBoxAddress: HmacHashesByBoxAddress = hmacHashesByBoxAddress
            .mergeWith(hmacHashesByDeletedBoxAddress);
          const systemBoxAddress: string = this._sessionService.session().systemBoxAddress();
          allHmacHashesByBoxAddress.set(systemBoxAddress, systemBoxAddressHmacHash);
          return this._conversationsRemote.deleteConversation2(conversation, allHmacHashesByBoxAddress);
        }),
        mergeMap(() => {
          this._conversationsTimeMap.remove(conversation.conversationId())
          return this._storage.remove(ConversationsStorageService.conversationKey(conversation.conversationId()));
        }),
        mergeMap(() => this.setConversationsTimeMapInStorage())
      );
  }

  // TODO Test
  fetchPage(page: number, pageSize: number) {
    this.logger.trace(`fetchPage(${page},${pageSize})`);

    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.initConversationTimeMap()),
        mergeMap(() => from(this._conversationsTimeMap.getPageOfConversationIds(page, pageSize))),
        mergeMap((conversationId: string) => this.getConversationWithId(conversationId)),
      );
  }

  fetchPageOfUnreadConversations(page: number = 0,
                                 pageSize: number = config.conversations.pageSize): Observable<Conversation[]> {
    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.initConversationTimeMap()),
        mergeMap(() => this.fetchConversationsFromStorage(page, pageSize)),
        filter((conversation: Conversation) => !conversation.read()),
        toArray()
      );
  }

  storeConversation(conversation: Conversation, forceUpdateOfReadStatus: boolean = false): Observable<Conversation> {
    this.logger.trace(`storeConversation(${conversation.conversationId()})`);

    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.initConversationTimeMap()),
        mergeMap(() => this.getConversationWithId(conversation.conversationId())),
        mergeMap((conversationToUpdate: Conversation) => {
          if (conversationToUpdate) {
            conversationToUpdate.setMessage(conversation.message());
            if (forceUpdateOfReadStatus || conversationToUpdate.read()) {
              conversationToUpdate.setRead(conversation.read());
            }
          } else {
            conversationToUpdate = Conversation.with(
              conversation.conversationId(),
              conversation.message(),
              conversation.read());
          }

          return this.setConversationInStorage(conversationToUpdate);
        }),
        map((updatedConversation: Conversation) => {
          return this._conversationsTimeMap.put(updatedConversation);
        }),
        mergeMap((updatedConversation: Conversation) => {
          return this.setConversationsTimeMapInStorage()
            .pipe(
              map(() => updatedConversation)
            );
        }),
      );
  }

  updateConversationWithMessage(message: Message, read: boolean): Observable<Conversation> {
    this.logger.trace(`updateConversationWithMessage(${message.messageId()}, ${read}`);
    const conversation: Conversation = Conversation.with(message.conversationId(), message, read);
    return this.storeConversation(conversation, true);
  }

  private conversationsTimeMapKey(): string {
    return ConversationsStorageService.CONVERSATIONS_TIME_MAP_KEY + '(' + this._sessionService.session().pseudo() + ')';
  }

  private getConversationWithId(conversationId: string): Observable<Conversation> {
    return from(this._storage.get(ConversationsStorageService.conversationKey(conversationId)))
      .pipe(
        mergeMap((encrypted: number[]) => {
          if (!encrypted) {
            return of(undefined);
          }
          return this._storageAes.decrypt(new Uint8Array(encrypted))
            .pipe(
              map((json: string) => Conversation.fromJson(json))
            );
        })
      );
  }

  private initConversationTimeMap(): Observable<ConversationsTimeMap> {

    if (this._conversationsTimeMap && !this._conversationsTimeMap.isEmpty()) {
      return of(this._conversationsTimeMap);
    }

    return this.initStorageAes()
      .pipe(
        mergeMap(() => this._storage.get(this.conversationsTimeMapKey())),
        mergeMap((encrypted: number[]) => encrypted
          ? this._storageAes.decrypt(new Uint8Array(encrypted))
          : of(JSON.stringify(ConversationsTimeMap.empty()))),
        map((json: string) => {
          this._conversationsTimeMap = ConversationsTimeMap.fromJson(json);
          return this._conversationsTimeMap;
        })
      );
  }

  private initStorageAes(): Observable<AES> {
    return this._sessionService.sessionStorageService().storageAes()
      .pipe(
        map((aes: AES) => {
          this._storageAes = aes;
          return this._storageAes;
        })
      );
  }

  private fetchConversationsFromStorage(page: number, pageSize: number = config.conversations.pageSize):
    Observable<Conversation> {

    return from(this._conversationsTimeMap.getPageOfConversationIds(page, pageSize))  // We process each conversation ID one by one... Hence the from()!
      .pipe(
        mergeMap((conversationId: string) => this.getConversationWithId(conversationId))
      );
  }

  private setConversationInStorage(conversation: Conversation): Observable<Conversation> {
    return this._storageAes.encrypt(conversation.toJson())
      .pipe(
        mergeMap((encrypted: Uint8Array) => this._storage
          .set(ConversationsStorageService.conversationKey(conversation.conversationId()), Array.from(encrypted))),
        map(() => conversation)
      );
  }

  private setConversationsTimeMapInStorage(): Observable<any> {
    return this._storageAes.encrypt(JSON.stringify(this._conversationsTimeMap))
      .pipe(
        mergeMap((payload: Uint8Array) => from(this._storage.set(this.conversationsTimeMapKey(), Array.from(payload))))
      );
  }
}
