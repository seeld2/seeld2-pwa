import {Conversation} from "./conversation";
import {ConversationFixture} from "../../core/conversations/fixtures/conversation-fixture";
import {ConversationsTimeMap} from "./conversations-time-map";

describe("ConversationsTimeMap", () => {

  const CONVERSATION_1: Conversation = ConversationFixture.withConversationIdAndSendDate('1', 1);
  const CONVERSATION_2: Conversation = ConversationFixture.withConversationIdAndSendDate('2', 2);
  const CONVERSATION_3: Conversation = ConversationFixture.withConversationIdAndSendDate('3', 3);

  const PAGE_ONE: number = 0;
  const PAGE_TWO: number = 1;
  const PAGE_SIZE_OF_TWO: number = 2;

  it("should instantiate an empty ConversationsTimeMap", () => {

    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.empty();

    expect(conversationsTimeMap).toBeDefined();
    expect(conversationsTimeMap['map'].length).toBe(0);
  });

  it("should instantiate a ConversationsTimeMap from an array of conversations", () => {
    const conversations: Conversation[] = [CONVERSATION_2, CONVERSATION_1];

    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.for(conversations);

    expect(conversationsTimeMap).not.toBeUndefined();
    expect(conversationsTimeMap['map'].length).toBe(2);
  });

  it("should instantiate a ConversationsTimeMap from a JSON string", () => {
    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.for([CONVERSATION_1]);

    const conversationsTimeMapFromJson: ConversationsTimeMap = ConversationsTimeMap.fromJson(JSON.stringify(conversationsTimeMap));

    expect(conversationsTimeMapFromJson).toEqual(conversationsTimeMap);
  });

  it("should return all conversation IDs", () => {
    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap
      .for([CONVERSATION_1, CONVERSATION_3, CONVERSATION_2]);

    const conversationIds: string[] = conversationsTimeMap.getAllConversationIds();

    expect(conversationIds.length).toBe(3);
    expect(conversationIds)
      .toEqual([CONVERSATION_3.conversationId(), CONVERSATION_2.conversationId(), CONVERSATION_1.conversationId()]);
  });

  describe(", when getting pages of conversation IDs,", () => {

    it("should return the first page of conversation IDs", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap
        .for([CONVERSATION_1, CONVERSATION_2, CONVERSATION_3]);

      const conversationIds: string[] = conversationsTimeMap.getPageOfConversationIds(PAGE_ONE, PAGE_SIZE_OF_TWO);

      expect(conversationIds.length).toBe(2);
      expect(conversationIds).toContain(CONVERSATION_3.conversationId());
      expect(conversationIds).toContain(CONVERSATION_2.conversationId());
    });

    it("should return the second page of conversation IDs", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap
        .for([CONVERSATION_1, CONVERSATION_2, CONVERSATION_3]);

      const conversationIds: string[] = conversationsTimeMap.getPageOfConversationIds(PAGE_TWO, PAGE_SIZE_OF_TWO);

      expect(conversationIds.length).toBe(1);
      expect(conversationIds).toContain(CONVERSATION_1.conversationId());
    });

    it("should return an empty page for nonexistent pages", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap
        .for([CONVERSATION_1, CONVERSATION_2, CONVERSATION_3]);

      const conversationIds: string[] = conversationsTimeMap.getPageOfConversationIds(23, PAGE_SIZE_OF_TWO);

      expect(conversationIds.length).toBe(0);
    });
  });

  describe(", when putting a new conversation into the map,", () => {

    it("should add the conversation into the map", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.for([CONVERSATION_3, CONVERSATION_2]);

      conversationsTimeMap.put(CONVERSATION_1);

      expect(conversationsTimeMap['map'].length).toBe(3);
    });

    it("should add the conversation while respecting the 'most recent first' order", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.for([CONVERSATION_2, CONVERSATION_1]);

      conversationsTimeMap.put(CONVERSATION_3);

      expect(conversationsTimeMap['map'][0].id).toBe(CONVERSATION_3.conversationId());
    });

    it("should update a conversation that already exists in the time map, instead of inserting a new one", () => {
      const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap.for([CONVERSATION_2, CONVERSATION_1]);
      const updatedConversation1: Conversation = ConversationFixture.withConversationIdAndSendDate(CONVERSATION_1.conversationId(), 789);

      conversationsTimeMap.put(updatedConversation1);

      expect(conversationsTimeMap['map'].length).toBe(2);
      expect(conversationsTimeMap['map'][0].id).toBe(CONVERSATION_1.conversationId());
      expect(conversationsTimeMap['map'][0].sendDate).toBe(789);
    });
  });

  it("should remove a conversation from the map", () => {
    const conversationsTimeMap: ConversationsTimeMap = ConversationsTimeMap
      .for([CONVERSATION_1, CONVERSATION_3, CONVERSATION_2]);

    conversationsTimeMap.remove(CONVERSATION_1.conversationId());

    expect(conversationsTimeMap['map'].length).toEqual(2);
  });
});
