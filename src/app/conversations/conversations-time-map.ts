import {Conversation} from "./conversation";

/**
 * This object holds a list of items mapping conversation IDs by their timestamps.
 * It allows keeping track of these conversations' time order.
 */
export class ConversationsTimeMap {

  /**
   * Build an empty ConversationsTimeMap instance.
   */
  static empty(): ConversationsTimeMap {
    return new ConversationsTimeMap([]);
  }

  /**
   * Builds an instance of ConversationsTimeMap for the specified array of conversations.
   */
  static for(conversations: Conversation[] = []): ConversationsTimeMap {
    const map: Item[] = [];
    conversations.forEach((conversation: Conversation) => map.push(Item.for(conversation)));
    return new ConversationsTimeMap(map);
  }

  static fromJson(json: string): ConversationsTimeMap {
    const parsed: ConversationsTimeMap = JSON.parse(json);
    const items: Item[] = parsed.map.map(object => Item.from(object));
    return new ConversationsTimeMap(items);
  }

  private static sort(map: Item[]): Item[] {
    return map.sort((item1: Item, item2: Item) => item2.sendDate - item1.sendDate);
  }

  private readonly map: Item[];

  private constructor(map: Item[]) {
    this.map = ConversationsTimeMap.sort(map);
  }

  /**
   * Returns all the conversation IDs of this ConversationsTimeMap instance.
   * The returned IDs are ordered so that they respect the timestamp order: most recent first.
   */
  getAllConversationIds(): string[] {
    return this.map.map((item: Item) => item.id);
  }

  /**
   * Returns a page of conversation IDs from this ConversationsTimeMap instance.
   * The returned IDs are ordered so that they respect the timestamp order: most recent first.
   *
   * @param page Zero-based, meaning that page 0 is the first page.
   * @param pageSize The amount of conversations to return for the page.
   * @returns An array of conversation IDs as strings.
   */
  getPageOfConversationIds(page: number, pageSize: number): string[] {
    const start: number = page * pageSize;
    const end: number = start + pageSize;
    return this.map.slice(start, end).map((item: Item) => item.id);
  }

  isEmpty(): boolean {
    return Object.keys(this.map).length === 0;
  }

  /**
   * Inserts or updates the ConversationsTimeMap instance with with the passed conversation.
   * If the conversation is already mapped, the method updates its timestamp in the time map.
   *
   * @param conversation The conversation to add or update.
   */
  put(conversation: Conversation): Conversation {
    this.removeConversationFromMap(conversation.conversationId());
    this.map.push(Item.for(conversation));
    ConversationsTimeMap.sort(this.map);
    return conversation;
  }

  remove(conversationId: string) {
    this.removeConversationFromMap(conversationId);
  }

  size(): number {
    return this.map.length;
  }

  private removeConversationFromMap(conversationId: string) {
    const index: number = this.map.findIndex((item: Item) => item.id === conversationId);
    if (index >= 0) {
      this.map.splice(index, 1);
    }
  }
}

class Item {

  private constructor(public readonly id: string,
                      public readonly sendDate: number) {
  }

  static for(conversation: Conversation): Item {
    return new Item(
      conversation.conversationId(),
      conversation.message().sendDate() ? conversation.message().sendDate() : undefined
    );
  }

  static from(object: any): Item {
    return new Item(object.id, object.sendDate);
  }
}
