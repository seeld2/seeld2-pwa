import {Objects} from "../utils/objects";
import {ConversationsRemote} from "./conversations-remote";
import {ConversationsStorageService} from "./conversations-storage.service";
import {ConversationsService} from "./conversations.service";

export class ConversationsJasmine {

  static conversationsRemote() {
    return jasmine.createSpyObj('conversationsRemote', Objects.propertyNamesOf(new ConversationsRemote(null, null)));
  }

  static conversationsService() {
    return jasmine.createSpyObj('conversationsService', Objects.propertyNamesOf(new ConversationsService(null, null, null, null)));
  }

  static conversationsStorageService() {
    return jasmine.createSpyObj('conversationsStorageService', Objects
      .propertyNamesOf(new ConversationsStorageService(null, null, null)));
  }
}
