import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {ConversationsStorageService} from "./conversations-storage.service";
import {ConversationsRemote} from "./conversations-remote";
import {ConversationsService} from "./conversations.service";
import {MessagesService} from "./messages/messages.service";
import {MessagesCache} from "./messages/messages-cache";
import {MessagesRemote} from "./messages/messages-remote";
import {MessagesStorageService} from "./messages/messages-storage.service";
import {UnreadMessagesPollerService} from "./messages/unread/unread-messages-poller.service";
import {CryptoModule} from "../crypto/crypto.module";
import {NotificationsModule} from "../notifications/notifications.module";
import {UnreadMessagesPoller} from "../notifications/unread-messages-poller";
import {SessionsModule} from "../sessions/sessions.module";
import {TimeModule} from "../time/time.module";
import {UiNotificationsModule} from "../uinotifications/ui-notifications.module";
import {UserModule} from "../user/user.module";
import {UtilsModule} from "../utils/utils.module";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    NotificationsModule,
    SessionsModule,
    TimeModule,
    UiNotificationsModule,
    UserModule,
    UtilsModule
  ],
  providers: [
    ConversationsService,
    ConversationsRemote,
    ConversationsStorageService,
    MessagesService,
    MessagesCache,
    MessagesRemote,
    MessagesStorageService,
    // TODO ... why two providers of UnreadMessagesPollerService ?
    UnreadMessagesPollerService,
    {provide: UnreadMessagesPoller, useClass: UnreadMessagesPollerService},
    //
  ]
})
export class ConversationsModule {
}
