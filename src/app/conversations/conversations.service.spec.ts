import {Events} from "ionic-angular";
import {of} from "rxjs/observable/of";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {Conversation} from "./conversation";
import {ConversationTestBuilder as aConversation} from "./conversation.test-builder";
import {ConversationsEvent} from "./conversations-event";
import {ConversationsStorageService} from "./conversations-storage.service";
import {ConversationsJasmine} from "./conversations.jasmine";
import {ConversationsService} from "./conversations.service";

describe("ConversationsService", () => {

  const CONVERSATION_ID_1: string = "conversationId1";
  const CONVERSATION_ID_2: string = "conversationId2";
  const CONVERSATION_ID_3: string = "conversationId3";

  const MARK_NEW_AS_UNREAD: boolean = true;
  const NOT_READ: boolean = false;
  const PAGE = 0;
  const PAGE_SIZE = 100;

  let conversationsStorageService: ConversationsStorageService;
  let events: Events;

  let conversationsService: ConversationsService;

  beforeEach(() => {
    conversationsStorageService = ConversationsJasmine.conversationsStorageService();
    events = IonicJasmine.events();

    conversationsService = new ConversationsService(null, conversationsStorageService, events, null);
  });

  it("should count the conversations having new messages", (done: DoneFn) => {

    const conversations: Conversation[] = [
      aConversation.with().conversationId(CONVERSATION_ID_2).read(NOT_READ).noBlanks(),
      aConversation.with().conversationId(CONVERSATION_ID_3).read(NOT_READ).noBlanks(),
    ];
    (<jasmine.Spy>conversationsStorageService.fetchPageOfUnreadConversations).and.returnValue(of(conversations));

    conversationsService.countConversationsWithUnreadMessagesInStorage().subscribe(
      ((count: number) => {
        expect(count).toEqual(2);
        done();
      })
    );
  });

  it("should delete a conversation", (done: DoneFn) => {

    const conversation = aConversation.with().noBlanks();

    (<jasmine.Spy>conversationsStorageService.deleteConversation).and.returnValue(of(''));

    conversationsService.deleteConversation(conversation).subscribe(
      (() => {
        expect(conversationsStorageService.deleteConversation).toHaveBeenCalledWith(conversation);
        done();
      })
    );
  });

  // it("should list conversations one by one", (done: DoneFn) => {
  //
  //   (<jasmine.Spy>conversationsStorageService.listConversationsOneByOne).and.returnValue(from([
  //     aConversation.with().conversationId(CONVERSATION_ID_1).noBlanks(),
  //     aConversation.with().conversationId(CONVERSATION_ID_2).noBlanks()
  //   ]));
  //
  //   const conversations: Conversation[] = [];
  //   conversationsService.listConversationsOneByOne(PAGE, PAGE_SIZE, MARK_NEW_AS_UNREAD)
  //     .subscribe(
  //       (conversation: Conversation) => {
  //         conversations.push(conversation);
  //       },
  //       undefined,
  //       (() => {
  //         expect(conversationsStorageService.listConversationsOneByOne)
  //           .toHaveBeenCalledWith(PAGE, PAGE_SIZE, MARK_NEW_AS_UNREAD);
  //
  //         expect(conversations.length).toEqual(2);
  //         expect(Locks['messagesLock'].isLocked()).toBe(false);
  //
  //         done();
  //       })
  //     );
  // });

  it("should mark a conversation as read", (done: DoneFn) => {

    const conversation: Conversation = aConversation.with().read(NOT_READ).noBlanks();

    (<jasmine.Spy>conversationsStorageService.storeConversation).and.returnValue(of(conversation));
    (<jasmine.Spy>conversationsStorageService.fetchPageOfUnreadConversations).and.returnValue(of([]));

    conversationsService.markConversationAsRead(conversation).subscribe(
      () => {
        expect(events.publish).toHaveBeenCalledWith(ConversationsEvent.REFRESH_NEW_CONVERSATIONS_COUNT, 0);
        expect(conversation.read()).toBe(true);
        done();
      }
    );
  });
});
