import {Injectable} from "@angular/core";
import {Events} from "ionic-angular";
import {Observable} from "rxjs/Observable";
import {concat} from "rxjs/observable/concat";
import {merge} from "rxjs/observable/merge";
import {finalize, map, mergeMap} from "rxjs/operators";
import {Locks} from "../../core/locks";
import {Lock} from "../../shared/concurrency/lock";
import {Logger} from "../utils/logging/logger";
import {Conversation} from "./conversation";
import {ConversationsEvent} from "./conversations-event";
import {ConversationsRemote} from "./conversations-remote";
import {ConversationsStorageService} from "./conversations-storage.service";
import {MessagesService} from "./messages/messages.service";


@Injectable()
export class ConversationsService {

  private readonly logger: Logger = Logger.for("ConversationsService");

  constructor(private readonly conversationsRemote: ConversationsRemote,
              private readonly conversationsStorageService: ConversationsStorageService,
              private readonly events: Events,
              private readonly messagesService: MessagesService) {
  }

  countConversationsWithUnreadMessagesInStorage(): Observable<number> {
    this.logger.trace('countConversationsWithNewMessages()');

    return this.conversationsStorageService.fetchPageOfUnreadConversations()
      .pipe(
        map((unreadConversations: Conversation[]) => unreadConversations.length)
      );
  }

  deleteConversation(conversation: Conversation): Observable<any> {
    this.logger.trace(`deleteConversation(${conversation.conversationId()})`);

    return Locks.getMessagesLock('ConversationsService.deleteConversation')
      .pipe(
        mergeMap((lock: Lock) => {
          try {
            return this.conversationsStorageService.deleteConversation(conversation)
              .pipe(
                finalize(() => lock.unlock())
              );
          } catch (e) {
            this.logger.error(lock.getOwner(), e);
            lock.unlock();
          }
        })
      );
  }

  fetchPage(page: number, pageSize: number): Observable<Conversation> {
    this.logger.trace(`fetchPage(${page},${pageSize})`);

    const fetchFromStorageObs: Observable<Conversation> = this.conversationsStorageService.fetchPage(page, pageSize);

    const unreadConversationsObs: Observable<Conversation> = this.messagesService.fetchUnreadMessages()
      .pipe(map(message => Conversation.with(message.conversationId(), message, false)));

    let serverConversationsObs: Observable<Conversation> = this.conversationsRemote.fetchPage(page, pageSize)
      .pipe(mergeMap(conversation => this.conversationsStorageService.storeConversation(conversation)));

    return concat(fetchFromStorageObs, merge(unreadConversationsObs, serverConversationsObs));
  }

  fetchPageFromStorage(page: number, pageSize: number): Observable<Conversation> {
    this.logger.trace(`fetchPageFromStorage(${page},${pageSize})`);
    return this.conversationsStorageService.fetchPage(page, pageSize);
  }

  markConversationAsRead(conversation: Conversation): Observable<any> {
    this.logger.trace(`markConversationAsRead(${conversation.conversationId()})`);

    return Locks.getMessagesLock('ConversationsService.markConversationAsRead')
      .pipe(
        mergeMap((lock: Lock) => {
          try {
            conversation.setRead(true);

            return this.conversationsStorageService.storeConversation(conversation, true)
              .pipe(
                mergeMap(() => this.countConversationsWithUnreadMessagesInStorage()),
                map((count: number) => this.events.publish(ConversationsEvent.REFRESH_NEW_CONVERSATIONS_COUNT, count)),
                finalize(() => lock.unlock())
              );

          } catch (e) {
            this.logger.error(lock.getOwner(), e);
            lock.unlock();
          }
        })
      );
  }
}
