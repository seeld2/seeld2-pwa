/**
 * Messages-related events' constants.
 */
export enum MessageEvent {
  CHECK_UNREAD = 'MessageEvent:CHECK_UNREAD'
}
