import {Message} from "./message";

export class MessageFixture {

  static readonly CONTENT: string = 'content';
  static readonly CONVERSATION_ID: string = 'conversationId';
  static readonly MESSAGE_ID: string = 'messageId';
  static readonly SEND_DATE: number = 1;
  static readonly SENDER: string = 'sender';
  static readonly TITLE: string = 'title';
  static readonly USERNAME: string = 'username';

  static build(): Message {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, this.SEND_DATE, this.participants(),
      this.CONVERSATION_ID, this.MESSAGE_ID);
  }

  static withConversationId(conversationId: string) {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, this.SEND_DATE, this.participants(),
      conversationId, this.MESSAGE_ID);
  }

  static withConversationIdAndSendDate(conversationId: string, sendDate: number) {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, sendDate, this.participants(),
      conversationId, this.MESSAGE_ID);
  }

  static withId(messageId: string) {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, this.SEND_DATE, this.participants(),
      this.CONVERSATION_ID, messageId);
  }

  static withMessageIdAndSendDate(messageId: string, sendDate: number) {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, sendDate, this.participants(),
      this.CONVERSATION_ID, messageId);
  }

  static withParticipants(participants: any): Message {
    return MessageFixture.with(this.TITLE, this.CONTENT, this.SENDER, this.SEND_DATE, participants,
      this.CONVERSATION_ID, this.MESSAGE_ID);
  }

  private static participants(): any {
    const participants: any = {};
    participants[this.USERNAME] = this.CONVERSATION_ID;
    return participants;
  }

  private static with(title: string, content: string, sender: string, sendDate: number, participants: any,
                      conversationId: string, messageId: string): Message {
    return Message.with(title, content, sender, sendDate, null, null, participants, 1, conversationId, messageId);
  }
}
