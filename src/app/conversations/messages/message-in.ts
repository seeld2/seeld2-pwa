export interface MessageIn {
  conversationId: string;
  messageId: string;
  payload: string;
  readFromBoxAddress: string;
}
