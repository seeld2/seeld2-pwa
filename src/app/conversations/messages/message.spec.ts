import {Message} from "./message";
import {Contact} from "../../user/contact/contact";
import {MessageTestBuilder as aMessage} from "./message.test-builder";

describe("Message", () => {

  const TEMPLATE_OBJECT: any = {
    title: "title",
    content: "content",
    sender: "sender",
    sendDate: 123,
    participants: {test: "test"},
    conversationId: "conversationId",
    messageId: "messageId"
  };

  const TEMPLATE_OBJECT_WITH_RECIPIENTS: any = {
    title: "title",
    content: "content",
    sender: "sender",
    sendDate: 123,
    participants: {test: "test"},
    conversationId: "conversationId",
    messageId: "messageId",
    recipients: ["alice", "bob"]
  };

  const TEMPLATE_OBJECT_WITH_ORDER: any = {
    title: "title",
    content: "content",
    sender: "sender",
    sendDate: 123,
    participants: {test: "test"},
    order: 456,
    conversationId: "conversationId",
    messageId: "messageId",
    recipients: ["alice", "bob"]
  };

  it("should produce an instance from a JSON string", () => {
    expectMessageToHavePropertiesOf(Message.fromJson(JSON.stringify(TEMPLATE_OBJECT)), TEMPLATE_OBJECT);

    expectMessageToHavePropertiesOf(Message.fromJson(JSON.stringify(TEMPLATE_OBJECT_WITH_RECIPIENTS)),
      TEMPLATE_OBJECT_WITH_RECIPIENTS);

    expectMessageToHavePropertiesOf(Message.fromJson(JSON.stringify(TEMPLATE_OBJECT_WITH_ORDER)),
      TEMPLATE_OBJECT_WITH_ORDER);
  });

  it("should produce an instance from an object", () => {
    expectMessageToHavePropertiesOf(Message.fromObject(TEMPLATE_OBJECT), TEMPLATE_OBJECT);
    expectMessageToHavePropertiesOf(Message.fromObject(TEMPLATE_OBJECT_WITH_RECIPIENTS), TEMPLATE_OBJECT_WITH_RECIPIENTS);
    expectMessageToHavePropertiesOf(Message.fromObject(TEMPLATE_OBJECT_WITH_ORDER), TEMPLATE_OBJECT_WITH_ORDER);
  });

  it("should produce an instance for a new message", () => {

    const message: Message = Message
      .newMessage("title", "content", 'sender', Date.now(), [aContact('receiver')]);

    expect(message.title()).toEqual('title');
    expect(message.content()).toEqual('content');
    expect(message.sender()).toEqual('sender');
    expect(message.sendDate()).toBeDefined();
    expect(Object.keys(message['_participants']).length).toEqual(2);
    expect(Object.keys(message['_participants'])).toContain('sender');
    expect(Object.keys(message['_participants']['sender'])).toBeDefined();
    expect(Object.keys(message['_participants'])).toContain('receiver');
    expect(Object.keys(message['_participants']['receiver'])).toBeDefined();
    expect(message['_order']).toEqual(1);
    expect(message.recipients().length).toEqual(2);
    expect(message.recipients()).toContain('sender');
    expect(message.recipients()).toContain('receiver');
  });

  it("should ensure the integrity of a message", () => {

    const messageWithNoConversationId: Message = aMessageWithConversationIdAndMessageId(null, 'messageId');
    const messageWithNoMessageId: Message = aMessageWithConversationIdAndMessageId('conversationId', null);

    expect(() => messageWithNoConversationId.checkIntegrity()).toThrowError();
    expect(() => messageWithNoMessageId.checkIntegrity()).toThrowError();
  });

  describe(", when comparing this message with another", () => {

    it("should give precedence to the first message according to the order", () => {
      expect(aMessageWithOrderAndSendDate(2, 2000).compareTo(aMessageWithOrderAndSendDate(1, 1000))).toBeLessThan(0);
      expect(aMessageWithOrderAndSendDate(2, 1000).compareTo(aMessageWithOrderAndSendDate(1, 2000))).toBeLessThan(0);
    });

    it("should give precedence to the first message according to date if order is identical", () => {
      expect(aMessageWithOrderAndSendDate(0, 2000).compareTo(aMessageWithOrderAndSendDate(0, 1000))).toBeLessThan(0);
    });

    it("should consider two message with same order and timestamp to be equivalent", () => {
      expect(aMessageWithOrderAndSendDate(6, 2000).compareTo(aMessageWithOrderAndSendDate(6, 2000))).toEqual(0);
    });
  });

  it("should indicate whether two messages are equal or not", () => {
    function m() {
      return aMessage.with().title("title").content("content").sender("sender").sendDate(1).order(1).conversationId("conversationId").messageId("messageId").asIs();
      // return new Message("title", "content", "sender", 1, null, null, 1, "conversationId", "messageId");
    }

    expect(m().equals(m())).toEqual(true);

    let other: Message = aMessage.with().title("changed").content("content").sender("sender").sendDate(1).order(1).conversationId("conversationId").messageId("messageId").asIs()
    // other.title = "changed";
    expect(other.equals(m())).toEqual(false);

    other = aMessage.with().title("title").content("changed").sender("sender").sendDate(1).order(1).conversationId("conversationId").messageId("messageId").asIs();
    // other.content = "changed";
    expect(other.equals(m())).toEqual(false);

    other = aMessage.with().title("title").content("content").sender("changed").sendDate(1).order(1).conversationId("conversationId").messageId("messageId").asIs()
    // other.sender = "changed";
    expect(other.equals(m())).toEqual(false);

    other = aMessage.with().title("title").content("content").sender("sender").sendDate(99).order(1).conversationId("conversationId").messageId("messageId").asIs()
    // other.sendDate = 999;
    expect(other.equals(m())).toEqual(false);

    other = aMessage.with().title("title").content("content").sender("sender").sendDate(1).order(1).conversationId("changed").messageId("messageId").asIs()
    // other.conversationId = "changed";
    expect(other.equals(m())).toEqual(false);

    other = aMessage.with().title("title").content("content").sender("sender").sendDate(1).order(1).conversationId("conversationId").messageId("changed").asIs()
    // other.messageId = "changed";
    expect(other.equals(m())).toEqual(false);
  });

  it("should indicate whether the message represented by the instance has been sent", () => {
    expect(aMessageWithConversationIdAndMessageId('conversationId', null).hasBeenSent()).toEqual(false);
    expect(aMessageWithConversationIdAndMessageId('conversationId', 'messageId').hasBeenSent()).toEqual(true);
  });

  it("should indicate if the message has an empty title", () => {

    // const message: Message = new Message('title', 'content', 'sender', 123, null, {});
    let message: Message = aMessage.with().title("title").noBlanks();
    expect(message.hasEmptyTitle()).toEqual(false);

    message = aMessage.with().title(' ').noBlanks();
    expect(message.hasEmptyTitle()).toEqual(true);

    message = aMessage.with().title(null).asIs();
    expect(message.hasEmptyTitle()).toEqual(true);
  });

  it("should return the list of participants pseudos, excluding the pseudos indicated", () => {

    // const message: Message = new Message('title', 'content', 'sender', 123, null, {test1: "1", test2: "2", test3: "3"});
    const message: Message = aMessage.with().title('title').content('content').sender('sender').sendDate(123).participants({test1: "1", test2: "2", test3: "3"}).asIs();

    const pseudos: string[] = message.participantsPseudos(["test2"]);

    expect(pseudos.length).toBe(2);
    expect(pseudos).toContain("test1");
    expect(pseudos).toContain("test3");
  });

  it("should return the list of recipients' pseudos, excluding the pseudos indicated", () => {

    const message: Message = aMessageWithRecipients(['test1', 'test2', 'test3']);

    const pseudos: string[] = message.recipientsPseudos(["test1"]);

    expect(pseudos.length).toBe(2);
    expect(pseudos).toContain("test2");
    expect(pseudos).toContain("test3");
  });

  it("should fall back on listing the participants if no recipients are present (legacy message", () => {

    // const message: Message = new Message('title', 'content', 'sender', 123, null, {test1: "1", test2: "2", test3: "3"});
    const message: Message = aMessage.with().title('title').content('content').sender('sender').sendDate(123).participants({test1: "1", test2: "2", test3: "3"}).asIs();

    const pseudos: string[] = message.recipientsPseudos(["test1"]);

    expect(pseudos.length).toBe(2);
    expect(pseudos).toContain("test2");
    expect(pseudos).toContain("test3");
  });

  function aMessageWithRecipients(recipients: string[]): Message {
    return aMessage.with().order(1).and.recipients(recipients).asIs();
    // return new Message(null, null, null, null, null, null, 1, null, null, recipients);
  }

  function aMessageWithConversationIdAndMessageId(conversationId: string, messageId: string) {
    return aMessage.with().order(1).conversationId(conversationId).and.messageId(messageId).asIs();
    // return new Message(null, null, null, null, null, null, 1, conversationId, messageId);
  }

  function aMessageWithOrderAndSendDate(order: number, sendDate: number): Message {
    return aMessage.with().sendDate(sendDate).order(order).asIs();
    // return new Message(null, null, null, sendDate, null, null, order);
  }

  function expectMessageToHavePropertiesOf(message: Message, templateObject: any) {
    expect(message.title()).toEqual(templateObject.title);
    expect(message.content()).toEqual(templateObject.content);
    expect(message.sender()).toEqual(templateObject.sender);
    expect(message.sendDate()).toEqual(templateObject.sendDate);
    expect(message['_participants']).toEqual(templateObject.participants);
    expect(message.conversationId()).toEqual(templateObject.conversationId);
    expect(message.messageId()).toEqual(templateObject.messageId);
    expect(message.recipients()).toEqual(templateObject.recipients);
  }

  function aContact(pseudo: string): Contact {
    return Contact.with(pseudo, null, null, null, null, null);
  }
});
