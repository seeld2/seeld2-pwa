import {Message} from "./message";

export class MessageTestBuilder {

  static readonly CONTENT: string = "title";
  static readonly CONVERSATION_ID: string = "conversationId";
  static readonly MESSAGE_ID: string = "messageId";
  static readonly ORDER: number = 0
  static readonly PARTICIPANTS: any = {};
  static readonly RECIPIENTS: string[] = [];
  static readonly SENDER: string = "sender";
  static readonly SEND_DATE: number = 1613744392727;
  static readonly SENT_TO_BOX_ADDRESS = "sentToBoxAddress";
  static readonly SENT_TO_HMAC_HASH = "sentToHmacHash";
  static readonly TITLE: string = "title";

  static with(): MessageTestBuilder {
    return new MessageTestBuilder();
  }

  and = this;

  private _content: string;
  private _conversationId: string;
  private _messageId: string;
  private _order: number;
  private _participants: any;
  private _recipients: string[];
  private _sender: string;
  private _sendDate: number;
  private _sentToBoxAddress: string;
  private _sentToHmacHash: string;
  private _title: string;

  asIs(): Message {
    return Message.with(
      this._title,
      this._content,
      this._sender,
      this._sendDate,
      this._sentToBoxAddress,
      this._sentToHmacHash,
      this._participants,
      this._order,
      this._conversationId,
      this._messageId,
      this._recipients);
  }

  noBlanks(): Message {
    return Message.with(
      this._title ? this._title : MessageTestBuilder.TITLE,
      this._content ? this._content : MessageTestBuilder.CONTENT,
      this._sender ? this._sender : MessageTestBuilder.SENDER,
      this._sendDate ? this._sendDate : MessageTestBuilder.SEND_DATE,
      this._sentToBoxAddress ? this._sentToBoxAddress : MessageTestBuilder.SENT_TO_BOX_ADDRESS,
      this._sentToHmacHash ? this._sentToHmacHash : MessageTestBuilder.SENT_TO_HMAC_HASH,
      this._participants ? this._participants : MessageTestBuilder.PARTICIPANTS,
      this._order ? this._order : MessageTestBuilder.ORDER,
      this._conversationId ? this._conversationId : MessageTestBuilder.CONVERSATION_ID,
      this._messageId ? this._messageId : MessageTestBuilder.MESSAGE_ID,
      this._recipients ? this._recipients : MessageTestBuilder.RECIPIENTS);
  }

  content(content: string): MessageTestBuilder {
    this._content = content;
    return this;
  }

  conversationId(conversationId: string): MessageTestBuilder {
    this._conversationId = conversationId;
    return this;
  }

  messageId(messageId: string): MessageTestBuilder {
    this._messageId = messageId;
    return this;
  }

  order(order: number): MessageTestBuilder {
    this._order = order;
    return this;
  }

  participants(participants: any): MessageTestBuilder {
    this._participants = participants;
    return this;
  }

  recipients(recipients: string[]): MessageTestBuilder {
    this._recipients = recipients;
    return this;
  }

  sender(sender: string): MessageTestBuilder {
    this._sender = sender;
    return this;
  }

  sendDate(sendDate: number): MessageTestBuilder {
    this._sendDate = sendDate;
    return this;
  }

  sentToBoxAddress(sentToBoxAddress: string): MessageTestBuilder {
    this._sentToBoxAddress = sentToBoxAddress;
    return this;
  }

  sentToHmacHash(sentToHmacHash: string): MessageTestBuilder {
    this._sentToHmacHash = sentToHmacHash;
    return this;
  }

  title(title: string): MessageTestBuilder {
    this._title = title;
    return this;
  }
}
