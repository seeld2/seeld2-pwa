/**
 * Represents a message's payload, who's meant to be ENCRYPTED before sent over the web.
 * For one functional message, there should be as many technical encrypted messages as there are recipients, since each
 * technical message is encrypted using the recipient's key, refers to its own conversation ID and bears its unique
 * message ID.
 * The structure defines the following properties:
 * - title
 * - content: the message's text
 * - sender: the pseudo of the person who sent the message
 * - sendDate: the exact moment this message was sent, in milliseconds since the Unix Epoch
 * - participants: a map of all participants of this message's conversation, whether this particular message is
 *     addressed to them or not. Each key is a person's pseudo, and the value is the conversation ID for that person.
 *     The map of participants includes the sender.
 * - conversationId: the (unique for this person) conversation ID that identifies this message's conversation for the
 *     user it is encrypted for
 * - order: the message's order, for easier sorting of the messages.
 * - messageId: the (unique for this person) message ID of this message
 * - recipients: an array of all the pseudos this particular message is addressed to. These can be all the pseudos
 *     listed in participants, or a subset of the participants (when a person from a group conversation excludes one
 *     or more persons from the "To:" field).
 *     The array of recipients includes the sender
 */
import {Contact} from "../../user/contact/contact";
import {Contacts} from "../../user/contact/contacts";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";
import {ConversationIds} from "../conversation-ids";

export class Message implements Serializable {

  static fromJson(json: string): Message {
    return Message.fromObject(JSON.parse(json));
  }

  static fromObject(object: any): Message {
    return new Message(
      object.content,
      object.conversationId,
      object.messageId,
      object.order,
      object.participants,
      object.recipients,
      object.sender,
      object.sendDate,
      object.sentToBoxAddress,
      object.sentToHmacHash,
      object.title
    );
  }

  static newMessage(title: string,
                    content: string,
                    sender: string,
                    sendDate: number,
                    tos: Contact[]): Message {
    const toPseudos = Contacts.fromArrayOfContacts(tos).pseudos();
    const participants: ConversationIds = new ConversationIds().addNew(sender).addAllNew(toPseudos);
    const recipients: string[] = [sender].concat(toPseudos);
    return new Message(
      content,
      undefined,
      undefined,
      1,
      participants.getMap(),
      recipients,
      sender,
      sendDate,
      undefined,
      undefined,
      title);
  }

  static with(title: string,
              content: string,
              sender: string,
              sendDate: number,
              sentToBoxAddress: string,
              sentToHmacHash: string,
              participants: any,
              order: number = 0,
              conversationId: string = undefined,
              messageId: string = undefined,
              recipients: string[] = undefined): Message {
    return new Message(
      content,
      conversationId,
      messageId,
      order,
      participants,
      recipients,
      sender,
      sendDate,
      sentToBoxAddress,
      sentToHmacHash,
      title);
  }

  private constructor(private readonly _content: string,
                      private _conversationId: string,
                      private _messageId: string,
                      private readonly _order: number,
                      private readonly _participants: any,
                      private readonly _recipients: string[],
                      private readonly _sender: string,
                      private readonly _sendDate: number,
                      private _sentToBoxAddress: string,
                      private _sentToHmacHash: string,
                      private readonly _title: string) {
  }

  asObject(): any {
    return {
      content: this._content,
      conversationId: this._conversationId,
      messageId: this._messageId,
      order: this._order,
      participants: this._participants,
      recipients: this._recipients,
      sender: this._sender,
      sendDate: this._sendDate,
      sentToBoxAddress: this._sentToBoxAddress,
      sentToHmacHash: this._sentToHmacHash,
      title: this._title
    };
  }

  /**
   * Checks the integrity of this message and throws an Error if the integrity is compromised.
   */
  checkIntegrity() {
    if (!this._conversationId) {
      throw new Error('MessageStore.checkIntegrity: missing conversation ID in message');
    }
    if (!this._messageId) {
      throw new Error('MessageStore.checkIntegrity: missing message ID in message');
    }
  }

  clone(): Message {
    return new Message(
      this._content,
      this._conversationId,
      this._messageId,
      this._order,
      this._participants,
      this._recipients,
      this._sender,
      this._sendDate,
      this._sentToBoxAddress,
      this._sentToHmacHash,
      this._title
    );
  }

  /**
   * Compares this message with another. The messages with the lowest order and/or the lowest send date have precedence
   * @param other {Message} The other message to compare with this
   * @returns {number} 0 if both messages are equal, >0 if this message has precedence and <0 if the other message has
   * precedence
   */
  compareTo(other: Message): number {
    let thisWeight: number = this._order < other._order ? 10 : 0;
    let otherWeight: number = other._order < this._order ? 10 : 0;
    thisWeight += this._sendDate < other._sendDate ? 1 : 0;
    otherWeight += other._sendDate < this._sendDate ? 1 : 0;
    return thisWeight - otherWeight;
  }

  content(): string {
    return this._content
  }

  conversationId(): string {
    return this._conversationId;
  }

  equals(other: Message): boolean {
    return this._title === other._title
      && this._content === other._content
      && this._sender === other._sender
      && this._sendDate === other._sendDate
      && this._sentToBoxAddress === other._sentToBoxAddress
      && this._sentToHmacHash === other._sentToHmacHash
      && this._conversationId === other._conversationId
      && this._messageId === other._messageId;
  }

  /**
   * Returns true if this message has been sent and stored remotely.
   */
  hasBeenSent(): boolean {
    return !!this._messageId;
  }

  /**
   * Returns true if no title has been set for this instance.
   */
  hasEmptyTitle(): boolean {
    return !this._title ? true : this._title.trim().length === 0;
  }

  messageId(): string {
    return this._messageId;
  }

  order(): number {
    return this._order;
  }

  participantConversationId(pseudo: string) {
    return this._participants[pseudo];
  }

  participants(): any {
    return this._participants;
  }

  participantsAmount(): number {
    return Object.keys(this._participants).length;
  }

  /**
   * Lists the participants of this message (sender and recipients).
   *
   * @param excluding An optional array of pseudos to be excluded from the returned list of participants
   */
  participantsPseudos(excluding: string[] = []): string[] {
    return Object.keys(this._participants).filter(pseudo => excluding.indexOf(pseudo) === -1);
  }

  recipients(): string[] {
    return this._recipients;
  }

  /**
   * Lists the recipients' pseudos of this message.
   *
   * The method falls back on listing the participants, if no recipients are defined in the message. This ensures
   * legacy compatibility with messages exchanged with Seeld 0.4.1 and before.
   *
   * @param excluding An optional array of pseudos to be excluded from the returned list of recipients
   */
  recipientsPseudos(excluding: string[] = []): string[] {
    return this._recipients
      ? this._recipients.filter(username => excluding.indexOf(username) === -1)
      : Object.keys(this._participants).filter(pseudo => excluding.indexOf(pseudo) === -1);
  }

  sendDate(): number {
    return this._sendDate ? this._sendDate : undefined;
  }

  sender(): string {
    return this._sender;
  }

  sentToBoxAddress(): string {
    return this._sentToBoxAddress;
  }

  sentToHmacHash(): string {
    return this._sentToHmacHash;
  }

  setConversationId(conversationId: string) {
    this._conversationId = conversationId;
  }

  setMessageId(messageId: string) {
    this._messageId = messageId;
  }

  setSentToBoxAddress(sentToBoxAddress: string) {
    this._sentToBoxAddress = sentToBoxAddress;
  }

  setSentToHmacHash(hmacHash: string) {
    this._sentToHmacHash = hmacHash;
  }

  title(): string {
    return this._title;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
