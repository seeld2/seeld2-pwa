import {MessageTestBuilder as aMessage} from "./message.test-builder";
import {MessagesCache} from "./messages-cache";
import {MessagesStorageService} from "./messages-storage.service";
import {SessionService} from "../../sessions/session.service";
import {Message} from "./message";
import {MessagesMap} from "./messages-map";
import {CoreJasmine} from "../../../core/core-jasmine";
import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {Session} from "../../sessions/session";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {ConversationTestBuilder as aConversation} from "../conversation.test-builder";
import {Conversation} from "../conversation";

describe("MessagesCache", () => {

  const LOGGED_PSEUDO: string = "loggedPseudo";

  const CONVERSATION_ID: string = "CONVERSATION ID";

  // const MESSAGE_1: Message = new Message("title", "content", "sender", 1, null, null, 1, CONVERSATION_ID, "M1");
  const MESSAGE_1: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(1).order(1).conversationId(CONVERSATION_ID).and.messageId("M1").asIs();
  // const MESSAGE_2: Message = new Message("title", "content", "sender", 2, null, {"loggedPseudo": CONVERSATION_ID}, 1, CONVERSATION_ID, "M2");
  const MESSAGE_2: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(2).participants({"loggedPseudo": CONVERSATION_ID}).order(1).conversationId(CONVERSATION_ID).and.messageId("M2").asIs();
  // const MESSAGE_3: Message = new Message("title", "content", "sender", 3, null, null, 1, CONVERSATION_ID, "M3");
  const MESSAGE_3: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(3).order(1).conversationId(CONVERSATION_ID).and.messageId("M3").asIs();

  const MESSAGE_ID_1: string = "messageId1";
  const MESSAGE_ID_2: string = "messageId2";

  const PAGE_1: number = 0;
  const PAGE_SIZE: number = 2;

  let messagesStorage: MessagesStorageService;
  let sessionService: SessionService;

  let messagesCache: MessagesCache;

  let session: Session;

  beforeEach(() => {
    messagesStorage = CoreJasmine.messagesStorage();
    sessionService = SessionsJasmine.sessionService();

    messagesCache = new MessagesCache(messagesStorage, sessionService);

    session = SessionsJasmine.session();
    (<jasmine.Spy>session.pseudo).and.returnValue(LOGGED_PSEUDO);
    (<jasmine.Spy>sessionService.session).and.returnValue(session);

    messagesCache['messagesMaps'][CONVERSATION_ID] = new MessagesMap();
    messagesCache['messagesMaps'][CONVERSATION_ID].putMessage(MESSAGE_1);
    messagesCache['messagesMaps'][CONVERSATION_ID].putMessage(MESSAGE_2);
    messagesCache['messagesMaps'][CONVERSATION_ID].putMessage(MESSAGE_3);
  });

  it("should clear the cached messages for a given conversation", () => {
    expect(messagesCache['messagesMaps'][CONVERSATION_ID]).toBeDefined()
    messagesCache.clearCache(CONVERSATION_ID);
    expect(messagesCache['messagesMaps'][CONVERSATION_ID]).not.toBeDefined();
  });

  it("should delete a message from the cache", (done: DoneFn) => {

    const messageToDelete: Message = aMessage.with().conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_1).noBlanks();
    const someOtherMessage: Message = aMessage.with().conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_2).noBlanks();

    const messagesMap: MessagesMap = new MessagesMap();
    messagesMap.putMessage(messageToDelete);
    messagesMap.putMessage(someOtherMessage);
    messagesCache['messagesMaps'][CONVERSATION_ID] = messagesMap;

    (<jasmine.Spy>messagesStorage.deleteMessage).and.returnValue(of(messageToDelete));

    messagesCache.deleteMessage(messageToDelete).subscribe(() => {
      expect(messagesStorage.deleteMessage).toHaveBeenCalledWith(messageToDelete);
      expect(messagesCache['messagesMaps'][CONVERSATION_ID].getAllMessages().length).toEqual(1);
      done();
    });
  });

  describe(", when listing messages,", () => {

    it("should return cached messages if no sync was made and a full page of messages is available in the cache",
      (done: DoneFn) => {

        (<jasmine.Spy>messagesStorage.syncMessages).and.returnValue(Observable.empty());

        const results: Message[] = [];
        const conversation: Conversation = aConversation.with().conversationId(CONVERSATION_ID).noBlanks();
        messagesCache.listMessages(conversation, PAGE_1, PAGE_SIZE)
          .subscribe((message: Message) => {
            results.push(message);
            if (results.length === 2) {

              expect(results[0]).toEqual(MESSAGE_3);
              expect(results[1]).toEqual(MESSAGE_2);

              done();
            }
          })
      });

    it("should return stored messages if a sync occurred", (done: DoneFn) => {
      messagesCache['messagesMaps'][CONVERSATION_ID] = new MessagesMap();
      messagesCache['messagesMaps'][CONVERSATION_ID].putMessage(MESSAGE_1);

      (<jasmine.Spy>messagesStorage.syncMessages).and.returnValue(from([MESSAGE_2, MESSAGE_3]));
      (<jasmine.Spy>messagesStorage.listMessagesFromStorage).and.returnValue(from([MESSAGE_3, MESSAGE_2]));

      const results: Message[] = [];
      const conversation: Conversation = aConversation.with().conversationId(CONVERSATION_ID).noBlanks();
      messagesCache.listMessages(conversation, PAGE_1, PAGE_SIZE)
        .subscribe((message: Message) => {
          results.push(message);
          if (results.length === 2) {

            expect(messagesCache['messagesMaps'][CONVERSATION_ID].getAllMessages().length).toEqual(3);

            expect(results[0]).toEqual(MESSAGE_3);
            expect(results[1]).toEqual(MESSAGE_2);

            done();
          }
        })
    });

    it("should return stored messages if the cached page is smaller than a full page", (done: DoneFn) => {
      messagesCache['messagesMaps'][CONVERSATION_ID] = new MessagesMap();
      messagesCache['messagesMaps'][CONVERSATION_ID].putMessage(MESSAGE_1);

      (<jasmine.Spy>messagesStorage.syncMessages).and.returnValue(Observable.empty());
      (<jasmine.Spy>messagesStorage.listMessagesFromStorage).and.returnValue(from([MESSAGE_3, MESSAGE_2]));

      const results: Message[] = [];
      const conversation: Conversation = aConversation.with().conversationId(CONVERSATION_ID).noBlanks();
      messagesCache.listMessages(conversation, PAGE_1, PAGE_SIZE)
        .subscribe((message: Message) => {
          results.push(message);
          if (results.length === 2) {

            expect(messagesCache['messagesMaps'][CONVERSATION_ID].getAllMessages().length).toEqual(3);

            expect(results[0]).toEqual(MESSAGE_3);
            expect(results[1]).toEqual(MESSAGE_2);

            done();
          }
        })
    });
  });

  it("should list unread messages", (done: DoneFn) => {

    (<jasmine.Spy>messagesStorage.listUnreadMessages).and.returnValue(of(MESSAGE_3));

    messagesCache.listUnreadMessages()
      .subscribe((message: Message) => {

        expect(message).toEqual(MESSAGE_3);
        expect(messagesCache['messagesMaps'][CONVERSATION_ID]).not.toBeDefined();

        done();
      });
  });

  it("should store a message", (done: DoneFn) => {

    (<jasmine.Spy>messagesStorage.storeMessage).and.returnValue(of(MESSAGE_2));

    messagesCache.storeMessage(MESSAGE_2)
      .subscribe((message: Message) => {

        expect(message).toEqual(MESSAGE_2);

        done();
      });
  });
});
