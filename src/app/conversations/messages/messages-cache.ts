import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";
import {map, mergeMap, toArray} from "rxjs/operators";
import {SessionService} from "../../sessions/session.service";
import {Logger} from "../../utils/logging/logger";
import {Conversation} from "../conversation";
import {Message} from "./message";
import {MessagesMap} from "./messages-map";
import {MessagesStorageService} from "./messages-storage.service";

@Injectable()
export class MessagesCache {

  private readonly logger: Logger = Logger.for("MessagesCache");

  private messagesMaps: any = {}; // A map of MessagesMap instances, mapped by the conversation ID they belong to.

  constructor(private messagesStorage: MessagesStorageService,
              private sessionService: SessionService) {
  }

  clearCache(conversationId: string) {
    delete this.messagesMaps[conversationId];
  }

  deleteMessage(messageToDelete: Message): Observable<Message> {
    this.logger.trace(`deleteMessage(${JSON.stringify(messageToDelete)})`);
    return this.messagesStorage.deleteMessage(messageToDelete)
      .pipe(
        map((deletedMessage: Message) => {
          const messagesMapForConversation: MessagesMap = this.getOrInitMessagesMapFor(deletedMessage.conversationId());
          messagesMapForConversation.deleteMessage(deletedMessage);
          return deletedMessage;
        })
      );
  }

  getMessagesFromCache(conversationId: string, page: number, pageSize: number): Message[] {
    const start: number = page * pageSize;
    const end: number = start + pageSize;
    return this.getOrInitMessagesMapFor(conversationId)
      .getAllMessages()
      .sort((a: Message, b: Message) => a.compareTo(b))
      .slice(start, end);
  }

  listMessages(conversation: Conversation, page: number, pageSize: number): Observable<Message> {
    const conversationId: string = conversation.conversationId();
    this.logger.trace(`listMessages(${conversationId}, ${page}, ${pageSize})`);

    return this.syncMessages(conversation, page, pageSize)
      .pipe(
        mergeMap((hasSynced: boolean) => {

          const cachedMessages: Message[] = this.getMessagesFromCache(conversationId, page, pageSize);

          return hasSynced || cachedMessages.length < pageSize
            ? this.listMessagesFromStorage(conversationId, page, pageSize)
            : from(cachedMessages);
        })
      );
  }

  private syncMessages(conversation: Conversation, page: number, pageSize: number): Observable<boolean> {
    this.logger.trace(`syncMessages(${conversation.conversationId}, ${page}, ${pageSize})`);

    return this.messagesStorage.syncMessages(conversation, page, pageSize)
      .pipe(
        map((message: Message) => {
          const messagesMap: MessagesMap = this.getOrInitMessagesMapFor(conversation.conversationId);
          return messagesMap.putMessage(message);
        }),

        toArray(), // This makes the Observable complete only when all messages have been sync'ed

        map((syncedMessages: Message[]) => {
          return syncedMessages.length > 0;
        })
      );
  }

  private listMessagesFromStorage(conversationId: string, page: number, pageSize: number):
    Observable<Message> {
    this.logger.trace(`listMessagesFromStorage(${conversationId}, ${page}, ${pageSize})`);

    return this.messagesStorage.listMessagesFromStorage(conversationId, page, pageSize)
      .pipe(
        map((message: Message) => {
          const messagesMap: MessagesMap = this.getOrInitMessagesMapFor(conversationId);
          return messagesMap.putMessage(message);
        })
      );
  }

  listUnreadMessages(): Observable<Message> {
    this.logger.trace('listUnreadMessages()');

    return this.messagesStorage.listUnreadMessages()
      .pipe(
        map((message: Message) => {
          // Invalidate cache for that message's conversation ID, otherwise the cache will be out of sync
          delete this.messagesMaps[message.conversationId()];
          return message;
        })
      );
  }

  storeMessage(message: Message): Observable<Message> {

    this.logger.trace(`storeMessage(${JSON.stringify(message)})`);

    return this.messagesStorage.storeMessage(message)
      .pipe(
        map((message: Message) => {
          message.checkIntegrity();

          const conversationId: string = message.participantConversationId(this.sessionService.session().pseudo());
          this.getOrInitMessagesMapFor(conversationId).putMessage(message);

          return message;
        })
      );
  }

  // PRIVATE

  private getOrInitMessagesMapFor(conversationId): MessagesMap {
    if (!this.messagesMaps[conversationId]) {
      this.messagesMaps[conversationId] = MessagesMap.empty();
    }
    return this.messagesMaps[conversationId];
  }
}
