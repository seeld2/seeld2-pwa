import {MessageIn} from "./message-in";

export class MessagesInResponse {

  constructor(public messages: MessageIn[]) {
  }
}
