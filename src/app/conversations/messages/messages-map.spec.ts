import {Message} from "./message";
import {MessageTestBuilder as aMessage} from "./message.test-builder";
import {MessagesMap} from "./messages-map";

describe("MessageMap", () => {

  const MESSAGE_ID_1: string = "messageId1";
  const MESSAGE_ID_2: string = "messageId2";

  it("should delete a message from the MessagesMap", () => {

    const messagesMap: MessagesMap = MessagesMap.empty();
    messagesMap.putMessage(aMessage.with().messageId(MESSAGE_ID_1).noBlanks());
    messagesMap.putMessage(aMessage.with().messageId(MESSAGE_ID_2).noBlanks());

    messagesMap.deleteMessage(aMessage.with().messageId(MESSAGE_ID_1).noBlanks());

    expect(Object.keys(messagesMap["map"]).length).toEqual(1);
    expect(messagesMap["map"][MESSAGE_ID_2]).toBeDefined();
  });

  it("should return all messages in the map", () => {

    const messagesMap: MessagesMap = MessagesMap.empty();
    messagesMap.putMessage(aMessage.with().messageId(MESSAGE_ID_1).noBlanks());
    messagesMap.putMessage(aMessage.with().messageId(MESSAGE_ID_2).noBlanks());

    const messages: Message[] = messagesMap.getAllMessages();

    expect(messages.length).toEqual(2);
    expect(messages[0].messageId()).toEqual(MESSAGE_ID_1);
    expect(messages[1].messageId()).toEqual(MESSAGE_ID_2);
  });
});
