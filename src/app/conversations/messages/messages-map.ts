import {Message} from "./message";

/**
 * A Map specifically for messages, which keeps a dictionary of messages by their ID.
 */
export class MessagesMap {

  private readonly map: any = {};

  static empty(): MessagesMap {
    return new MessagesMap();
  }

  deleteMessage(message: Message) {
    delete this.map[message.messageId()];
  }

  getAllMessages(): Message[] {
    return Object.keys(this.map).map(key => (<Message>this.map[key]).clone());
  }

  putMessage(message: Message): Message {
    this.map[message.messageId()] = message;
    return message.clone();
  }
}
