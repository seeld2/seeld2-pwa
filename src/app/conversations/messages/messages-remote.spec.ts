import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {AngularJasmine} from "../../../shared/angular-jasmine";
import {config} from "../../app.config";
import {AES} from "../../crypto/aes/aes";
import {CryptoJasmine} from "../../crypto/crypto.jasmine";
import {OpenPgp} from "../../crypto/openpgp/open-pgp";
import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";
import {HmacHashesByBoxAddressTestBuilder as aHmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address.test-builder";
import {Session} from "../../sessions/session";
import {SessionService} from "../../sessions/session.service";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {Contact} from "../../user/contact/contact";
import {ContactKey} from "../../user/contact/contact-key";
import {ContactTestBuilder as aContact} from "../../user/contact/contact.test-builder";
import {Keys} from "../../user/key/keys";
import {Conversation} from "../conversation";
import {Message} from "./message";
import {MessageIn} from "./message-in";
import {MessageTestBuilder as aMessage} from "./message.test-builder";
import {MessagesInResponse} from "./messages-in-response";
import {
  ConversationMessagesIdsRequest,
  ConversationMessagesIdsResponse,
  ConversationMessagesRequest,
  MessagesRemote
} from "./messages-remote";

describe("MessagesRemote", () => {

  const CONVERSATION_ID: string = "CID1";
  const HMAC_HASH: string = "hmac hash";
  const MESSAGE_ID_1: string = "MID1";
  const MESSAGE_ID_2: string = "MID2";
  const PAGE: number = 1;
  const PAGE_SIZE: number = 10;
  const PAYLOAD: string = "payload";
  const SYSTEM_PUBLIC_KEYS: string = "systemPublicKeys";

  const ALICE_BOX_ADDRESS: string = "ALICE_BOX_ADDRESS";
  const ALICE_CONVERSATION_ID: string = "ALICE_CONVERSATION_ID";
  const ALICE_HMAC_HASH: string = "aliceHmacHash";
  const ALICE_PSEUDO: string = "ALICE_PSEUDO";
  const ALICE_WRITE_WITH: ContactKey = ContactKey.with(Keys.systemKeysId(), Keys.SYSTEM_KEYS_TYPE, "alicePublicKeys");

  const ALICE_BOB_SECRET_KEY: string = "aliceBobSecretKey";

  const BOB_BOX_ADDRESS: string = "BOB_BOX_ADDRESS";
  const BOB_CONVERSATION_ID: string = "BOB_CONVERSATION_ID";
  const BOB_PSEUDO: string = "BOB_PSEUDO";
  const BOB_WRITE_WITH: ContactKey = ContactKey.with(Keys.systemKeysId(), Keys.SYSTEM_KEYS_TYPE, "bobPublicKeys");

  const CAROL_BOX_ADDRESS: string = "carolBoxAddress";

  const MESSAGE_PGP: string = "messagePgp";
  const REQUEST_PAYLOAD_AES: Uint8Array = new Uint8Array([1, 3, 5, 7, 9]);
  const REQUEST_RESPONSE_AES: Uint8Array = new Uint8Array([0, 2, 4, 6, 8]);

  let httpClient: HttpClient;
  let sessionService: SessionService;

  let messagesRemote: MessagesRemote;

  let exchangeAes: AES;
  let openPgp: OpenPgp;
  let session: Session;

  beforeEach(() => {
    httpClient = AngularJasmine.httpClient();
    sessionService = SessionsJasmine.sessionService();

    messagesRemote = new MessagesRemote(httpClient, sessionService);

    exchangeAes = CryptoJasmine.exchangeAes();
    messagesRemote['exchangeAes'] = exchangeAes;

    session = SessionsJasmine.session();
    (<jasmine.Spy>session.contactsAsArray).and.returnValue([]);
    (<jasmine.Spy>session.systemPublicKeys).and.returnValue(SYSTEM_PUBLIC_KEYS);
    (<jasmine.Spy>sessionService.session).and.returnValue(session);

    openPgp = CryptoJasmine.openPgp();
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);
  });

  it("should fetch all messages, for a given conversation, matching the specified IDs", (done: DoneFn) => {

    const conversation: Conversation = Conversation.with(
      CONVERSATION_ID,
      aMessage.with().participants(participants()).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_2).asIs());
      // new Message(null, null, null, null, null, participants(), null, CONVERSATION_ID, MESSAGE_ID_2, null));

    const hmacHashesByBoxAddress: HmacHashesByBoxAddress = aHmacHashesByBoxAddress.with()
      .entry(ALICE_BOX_ADDRESS, HMAC_HASH).and
      .entry(BOB_BOX_ADDRESS, HMAC_HASH).asIs();
    (<jasmine.Spy>sessionService.hmacHashesByReadFromBoxAddresses).and.returnValue(of(hmacHashesByBoxAddress));
    (<jasmine.Spy>sessionService.hmacHashesByDeletedReadFromBoxAddresses).and.returnValue(of(HmacHashesByBoxAddress.empty()));
    (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of(HMAC_HASH));
    (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(CAROL_BOX_ADDRESS);

    (<jasmine.Spy>httpClient.put).and.returnValue(of(new MessagesInResponse([
      {
        conversationId: CONVERSATION_ID,
        messageId: MESSAGE_ID_1,
        payload: PAYLOAD,
        readFromBoxAddress: ALICE_BOX_ADDRESS
      },
      {
        conversationId: CONVERSATION_ID,
        messageId: MESSAGE_ID_2,
        payload: PAYLOAD,
        readFromBoxAddress: ALICE_BOX_ADDRESS
      }
    ])));

    (<jasmine.Spy>sessionService.contactWithReadFromBoxAddress).and.returnValue(undefined); // Because Alice is the logged user
    (<jasmine.Spy>openPgp.decrypt).and.returnValues(
      of(aMessage.with().sender(ALICE_PSEUDO).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_1).noBlanks().toJson()),
      of(aMessage.with().sender(ALICE_PSEUDO).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_2).noBlanks().toJson())
    );

    const messages: Message[] = [];
    messagesRemote.fetchMessagesByIds(conversation, [MESSAGE_ID_1, MESSAGE_ID_2])
      .subscribe((message: Message) => {
        messages.push(message);
        if (messages.length === 2) {

          expect(sessionService.hmacHashesByReadFromBoxAddresses).toHaveBeenCalledWith([ALICE_PSEUDO, BOB_PSEUDO]);
          const request: ConversationMessagesRequest = ConversationMessagesRequest
            .forMessagesIds(hmacHashesByBoxAddress, [MESSAGE_ID_1, MESSAGE_ID_2]);
          expect(httpClient.put)
            .toHaveBeenCalledWith(`${config.api.conversations}/${CONVERSATION_ID}/messages`, request.toJson());

          expect(messages[0].messageId()).toEqual(MESSAGE_ID_1);
          expect(messages[1].messageId()).toEqual(MESSAGE_ID_2);

          done();
        }
      });
  });

  describe(", when fetching missing messages,", () => {

    it("should return messages detected as missing", (done: DoneFn) => {

      const message1: Message = aMessage.with().participants(participants()).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_1).asIs();
      // const message1: Message = new Message(null, null, null, null, null, participants(), null, CONVERSATION_ID, MESSAGE_ID_1, null);
      const message2: Message = aMessage.with().participants(participants()).conversationId(CONVERSATION_ID).messageId(MESSAGE_ID_2).asIs();
      // const message2: Message = new Message(null, null, null, null, null, participants(), null, CONVERSATION_ID, MESSAGE_ID_2, null);
      const conversation: Conversation = Conversation.with(CONVERSATION_ID, message2);

      const hmacHashesByBoxAddress: HmacHashesByBoxAddress = aHmacHashesByBoxAddress.with()
        .entry(ALICE_BOX_ADDRESS, HMAC_HASH).and
        .entry(BOB_BOX_ADDRESS, HMAC_HASH).asIs();
      (<jasmine.Spy>sessionService.hmacHashesByReadFromBoxAddresses).and.returnValue(of(hmacHashesByBoxAddress));
      (<jasmine.Spy>sessionService.hmacHashesByDeletedReadFromBoxAddresses).and.returnValue(of(HmacHashesByBoxAddress.empty()));
      (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of(HMAC_HASH));
      (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(CAROL_BOX_ADDRESS);

      (<jasmine.Spy>httpClient.put).and.returnValues(
        of(<ConversationMessagesIdsResponse>{messagesIds: [MESSAGE_ID_1, MESSAGE_ID_2]}),
        of(new MessagesInResponse([
            {
              conversationId: CONVERSATION_ID,
              messageId: MESSAGE_ID_1,
              payload: PAYLOAD,
              readFromBoxAddress: ALICE_BOX_ADDRESS
            }
          ]))
      );

      (<jasmine.Spy>sessionService.contactWithReadFromBoxAddress).and.returnValue(undefined); // Because Alice is the logged user
      (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(message1.toJson()));

      messagesRemote.fetchMissingMessages(conversation, [MESSAGE_ID_2], PAGE, PAGE_SIZE)
        .subscribe((message: Message) => {

          expect(sessionService.hmacHashesByReadFromBoxAddresses).toHaveBeenCalledWith([ALICE_PSEUDO, BOB_PSEUDO]);
          const conversationMessagesIdsRequest: ConversationMessagesIdsRequest = ConversationMessagesIdsRequest
            .for(hmacHashesByBoxAddress, PAGE, PAGE_SIZE);
          expect((<jasmine.Spy>httpClient.put).calls.allArgs()[0]).toEqual([
            `${config.api.conversations}/${CONVERSATION_ID}/messages/ids`,
            conversationMessagesIdsRequest.toJson()
          ]);

          const conversationMessagesRequest: ConversationMessagesRequest = ConversationMessagesRequest
            .forMessagesIds(hmacHashesByBoxAddress, [MESSAGE_ID_1]);
          expect((<jasmine.Spy>httpClient.put).calls.allArgs()[1])
            .toEqual([`${config.api.conversations}/${CONVERSATION_ID}/messages`, conversationMessagesRequest.toJson()]);

          expect(openPgp.decrypt).toHaveBeenCalledWith(PAYLOAD, SYSTEM_PUBLIC_KEYS);

          expect(message).toEqual(message1);

          done();
        });
    });

    it("should return an empty observable if no messages are missing", (done: DoneFn) => {

      // const message2: Message = new Message(null, null, null, null, null, participants(), null, CONVERSATION_ID, MESSAGE_ID_2, null);
      const message2: Message = aMessage.with().participants(participants()).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_2).asIs();
      const conversation: Conversation = Conversation.with(CONVERSATION_ID, message2);

      const hmacHashesByBoxAddress: HmacHashesByBoxAddress = aHmacHashesByBoxAddress.with()
        .entry(ALICE_BOX_ADDRESS, HMAC_HASH).and
        .entry(BOB_BOX_ADDRESS, HMAC_HASH).asIs();
      (<jasmine.Spy>sessionService.hmacHashesByReadFromBoxAddresses).and.returnValue(of(hmacHashesByBoxAddress));
      (<jasmine.Spy>sessionService.hmacHashesByDeletedReadFromBoxAddresses).and.returnValue(of(HmacHashesByBoxAddress.empty()));
      (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of(HMAC_HASH));
      (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(CAROL_BOX_ADDRESS);

      (<jasmine.Spy>httpClient.put).and.returnValue(of(<ConversationMessagesIdsResponse>{messagesIds: [MESSAGE_ID_1, MESSAGE_ID_2]}));

      messagesRemote.fetchMissingMessages(conversation, [MESSAGE_ID_1, MESSAGE_ID_2], PAGE, PAGE_SIZE)
        .subscribe(
          () => fail("Unexpected return of a message"),
          null,
          () => done()
        );
    });
  });

  describe(", when fetching new messages,", () => {

    it("should attempt to fetch new messages when the logged user has contacts", (done: DoneFn) => {

      (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
        .returnValue(of(aHmacHashesByBoxAddress.with().entry(ALICE_BOX_ADDRESS, HMAC_HASH).asIs()));

      const messagesResponse: MessagesInResponse = new MessagesInResponse([
        aMessagesResponseMessage(CONVERSATION_ID, MESSAGE_ID_1, PAYLOAD, ALICE_BOX_ADDRESS)
      ]);
      (<jasmine.Spy>httpClient.put).and.returnValue(of(messagesResponse));

      (<jasmine.Spy>sessionService.contactWithReadFromBoxAddress).and
        .returnValue(aContact.with().pseudo(ALICE_PSEUDO).readFrom(ALICE_BOX_ADDRESS).and.writeWith(ALICE_WRITE_WITH).noBlanks());

      const message: Message = aMessage.with().sender(ALICE_BOX_ADDRESS).conversationId(CONVERSATION_ID).and.messageId(MESSAGE_ID_1).noBlanks();
      (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(message.toJson()));

      messagesRemote.fetchUnreadMessages().subscribe((returnedMessage: Message) => {

        expect(sessionService.contactWithReadFromBoxAddress).toHaveBeenCalledWith(ALICE_BOX_ADDRESS);

        expect(httpClient.put).toHaveBeenCalledWith(
          `${config.api.messages}/new`, `{"hmacHashesByBoxAddress":{"${ALICE_BOX_ADDRESS}":"${HMAC_HASH}"}}`);

        expect(returnedMessage).toEqual(message);

        done();
      });
    });

    it("should avoid fetching anything if the logged user has no contacts", (done: DoneFn) => {

      (<jasmine.Spy>session.hmacHashesByReadFromBoxAddresses).and
        .returnValue(of(aHmacHashesByBoxAddress.withoutEntries()));

      messagesRemote.fetchUnreadMessages().subscribe(() => {
        done.fail("Unexpected conversation emitted");
      });

      setTimeout(() => {
        expect(session.hmacHashesByReadFromBoxAddresses).toHaveBeenCalled();
        expect(httpClient.put).not.toHaveBeenCalled();
        done();
      }, 100)
    });
  });

  it("should store a message", (done: DoneFn) => {
    (<jasmine.Spy>sessionService.exchangeAes).and.returnValue(of(exchangeAes));
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);
    (<jasmine.Spy>sessionService.pseudo).and.returnValue(ALICE_PSEUDO);

    const contactBob: Contact = aContact.with().pseudo(BOB_PSEUDO).readFrom(BOB_BOX_ADDRESS)
      .secretKey(ALICE_BOB_SECRET_KEY).and.writeWith(BOB_WRITE_WITH).noBlanks();
    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([contactBob]);

    (<jasmine.Spy>openPgp.encrypt).and.returnValue(of(MESSAGE_PGP));

    (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of(ALICE_HMAC_HASH));
    (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);

    (<jasmine.Spy>exchangeAes.encrypt).and.returnValue(of(REQUEST_PAYLOAD_AES));
    (<jasmine.Spy>httpClient.post).and.returnValue(of({payload: REQUEST_RESPONSE_AES}));
    (<jasmine.Spy>exchangeAes.decrypt).and.returnValue(of(MESSAGE_ID_1));

    const participants: any = {};
    participants[ALICE_PSEUDO] = ALICE_CONVERSATION_ID;
    participants[BOB_PSEUDO] = BOB_CONVERSATION_ID;
    const message: Message = aMessage.with().participants(participants).recipients([ALICE_PSEUDO, BOB_PSEUDO]).and.sender(ALICE_PSEUDO).noBlanks();
    messagesRemote.storeMessage(message).subscribe((storedMessage: Message) => {

      expect(sessionService.contactsAsArray).toHaveBeenCalledWith([BOB_PSEUDO]);

      expect(httpClient.post).toHaveBeenCalledWith(`${config.api.messages}`, {payload: Array.from(REQUEST_PAYLOAD_AES)});

      expect(storedMessage.conversationId()).toBe(ALICE_CONVERSATION_ID);
      expect(storedMessage.messageId()).toBe(MESSAGE_ID_1);

      done()
    });
  });

  function aMessagesResponseMessage(conversationId: string, messageId: string,
                                    payload: string, readFromBoxAddress: string) {
    return {
      conversationId: conversationId,
      messageId: messageId,
      payload: payload,
      readFromBoxAddress: readFromBoxAddress
    } as MessageIn;
  }

  function participants(): any {
    const participants: any = {};
    participants[ALICE_PSEUDO] = ALICE_CONVERSATION_ID;
    participants[BOB_PSEUDO] = BOB_CONVERSATION_ID;
    return participants;
  }
});
