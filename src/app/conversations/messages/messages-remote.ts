import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../../app.config";
import {OpenPgp} from "../../crypto/openpgp/open-pgp";
import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";
import {SessionService} from "../../sessions/session.service";
import {Contact} from "../../user/contact/contact";
import {Logger} from "../../utils/logging/logger";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";
import {AesRequest} from "../../crypto/aes/aes-request";
import {AesResponse} from "../../crypto/aes/aes-response";
import {Conversation} from "../conversation";
import {Message} from "./message";
import {MessageIn} from "./message-in";
import {MessagesInResponse} from "./messages-in-response";

@Injectable()
export class MessagesRemote {

  private readonly logger: Logger = Logger.for('MessagesRemote');

  constructor(private httpClient: HttpClient,
              private sessionService: SessionService) {
  }

  fetchUnreadMessages(): Observable<Message> {
    this.logger.trace('fetchUnreadMessages()');

    return this.sessionService.session().hmacHashesByReadFromBoxAddresses()
      .pipe(
        mergeMap((hmacHashesByReadFromBoxAddresses: HmacHashesByBoxAddress) => {
          if (hmacHashesByReadFromBoxAddresses.isEmpty()) {
            return Observable.empty();
          }
          return this.httpClient
            .put(`${config.api.messages}/new`, NewMessagesRequest.for(hmacHashesByReadFromBoxAddresses).toJson())
            .pipe(
              mergeMap((messagesResponse: MessagesInResponse) => from(messagesResponse.messages)), // We process the messages one by one... hence the "from" function!
              mergeMap((messageIn: MessageIn) => this.buildMessageFromResponse(messageIn))
            );
        })
      );
  }

  // TODO Test
  fetchPage(conversation: Conversation, page, pageSize): Observable<Message> {
    this.logger.trace(`fetchPage(${conversation.conversationId()}, ${page}, ${pageSize})`);

    const participantPseudos: string[] = conversation.participantsPseudos();
    return forkJoin(
      this.sessionService.session().hmacHashesByReadFromBoxAddresses(participantPseudos),
      this.sessionService.session().hmacHashesByDeletedReadFromBoxAddresses(participantPseudos),
      this.sessionService.session().hmacHashOfSystemBoxAddress())
      .pipe(
        mergeMap(([hmacHashesByBoxAddress, hmacHashesByDeletedBoxAddresses, hmacHashOfSystemBoxAddress]) => {
          hmacHashesByBoxAddress.set(this.sessionService.session().systemBoxAddress(), hmacHashOfSystemBoxAddress);
          hmacHashesByBoxAddress = hmacHashesByBoxAddress.mergeWith(hmacHashesByDeletedBoxAddresses);

          const request = ConversationMessagesRequest.forPage(hmacHashesByBoxAddress, page, pageSize);
          return this.httpClient
            .put(`${config.api.conversations}/${conversation.conversationId()}/messages`, request.toJson())
        }),
        // This is to process the response's messages one by one
        mergeMap((messagesInResponse: MessagesInResponse) => from(messagesInResponse.messages)),
        mergeMap((messageIn: MessageIn) => this.buildMessageFromResponse(messageIn)),
      );
  }

  /* */

  fetchMessagesByIds(conversation: Conversation, messagesIdsToFetch: string[]): Observable<Message> {
    this.logger.trace(`fetchMessagesByIds(${conversation.conversationId()}, ${messagesIdsToFetch})`);

    const participantPseudos: string[] = conversation.participantsPseudos();
    return forkJoin(
      this.sessionService.hmacHashesByReadFromBoxAddresses(participantPseudos),
      this.sessionService.hmacHashesByDeletedReadFromBoxAddresses(participantPseudos),
      this.sessionService.hmacHashOfSystemBoxAddress())
      .pipe(
        mergeMap(([hmacHashesByBoxAddress, hmacHashesByDeletedBoxAddresses, hmacHashOfSystemBoxAddress]) => {
          hmacHashesByBoxAddress.set(this.sessionService.systemBoxAddress(), hmacHashOfSystemBoxAddress);

          const request: ConversationMessagesRequest = ConversationMessagesRequest.forMessagesIds(
            hmacHashesByBoxAddress.mergeWith(hmacHashesByDeletedBoxAddresses),
            messagesIdsToFetch);
          return this.httpClient.put(`${config.api.conversations}/${conversation.conversationId()}/messages`, request.toJson());
        }),
        mergeMap((messagesResponse: MessagesInResponse) => {
          return from(messagesResponse.messages); // This is to process the response's messages one by one
        }),
        mergeMap((messageIn: MessageIn) => {
          return this.buildMessageFromResponse(messageIn);
        })
      );
  }

  fetchMissingMessages(conversation: Conversation,
                       pageOfStoredMessageIds: string[],
                       page: number,
                       pageSize: number): Observable<Message> {
    this.logger.trace(`fetchMissingMessages(${pageOfStoredMessageIds}, ${page}, ${pageSize})`);

    const participantPseudos: string[] = conversation.participantsPseudos();
    return forkJoin(
      this.sessionService.hmacHashesByReadFromBoxAddresses(participantPseudos),
      this.sessionService.hmacHashesByDeletedReadFromBoxAddresses(participantPseudos),
      this.sessionService.hmacHashOfSystemBoxAddress())
      .pipe(
        mergeMap(([hmacHashesByBoxAddress, hmacHashesByDeletedBoxAddresses, hmacHashOfSystemBoxAddress]) => {
          hmacHashesByBoxAddress.set(this.sessionService.systemBoxAddress(), hmacHashOfSystemBoxAddress);

          const request: ConversationMessagesIdsRequest = ConversationMessagesIdsRequest.for(
            hmacHashesByBoxAddress.mergeWith(hmacHashesByDeletedBoxAddresses),
            page,
            pageSize);
          return this.httpClient
            .put(`${config.api.conversations}/${conversation.conversationId()}/messages/ids`, request.toJson());
        }),
        mergeMap((conversationMessagesIdsResponse: ConversationMessagesIdsResponse) => {
          const missingMessageIds: string[] = conversationMessagesIdsResponse.messagesIds
            .filter(id => pageOfStoredMessageIds.indexOf(id) === -1);
          this.logger.debug(`Missing message IDs: ${missingMessageIds}`);

          return missingMessageIds.length > 0
            ? this.fetchMessagesByIds(conversation, missingMessageIds)
            : Observable.empty();
        })
      );
  }

  storeMessage(message: Message): Observable<Message> { // TODO Break this method up and make it more easy to test
    const openPgp: OpenPgp = this.sessionService.openPgp();
    const senderPseudo: string = this.sessionService.pseudo();

    const receiversPseudos: string[] = message.recipientsPseudos([senderPseudo]);
    const receivers: Contact[] = this.sessionService.contactsAsArray(receiversPseudos);
    const receiversMessageOutsObs: Observable<MessageOut>[] = receivers.map((receiver: Contact) => {
      const receiverMessage: Message = message.clone();
      return receiver.hmacHash(receiver.writeTo())
        .pipe(
          mergeMap((hmacHash: string) => {
            receiverMessage.setConversationId(message.participantConversationId(receiver.pseudo()));
            receiverMessage.setSentToBoxAddress(receiver.writeTo());
            receiverMessage.setSentToHmacHash(hmacHash);
            const encryptObs: Observable<string> = openPgp.encrypt(receiverMessage.toJson(), receiver.writeWith().publicKeys());
            return forkJoin(encryptObs, of(hmacHash));
          }),
          map(([payload, hmacHash]) => {
            return MessageOut.with(
              receiver.writeTo(),
              receiverMessage.participantConversationId(receiver.pseudo()),
              hmacHash,
              payload);
          })
        );
    });
    const receiverMessagesOutObs: Observable<MessageOut[]> = forkJoin(receiversMessageOutsObs);

    const senderMessage: Message = message.clone();
    const senderMessageOutObs: Observable<MessageOut> = this.sessionService.hmacHashOfSystemBoxAddress()
      .pipe(
        mergeMap((hmacHash: string) => {
          senderMessage.setConversationId(senderMessage.participantConversationId(this.sessionService.pseudo()));
          senderMessage.setSentToBoxAddress(this.sessionService.systemBoxAddress());
          senderMessage.setSentToHmacHash(hmacHash);
          return forkJoin(openPgp.encrypt(senderMessage.toJson()), of(hmacHash));
        }),
        map(([payload, hmacHash]) => {
          return MessageOut.with(
            this.sessionService.systemBoxAddress(),
            message.participantConversationId(this.sessionService.pseudo()),
            hmacHash,
            payload);
        })
      );

    return forkJoin(receiverMessagesOutObs, senderMessageOutObs, this.sessionService.exchangeAes())
      .pipe(
        mergeMap(([receiverMessagesOut, senderMessageOut, exchangeAes]) => {
          const messagesOut: MessagesOut = MessagesOut.with(receiverMessagesOut.concat([senderMessageOut]));
          return exchangeAes.encrypt(messagesOut.toJson());
        }),
        mergeMap((payload: Uint8Array) => {
          return forkJoin(
            this.sessionService.exchangeAes(),
            this.httpClient.post(`${config.api.messages}`, <AesRequest>{payload: Array.from(payload)})
          );
        }),
        mergeMap(([exchangeAes, aesResponse]) => {
          return forkJoin(
            exchangeAes.decrypt(new Uint8Array((<AesResponse>aesResponse).payload)),
            this.sessionService.hmacHashOfSystemBoxAddress());
        }),
        mergeMap(([messageId, hmacHash]) => {
          message.setConversationId(message.participantConversationId(this.sessionService.pseudo()));
          message.setMessageId(messageId);
          message.setSentToBoxAddress(this.sessionService.systemBoxAddress());
          message.setSentToHmacHash(hmacHash);
          message.checkIntegrity();
          return of(message);
        })
      );
  }

  private buildMessageFromResponse(messageIn: MessageIn): Observable<Message> {

    const sender: Contact = this.sessionService.contactWithReadFromBoxAddress(messageIn.readFromBoxAddress);
    const senderArmoredPublicKey: string = sender
      ? sender.writeWith().publicKeys()
      : this.sessionService.session().systemPublicKeys(); // This is the logged user's public keys

    return this.sessionService.openPgp()
      .decrypt(messageIn.payload, senderArmoredPublicKey)
      .pipe(
        map((json: string) => {
          const message = Message.fromJson(json);
          if (!message.hasBeenSent()) {
            /*
            The absence of a message ID here (in the PGP-unencrypted message) is normal.
            On the server, the messages will have had a message ID generated and stored in the DB.
            However at the time of sending the message, when it was encrypted by the sender, the message ID had not been generated yet.
            This is why the line below "completes" the PGP-unencrypted message with the ID returned by MessageIn: the MessageIn can provide the ID from the DB, while the encrypted message could not have known about it.
             */
            message.setMessageId(messageIn.messageId);
          }
          return message;
        })
      );
  }
}

export class ConversationMessagesIdsRequest implements Serializable {

  static for(hmacHashesByBoxAddress: HmacHashesByBoxAddress, page: number, pageSize: number) {
    return new ConversationMessagesIdsRequest(hmacHashesByBoxAddress, page, pageSize);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: any,
                      private readonly _page: number,
                      private readonly _pageSize: number) {
  }

  asObject(): any {
    return {
      hmacHashesByBoxAddress: this._hmacHashesByBoxAddress,
      page: this._page,
      pageSize: this._pageSize
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export interface ConversationMessagesIdsResponse {
  messagesIds: string[];
}

export class ConversationMessagesRequest implements Serializable {

  static forMessagesIds(hmacHashesByBoxAddress: HmacHashesByBoxAddress, messagesIds: string[]): ConversationMessagesRequest {
    return new ConversationMessagesRequest(hmacHashesByBoxAddress, messagesIds, null, null);
  }

  static forPage(hmacHashesByBoxAddress: HmacHashesByBoxAddress, page: number, pageSize: number): ConversationMessagesRequest {
    return new ConversationMessagesRequest(hmacHashesByBoxAddress, null, page, pageSize);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: HmacHashesByBoxAddress,
                      private readonly _messagesIds: string[],
                      private readonly _page: number,
                      private readonly _pageSize: number) {
  }

  asObject(): any {
    return {
      hmacHashesByBoxAddress: this._hmacHashesByBoxAddress,
      messagesIds: this._messagesIds,
      page: this._page,
      pageSize: this._pageSize,
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export class MessageOut implements Serializable {

  static with(boxAddress: string, conversationId: string, hmacHash: string, payload: string): MessageOut {
    return new MessageOut(boxAddress, conversationId, hmacHash, payload);
  }

  private constructor(private readonly _boxAddress: string,
                      private readonly _conversationId: string,
                      private readonly _hmacHash: string,
                      private readonly _payload: string) {
  }

  asObject(): any {
    return {
      boxAddress: this._boxAddress,
      conversationId: this._conversationId,
      hmacHash: this._hmacHash,
      payload: this._payload
    };
  }
}

export class MessagesOut implements Serializable {

  static with(messagesOut: MessageOut[]): MessagesOut {
    return new MessagesOut(messagesOut)
  }

  constructor(private _messagesOut: MessageOut[] = []) {
  }

  asObject(): any {
    return {messages: this._messagesOut}
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export class NewMessagesRequest implements Serializable {

  static for(hmacHashesByBoxAddress: HmacHashesByBoxAddress): NewMessagesRequest {
    return new NewMessagesRequest(hmacHashesByBoxAddress);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: HmacHashesByBoxAddress) {
  }

  asObject(): any {
    return {hmacHashesByBoxAddress: this._hmacHashesByBoxAddress};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
