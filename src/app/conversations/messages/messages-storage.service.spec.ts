import {Storage} from "@ionic/storage";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {CoreJasmine} from "../../../core/core-jasmine";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {AES} from "../../crypto/aes/aes";
import {CryptoJasmine} from "../../crypto/crypto.jasmine";
import {SessionService} from "../../sessions/session.service";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {Contact} from "../../user/contact/contact";
import {ContactTestBuilder as aContact} from "../../user/contact/contact.test-builder";
import {Conversation} from "../conversation";
import {ConversationTestBuilder as aConversation} from "../conversation.test-builder";
import {ConversationsRemote} from "../conversations-remote";
import {Message} from "./message";
import {MessageFixture} from "./message-fixture";
import {MessageTestBuilder as aMessage} from "./message.test-builder";
import {MessagesRemote} from "./messages-remote";
import {MessagesStorageService} from "./messages-storage.service";
import {MessagesTimeMap} from "./messages-time-map";

describe("MessagesStorage", () => {

  const AES_DATA: Uint8Array = new Uint8Array([1, 2, 3]);

  const ALICE_BOX_ADDRESS: string = "aliceBoxAddress";
  const ALICE_HMAC_HASH_FOR_BOB_READ_FROM = "855tH6ZLYAwW3ehAm3nA5GVihyFIn6ysod+rJk7NCFq2JQCCnjvMUB8DZURrj3z//OEHF6NowChOTyuHrftyHQ==";
  const ALICE_PSEUDO: string = "alicePseudo";
  const BOB_PSEUDO: string = "bobPseudo";

  const ALICE_BOB_BOX_ADDRESS: string = "aliceBobBoxAddress";

  const CONVERSATION_ID: string = '132';
  const CONVERSATION_MESSAGES_ENCRYPTED_DATA: Uint8Array = new Uint8Array([198, 765, 111]);
  const ENCRYPTED_DATA: Uint8Array = new Uint8Array([1, 2, 3]);
  const M_ID_1: string = 'm1';
  const M_ID_2: string = 'm2';
  const M_ID_3: string = 'm3';
  const M1: Message = MessageFixture.withId(M_ID_1);
  const M1_ENCRYPTED_DATA: Uint8Array = new Uint8Array([9, 5, 1]);
  const M2: Message = MessageFixture.withId(M_ID_2);
  const M2_ENCRYPTED_DATA: Uint8Array = new Uint8Array([1, 2, 3]);
  const M3: Message = MessageFixture.withId(M_ID_3);
  const M3_ENCRYPTED_DATA: Uint8Array = new Uint8Array([4, 5, 6]);
  const MESSAGE_TIME_MAP_ENCRYPTED_DATA: Uint8Array = new Uint8Array([77, 88, 99]);
  const MESSAGES_TIME_MAP_132: MessagesTimeMap = MessagesTimeMap.for([M1, M2, M3]);
  const PAGE: number = 0;
  const PAGE_SIZE: number = 10;

  let conversationsRemote: ConversationsRemote;
  let messagesRemote: MessagesRemote;
  let sessionService: SessionService;
  let storage: Storage;

  let messagesStorage: MessagesStorageService;

  let storageAes: AES;

  beforeEach(() => {
    conversationsRemote = CoreJasmine.conversationsRemote();
    messagesRemote = CoreJasmine.messagesRemote();
    sessionService = SessionsJasmine.sessionService();
    storage = IonicJasmine.storage();

    messagesStorage = new MessagesStorageService(conversationsRemote, messagesRemote, sessionService, storage);

    storageAes = CryptoJasmine.storageAes();
    (<jasmine.Spy>sessionService.storageAes).and.returnValue(of(storageAes));
  });

  describe(", when deleting a message,", () => {

    it("should delete a message based on the message's \"sent to\" box address and HMAC hash", (done: DoneFn) => {

      const messageToDelete: Message = aMessage.with()
        .conversationId(CONVERSATION_ID)
        .messageId(M_ID_2)
        .sentToBoxAddress(ALICE_BOB_BOX_ADDRESS)
        .sentToHmacHash(ALICE_HMAC_HASH_FOR_BOB_READ_FROM)
        .asIs();
      messagesStorage["messagesTimeMaps"][CONVERSATION_ID] = MessagesTimeMap.for([M1, messageToDelete, M3]);

      (<jasmine.Spy>conversationsRemote.deleteConversationMessage).and.returnValue(of({}));

      (<jasmine.Spy>storageAes.encrypt).and.returnValue(of(AES_DATA));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      messagesStorage.deleteMessage(messageToDelete).subscribe(() => {
        expect(conversationsRemote.deleteConversationMessage)
          .toHaveBeenCalledWith(ALICE_BOB_BOX_ADDRESS, CONVERSATION_ID, M_ID_2, ALICE_HMAC_HASH_FOR_BOB_READ_FROM)
        done();
      });
    });

    it("should delete a message on the user's system box address", (done: DoneFn) => {

      messagesStorage["messagesTimeMaps"][CONVERSATION_ID] = MessagesTimeMap.for([M1, M2, M3]);

      (<jasmine.Spy>sessionService.pseudo).and.returnValue(ALICE_PSEUDO);
      (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(ALICE_BOX_ADDRESS);
      (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of('hmacHashOfAliceSystemBoxAddress'));

      (<jasmine.Spy>conversationsRemote.deleteConversationMessage).and.returnValue(of({}));

      (<jasmine.Spy>storageAes.encrypt).and.returnValue(of(AES_DATA));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      const message: Message = aMessage.with()
        .conversationId(CONVERSATION_ID).messageId(M_ID_1).sender(ALICE_PSEUDO).asIs();
      messagesStorage.deleteMessage(message).subscribe(() => {

        expect(conversationsRemote.deleteConversationMessage)
          .toHaveBeenCalledWith(ALICE_BOX_ADDRESS, CONVERSATION_ID, M_ID_1, 'hmacHashOfAliceSystemBoxAddress');
        done();
      });
    });

    it("should delete a message on a contact's \"read from\" box address", (done: DoneFn) => {

      messagesStorage["messagesTimeMaps"][CONVERSATION_ID] = MessagesTimeMap.for([M1, M2, M3]);

      const contact: Contact = aContact.with().pseudo(BOB_PSEUDO).readFrom(ALICE_BOB_BOX_ADDRESS).noBlanks();

      (<jasmine.Spy>sessionService.pseudo).and.returnValue(ALICE_PSEUDO);
      (<jasmine.Spy>sessionService.contact).and.returnValue(contact);

      (<jasmine.Spy>conversationsRemote.deleteConversationMessage).and.returnValue(of({}));

      (<jasmine.Spy>storageAes.encrypt).and.returnValue(of(AES_DATA));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      const message: Message = aMessage.with()
        .conversationId(CONVERSATION_ID).messageId(M_ID_1).sender(BOB_PSEUDO).asIs();
      messagesStorage.deleteMessage(message).subscribe(() => {

        expect(sessionService.contact).toHaveBeenCalledWith(BOB_PSEUDO);
        expect(conversationsRemote.deleteConversationMessage)
          .toHaveBeenCalledWith(ALICE_BOB_BOX_ADDRESS, CONVERSATION_ID, M_ID_1, ALICE_HMAC_HASH_FOR_BOB_READ_FROM);
        done();
      });
    });
  });

  describe(", when listing messages from storage,", () => {

    it("returns the messages present in storage", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(ENCRYPTED_DATA));
      (<jasmine.Spy>storageAes.decrypt).and.returnValues(
        of(JSON.stringify(MESSAGES_TIME_MAP_132)),
        of(M1.toJson()),
        of(M2.toJson()),
        of(M3.toJson())
      );

      const returnedMessages: Message[] = [];
      messagesStorage.listMessagesFromStorage(CONVERSATION_ID, PAGE, PAGE_SIZE)
        .subscribe((result: Message) => {
          returnedMessages.push(result);
          if (returnedMessages.length === 3) {

            expect(storage.get).toHaveBeenCalledTimes(4);
            expect(storageAes.decrypt).toHaveBeenCalledTimes(4);

            expect(returnedMessages[0]).toEqual(M1);
            expect(returnedMessages[1]).toEqual(M2);
            expect(returnedMessages[2]).toEqual(M3);

            done();
          }
        });
    });

    it("returns an empty observable if no messages are in storage", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(ENCRYPTED_DATA));
      (<jasmine.Spy>storageAes.decrypt).and.returnValue(of(JSON.stringify(MessagesTimeMap.empty())));

      messagesStorage.listMessagesFromStorage(CONVERSATION_ID, PAGE, PAGE_SIZE)
        .subscribe(
          () => fail("unexpected value returned"),
          null,
          () => done()
        );
    });
  });

  it("should retrieve and store unread messages from the server", (done: DoneFn) => {

    (<jasmine.Spy>messagesRemote.fetchUnreadMessages).and.returnValue(from([M2, M3]));

    // GLOBAL
    (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());
    // saveMessageInStorage
    (<jasmine.Spy>storageAes.encrypt).and.returnValues(
      of(M2_ENCRYPTED_DATA), of(M3_ENCRYPTED_DATA),
      of(MESSAGE_TIME_MAP_ENCRYPTED_DATA), of(MESSAGE_TIME_MAP_ENCRYPTED_DATA));

    (<jasmine.Spy>storage.get).and.returnValue(of(CONVERSATION_MESSAGES_ENCRYPTED_DATA));
    (<jasmine.Spy>storageAes.decrypt).and.returnValue(of(JSON.stringify(MessagesTimeMap.empty())));

    const returnedMessages: Message[] = [];
    messagesStorage.listUnreadMessages()
      .subscribe((message: Message) => {
        returnedMessages.push(message);
        if (returnedMessages.length === 2) {

          expect((<jasmine.Spy>storageAes.encrypt).calls.argsFor(0)[0]).toEqual(M2.toJson());
          expect((<jasmine.Spy>storageAes.encrypt).calls.argsFor(1)[0]).toEqual(M3.toJson());

          done();
        }
      });
  });

  it("should sync a page of messages with the server, for a given conversation ID", (done: DoneFn) => {
    messagesStorage['messagesTimeMaps'][CONVERSATION_ID] = MessagesTimeMap.empty();
    const serverMessage: Message = aMessage0(M_ID_1, CONVERSATION_ID);

    (<jasmine.Spy>messagesRemote.fetchMissingMessages).and.returnValue(of(serverMessage));

    (<jasmine.Spy>storageAes.encrypt).and.returnValues(of(M1_ENCRYPTED_DATA), of(MESSAGE_TIME_MAP_ENCRYPTED_DATA));
    (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

    const conversation: Conversation = aConversation.with().conversationId(CONVERSATION_ID).noBlanks();
    messagesStorage.syncMessages(conversation, PAGE, PAGE_SIZE)
      .subscribe((message: Message) => {

        expect(messagesRemote.fetchMissingMessages).toHaveBeenCalledWith(conversation, [], PAGE, PAGE_SIZE);
        expect((<jasmine.Spy>storageAes.encrypt).calls.argsFor(0)[0]).toEqual(serverMessage.toJson());

        expect(message).toEqual(serverMessage);

        done();
      });
  });

  it("should save a message in storage", (done: DoneFn) => {

    const message: Message = MessageFixture.withConversationIdAndSendDate(CONVERSATION_ID, 123);
    message.setMessageId('m123');

    (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(ENCRYPTED_DATA));
    (<jasmine.Spy>storageAes.decrypt).and.returnValue(of(JSON.stringify(MessagesTimeMap.empty())));

    (<jasmine.Spy>messagesRemote.storeMessage).and.returnValue(of(message));

    (<jasmine.Spy>storageAes.encrypt).and.returnValue(of(ENCRYPTED_DATA));
    (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

    messagesStorage.storeMessage(message)
      .subscribe((result: Message) => {

        expect(storage.get).toHaveBeenCalledTimes(1);
        expect(storageAes.decrypt).toHaveBeenCalledTimes(1);

        expect(messagesRemote.storeMessage).toHaveBeenCalledTimes(1);
        expect(storageAes.encrypt).toHaveBeenCalledTimes(2);

        expect(result).toEqual(message);

        const messagesTimeMaps: any = messagesStorage['messagesTimeMaps'];
        expect(Object.keys(messagesTimeMaps).length).toEqual(1);
        expect(messagesTimeMaps[CONVERSATION_ID]).toBeDefined();
        const messagesTimeMap: MessagesTimeMap = messagesTimeMaps[CONVERSATION_ID];
        expect(messagesTimeMap.getPageOfMessageIds()).toContain('m123');

        done();
      });
  });

  function aMessage0(conversationId: string, messageId: string) {
    return aMessage.with().title("title").content("content").order(1)
      .conversationId(conversationId).and.messageId(messageId).asIs();
    //     return new Message("title", "content", null, null, null, null, 1, conversationId, messageId);
  }
});
