import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {Observable} from "rxjs";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {map, mergeMap, tap} from "rxjs/operators";
import {AES} from "../../crypto/aes/aes";
import {SessionService} from "../../sessions/session.service";
import {Contact} from "../../user/contact/contact";
import {Logger} from "../../utils/logging/logger";
import {Conversation} from "../conversation";
import {ConversationsRemote} from "../conversations-remote";
import {Message} from "./message";
import {MessagesRemote} from "./messages-remote";
import {MessagesTimeMap} from "./messages-time-map";

@Injectable()
export class MessagesStorageService {

  private static readonly FORCE_RELOAD_FROM_STORAGE: boolean = true;

  private static readonly MESSAGE_STORAGE_KEY: string = 'MESSAGE';
  private static readonly CONVERSATION_MESSAGES_STORAGE_KEY: string = 'CONVERSATION_MESSAGES';

  private static messageKey(messageId: string) {
    return `${MessagesStorageService.MESSAGE_STORAGE_KEY}(${messageId})`;
  }

  private static conversationsMessagesKey(conversationId: string) {
    return `${MessagesStorageService.CONVERSATION_MESSAGES_STORAGE_KEY}(${conversationId})`;
  }

  private readonly logger: Logger = Logger.for("MessagesStorage");

  private messagesTimeMaps: any = {};
  private storageAes: AES;

  constructor(private readonly conversationsRemote: ConversationsRemote,
              private readonly messagesRemote: MessagesRemote,
              private readonly sessionService: SessionService,
              private readonly storage: Storage) {
  }

  deleteMessage(message: Message): Observable<Message> {
    // TODO Move to session service?
    let boxAddress: string = message.sentToBoxAddress();
    if (!boxAddress) {
      // Fallback logic in case of issues with sentToBoxAddress
      if (message.sender() === this.sessionService.pseudo()) {
        boxAddress = this.sessionService.systemBoxAddress();
      } else {
        const contact: Contact = this.sessionService.contact(message.sender());
        boxAddress = contact ? contact.readFrom() : null;
      }
    }
    //

    // TODO Move to session service?
    let hmacHashObs: Observable<string>;
    if (message.sentToHmacHash()) {
      hmacHashObs = of(message.sentToHmacHash());
    } else {
      // Fallback logic in case of issues with sentToBoxAddress
      if (message.sender() === this.sessionService.pseudo()) {
        hmacHashObs = this.sessionService.hmacHashOfSystemBoxAddress();
      } else {
        const contact: Contact = this.sessionService.contact(message.sender());
        hmacHashObs = contact ? contact.hmacHash(contact.readFrom()) : of(null);
      }
    }
    //

    return hmacHashObs
      .pipe(
        mergeMap((hmacHash: string) => this.conversationsRemote
          .deleteConversationMessage(boxAddress, message.conversationId(), message.messageId(), hmacHash)),
        mergeMap(() => this.initStorageAes()),
        mergeMap(() => {
          const messagesTimeMap: MessagesTimeMap = this.messagesTimeMaps[message.conversationId()];
          messagesTimeMap.deleteWithId(message.messageId());
          return this.saveConversationMessages(message.conversationId());
        }),
        map(() => message)
      );
  }

  /**
   * Lists the specified page of messages from storage, for the specified conversation ID.
   */
  // TODO Rename to fetchPage
  listMessagesFromStorage(conversationId: string, page: number, pageSize: number): Observable<Message> {
    this.logger.trace(`listMessagesFromStorage(${conversationId}, ${page}, ${pageSize})`);

    return this.initMessagesTimeMap(conversationId)
      .pipe(
        mergeMap(() => {
          const messagesTimeMap: MessagesTimeMap = this.messagesTimeMaps[conversationId];
          return from(messagesTimeMap.getPageOfMessageIds(page, pageSize));
        }),

        mergeMap((messageId: string) => {
          return from(this.storage.get(MessagesStorageService.messageKey(messageId)));
        }),

        mergeMap((encrypted: number[]) => {
          if (encrypted) {
            return this.storageAes.decrypt(new Uint8Array(encrypted))
          } else {
            this.logger.warn("Got no message in storage for one of the message IDs...");
            return Observable.empty();
          }
        }),

        map((json: string) => Message.fromJson(json))
      );
  }

  /**
   * Retrieves and stores unread messages from the server.
   */
  listUnreadMessages(): Observable<Message> {

    this.logger.trace("listUnreadMessages()");

    return this.messagesRemote.fetchUnreadMessages()
      .pipe(

        mergeMap((message: Message) => {
          return this.saveMessageInStorage(message)
        }),

        mergeMap((message: Message) => {
          const conversationId: string = message.conversationId();

          // We need to first make sure that the MessagesTimeMap of this conversation contains all messages,
          // otherwise the time map will be out of sync' and only the unread message will be listed
          return this.initMessagesTimeMap(conversationId, MessagesStorageService.FORCE_RELOAD_FROM_STORAGE)
            .pipe(
              mergeMap(() => of((<MessagesTimeMap>this.messagesTimeMaps[conversationId]).put(message)))
            );
        }),

        mergeMap((message: Message) => {
          return this.saveConversationMessages(message.conversationId())
            .pipe(
              map(() => message)
            );
        })
      );
  }

  storeMessageInStorage(message: Message): Observable<Message> {
    this.logger.trace(`storeMessageInStorage(${message.messageId()})`);

    message.checkIntegrity();

    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.initMessagesTimeMap(message.conversationId(), true)),
        mergeMap(() => this.saveMessageInStorage(message)),
        map((message: Message) => (<MessagesTimeMap>this.messagesTimeMaps[message.conversationId()]).put(message)),
        mergeMap(() => this.saveConversationMessages(message.conversationId())),
        map(() => message)
      );
  }

  syncMessages(conversation: Conversation, page: number, pageSize: number): Observable<Message> {
    this.logger.trace(`syncMessages(${conversation.conversationId}, ${page}, ${pageSize})`);

    return this.initMessagesTimeMap(conversation.conversationId())
      .pipe(
        mergeMap(() => {
          const messagesTimeMap: MessagesTimeMap = <MessagesTimeMap>this.messagesTimeMaps[conversation.conversationId()];
          const pageOfStoredMessageIds: string[] = messagesTimeMap.getPageOfMessageIds(page, pageSize);
          return this.messagesRemote.fetchMissingMessages(conversation, pageOfStoredMessageIds, page, pageSize);
        }),

        map((message: Message) => {
          return (<MessagesTimeMap>this.messagesTimeMaps[conversation.conversationId()]).put(message);
        }),

        mergeMap((message: Message) => {
          return this.saveMessageInStorage(message);
        }),

        mergeMap((message: Message) => {
          return this.saveConversationMessages(conversation.conversationId())
            .pipe(
              map(() => message)
            );
        })
      );
  }

  storeMessage(message: Message): Observable<Message> {

    return this.messagesRemote.storeMessage(message)
      .pipe(
        tap((message: Message) => {
          return message.checkIntegrity();
        }),

        mergeMap((message: Message) => {
          return this.saveMessageInStorage(message);
        }),

        mergeMap((message: Message) => {

          return this.initMessagesTimeMap(message.conversationId())
            .pipe(
              map(() => {
                return (<MessagesTimeMap>this.messagesTimeMaps[message.conversationId()]).put(message);
              })
            );
        }),

        mergeMap((message: Message) => {
          return this.saveConversationMessages(message.conversationId())
            .pipe(
              map(() => message)
            );
        })
      );
  }

  private saveConversationMessages(conversationId: string): Observable<any> {
    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.storageAes.encrypt(JSON.stringify(this.messagesTimeMaps[conversationId]))),
        mergeMap((payload: Uint8Array) =>
          from(this.storage.set(MessagesStorageService.conversationsMessagesKey(conversationId), Array.from(payload))))
      );
  }

  private saveMessageInStorage(message: Message): Observable<Message> {
    return this.initStorageAes()
      .pipe(
        mergeMap(() => this.storageAes.encrypt(message.toJson())),
        mergeMap((encrypted: Uint8Array) => {
          return forkJoin(
            of(message),
            from(this.storage.set(MessagesStorageService.messageKey(message.messageId()), Array.from(encrypted)))
          );
        }),
        map(([message]) => message)
      );
  }

  // PRIVATE

  private initMessagesTimeMap(conversationId: string, forceReloadFromStorage: boolean = false): Observable<MessagesTimeMap> {

    if (this.messagesTimeMaps[conversationId] && !forceReloadFromStorage) {
      return of(this.messagesTimeMaps[conversationId]);
    }

    return this.initStorageAes()
      .pipe(

        mergeMap(() => {
          return from(this.storage.get(MessagesStorageService.conversationsMessagesKey(conversationId)))
        }),

        mergeMap((encrypted: number[]) => {
          return encrypted
            ? this.storageAes.decrypt(new Uint8Array(encrypted))
            : of(JSON.stringify(MessagesTimeMap.empty()));
        }),

        map((json: string) => {
          this.messagesTimeMaps[conversationId] = MessagesTimeMap.fromJson(json);

          return this.messagesTimeMaps[conversationId];
        })
      );
  }

  private initStorageAes(): Observable<AES> {
    return this.sessionService.storageAes()
      .pipe(
        tap((aes: AES) => this.storageAes = aes)
      );
  }
}
