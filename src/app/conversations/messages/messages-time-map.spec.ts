import {Message} from "./message";
import {MessageFixture} from "./message-fixture";
import {MessagesTimeMap} from "./messages-time-map";

describe("MessagesTimeMap", () => {

  const MESSAGE_ID_1: string = '1';
  const MESSAGE_ID_2: string = '2';
  const MESSAGE_ID_3: string = '3';

  const MESSAGE_1: Message = MessageFixture.withMessageIdAndSendDate(MESSAGE_ID_1, 1);
  const MESSAGE_2: Message = MessageFixture.withMessageIdAndSendDate(MESSAGE_ID_2, 2);
  const MESSAGE_3: Message = MessageFixture.withMessageIdAndSendDate(MESSAGE_ID_3, 3);

  const PAGE_ONE: number = 0;
  const PAGE_TWO: number = 1;
  const PAGE_SIZE_OF_TWO: number = 2;

  it("should instantiate an empty MessagesTimeMap", () => {

    const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.empty();

    expect(messagesTimeMap).toBeDefined();
    expect(messagesTimeMap['map'].length).toBe(0);
  });

  it("should instantiate a MessagesTimeMap from an array of conversations", () => {
    const messages: Message[] = [MESSAGE_2, MESSAGE_1];

    const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for(messages);

    expect(messagesTimeMap).not.toBeUndefined();
    expect(messagesTimeMap['map'].length).toBe(2);
  });

  it("should instantiate a MessagesTimeMap from a JSON string", () => {
    const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1]);

    const messagesTimeMapFromJson: MessagesTimeMap = MessagesTimeMap.fromJson(JSON.stringify(messagesTimeMap));

    expect(messagesTimeMapFromJson).toEqual(messagesTimeMap);
  });

  it("should delete an item with the specified message ID", () => {
    const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1, MESSAGE_2, MESSAGE_3]);

    messagesTimeMap.deleteWithId(MESSAGE_ID_1);

    const allMessageIds: string[] = messagesTimeMap.getPageOfMessageIds();
    expect(allMessageIds).toContain(MESSAGE_ID_2);
    expect(allMessageIds).toContain(MESSAGE_ID_3);
  });

  describe(", when getting pages of message IDs,", () => {

    it("should return all message IDs", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1, MESSAGE_3, MESSAGE_2]);

      const messageIds: string[] = messagesTimeMap.getPageOfMessageIds();

      expect(messageIds.length).toBe(3);
      expect(messageIds)
        .toEqual([MESSAGE_3.messageId(), MESSAGE_2.messageId(), MESSAGE_1.messageId()]);
    });

    it("should return the first page of message IDs", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1, MESSAGE_2, MESSAGE_3]);

      const messageIds: string[] = messagesTimeMap.getPageOfMessageIds(PAGE_ONE, PAGE_SIZE_OF_TWO);

      expect(messageIds.length).toBe(2);
      expect(messageIds).toContain(MESSAGE_3.messageId());
      expect(messageIds).toContain(MESSAGE_2.messageId());
    });

    it("should return the second page of message IDs", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1, MESSAGE_2, MESSAGE_3]);

      const messageIds: string[] = messagesTimeMap.getPageOfMessageIds(PAGE_TWO, PAGE_SIZE_OF_TWO);

      expect(messageIds.length).toBe(1);
      expect(messageIds).toContain(MESSAGE_1.messageId());
    });

    it("should return an empty page for nonexistent pages", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_1, MESSAGE_2, MESSAGE_3]);

      const messageIds: string[] = messagesTimeMap.getPageOfMessageIds(23, PAGE_SIZE_OF_TWO);

      expect(messageIds.length).toBe(0);
    });
  });

  describe(", when putting a new message into the map,", () => {

    it("should add the message into the map", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_3, MESSAGE_2]);

      messagesTimeMap.put(MESSAGE_1);

      expect(messagesTimeMap['map'].length).toBe(3);
    });

    it("should add the message while respecting the 'most recent first' order", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_2, MESSAGE_1]);

      messagesTimeMap.put(MESSAGE_3);

      expect(messagesTimeMap['map'][0].id).toBe(MESSAGE_3.messageId());
    });

    it("should update a message that already exists in the time map, instead of inserting a new one", () => {
      const messagesTimeMap: MessagesTimeMap = MessagesTimeMap.for([MESSAGE_2, MESSAGE_1]);
      const updatedMessage1: Message = MessageFixture.withMessageIdAndSendDate(MESSAGE_1.messageId(), 789);

      messagesTimeMap.put(updatedMessage1);

      expect(messagesTimeMap['map'].length).toBe(2);
      expect(messagesTimeMap['map'][0].id).toBe(MESSAGE_1.messageId());
      expect(messagesTimeMap['map'][0].sendDate).toBe(789);
    });
  });
});
