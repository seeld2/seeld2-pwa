import {Message} from "./message";

/**
 * This object holds a list of items mapping message IDs by their timestamps.
 * It allows keeping track of these messages' time order.
 */
export class MessagesTimeMap {

  /**
   * Build an empty MessagesTimeMap instance.
   */
  static empty(): MessagesTimeMap {
    return new MessagesTimeMap([]);
  }

  /**
   * Builds an instance of MessagesTimeMap for the specified array of messages.
   */
  static for(messages: Message[] = []): MessagesTimeMap {
    const map: Item[] = [];
    messages.forEach((message: Message) => map.push(Item.for(message)));
    return new MessagesTimeMap(map);
  }

  static fromJson(json: string): MessagesTimeMap {
    const parsed: MessagesTimeMap = JSON.parse(json);
    const items: Item[] = parsed.map.map(object => Item.from(object));
    return new MessagesTimeMap(items);
  }

  private static sort(map: Item[]): Item[] {
    return map.sort((item1: Item, item2: Item) => item2.sendDate - item1.sendDate);
  }

  private readonly map: Item[];

  private constructor(map: Item[]) {
    this.map = MessagesTimeMap.sort(map);
  }

  deleteWithId(messageId: string) {
    const index: number = this.map.findIndex((item: Item) => item.id === messageId);
    if (index < 0) {
      return;
    }
    this.map.splice(index, 1);
  }

  /**
   * Returns all the message IDs of this MessagesTimeMap instance.
   * The returned IDs are ordered so that they respect the timestamp order: most recent first.
   *
   * @param page Zero-based, meaning that page 0 is the first page.
   * @param pageSize The amount of messages to return for the page.
   * @returns An array of message IDs as strings.
   */
  getPageOfMessageIds(page?: number, pageSize?: number): string[] {
    let items: Item[] = this.map;
    if (page >= 0 && pageSize >= 0) {
      const from: number = page * pageSize;
      const to: number = (page + 1) * pageSize;
      items = items.slice(from, to);
    }
    return items.map((item: Item) => item.id);
  }

  isEmpty(): boolean {
    return Object.keys(this.map).length === 0;
  }

  /**
   * Inserts or updates the MessagesTimeMap instance with with the passed message.
   * If the message is already mapped, the method updates its timestamp in the time map.
   *
   * @param message The message to add or update.
   */
  put(message: Message): Message {

    const index: number = this.map.findIndex((item: Item) => item.id === message.messageId());
    if (index >= 0) {
      this.map.splice(index, 1);
    }

    this.map.push(Item.for(message));

    MessagesTimeMap.sort(this.map);

    return message;
  }
}

class Item {

  private constructor(public readonly id: string,
                      public readonly sendDate: number) {
  }

  static for(message: Message): Item {
    return new Item(message.messageId(), message.sendDate());
  }

  static from(object: any): Item {
    return new Item(object.id, object.sendDate);
  }
}


