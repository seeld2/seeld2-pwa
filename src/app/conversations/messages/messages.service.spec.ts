import {ConversationsStorageService} from "../conversations-storage.service";
import {ConversationsJasmine} from "../conversations.jasmine";
import {MessageTestBuilder as aMessage} from "./message.test-builder";
import {MessagesService} from "./messages.service";
import {SessionService} from "../../sessions/session.service";
import {Message} from "./message";
import {MessagesCache} from "./messages-cache";
import {Contact} from "../../user/contact/contact";
import {of} from "rxjs/observable/of";
import {Conversation} from "../conversation";
import {CoreJasmine} from "../../../core/core-jasmine";
import {Session} from "../../sessions/session";
import {TimeService} from "../../time/time.service";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {Contacts} from "../../user/contact/contacts";
import {ContactTestBuilder as aContact} from "../../user/contact/contact.test-builder";
import {ConversationTestBuilder as aConversation} from "../conversation.test-builder";

describe("MessagesService", () => {

  const CONVERSATION_ID: string = 'CONVERSATION ID';
  const LOGGED_USERNAME: string = 'LOGGED USERNAME';

  const MESSAGE: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(1).order(1).conversationId(CONVERSATION_ID).and.messageId("M1").asIs();

  const PAGE: number = 0;
  const PAGE_SIZE: number = 10;

  let conversationsStorageService: ConversationsStorageService;
  let messagesCache: MessagesCache;
  let sessionService: SessionService;
  let timeService: TimeService;

  let messages: MessagesService;

  beforeEach(() => {
    conversationsStorageService = ConversationsJasmine.conversationsStorageService()
    messagesCache = CoreJasmine.messagesCache();
    sessionService = SessionsJasmine.sessionService();
    timeService = new TimeService(null);

    messages = new MessagesService(conversationsStorageService, messagesCache, null, null, sessionService, timeService);
  });

  it("should clear the messages' cache", () => {
    messages.clearCache(CONVERSATION_ID);
    expect(messagesCache.clearCache).toHaveBeenCalledWith(CONVERSATION_ID);
  });

  it("should delete an array of messages", (done: DoneFn) => {

    const message1: Message = aMessage.with().conversationId(CONVERSATION_ID).asIs();
    const message2: Message = aMessage.with().conversationId(CONVERSATION_ID).asIs();

    (<jasmine.Spy>messagesCache.deleteMessage).and.returnValue(of({}));
    (<jasmine.Spy>messagesCache.getMessagesFromCache).and.returnValue([message1]);
    (<jasmine.Spy>conversationsStorageService.updateConversationWithMessage).and.returnValue(of({}));

    messages.deleteMessages([message1]).subscribe(() => {
      expect(messagesCache.deleteMessage).toHaveBeenCalledWith(message1);
      expect(messagesCache.getMessagesFromCache).toHaveBeenCalledWith(CONVERSATION_ID, 0, 1);
      expect(conversationsStorageService.updateConversationWithMessage).toHaveBeenCalledWith(message2, true)
      done();
    });
  });

  describe(", when describing the participants of a message", () => {

    it("should describe the participants of a message", () => {

      const message: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(1)
        .participants({sender: 'sender conversation ID', receiver: 'receiver conversation ID'}).asIs();

      (<jasmine.Spy>sessionService.session).and.returnValue(
        aSession(LOGGED_USERNAME, [
          aContact.with().pseudo("sender").and.displayName("Sender Display Name").asIs(),
          aContact.with().pseudo("receiver").asIs()]));

      let description: string = messages.describeParticipantsOf(message);

      expect(description).toContain('Sender Display Name');
      expect(description).toContain('receiver');

      description = messages.describeParticipantsOf(message, ['sender']);

      expect(description).toContain('receiver');
    });

    it("should replace unknown participants by their username only", () => {

      const message: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(1)
        .participants({
          sender: 'sender conversation ID',
          known: 'known conversation ID',
          unknown: 'unknown conversation ID'
        }).asIs();

      (<jasmine.Spy>sessionService.session).and.returnValue(
        aSession(LOGGED_USERNAME, [
          aContact.with().pseudo("sender").and.displayName("Sender Display Name").asIs(),
          aContact.with().pseudo("atchoum").asIs()
        ]));

      let description: string = messages.describeParticipantsOf(message);

      expect(description).toContain('Sender Display Name');
      expect(description).toContain('known');
      expect(description).toContain('unknown');
    });
  });

  describe(", when describing a message's recipients,", () => {

    it("should handle a message sent by the logged user", () => {

      (<jasmine.Spy>sessionService.session).and.returnValue(
        aSession(LOGGED_USERNAME, [
          aContact.with().pseudo('roy').asIs(),
          aContact.with().pseudo('redmond').asIs()
        ]));

      const message: Message = aMessage.with().recipients([LOGGED_USERNAME, 'roy', 'redmond']).and.sender(LOGGED_USERNAME).asIs();

      const description = messages.describeRecipientsOf(message);

      expect(description).toBe("roy,redmond");
    });

    it("should handle a message sent by another participant", () => {

      (<jasmine.Spy>sessionService.session).and
        .returnValue(aSession(LOGGED_USERNAME, [aContact.with().pseudo('redmond').asIs()]));

      const message: Message = aMessage.with().recipients([LOGGED_USERNAME, 'roy', 'redmond']).and.sender('roy').asIs();

      const description = messages.describeRecipientsOf(message);

      expect(description).toBe("You,redmond");
    });

    it("should handle a message sent by another participant to the logged user only", () => {

      (<jasmine.Spy>sessionService.session).and.returnValue(aSession(LOGGED_USERNAME, []));

      const message: Message = aMessage.with().recipients([LOGGED_USERNAME, 'roy']).and.sender('roy').asIs();

      const description = messages.describeRecipientsOf(message);

      expect(description).toBe("You");
    });

    it("should handle a message sent to one other contact only only", () => {

      (<jasmine.Spy>sessionService.session).and.returnValue(
        aSession(LOGGED_USERNAME, [aContact.with().pseudo('redmond').asIs()]));

      const message: Message = aMessage.with().recipients([LOGGED_USERNAME, 'redmond']).and.sender(LOGGED_USERNAME).asIs();

      const description = messages.describeRecipientsOf(message);

      expect(description).toBe("redmond");
    });

    it("should handle a message that includes participants not connected to the logged user", () => {

      (<jasmine.Spy>sessionService.session).and.returnValue(
        aSession(LOGGED_USERNAME, [aContact.with().pseudo('roy').and.displayName('ROY').asIs()]));

      const message: Message = aMessage.with().recipients([LOGGED_USERNAME, 'roy', 'redmond']).and.sender(LOGGED_USERNAME).asIs()

      const description = messages.describeRecipientsOf(message);

      expect(description).toBe("ROY,redmond");
    });
  });

  describe(", when describing the sender of a message,", () => {

    beforeEach(() => {
      (<jasmine.Spy>sessionService.pseudo).and.returnValue(LOGGED_USERNAME);
    });

    it("should handle messages where logged user is the sender", () => {

      const message: Message = aMessage.with().title("title").content("content").sender(LOGGED_USERNAME)
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('message ID').recipients(['sender', 'receiver]']).asIs();
      const description: string = messages.describeSenderOf(message);

      expect(description).toBe('You');
    });

    it("should handle messages where sender is one of the logged user's contacts", () => {

      (<jasmine.Spy>sessionService.contact).and.returnValue(aContact.with().pseudo('roy').asIs());

      const message: Message = aMessage.with().title("title").content("content").sender('roy')
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('message ID').recipients(['sender', 'receiver]']).asIs();
      const description: string = messages.describeSenderOf(message);

      expect(description).toBe('roy');
    });

    it("should handle the case where the sender is no longer a contact of the logged user", () => {

      (<jasmine.Spy>sessionService.contact).and.returnValue(undefined);

      const message: Message = aMessage.with().title("title").content("content").sender('charles')
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('message ID').recipients(['sender', 'receiver]']).asIs();
      const description: string = messages.describeSenderOf(message);

      expect(description).toBe('charles');
    });
  });

  // it("should return the message's participants as contacts, excluding those specified as 'excluded'", () => {
  //
  //   const message: Message = aMessage.with().title("title").content("content").sender("sender").sendDate(1)
  //     .participants({
  //       sender: 'sender conversation ID',
  //       known: 'known conversation ID',
  //       unknown: 'unknown conversation ID'
  //     }).asIs();
  //
  //   (<jasmine.Spy>sessionService.session).and.returnValue(
  //     aSession(LOGGED_USERNAME, [aContact.with().pseudo("known").and.displayName(undefined).asIs()]));
  //
  //   const participants: Contact[] = messages.getParticipantsOf(message, ['sender']);
  //
  //   expect(participants.length).toEqual(1);
  //   expect(participants[0].pseudo()).toEqual('known');
  // });

  it("should return the message's recipients as contacts, excluding those specified as 'excluded'", () => {

    let message: Message = aMessage.with().order(1).and.recipients(['sender', 'receiver1', 'receiver2']).asIs();

    (<jasmine.Spy>sessionService.session).and.returnValue(
      aSession(LOGGED_USERNAME, [
        aContact.with().pseudo('receiver1').noBlanks(),
        aContact.with().pseudo('receiver2').noBlanks()
      ]));

    const recipients: Contact[] = messages.getRecipientsOf(message, ['receiver1']);

    expect(recipients.length).toEqual(1);
    expect(recipients[0].pseudo()).toEqual('receiver2');
  });

  it("should list a conversation's messages", (done: DoneFn) => {

    (messagesCache.listMessages as jasmine.Spy).and.returnValue(of(MESSAGE));

    const conversation: Conversation = aConversation.with().message(MESSAGE).noBlanks();
    messages.listMessages(conversation, PAGE, PAGE_SIZE)
      .subscribe((message: Message) => {
        expect(message).toEqual(MESSAGE);
        done();
      });
  });

  // it("should list unread messages", (done: DoneFn) => {
  //
  //   (messagesCache.listUnreadMessages as jasmine.Spy).and.returnValue(of(MESSAGE));
  //   (conversationsStorageService.updateConversationWithMessage as jasmine.Spy).and.returnValue(of(null));
  //
  //   messages.listUnreadMessages()
  //     .subscribe((message: Message) => {
  //
  //       expect(message).toEqual(MESSAGE);
  //
  //       expect((<jasmine.Spy>conversationsStorageService.updateConversationWithMessage).calls.allArgs()[0][0])
  //         .toEqual(MESSAGE);
  //       expect((<jasmine.Spy>conversationsStorageService.updateConversationWithMessage).calls.allArgs()[0][1])
  //         .toEqual(false);
  //
  //       done();
  //     });
  // });

  describe(", when sending a message", () => {

    const SESSION: Session = aSession('alice', []);

    beforeEach(() => {
      (<jasmine.Spy>sessionService.session).and.returnValue(SESSION);
    });

    it("should send a new conversation's first message", (done: DoneFn) => {

      const generatedMessage: Message = aMessage.with().title("title").content("content").sender('roy')
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('message ID').recipients(['sender', 'receiver]']).asIs();
      (<jasmine.Spy>messagesCache.storeMessage).and.returnValue(of(generatedMessage));

      const generatedConversation: Conversation = Conversation.with('conversation ID', generatedMessage);
      (<jasmine.Spy>conversationsStorageService.updateConversationWithMessage).and
        .returnValue(of(generatedConversation));

      const tos: Contact[] = [aContact.with().pseudo('receiver').and.displayName('Receiver').asIs()];
      messages.sendMessage('title', 'content', tos)
        .subscribe((sentMessage: Message) => {

          expect(sentMessage).toEqual(generatedMessage);

          done();
        });
    });

    it("should send a reply to an existing conversation", (done: DoneFn) => {

      const generatedMessage: Message = aMessage.with().title("title").content("content").sender('roy')
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('reply message ID').recipients(['sender', 'receiver]']).asIs();
      (<jasmine.Spy>messagesCache.storeMessage).and.returnValue(of(generatedMessage));

      const generatedConversation: Conversation = Conversation.with('conversation ID', generatedMessage);
      (<jasmine.Spy>conversationsStorageService.updateConversationWithMessage).and
        .returnValue(of(generatedConversation));

      const existingMessage: Message = aMessage.with().title("title").content("content").sender('roy')
        .sendDate(Date.now()).participants({"sender": "a", "receiver": "b"}).order(1).conversationId("conversation_ID")
        .messageId('existing message ID').recipients(['sender', 'receiver]']).asIs();
      const conversation: Conversation = Conversation.with('conversation ID', existingMessage);
      const tos: Contact[] = [aContact.with().pseudo('receiver').and.displayName('Receiver').asIs()];
      messages.sendMessage('title', 'content', tos, conversation)
        .subscribe((sentMessage: Message) => {

          expect(sentMessage).toEqual(generatedMessage);

          done();
        });
    });
  });

  function aSession(pseudo: string, contacts: Contact[]) {
    return Session.build(null, null, Contacts.fromArrayOfContacts(contacts), null, null, null, null, null, null, pseudo, null);
  }
});
