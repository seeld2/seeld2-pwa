import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {concat} from "rxjs/observable/concat";
import {forkJoin} from "rxjs/observable/forkJoin";
import {finalize, map, mergeMap, toArray} from "rxjs/operators";
import {Locks} from "../../../core/locks";
import {Lock} from "../../../shared/concurrency/lock";
import {SessionService} from "../../sessions/session.service";
import {TimeService} from "../../time/time.service";
import {Contact} from "../../user/contact/contact";
import {Contacts} from "../../user/contact/contacts";
import {Logger} from "../../utils/logging/logger";
import {Conversation} from "../conversation";
import {ConversationsStorageService} from "../conversations-storage.service";
import {Message} from "./message";
import {MessagesCache} from "./messages-cache";
import {MessagesRemote} from "./messages-remote";
import {MessagesStorageService} from "./messages-storage.service";

@Injectable()
export class MessagesService {

  private readonly logger: Logger = Logger.for("Messages");

  constructor(private readonly conversationsStorageService: ConversationsStorageService,
              private readonly messagesCache: MessagesCache,
              private readonly messagesRemote: MessagesRemote,
              private readonly messagesStorageService: MessagesStorageService,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService) {
  }

  clearCache(conversationId: string) {
    this.messagesCache.clearCache(conversationId);
  }

  deleteMessages(messagesToDelete: Message[]): Observable<any> {
    const conversationId: string = messagesToDelete[0].conversationId();

    return Locks.getMessagesLock('Messages.deleteMessages')
      .pipe(
        mergeMap((lock: Lock) => {

          try {
            const obs: Observable<any>[] = messagesToDelete
              .map((messageToDelete: Message) => this.messagesCache.deleteMessage(messageToDelete));

            return forkJoin(obs)
              .pipe(
                toArray(), // This is to ensure that finalize is run once when all deletions are done
                mergeMap(() => {
                  const messages: Message[] = this.messagesCache.getMessagesFromCache(conversationId, 0, 1);
                  return this.conversationsStorageService.updateConversationWithMessage(messages[0], true);
                }),
                finalize(() => lock.unlock())
              );

          } catch (e) {
            this.logger.error(lock.getOwner(), e);
            lock.unlock();
          }
        })
      );
  }

  /**
   * Returns a string that describes the participants of the specified message.
   * The message's participants that are NOT contacts of the logged user will be ignored.
   *
   * @param message The message whose participants must be described
   * @param excluding An array of usernames to be excluded from the list of participants
   */
  describeParticipantsOf(message: Message, excluding: string[] = []): string {

    const participantsUsernames: string[] = message.participantsPseudos(excluding);

    const connectedParticipants: Contact[] = this.sessionService.session().contactsAsArraySortedByDescription(participantsUsernames);

    const unknownParticipants: string[] = participantsUsernames.filter((username: string) => {
      const contactsPseudos: string[] = Contacts.fromArrayOfContacts(connectedParticipants).pseudos();
      return contactsPseudos.indexOf(username) < 0;
    });

    let description: string = Contacts.fromArrayOfContacts(connectedParticipants).describeContacts();
    if (unknownParticipants.length > 0) {
      if (!!description && description.length > 0) {
        description += ",";
      }
      description += unknownParticipants.join(',');
    }
    return description;
  }

  /**
   * Returns a string that describes the recipients of the specified message.
   * The message's recipients that are NOT contacts of the logged user will be ignored.
   *
   * @param message The message whose participants must be described
   */
  describeRecipientsOf(message: Message): string {

    const recipientsPseudos: string[] = message.recipients().slice()
      .filter((pseudo: string) => pseudo !== message.sender())
      .filter((pseudo: string) => pseudo !== this.sessionService.session().pseudo());

    let description: string = "";

    if (message.sender() !== this.sessionService.session().pseudo()) {
      description += "You";
    }

    if (recipientsPseudos.length > 0) {

      const connectedRecipients: Contact[] = this.sessionService.session().contactsAsArraySortedByDescription(recipientsPseudos);
      if (connectedRecipients.length > 0) {
        if (description.length > 0) {
          description += ",";
        }
        description += Contacts.fromArrayOfContacts(connectedRecipients).describeContacts();
      }

      const unknownParticipantsPseudos: string[] = recipientsPseudos.filter((username: string) => {
        const contactsPseudos: string[] = Contacts.fromArrayOfContacts(connectedRecipients).pseudos();
        return contactsPseudos.indexOf(username) < 0;
      });
      if (unknownParticipantsPseudos.length > 0) {
        if (description.length > 0) {
          description += ","
        }
        description += unknownParticipantsPseudos.join(",");
      }
    }

    return description;
  }

  describeSenderOf(message: Message): string {
    if (message.sender() === this.sessionService.pseudo()) {
      return 'You';
    } else {
      const senderContact: Contact = this.sessionService.contact(message.sender());
      return senderContact ? senderContact.description() : message.sender();
    }
  }

  // TODO Test
  fetchUnreadMessages(): Observable<Message> {
    this.logger.trace('fetchUnreadMessages');
    return this.messagesRemote.fetchUnreadMessages()
      .pipe(
        mergeMap((message: Message) => this.messagesStorageService.storeMessageInStorage(message)),
        mergeMap((message: Message) => this.conversationsStorageService.updateConversationWithMessage(message, false)
          .pipe(map(() => message))
        ),
      );
  }

  /**
   * Returns the recipients of the specified message.
   * The message's recipients that are NOT contacts of the logged user will be ignored.
   *
   * @param message The message whose recipients must be extracted
   * @param excluding An array of pseudos to be excluded from the list of recipients
   */
  getRecipientsOf(message: Message, excluding: string[] = []): Contact[] {
    const tosPseudos: string[] = message.recipientsPseudos(excluding);
    return this.sessionService.session().contactsAsArraySortedByDescription(tosPseudos);
  }

  // TODO Test
  fetchPage(conversation: Conversation, page: number, pageSize: number): Observable<Message> {
    this.logger.trace(`fetchPage(${conversation.conversationId()}, ${page}, ${pageSize})`);

    const fetchFromStorageObs = this.messagesStorageService
      .listMessagesFromStorage(conversation.conversationId(), page, pageSize);

    const fetchFromServerObs = this.messagesRemote.fetchPage(conversation, page, pageSize)
      .pipe(
        mergeMap((message: Message) => this.messagesStorageService.storeMessageInStorage(message)),
      );

    return concat(fetchFromStorageObs, fetchFromServerObs);
  }

  // TODO Test
  fetchPageFromStorage(conversationId: string, page: number, pageSize: number): Observable<Message> {
    this.logger.trace(`fetchPageFromStorage(${conversationId}, ${page}, ${pageSize})`);
    return this.messagesStorageService.listMessagesFromStorage(conversationId, page, pageSize);
  }

  /**
   * Lists a conversation's messages.
   */
  listMessages(conversation: Conversation,
               page: number,
               pageSize: number): Observable<Message> {
    this.logger.trace(`listMessages(${conversation.conversationId()}, ${page}, ${pageSize})`);

    return Locks.getMessagesLock('Messages.listMessages')
      .pipe(
        mergeMap((lock: Lock) => {

          try {
            return this.messagesCache.listMessages(conversation, page, pageSize)
              .pipe(
                finalize(() => lock.unlock())
              );
          } catch (e) {
            this.logger.error(lock.getOwner(), e);
            lock.unlock();
          }
        })
      );
  }

  /**
   * Sends a message to the specified recipients.
   * The message will be a reply on the specified conversation, or the beginning of a new conversation if no
   * conversation ID is specified.
   */
  sendMessage(title: string, content: string, tos: Contact[], conversation: Conversation = null):
    Observable<Message> {

    return Locks.getMessagesLock('Messages.sendMessage')
      .pipe(
        mergeMap((lock: Lock) => {

          try {

            const now: number = this.timeService.nowAdjusted();

            let message: Message;
            const pseudo = this.sessionService.session().pseudo();
            if (conversation) {
              message = conversation.reply(content, pseudo, now, tos);
            } else {
              message = Message.newMessage(title, content, pseudo, now, tos);
            }

            return this.messagesCache.storeMessage(message)
              .pipe(
                mergeMap((message: Message) => {
                  return this.conversationsStorageService.updateConversationWithMessage(message, true);
                }),

                map((conversation: Conversation) => {
                  return conversation.message();
                }),

                finalize(() => lock.unlock()),
              );

          } catch (e) {
            this.logger.error(lock.getOwner(), e);
            lock.unlock();
          }
        })
      );
  }
}
