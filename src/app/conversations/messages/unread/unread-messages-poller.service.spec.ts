import {Events} from "ionic-angular";
import {from} from "rxjs/observable/from";
import {CoreJasmine} from "../../../../core/core-jasmine";
import {Locks} from "../../../../core/locks";
import {Lock} from "../../../../shared/concurrency/lock";
import {IonicJasmine} from "../../../../shared/ionic-jasmine";
import {Session} from "../../../sessions/session";
import {SessionService} from "../../../sessions/session.service";
import {SessionsJasmine} from "../../../sessions/sessions.jasmine";
import {UiNotifications} from "../../../uinotifications/ui-notifications";
import {UiNotificationsJasmine} from "../../../uinotifications/ui-notifications.jasmine";
import {Contact} from "../../../user/contact/contact";
import {ContactKey} from "../../../user/contact/contact-key";
import {Contacts} from "../../../user/contact/contacts";
import {ConversationsJasmine} from "../../conversations.jasmine";
import {ConversationsService} from "../../conversations.service";
import {Message} from "../message";
import {MessagesService} from "../messages.service";
import {UnreadMessagesPollerService} from "./unread-messages-poller.service";

describe("UnreadMessagesPollerService", () => {

  let conversations: ConversationsService;
  let events: Events;
  let messages: MessagesService;
  let notifications: UiNotifications;
  let sessionService: SessionService;

  let unreadMessagesPollerService: UnreadMessagesPollerService;

  beforeEach(() => {
    conversations = ConversationsJasmine.conversationsService();
    events = IonicJasmine.events();
    messages = CoreJasmine.messages();
    notifications = UiNotificationsJasmine.uiNotifications();
    sessionService = SessionsJasmine.sessionService();

    unreadMessagesPollerService = new UnreadMessagesPollerService(conversations, events, messages, notifications,
      sessionService);

    (<jasmine.Spy>sessionService.session).and.returnValue(aSession(null, [aContact("sender")]));
  });

  // describe(", when polling unread messages,", () => {
  //
  //   it("should fetch unread messages by querying the server", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(true);
  //     (<jasmine.Spy>messages.listUnreadMessages).and.returnValue(from([
  //       message("t1", "c1", "sender"),
  //       message("t2", "c2", "sender")
  //     ]));
  //
  //     unreadMessagesPollerService.pollUnreadMessages()
  //       .subscribe((unreadMessages: number) => {
  //
  //         expect(messages.listUnreadMessages).toHaveBeenCalled();
  //         expect(notifications.showOnDevice).toHaveBeenCalled();
  //
  //         expect(events.publish).toHaveBeenCalledTimes(3);
  //         expect(conversations.countConversationsWithUnreadMessagesInStorage).toHaveBeenCalled();
  //
  //         expect(unreadMessages).toEqual(2);
  //
  //         done();
  //       });
  //   });
  //
  //   it("should skip polling messages if session is not initialized", (done: DoneFn) => {
  //
  //     (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(false);
  //
  //     unreadMessagesPollerService.pollUnreadMessages()
  //       .subscribe(
  //         undefined,
  //         undefined,
  //         // complete
  //         () => {
  //
  //           expect(messages.listUnreadMessages).not.toHaveBeenCalled();
  //
  //           done();
  //         });
  //   });
  //
  //   it("should skip polling messages if the messages' lock is currently locked", (done: DoneFn) => {
  //
  //     let l: Lock;
  //
  //     (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(true);
  //     Locks.getMessagesLock('jasmine')
  //       .subscribe(
  //         (lock: Lock) => {
  //           l = lock;
  //
  //           unreadMessagesPollerService.pollUnreadMessages()
  //             .subscribe(
  //               undefined,
  //               undefined,
  //               () => {
  //
  //                 expect(messages.listUnreadMessages).not.toHaveBeenCalled();
  //
  //                 done();
  //               });
  //         },
  //         // error
  //         undefined,
  //         // complete
  //         () => l.unlock()
  //       );
  //   });
  // });

  function aSession(pseudo: string, contacts: Contact[]) {
    return Session.build(null, null, Contacts.fromArrayOfContacts(contacts), null, null, null, null, null, null, pseudo, null);
  }

  function message(title: string, content: string, sender: string) {
    return Message.with(title, content, sender, null, null, null, null);
  }

  function aContact(username: string) {
    return Contact.with(username, null, null, null, ContactKey.with(null, null, null), null);
  }
});
