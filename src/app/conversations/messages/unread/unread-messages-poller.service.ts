import {Injectable} from "@angular/core";
import {Events} from "ionic-angular";
import {Observable} from "rxjs";
import {count, map} from "rxjs/operators";
import {Locks} from "../../../../core/locks";
import {environment} from "../../../../environments/environment";
import {config} from "../../../app.config";
import {NotificationEvent} from "../../../notifications/notification.event";
import {UnreadMessagesPoller} from "../../../notifications/unread-messages-poller";
import {SessionService} from "../../../sessions/session.service";
import {UiNotifications} from "../../../uinotifications/ui-notifications";
import {Contact} from "../../../user/contact/contact";
import {Logger} from "../../../utils/logging/logger";
import {ConversationsEvent} from "../../conversations-event";
import {ConversationsService} from "../../conversations.service";
import {Message} from "../message";
import {MessagesService} from "../messages.service";

@Injectable()
export class UnreadMessagesPollerService extends UnreadMessagesPoller {

  private readonly logger: Logger = Logger.for('UnreadMessagesPollerService');

  private pollingInterval = environment.messages.polling.interval;
  private timeoutId = null;

  constructor(private readonly conversationsService: ConversationsService,
              private readonly events: Events,
              private readonly messages: MessagesService,
              private readonly uiNotifications: UiNotifications,
              private readonly sessionService: SessionService) {
    super();
  }

  schedulePolling() {
    this.logger.trace("schedulePolling()");

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(() => {
      this.pollUnreadMessages()
        .subscribe(
          //next
          () => {
          },
          // error
          ((error) => this.logger.error('Error while attempting to fetch unread messages', error)),
          // complete
          () => this.schedulePolling()
        );
    }, this.pollingInterval);
  }

  pollUnreadMessages(): Observable<number> {
    this.logger.trace("pollUnreadMessages()");

    if (this.sessionService.isSessionInitialized() && Locks.isMessagesLockAvailable()) {

      return this.messages.fetchUnreadMessages()
        .pipe(
          map((message: Message) => {

            const sender: Contact = this.sessionService.session().contactsAsArray()
              .find((contact: Contact) => message.sender() === contact.pseudo());

            const notificationContent: string = message.content().length >= config.ui.notifications.textLength
              ? message.content().substring(0, config.ui.notifications.textLength - 3) + '...'
              : message.content();

            this.uiNotifications.showOnDevice(`${sender.description()}: ${notificationContent}`);

            return message;
          }),

          count(),

          map((unreadMessages: number) => {
            this.logger.debug(`${unreadMessages} unread messages found by poller service`);

            if (unreadMessages === 0) {
              return 0;
            }

            this.events.publish(NotificationEvent.DETECTED_NEW_MESSAGES);
            this.events.publish(
              ConversationsEvent.REFRESH_NEW_CONVERSATIONS_COUNT,
              this.conversationsService.countConversationsWithUnreadMessagesInStorage()
            );

            return unreadMessages;
          })
        );
    }

    return Observable.empty();
  }

  switchToFallbackInterval() {
    this.pollingInterval = environment.messages.polling.fallbackInterval;
    this.schedulePolling();
  }

  switchToNormalInterval() {
    this.pollingInterval = environment.messages.polling.interval;
    this.schedulePolling();
  }
}
