import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {Uint8Arrays} from "../../utils/uint8-arrays";
import {map} from "rxjs/operators";
import {Random} from "../random/random";

declare var base64js: any;
declare var TextDecoder: any;
declare var TextEncoder: any;

export class BlockCipherMode {

  public static readonly GCM: BlockCipherMode = new BlockCipherMode('A256GCM', 'AES-GCM', 256);

  private constructor(public readonly alg: string,
                      public readonly name: string,
                      public readonly length: number) {
  }
}

export class AES {

  private static readonly IV_LENGTH = 12;

  private static readonly EXPORTABLE = true;
  private static readonly NOT_EXPORTABLE = false;

  private static readonly JWK_ALGORITHMS_MAP = {
    'A256GCM': BlockCipherMode.GCM
  };

  /**
   * Build an instance of AES using the specified block cypher mode of operation, with a randomly-generated key.
   */
  static build(blockCipherMode: BlockCipherMode): Observable<AES> {
    const aes: AES = new AES(blockCipherMode);
    return from(
      window.crypto.subtle
        .generateKey(aes.algorithm, AES.EXPORTABLE, ['encrypt', 'decrypt'])
        .then(key => {
          aes.key = key;
          return aes;
        }));
  }

  /**
   * Build an instance of AES with the specified "base64" key and block cypher mode of operation.
   */
  static fromBase64Key(base64Key: string, blockCypherMode: BlockCipherMode): Observable<AES> {
    return AES.fromJwk({kty: 'oct', k: base64Key, alg: blockCypherMode.alg});
  }

  /**
   * Build an instance of AES with the specified "hex" key and block cypher mode of operation.
   */
  static fromHexKey(hexKey: string, blockCypherMode: BlockCipherMode): Observable<AES> {

    let hexKeyBytes = [];
    for (let charIndex = 0; charIndex < hexKey.length; charIndex += 2) {
      hexKeyBytes.push(parseInt(hexKey.substr(charIndex, 2), 16));
    }

    const base64Key: string = AES.urlFriendly(base64js.fromByteArray(hexKeyBytes))
      .replace(/=/gi, '');

    return AES.fromBase64Key(base64Key, blockCypherMode);
  }

  /**
   * Build an instance of AES using the specified JsonWebKey.
   */
  static fromJwk(jwk: JsonWebKey): Observable<AES> {
    const algorithm: any = AES.JWK_ALGORITHMS_MAP[jwk.alg];
    if (!algorithm) {
      throw new Error('Unknown algorithm ' + jwk.alg);
    }

    const aes: AES = new AES(algorithm);

    return from(
      window.crypto.subtle
        .importKey("jwk", jwk, algorithm, AES.NOT_EXPORTABLE, ["encrypt", "decrypt"])
        .then(key => {
          aes.key = key;
          return aes;
        }));
  }

  /**
   * Expresses the specified key, for the specified algorithm, as a valid JsonWekKey object.
   */
  static jsonWebKeyOf(key: string, algorithm: BlockCipherMode): JsonWebKey {
    return {kty: 'oct', k: key, alg: algorithm.alg};
  }

  static urlFriendly(text: string): string {
    return text.replace(/\+/gi, '-').replace(/\//gi, '_');
  }

  private key: CryptoKey | CryptoKeyPair;

  constructor(private algorithm: any) {
  }

  decrypt(array: Uint8Array): Observable<string> {

    const iv = array.subarray(0, AES.IV_LENGTH);
    const encrypted = array.subarray(AES.IV_LENGTH);

    return from(
      window.crypto.subtle.decrypt(
        {name: this.algorithm.name, iv: iv, tagLength: 128},
        this.key as CryptoKey,
        encrypted

      ).then(unencrypted => {
        // noinspection TypeScriptValidateJSTypes
        return new TextDecoder().decode(unencrypted);
      }));
  }

  decryptFromText(base64text: string): Observable<string> {
    const array: Uint8Array = base64js.toByteArray(base64text);
    return this.decrypt(array);
  }

  encrypt(text: string): Observable<Uint8Array> {

    const iv: Uint8Array = Random.generateUint8Array(AES.IV_LENGTH);

    return from(

      window.crypto.subtle.encrypt(
        {name: this.algorithm.name, iv: iv, tagLength: 128},
        this.key as CryptoKey,
        new TextEncoder().encode(text)

      ).then(encrypted => {
        return Uint8Arrays.concat(iv, new Uint8Array(encrypted));
      }));
  }

  encryptAsText(text: string): Observable<string> {
    return this.encrypt(text)
      .pipe(
        map((array: Uint8Array) => {
          return base64js.fromByteArray(array);
        })
      );
  }

  exportKey(): Observable<JsonWebKey> {
    return from(window.crypto.subtle.exportKey("jwk", this.key as CryptoKey));
  }
}
