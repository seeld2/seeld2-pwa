import {Objects} from "../utils/objects";
import {AES} from "./aes/aes";

export class CryptoJasmine {

  static exchangeAes() {
    return jasmine.createSpyObj('exchangeAes', Objects.propertyNamesOf(new AES(null)));
  }

  static openPgp() {
    return jasmine.createSpyObj('openPgp', ['decrypt', 'decryptNoVerify', 'encrypt']);
  }

  static storageAes() {
    return jasmine.createSpyObj('storageAes', Objects.propertyNamesOf(new AES(null)));
  }
}
