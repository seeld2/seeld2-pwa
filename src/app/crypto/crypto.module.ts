import {NgModule} from "@angular/core";
import {UtilsModule} from "../utils/utils.module";

@NgModule({
  imports: [
    UtilsModule
  ]
})
export class CryptoModule {
}
