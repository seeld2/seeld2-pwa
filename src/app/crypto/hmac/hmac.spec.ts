import {mergeMap} from "rxjs/operators";
import {Contact} from "../../user/contact/contact";
import {Hmac} from "./hmac";

describe("HMAC", () => {

  const SECRET_KEY: string = "secretKey";

  it("should generate a random secret key", () => {
    const secretKey: string = Contact.generateSecretKey();
    expect(secretKey.length).toEqual(64);
  });

  it("should return a key for a specified secret key string ", (done: DoneFn) => {
    Hmac.keyFor(SECRET_KEY).subscribe((key: CryptoKey) => {
      expect(key).toBeDefined();
      done();
    });
  });

  it("should hash the specified text using the specified secret key", (done: DoneFn) => {
    Hmac.keyFor(SECRET_KEY)
      .pipe(
        mergeMap((key: CryptoKey) => Hmac.hash("text to hash", key))
      )
      .subscribe((hash: string) => {
        expect(hash).toEqual('/ZHQG3+FudyUv99fv0QeDkji20/kuS16dAvuXTfbcyEaDKPGtBUG2pOjOReanSThKDDRv4+yUnsjml2fZYYKjg==');
        done();
      });
  });
});
