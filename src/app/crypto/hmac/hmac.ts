import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {map} from "rxjs/operators";
import {Random} from "../random/random";

declare var base64js: any;
declare var TextEncoder: any;

export class Hmac {

  static generateSecretKey(): string {
    return Random.generateBase64String(64);
  }

  static keyFor(secretKey: string): Observable<CryptoKey> {
    return from(window.crypto.subtle.importKey(
      "raw",
      new TextEncoder().encode(secretKey),
      {name: "HMAC", hash: {name: "SHA-512"}},
      false,
      ["sign"])
    );
  }

  static hash(text: string, hmacKey: CryptoKey): Observable<string> {
    const encodedData = new TextEncoder().encode(text);
    return from(window.crypto.subtle.sign("HMAC", hmacKey, encodedData))
      .pipe(
        map((encodedHashedData: ArrayBuffer) => base64js.fromByteArray(new Uint8Array(encodedHashedData)))
      );
  }
}
