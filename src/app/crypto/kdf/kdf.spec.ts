import {KDF} from "./kdf";
import {config} from "../../app.config";

describe("KDF", () => {

  const DERIVED_KEY = 'fad57c1fcb802c75107d0dc74edb99c4357d69822872aa4408a792377fa960352286159edf0982d36805221f980d10a0606170ec8ed6c3788e20eb90af484a3e';
  const PASSPHRASE = "Such a cool passphrase!";
  const SALT = '64longsalt123456789012345678901234567890123456789012345678901234';

  let kdf: KDF;

  beforeEach(() => {
    kdf = new KDF(config.kdf);
  });

  it("should provide an Observable that emits one derivation", (done: DoneFn) => {

    kdf.deriveKey(PASSPHRASE, SALT).subscribe((derivedKey) => {

      expect(derivedKey).toBeDefined();
      expect(derivedKey).toEqual(DERIVED_KEY);

      done();
    });
  });
});
