import {Observable} from "rxjs";

import * as scrypt from "scrypt-async";

export class KDF {

  constructor(private options: any) {
  }

  deriveKey(passphrase: string, salt: string): Observable<string> {
    return new Observable(subscriber => {
      const options = {
        N: this.options.N,
        r: this.options.r,
        p: this.options.p,
        dkLen: this.options.dkLen,
        encoding: this.options.encoding
      };

      scrypt(passphrase, salt, options, (derivedKey) => {
        subscriber.next(derivedKey);
        subscriber.complete();
      });
    });
  }
}
