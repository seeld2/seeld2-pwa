export interface ArmoredKeyPair {
  publicKeys: string;
  privateKeys: string;
}
