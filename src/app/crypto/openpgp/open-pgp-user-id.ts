export interface OpenPgpUserId {
  email: string
  name: string,
}
