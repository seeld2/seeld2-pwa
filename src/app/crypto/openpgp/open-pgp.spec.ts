import {OpenPgp} from "./open-pgp";
import {ArmoredKeyPair} from "./armored-key-pair";
import {flatMap} from "rxjs/operators";
import {OpenPgpUserId} from "./open-pgp-user-id";

describe('Given OpenPgp', () => {

  const textToEncrypt = 'text to encrypt';

  const alicePassphrase = 'passphrase';
  const aliceUserId: OpenPgpUserId = {email: 'alice@test.com', name: 'alice'};
  const bobPassphrase = 'bob passphrase';
  // noinspection JSUnusedLocalSymbols
  const bobUserId: OpenPgpUserId = {email: 'bob@test.com', name: 'bob'};

  it('should generate armored keys', (done: DoneFn) => {
    OpenPgp.generateKeys(aliceUserId, alicePassphrase)
      .subscribe((armoredKeyPair: ArmoredKeyPair) => {
        expect(armoredKeyPair).toBeDefined();
        expect(armoredKeyPair.privateKeys).toBeDefined();
        expect(armoredKeyPair.publicKeys).toBeDefined();
        // console.log(armoredKeyPair.privateKeys);
        // console.log(armoredKeyPair.publicKeys);
        done();
      })
  });

  it('should provide an instance from the armored keys and the passphrase', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: aliceArmoredPrivateKeys, publicKeys: aliceArmoredPublicKeys}, alicePassphrase)
      .subscribe((openPgp: OpenPgp) => {
        expect(openPgp).toBeDefined();
        expect(openPgp['privateKeys']).toBeDefined();
        expect(openPgp['publicKeys']).toBeDefined();
        done();
      });
  });

  it('should self-encrypt a text message', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: aliceArmoredPrivateKeys, publicKeys: aliceArmoredPublicKeys}, alicePassphrase)
      .pipe(
        flatMap((openPgp: OpenPgp) => {
          return openPgp.encrypt(textToEncrypt);
      }))
      .subscribe((armoredMessage: string) => {
        expect(armoredMessage).toBeDefined();
        // console.log(armoredMessage);
        done();
      });
  });

  it('should decrypt an self-encrypted text message', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: aliceArmoredPrivateKeys, publicKeys: aliceArmoredPublicKeys}, alicePassphrase)
      .pipe(
        flatMap((openPgp: OpenPgp) => {
          return openPgp.decrypt(aliceArmoredSelfEncryptedMessage);
        }))
      .subscribe((decryptedText: string) => {
        expect(decryptedText).toEqual(textToEncrypt);
        done();
      });
  });

  it('should encrypt a text message addressed to a receiver', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: aliceArmoredPrivateKeys, publicKeys: aliceArmoredPublicKeys}, alicePassphrase)
      .pipe(
        flatMap((openPgp: OpenPgp) => {
          return openPgp.encrypt(textToEncrypt, bobArmoredPublicKeys);
        }))
      .subscribe((armoredMessage: string) => {
        expect(armoredMessage).toBeDefined();
        // console.log(armoredMessage);
        done();
      });
  });

  it('should decrypt an encrypted message from a sender', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: bobArmoredPrivateKeys, publicKeys: bobArmoredPublicKeys}, bobPassphrase)
      .pipe(
        flatMap((openPgp: OpenPgp) => {
          return openPgp.decrypt(aliceArmoredEncryptedMessageToBob, aliceArmoredPublicKeys);
        }))
      .subscribe((decryptedText: string) => {
        expect(decryptedText).toEqual(textToEncrypt);
        done();
      });
  });

  it('should decrypt an encrypted message without verifying its signature', (done: DoneFn) => {
    OpenPgp.fromArmoredKeyPair({privateKeys: bobArmoredPrivateKeys, publicKeys: bobArmoredPublicKeys}, bobPassphrase)
      .pipe(
        flatMap((openPgp: OpenPgp) => {
          return openPgp.decryptNoVerify(aliceArmoredEncryptedMessageToBob);
        }))
      .subscribe((decryptedText: string) => {
        expect(decryptedText).toEqual(textToEncrypt);
        done();
      });
  });

  const aliceArmoredPrivateKeys: string = `-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

xYYEXWWSZhYJKwYBBAHaRw8BAQdAbe+5/S7m5T/HfxpsaD8J9qVACLrWiTVr
IGtBUefaBkv+CQMI3mCFcdP5hrLgRAqmAEqKkIwz7KfkJtfxP2cVOvtvcoNq
www4rBnHeIo6hgDVcel668KOuSH/+ZBm07pk0W8A2D+E4GcB3JuywemoORpa
Lc0WYWxpY2UgPGFsaWNlQHRlc3QuY29tPsJ3BBAWCgAfBQJdZZJmBgsJBwgD
AgQVCAoCAxYCAQIZAQIbAwIeAQAKCRA6fs6/wAMlJHDnAQCwXwN4dxXB5jV/
p+tC9dx+OlZw18t3gv5KZ4izpddstQD/d0J3kpwbTr0lXnQ//+Wf7RCna/NL
6kazuIcsmIPcPQ3HiwRdZZJmEgorBgEEAZdVAQUBAQdAhf/aoCy69DRCnHRJ
yA7V4RZ3GefZVU33+tpOLyfRCnIDAQgH/gkDCMspJiJgDhHY4PD6VUBCpY47
CGMD61zMI0gUVy35KDq4E8fOwPKZZqd45ELDrXtfKarkcuplJdGDH1G6DGh6
XiNC3pEdPklh4ExwkxQwhYPCYQQYFggACQUCXWWSZgIbDAAKCRA6fs6/wAMl
JPUcAP9XwkhFK8v/qhGWfxfRtieBzIAPI4Hdkl0VwX0E5Zc0OQEAqwBUoBP9
rd+oKDcRhBkxvHLyewk2ZeTSWzI6lpeeiwc=
=gjQE
-----END PGP PRIVATE KEY BLOCK-----
`;

  const aliceArmoredPublicKeys: string = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

xjMEXWWSZhYJKwYBBAHaRw8BAQdAbe+5/S7m5T/HfxpsaD8J9qVACLrWiTVr
IGtBUefaBkvNFmFsaWNlIDxhbGljZUB0ZXN0LmNvbT7CdwQQFgoAHwUCXWWS
ZgYLCQcIAwIEFQgKAgMWAgECGQECGwMCHgEACgkQOn7Ov8ADJSRw5wEAsF8D
eHcVweY1f6frQvXcfjpWcNfLd4L+SmeIs6XXbLUA/3dCd5KcG069JV50P//l
n+0Qp2vzS+pGs7iHLJiD3D0NzjgEXWWSZhIKKwYBBAGXVQEFAQEHQIX/2qAs
uvQ0Qpx0ScgO1eEWdxnn2VVN9/raTi8n0QpyAwEIB8JhBBgWCAAJBQJdZZJm
AhsMAAoJEDp+zr/AAyUk9RwA/1fCSEUry/+qEZZ/F9G2J4HMgA8jgd2SXRXB
fQTllzQ5AQCrAFSgE/2t36goNxGEGTG8cvJ7CTZl5NJbMjqWl56LBw==
=Dcaj
-----END PGP PUBLIC KEY BLOCK-----
`;

  const bobArmoredPrivateKeys: string = `-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

xYYEXWWS1xYJKwYBBAHaRw8BAQdAyVI0n+bYK3fVkgrfx1y/WS1+81fG1tmu
H5UwpvLfGlT+CQMIOMAyMsT0+C7gEPPouwItDZUPOHWyT5B1MD2xVR+shV1+
1YpPhvy9ycRu25FP+vI6JIkES/DwAG1nG/MUw47ggISIbzJA6omP0VWk7geq
js0SYm9iIDxib2JAdGVzdC5jb20+wncEEBYKAB8FAl1lktcGCwkHCAMCBBUI
CgIDFgIBAhkBAhsDAh4BAAoJEJ5GN2yg4rqrOMoBAOinJfc+juSBcwXQGbCz
GXD7bW6QodJE7V8ZyTo/wLNXAP0f5tSChNlNVtCyz2lfssCwJwEq60VRy55M
zZS0yDzmAceLBF1lktcSCisGAQQBl1UBBQEBB0DCDdxjySfZfrBVnX7vu+XB
KxHqBJak++YWQF0x7hM4QwMBCAf+CQMI2/uvIQN4ZcPgG7M+854PahBaEZkJ
W+0wYydD5nCWfU/ook306puIKB4lAJ82pExUnGwDHVb4TzLMuwDG4eBHcXJZ
p0iJ/byk9MjjTIyFxcJhBBgWCAAJBQJdZZLXAhsMAAoJEJ5GN2yg4rqrnokA
/isgaMRdw5UI9X2pl058hAM5VU8IlKjMp3ZysCZlZDORAP9rb+kxtZqaLgdC
WhFw9YVu2cnT0FyDEBOZidrtrhItBw==
=s45H
-----END PGP PRIVATE KEY BLOCK-----
`;

  const bobArmoredPublicKeys: string = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

xjMEXWWS1xYJKwYBBAHaRw8BAQdAyVI0n+bYK3fVkgrfx1y/WS1+81fG1tmu
H5UwpvLfGlTNEmJvYiA8Ym9iQHRlc3QuY29tPsJ3BBAWCgAfBQJdZZLXBgsJ
BwgDAgQVCAoCAxYCAQIZAQIbAwIeAQAKCRCeRjdsoOK6qzjKAQDopyX3Po7k
gXMF0Bmwsxlw+21ukKHSRO1fGck6P8CzVwD9H+bUgoTZTVbQss9pX7LAsCcB
KutFUcueTM2UtMg85gHOOARdZZLXEgorBgEEAZdVAQUBAQdAwg3cY8kn2X6w
VZ1+77vlwSsR6gSWpPvmFkBdMe4TOEMDAQgHwmEEGBYIAAkFAl1lktcCGwwA
CgkQnkY3bKDiuqueiQD+KyBoxF3DlQj1famXTnyEAzlVTwiUqMyndnKwJmVk
M5EA/2tv6TG1mpouB0JaEXD1hW7ZydPQXIMQE5mJ2u2uEi0H
=I11D
-----END PGP PUBLIC KEY BLOCK-----
`;

  const aliceArmoredSelfEncryptedMessage: string = `-----BEGIN PGP MESSAGE-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

wV4D1REj1kUDMasSAQdA5FzUEx7sLUll60FahfPOqf48cERe6PHtxYLTRz5N
TDQwgPrKiT1eeVgH+ujK4pONEAZ5e6+l7h5bv9dOzFEKxLZKiG+NGMUQ9LHU
GVnjN69b0sA0AT0oNxmZxn0iTIPIn4gyb8uEDQ0iKnd5Si1c1o4oybQW7tFb
gCll7B9AvaKUOmtzerYZ4LztA1gdk6v0ayEe2mFLHGHSzy+xljQuNlBVdRmz
tY64aoDDI2FL9+PoFhKYGnJkCypuMEpGeNRASdrTdf7iwlS/5HDGag9AKk2P
q3cNd1ed8VI6QQXo+c2IDvz6Zn/5d5Hm3Rd+9PenEygkEITNxRh52OkxBBJp
W/w3vhUzTnHwF/8rUSh+zDi6mLGJAEYEgVRilb4M5Hui8qug/NwCo4narvcK
AZ1HGYQaCuVgMyn7u0S1GHgUVJD1wpDN0LSXGQ==
=1O42
-----END PGP MESSAGE-----
`;

  const aliceArmoredEncryptedMessageToBob: string = `-----BEGIN PGP MESSAGE-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

wV4Ds4X/9QZQ/vQSAQdAX6r/raAkAeE11fuCYiCE/D6D416upohUV94IROuu
+lMw7V/pv1uSRsldUnMbPy1oQLOGRRXQqvtQVDDb39/aGEOQUi6R6phekkEi
YLDxatb50sA0AXHiZJIn0NIJUWvG+3aMLU/XnpE9DvZiNcl5UuSyShBfz9C4
ueulaEE+xofsI5Gq/4BPpmWztlVYMDjvBxQ/7G/r+9A85m7r8JGlCW6poYJQ
PlTs05imgvpofkg8x0TgLf9Anna8GjuR10zr0BcMsy55kD12dpXzCIJTM3VI
imvsWBdYsmbY8h5c/kAgQH8AxYEuWJCXr0utxAmBiqexxVJBE7iYZRXKPAI1
EY9UGp6w2esihv0w6ePyysOEb1M55C9ajx+1mo6YIdX341XUaJ81GH2TLpDV
fdZsWvcMHrv/xNb5mttotvAQdrkSDepFsFl5Ow==
=a00t
-----END PGP MESSAGE-----
`;
});
