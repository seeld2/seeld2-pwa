import {ArmoredKeyPair} from "./armored-key-pair";
import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {OpenPgpUserId} from "./open-pgp-user-id";
import {of} from "rxjs/observable/of";
import {config} from "../../app.config";

declare var openpgp: any;

export class OpenPgp {

  static fromArmoredKeyPair(armoredKeyPair: ArmoredKeyPair, passphrase: string): Observable<OpenPgp> {
    let privateKeys;

    return from(
      openpgp.key.readArmored(armoredKeyPair.privateKeys)
        .then((readKeys) => {
          privateKeys = readKeys;
          return Promise.all(privateKeys.keys.map(privateKey => privateKey.decrypt(passphrase)));
        })
        .then(() => openpgp.key.readArmored(armoredKeyPair.publicKeys))
        .then((publicKeys) => new OpenPgp(publicKeys, privateKeys))
    );
  }

  static fromUnencryptedArmoredKeys(armoredUnencryptedPrivateKeys: string, armoredUnencryptedPublicKeys: string): Observable<OpenPgp> {
    const openPgp = new OpenPgp(openpgp.key.readArmored(armoredUnencryptedPublicKeys), openpgp.key.readArmored(armoredUnencryptedPrivateKeys));
    return of(openPgp);
  }

  static generateKeys(userId: OpenPgpUserId, passphrase: string): Observable<ArmoredKeyPair> {
    return from(
      openpgp.generateKey({
        curve: config.pgp.ecc.curve,
        passphrase: passphrase,
        userIds: userId,
      }).then(key => {
        return Promise.resolve({
          publicKeys: key.publicKeyArmored,
          privateKeys: key.privateKeyArmored
        } as ArmoredKeyPair);
      }));
  }

  private constructor(private publicKeys,
                      private privateKeys) {
  }

  /**
   * If the <em>senderArmoredPublicKeys<em> parameter is omitted, the text is self-encrypted, meaning the public keys
   * used to verify the encrypted message will be this instance's public keys.
   * (it's essentially a "note to myself" situation)
   *
   * @param {string} armoredMessage The armored message to decrypt
   * @param {string} senderArmoredPublicKey The sender's armored public keys
   * @returns {Observable<string>} An observable resolving into the decrypted text
   */
  decrypt(armoredMessage: string, senderArmoredPublicKey?: string): Observable<string> {
    let publicKeys;

    const publicKeysPromise = senderArmoredPublicKey
      ? openpgp.key.readArmored(senderArmoredPublicKey)
      : Promise.resolve(this.publicKeys);

    return from(
      publicKeysPromise
        .then((readKeys) => {
          if (!readKeys.keys || readKeys.keys.length === 0) {
            throw new Error("Armored public keys could not be read!");
          }

          publicKeys = readKeys;
          return openpgp.message.readArmored(armoredMessage);
        })
        .then((readMessage) =>
          openpgp.decrypt({
            message: readMessage,
            publicKeys: publicKeys.keys,
            privateKeys: this.privateKeys.keys
          }))
        .then(decrypted => decrypted.data)
    );
  }

  /**
   * Decrypts the specified armored message without verifying its signature.
   *
   * @param armoredMessage The armored message to decrypt
   */
  decryptNoVerify(armoredMessage: string): Observable<string> {
    return from(
      openpgp.message.readArmored(armoredMessage)
        .then((readMessage) =>
          openpgp.decrypt({
            message: readMessage,
            privateKeys: this.privateKeys.keys
          }))
        .then(decrypted => decrypted.data)
    );
  }

  /**
   * If the <em>receiverArmoredPublicKeys<em> parameter is omitted, the text is self-encrypted for the sender
   * (it's essentially a "note to myself" situation).
   *
   * @param {string} text The text to encrypt
   * @param receiverArmoredPublicKeys The armored public keys of the person to whom this encrypted text is addressed
   * @returns {Observable<string>} An observable resolving into an <em>armored</em> message
   */
  encrypt(text: string, receiverArmoredPublicKeys ?: string): Observable<string> {

    if (!this.privateKeys.keys) {
      throw new Error('OpenPgp.encrypt(): private keys is empty!');
    }

    const publicKeysPromise = receiverArmoredPublicKeys
      ? openpgp.key.readArmored(receiverArmoredPublicKeys)
      : Promise.resolve(this.publicKeys);

    return from(
      publicKeysPromise
        .then((publicKeys) =>
          openpgp.encrypt({
            armor: true,
            compression: openpgp.enums.compression.zlib,
            message: openpgp.message.fromText(text),
            privateKeys: this.privateKeys.keys,
            publicKeys: publicKeys.keys
          }))
        .then(encrypted => encrypted.data)
    );
  }
}
