import {Random} from "./random";

describe("Random", () => {

  it("should generate an Uint8Array of random values of a given length", () => {
    const uint8Array: Uint8Array = Random.generateUint8Array(43);
    expect(uint8Array).toBeDefined();
    expect(uint8Array.length).toEqual(43);
    uint8Array.forEach(value => expect(value).toBeDefined())
  });

  it("should generate a Base64 string of random values of a given length", () => {
    const base64String: string = Random.generateBase64String(43);
    expect(base64String).toBeDefined();
    expect(base64String.length).toEqual(43);
  });
});
