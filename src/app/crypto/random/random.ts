declare var base64js: any;

/**
 * Requires window.crypto
 */
export class Random {

  static generateUint8Array(length: number): Uint8Array {
    const array: Uint8Array = new Uint8Array(length);
    window.crypto.getRandomValues(array);
    return array;
  }

  static generateBase64String(length: number): string {
    const array: Uint8Array = this.generateUint8Array(length);
    return base64js.fromByteArray(array).slice(0, length);
  }
}
