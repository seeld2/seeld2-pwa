import {SRP} from "./srp";
import {Challenge} from "./challenge";

describe("SRP", () => {

  const CHALLENGE: Challenge = {
    b: '4a916e8a00fc442e8b3a40d563cf85d521d79e703660414d5c5e42f882b347c59246e8018a9d59414623ec2507d90d4a3d0777b358b01901884f5a39ea63104a0c10b8df772b666a24672ee37432a85284afe62b1a284efac06413f4558c41ef10a73bb2ad94f92dd98651ecc6d0f06ab936aad952e81c308ab0e0284c82645fed1f7f7949e26f84ecff266ba11beb062249936d3d8cbe2bd6f863d352a281929bab347e18cbf5cf3ad3e78df1628560cdcc5e57b329c2e9a55728565fe5639a401c6388b7fa7d669edeb7801e2440a3e4ddcc7bf3bde5c91b2322abff79579eb0b8b76ef4cd4269abe3efe1111aa352c6e38f05a94e7401aa9bd42d806c',
    salt: 'e87d3b4220b2cba147d0027e017c2a6f09ec23268afd1c8ffbac0fb9fc1669ef'
  };
  const SALT: string = 'e87d3b4220b2cba147d0027e017c2a6f09ec23268afd1c8ffbac0fb9fc1669ef';
  const PASSPHRASE: string = 'test03test03test03';
  const USERNAME: string = 'test03';

  it("should generate a random salt of length 64", () => {

    const salt: string = SRP.generateSalt();

    expect(salt).toBeDefined();
    expect(salt.length).toBe(64);
  });

  it("should generate a a verifier using a given salt", () => {

    const verifier: string = SRP.generateVerifier(USERNAME, PASSPHRASE, SALT);

    expect(verifier).toBeDefined();
    expect(verifier.length).toBe(508);
  });

  it("should initialize an instance of SRP from given username, password and challenge", () => {

    const srp: SRP = SRP.init(USERNAME, PASSPHRASE, CHALLENGE);

    expect(srp).toBeDefined();
  });
});
