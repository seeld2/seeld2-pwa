import {Challenge} from "./challenge";
import {Credentials} from "./ccredentials";

declare var SRP6JavascriptClientSessionSHA256: any;

export class SRP {

  static generateSalt(): string {
    return new SRP6JavascriptClientSessionSHA256().generateRandomSalt();
  }

  static generateVerifier(username: string, passphrase: string, generatedSalt: string) {
    const verifier = new SRP6JavascriptClientSessionSHA256().generateVerifier(generatedSalt, username, passphrase);
    // WATCH OUT! The order of these parameters is correct: the generateVerifier() from thinbus takes the "salt" as the first parameter
    passphrase = null;
    return verifier;
  }

  static init(username: string, passphrase: string, challenge: Challenge): SRP {
    const session = new SRP6JavascriptClientSessionSHA256();

    session.step1(username, passphrase);
    const clientCredentials = session.step2(challenge.salt, challenge.b);

    // noinspection JSUnusedAssignment
    passphrase = undefined;
    // noinspection JSUnusedAssignment
    challenge = undefined;

    return new SRP(session, clientCredentials.A, clientCredentials.M1);
  }

  private constructor(private session: any,
                      public a: string,
                      public m1: string) {
  }

  clear() {
    this.session = undefined;
    this.a = undefined;
    this.m1 = undefined;
  }

  computeAesSecretKey(m2: string): string {
    this.session.step3(m2);
    return this.session.getSessionKey(true);
  }

  getCredentials(): Credentials {
    return {a: this.a, m1: this.m1}
  }
}
