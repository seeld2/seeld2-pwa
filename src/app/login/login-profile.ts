export class LoginProfile { // TODO Test

  static fromJson(json: string): LoginProfile {
    const object: any = JSON.parse(json);
    return new LoginProfile(
      object.boxAddress,
      JSON.parse(object.credentials),
      object.deviceStorageKey,
      object.displayName,
      object.exchangeKey,
      object.picture);
  }

  private constructor(
    public boxAddress: string,
    public credentials: Credentials,
    public deviceStorageKey: string,
    public displayName: string,
    public exchangeKey: string,
    public picture: string) {
  }
}

export interface Credentials {
  kdf: LoginProfileKdf;
  srp: LoginProfileSrp;
  systemKeys: LoginProfileSystemKeys;
}

export interface LoginProfileKdf {
  type: string;
  salt: string;
}

export interface LoginProfileSrp {
  type: string;
  salt: string;
  verifier: string;
}

export interface LoginProfileSystemKeys {
  type: string;
  privateKeys: string;
  publicKeys: string;
}
