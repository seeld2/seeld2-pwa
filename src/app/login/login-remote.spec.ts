import {LoginRemote} from "./login-remote";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {ChallengeResponse} from "./challenge-response";
import {of} from "rxjs/observable/of";
import {LoginResponse} from "./login-response";
import {AngularJasmine} from "../../shared/angular-jasmine";

describe("LoginRemote", () => {

  const A: string = 'A';
  const CHALLENGE_RESPONSE: ChallengeResponse = {salt: 'salt', b: 'b'};
  const JWT_TOKEN = 'jwtT0ken';
  const M1: string = 'M1';
  const M2: string = 'M2';
  const PAYLOAD: string = 'pgp-encrypted payload';
  const PROFILE: number[] = [1, 2, 3];
  const USERNAME: string = 'username';

  let httpClient: HttpClient;

  let loginRemote: LoginRemote;

  beforeEach(() => {
    httpClient = AngularJasmine.httpClient();

    loginRemote = new LoginRemote(httpClient);
  });

  it("should query for a pre-login challenge", (done: DoneFn) => {

    (httpClient.get as jasmine.Spy).and.returnValue(of(CHALLENGE_RESPONSE));

    loginRemote.challenge(USERNAME)
      .subscribe((challengeResponse: ChallengeResponse) => {

        expect((httpClient.get as jasmine.Spy).calls.argsFor(0)[0]).toContain(USERNAME);

        expect(challengeResponse).toEqual(CHALLENGE_RESPONSE);

        done();
      });
  });

  it("should login", (done: DoneFn) => {

    // noinspection JSUnusedLocalSymbols
    (httpClient.post as jasmine.Spy).and.returnValue(of({
      headers: {
        get(name: string): string | null {return 'Bearer jwtT0ken'}
      },
      body: {m2: M2, payload: PAYLOAD, profile: PROFILE}
    } as HttpResponse<any>));

    loginRemote.login(USERNAME, A, M1)
      .subscribe((loginResponse: LoginResponse) => {

        expect(httpClient.post).toHaveBeenCalledTimes(1);

        expect(loginResponse.auth).toEqual(JWT_TOKEN);
        expect(loginResponse.m2).toEqual(M2);
        expect(loginResponse.payload).toEqual(PAYLOAD);
        expect(loginResponse.profile).toEqual(PROFILE);

        done();
      });
  });
});
