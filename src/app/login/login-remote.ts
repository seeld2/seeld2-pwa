import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {config} from "../app.config";
import {Logger} from "../utils/logging/logger";
import {ChallengeResponse} from "./challenge-response";
import {LoginRequest} from "./login-request";
import {LoginResponse} from "./login-response";

@Injectable()
export class LoginRemote {

  private readonly logger: Logger = Logger.for('LoginRemote');

  constructor(private httpClient: HttpClient) {
  }

  challenge(username: string): Observable<ChallengeResponse> {
    return this.httpClient.get(`${config.api.login}/challenge/${username}`)
      .pipe(
        map((challengeResponse: ChallengeResponse) => challengeResponse)
      );
  }

  login(username: string, a: string, m1: string): Observable<LoginResponse> {
    const loginRequest: LoginRequest = {a: a, m1: m1, username: username};
    return this.httpClient.post(`${config.api.login}`, loginRequest, {observe: 'response'})
      .pipe(
        map((httpResponse) => {

          const jwt: string = httpResponse.headers.get('Authorization');
          if (!jwt) {
            this.logger.error('Missing authorization token in header');
            // TODO handle error here
          }
          const auth: string = jwt.replace('Bearer ', '');

          return {
            auth: auth,
            m2: httpResponse.body["m2"],
            payload: httpResponse.body["payload"],
            profile: httpResponse.body["profile"]
          } as LoginResponse;
        })
      );
  }
}
