export interface LoginResponse {
  auth: string,
  m2: string;
  payload: string;
  profile: number[]
}
