import {Objects} from "../utils/objects";
import {PostLoginProcessesService} from "./post-login-processes.service";

export class LoginJasmine {

  static loginRemote() {
    return jasmine.createSpyObj('loginRemote', ['login']);
  }

  static loginService() {
    return jasmine.createSpyObj('loginService', ['performLogin']);
  }

  static postLoginProcessesService() {
    return jasmine.createSpyObj('postLoginProcessesService',
      Objects.propertyNamesOf(new PostLoginProcessesService(null, null, null, null)));
  }
}
