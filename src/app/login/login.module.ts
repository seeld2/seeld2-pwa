import {NgModule} from "@angular/core";
import {LoginService} from "./login.service";
import {LoginRemote} from "./login-remote";
import {CryptoModule} from "../crypto/crypto.module";
import {HttpClientModule} from "@angular/common/http";
import {SessionsModule} from "../sessions/sessions.module";
import {UtilsModule} from "../utils/utils.module";
import {UserEventsModule} from "../userevents/user-events.module";
import {PostLoginProcessesService} from "./post-login-processes.service";
import {ContactsModule} from "../contacts/contacts.module";

@NgModule({
  imports: [
    ContactsModule,
    CryptoModule,
    HttpClientModule,
    SessionsModule,
    UserEventsModule,
    UtilsModule
  ],
  providers: [
    LoginRemote,
    LoginService,
    PostLoginProcessesService
  ]
})
export class LoginModule {
}
