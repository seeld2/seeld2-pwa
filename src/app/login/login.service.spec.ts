import {LoginService} from "./login.service";
import {SessionService} from "../sessions/session.service";
import {of} from "rxjs/observable/of";
import {LoginRemote} from "./login-remote";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {PostLoginProcessesService} from "./post-login-processes.service";
import {LoginJasmine} from "./login.jasmine";

describe("Login", () => {

  let loginRemote: LoginRemote;
  let postLoginProcessesService: PostLoginProcessesService;
  let sessionService: SessionService;

  let login: LoginService;

  beforeEach(() => {
    loginRemote = LoginJasmine.loginRemote();
    postLoginProcessesService = LoginJasmine.postLoginProcessesService();
    sessionService = SessionsJasmine.sessionService();

    login = new LoginService(loginRemote, postLoginProcessesService, sessionService);
  });

  describe(", when performing a logout", () => {

    it("should clear and remove the session", (done: DoneFn) => {

      (sessionService.clearAndRemoveSession as jasmine.Spy).and.returnValue(of(true));

      login.performLogout()
        .subscribe((result: boolean) => {
          expect(result).toBe(true);
          done();
        });
    });
  });
});
