import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {SessionService} from "../sessions/session.service";
import {catchError, mergeMap, tap} from "rxjs/operators";
import {_throw} from "rxjs/observable/throw";
import {Logger} from "../utils/logging/logger";
import {LoginRemote} from "./login-remote";
import {ChallengeResponse} from "./challenge-response";
import {LoginResponse} from "./login-response";
import {PostLoginProcessesService} from "./post-login-processes.service";
import {of} from "rxjs/observable/of";

declare var SRP6JavascriptClientSessionSHA256: any;

@Injectable()
export class LoginService {

  private readonly logger: Logger = Logger.for('Login');

  constructor(private readonly loginRemote: LoginRemote,
              private readonly postLoginProcessesService: PostLoginProcessesService,
              private readonly sessionService: SessionService) {
  }

  /**
   * Logs in the user with the specified username and passphrase.
   */
  performLogin(username: string, passphrase: string): Observable<boolean> { // TODO Test
    const usernameLowerCased = username.toLowerCase();
    return this.doLogin(usernameLowerCased, passphrase)
      .pipe(
        catchError((error) => {
          if (username !== usernameLowerCased) {
            this.logger.warn('Login failed with ' + usernameLowerCased + ', trying with case-sensitive ' + username);
            return this.doLogin(username, passphrase);
          } else {
            throw error;
          }
        })
      );
  }

  private doLogin(username: string, passphrase: string): Observable<boolean> {

    // TODO Try and replace this logic with SRP object...

    let srpSession = new SRP6JavascriptClientSessionSHA256();
    srpSession.step1(username, passphrase);

    return this.loginRemote.challenge(username)
      .pipe(
        mergeMap((challengeResponse: ChallengeResponse) => {
          const credentials: any = srpSession.step2(challengeResponse.salt, challengeResponse.b);
          return this.loginRemote.login(username, credentials.A, credentials.M1)
        }),

        mergeMap((loginResponse: LoginResponse) => {
          srpSession.step3(loginResponse.m2);
          const sessionKey: string = srpSession.getSessionKey(true);
          return this.sessionService.initializeNewSession(username, passphrase, loginResponse, sessionKey);
        }),

        tap((result: boolean) => {
          passphrase = undefined;
          srpSession = undefined;

          if (!result) {
            this.logger.error('Could not initialize session');
          }
        }),

        mergeMap((sessionInitialized: boolean) => {
          if (sessionInitialized) {
            return this.postLoginProcessesService.runPostLoginProcesses()
              .pipe(
                mergeMap(() => of(sessionInitialized))
              );
          }
          return of(sessionInitialized);
        })
      );
  }

  /**
   * Logs the user out.
   */
  performLogout(): Observable<boolean> {

    return this.sessionService.clearAndRemoveSession()
      .pipe(
        catchError((error) => {
          this.logger.error('Error while attempting to clear the session', error);
          return _throw(error);
        })
      );
  }

  /**
   * Restores the persistent session, if available.
   */
  restoreSession(): Observable<boolean> {
    return this.sessionService.restoreSession()
      .pipe(
        mergeMap((restored: boolean) => {
          if (restored) {
            return this.postLoginProcessesService.runPostLoginProcesses()
              .pipe(
                mergeMap(() => of(restored))
              );
          }
          return of(restored);
        })
      );
  }
}
