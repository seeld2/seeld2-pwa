import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {ContactSecretKey} from "../contacts/secretkey/contact-secret-key";
import {Profile} from "../profiles/profile";
import {ProfilesJasmine} from "../profiles/profiles.jasmine";
import {ProfilesService} from "../profiles/profiles.service";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionTestBuilder as aSession} from "../sessions/session.test-builder";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {TimeJasmine} from "../time/time.jasmine";
import {TimeService} from "../time/time.service";
import {Contact} from "../user/contact/contact";
import {ContactKey} from "../user/contact/contact-key";
import {UserEvent} from "../userevents/user-event";
import {UserEventType} from "../userevents/user-event-type";
import {UserEventsJasmine} from "../userevents/user-events.jasmine";
import {UserEventsService} from "../userevents/user-events.service";
import {PostLoginProcessesService} from "./post-login-processes.service";

describe("PostLoginProcessService", () => {

  const BOBBY_BOX_ADDRESS: string = 'bobbyBoxAddress';
  const BOBBY_PSEUDO: string = 'bobby';
  const BOBBY_SECRET_KEY: string = 'bobbySecretKey';
  const BOBBY_EVENT_ID: string = 'bobbyEventId';

  const CAROL_BOX_ADDRESS: string = 'carolBoxAddress';
  const CAROL_PSEUDO: string = 'carol';
  const CAROL_SECRET_KEY: string = 'carolSecretKey';
  const CAROL_EVENT_ID: string = 'carolEventId';

  const LOGGED_PSEUDO: string = 'loggedPseudo';

  const NOW_ADJUSTED: number = 123456789;

  let profilesService: ProfilesService;
  let sessionService: SessionService;
  let timeService: TimeService;
  let userEventsService: UserEventsService;

  let postLoginProcessService: PostLoginProcessesService;

  let session: Session;

  beforeEach(() => {
    profilesService = ProfilesJasmine.profilesService();
    sessionService = SessionsJasmine.sessionService();
    timeService = TimeJasmine.timeService();
    userEventsService = UserEventsJasmine.userEventsService();

    postLoginProcessService = new PostLoginProcessesService(profilesService, sessionService, timeService, userEventsService);

    session = aSession.with().noBlanks();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);

    (<jasmine.Spy>timeService.nowAdjusted).and.returnValue(NOW_ADJUSTED);
  });

  describe(", when processing the creation of a logged user's secret key,", () => {

    it("should skip creation when the logged user already has a key", (done: DoneFn) => {
      postLoginProcessService['createLoggedUserSecretKeyIfMissing']().subscribe(() => {
        done();
      });
    });

    it("should add a random-generated secret to the session and update the payload stored remotely", (done: DoneFn) => {

      const session = aSession.with().pseudo(LOGGED_PSEUDO).asIs();
      (<jasmine.Spy>sessionService.session).and.returnValue(session);
      (<jasmine.Spy>sessionService.updateSession).and.returnValue(of({}));
      (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));

      postLoginProcessService['createLoggedUserSecretKeyIfMissing']().subscribe(() => {

        const updatedSession: Session = (<jasmine.Spy>sessionService.updateSession).calls.argsFor(0)[0];
        expect(updatedSession.secretKey()).toBeDefined();

        done();
      });
    });
  });

  it("should register secret keys events sent by the other contacts", (done: DoneFn) => {
    const bobbyContact: Contact = aContact(BOBBY_PSEUDO);
    const carolContact: Contact = aContact(CAROL_PSEUDO);

    (<jasmine.Spy>userEventsService.fetchUserEvents).and.returnValue(from([
      aContactSecretKeyUserEvent(BOBBY_PSEUDO, BOBBY_SECRET_KEY, BOBBY_EVENT_ID),
      aContactSecretKeyUserEvent(CAROL_PSEUDO, CAROL_SECRET_KEY, CAROL_EVENT_ID)
    ]));
    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));
    (<jasmine.Spy>sessionService.contact).and.returnValues(bobbyContact, carolContact);

    const updatedContacts: Contact[] = [];
    (<jasmine.Spy>sessionService.updateContact).and.callFake((updatedContact: Contact) => {
      updatedContacts.push(updatedContact);
      return of({});
    });
    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue(updatedContacts);

    postLoginProcessService.runPostLoginProcesses()
      .subscribe(() => {

        expect((<jasmine.Spy>(userEventsService.deleteEventFromSystemBoxAddress)).calls.argsFor(0)[0]).toEqual(BOBBY_EVENT_ID);
        expect((<jasmine.Spy>(userEventsService.deleteEventFromSystemBoxAddress)).calls.argsFor(1)[0]).toEqual(CAROL_EVENT_ID);

        expect(updatedContacts.length).toEqual(2);
        expect(updatedContacts[0].secretKey()).toEqual(BOBBY_SECRET_KEY);
        expect(updatedContacts[1].secretKey()).toEqual(CAROL_SECRET_KEY);

        expect(profilesService.updatePayload).toHaveBeenCalled();

        done();
      });
  });

  it("should exclude events that are not related to secret keys", (done: DoneFn) => {

    (<jasmine.Spy>userEventsService.fetchUserEvents).and
      .returnValue(from([UserEvent.build({}, BOBBY_EVENT_ID, UserEventType.CONTACT_REMOVE)]));

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([]);

    postLoginProcessService.runPostLoginProcesses().subscribe(() => done());
  });

  it("should exclude secret key events for contacts that are not in the logged user's contacts list", (done: DoneFn) => {

    (<jasmine.Spy>userEventsService.fetchUserEvents).and
      .returnValue(from([aContactSecretKeyUserEvent(BOBBY_PSEUDO, BOBBY_SECRET_KEY, BOBBY_EVENT_ID)]));

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([]);

    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    postLoginProcessService.runPostLoginProcesses()
      .subscribe(() => {
        expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(BOBBY_EVENT_ID);
        expect(sessionService.updateContact).not.toHaveBeenCalled();

        done();
      });
  });

  it("should exclude secret key events for contacts that already have a secret key registered", (done: DoneFn) => {

    (<jasmine.Spy>userEventsService.fetchUserEvents).and
      .returnValue(from([aContactSecretKeyUserEvent(BOBBY_PSEUDO, BOBBY_SECRET_KEY, BOBBY_EVENT_ID)]));

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([aContact(BOBBY_PSEUDO, BOBBY_SECRET_KEY)]);

    (<jasmine.Spy>userEventsService.deleteEventFromSystemBoxAddress).and.returnValue(of({}));

    postLoginProcessService.runPostLoginProcesses()
      .subscribe(() => {
        expect(userEventsService.deleteEventFromSystemBoxAddress).toHaveBeenCalledWith(BOBBY_EVENT_ID);
        expect(sessionService.updateContact).not.toHaveBeenCalled();

        done();
      });
  });

  it("should generate secret keys for contacts not having them, and post an event to share those keys", (done: DoneFn) => {

    (<jasmine.Spy>userEventsService.fetchUserEvents).and.returnValue(Observable.empty());

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([aContact(BOBBY_PSEUDO), aContact(CAROL_PSEUDO)]);
    (<jasmine.Spy>profilesService.fetchDetails).and
      .returnValues(of(aProfile(BOBBY_BOX_ADDRESS)), of(aProfile(CAROL_BOX_ADDRESS)));

    const updatedContacts: Contact[] = [];
    (<jasmine.Spy>sessionService.updateContact).and.callFake((updatedContact: Contact) => {
      updatedContacts.push(updatedContact);
      return of({});
    });

    (<jasmine.Spy>sessionService.pseudo).and.returnValue(LOGGED_PSEUDO);

    const contactSecretKeys: ContactSecretKey[] = [];
    (<jasmine.Spy>userEventsService.postEventToContact).and.callFake((contactSecretKey: ContactSecretKey) => {
      contactSecretKeys.push(contactSecretKey);
      return of({});
    });
    (<jasmine.Spy>profilesService.updatePayload).and.returnValue(of({}));

    postLoginProcessService.runPostLoginProcesses()
      .subscribe(() => {

        expect(updatedContacts.length).toEqual(2);
        expect(updatedContacts[0].pseudo()).toEqual(BOBBY_PSEUDO);
        expect(updatedContacts[1].pseudo()).toEqual(CAROL_PSEUDO);

        expect(contactSecretKeys.length).toEqual(2);
        expect(contactSecretKeys[0].pseudo()).toEqual(LOGGED_PSEUDO);
        expect(contactSecretKeys[1].pseudo()).toEqual(LOGGED_PSEUDO);

        expect(profilesService.updatePayload).toHaveBeenCalled();

        done();
      });
  });

  it("should avoid generating secret keys for contacts that already have them", (done: DoneFn) => {

    (<jasmine.Spy>userEventsService.fetchUserEvents).and.returnValue(Observable.empty());

    (<jasmine.Spy>sessionService.contactsAsArray).and.returnValue([
        aContact(BOBBY_PSEUDO, BOBBY_SECRET_KEY),
        aContact(CAROL_PSEUDO, BOBBY_SECRET_KEY)
      ]);

    postLoginProcessService.runPostLoginProcesses()
      .subscribe(() => {
        expect(sessionService.updateContact).not.toHaveBeenCalled();
        done();
      });
  });

  const aContact = (pseudo: string, secretKey: string = null): Contact =>
    Contact.with(pseudo, null, null, null, ContactKey.with(null, null, null), secretKey, null);

  const aContactSecretKeyUserEvent = (pseudo: string, secretKey: string, eventId: string): UserEvent =>
    UserEvent.build(ContactSecretKey.for(pseudo, secretKey).asObject(), eventId, UserEventType.CONTACT_SECRET_KEY);

  const aProfile = (boxAddress: string): Profile => {
    return Profile.build(boxAddress, null, null);
  }
});
