import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {concat} from "rxjs/observable/concat";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {filter, map, mergeMap, toArray} from "rxjs/operators";
import {ContactSecretKey} from "../contacts/secretkey/contact-secret-key";
import {ContactSecretKeyEvent} from "../contacts/secretkey/contact-secret-key-event";
import {Hmac} from "../crypto/hmac/hmac";
import {Profile} from "../profiles/profile";
import {ProfilesService} from "../profiles/profiles.service";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {TimeService} from "../time/time.service";
import {Contact} from "../user/contact/contact";
import {UserEvent} from "../userevents/user-event";
import {UserEventReturn} from "../userevents/user-event-return";
import {UserEventType} from "../userevents/user-event-type";
import {UserEventsService} from "../userevents/user-events.service";

@Injectable()
export class PostLoginProcessesService {

  constructor(private readonly profilesService: ProfilesService,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService,
              private readonly userEventsService: UserEventsService) {
  }

  runPostLoginProcesses(): Observable<any> {
    return concat(
      this.fetchSecretKeyEventsSentByContacts(), // The order is important! First we register the received secret keys...
      this.sendSecretKeyEventsToContactsWithoutSecretKeys(), // ...and only then we send secret keys' events!
      this.createLoggedUserSecretKeyIfMissing()
    )
      .pipe(
        toArray() // This is to make sure we emit ONE event: once all processes have been completed
      );
  }

  private createLoggedUserSecretKeyIfMissing(): Observable<any> {
    const currentSession: Session = this.sessionService.session();
    if (currentSession.secretKey()) {
      return of({});
    }

    return this.sessionService.updateSession(Session.build(
      currentSession.auth(),
      currentSession.systemBoxAddress(),
      currentSession.contacts(),
      currentSession.deletedContacts(),
      currentSession.derivedKdfKey(),
      currentSession.displayName(),
      currentSession.exchangeKey(),
      currentSession.keys(),
      currentSession.picture(),
      currentSession.pseudo(),
      Hmac.generateSecretKey()
    ))
      .pipe(
        mergeMap(() => this.profilesService.updatePayload())
      );
  }

  private fetchSecretKeyEventsSentByContacts(): Observable<any> {
    return this.userEventsService.fetchUserEvents()
      .pipe(
        filter((userEvent: UserEvent) => {
          return userEvent.isOfType(UserEventType.CONTACT_SECRET_KEY);
        }),
        map((userEvent: UserEvent) => {
          return <ContactSecretKeyEvent>{
            contactSecretKey: ContactSecretKey.fromUserEvent(userEvent),
            eventId: userEvent.eventId()
          };
        }),
        mergeMap((event: ContactSecretKeyEvent) => {
          const removeEventObs: Observable<any> = this.userEventsService.deleteEventFromSystemBoxAddress(event.eventId);

          const contact: Contact = this.sessionService.contact(event.contactSecretKey.pseudo());
          if (contact && !contact.secretKey()) {
            const updatedContact: Contact = Contact.with(
              contact.pseudo(),
              contact.displayName(),
              contact.readFrom(),
              contact.writeTo(),
              contact.writeWith(),
              event.contactSecretKey.key(),
              contact.connectedSince()
            );
            return forkJoin(this.sessionService.updateContact(updatedContact), removeEventObs)
              .pipe(
                mergeMap(() => {
                  return this.profilesService.updatePayload();
                })
              );
          }
          return removeEventObs;
        })
      );
  }

  private sendSecretKeyEventsToContactsWithoutSecretKeys(): Observable<any> {
    return of(true) // We cannot start with from(...) because the session's contacts must be fetched AFTER fetchSecretKeyEventsObs is completed...
      .pipe(
        mergeMap(() => {
          return from(this.sessionService.contactsAsArray());
        }),
        filter((contact: Contact) => {
          return !contact.secretKey();
        }),
        mergeMap((contact: Contact) => {
          const updatedContact: Contact = Contact.with(
            contact.pseudo(),
            contact.displayName(),
            contact.readFrom(),
            contact.writeTo(),
            contact.writeWith(),
            Hmac.generateSecretKey(),
            contact.connectedSince()
          );
          return forkJoin(of(updatedContact), this.profilesService.fetchDetails(contact.pseudo()));
        }),
        mergeMap((results: any) => {
          const updatedContact: Contact = results[0];
          const profile: Profile = results[1];

          const updateObs: Observable<any> = this.sessionService.updateContact(updatedContact)

          const userEventReturn: UserEventReturn = UserEventReturn.for(
            UserEventType.CONTACT_SECRET_KEY_RETURN,
            this.sessionService.pseudo(),
            this.timeService.nowAdjusted());
          const postEventObs: Observable<any> = this.userEventsService.postEventToContact(
            ContactSecretKey.for(this.sessionService.pseudo(), updatedContact.secretKey()),
            userEventReturn,
            updatedContact,
            profile.boxAddress() // TODO For the moment this event is sent to the system box address. Afterwards it will need to use the contact's implementing HMAC and having events fetched on each contact's writeTo box address
          );

          return forkJoin(updateObs, postEventObs);
        }),
        mergeMap(() => {
          return this.profilesService.updatePayload();
        })
      );
  }
}
