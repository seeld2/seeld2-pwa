import {LoaderService} from "./loader.service";
import {Loading, LoadingController} from "ionic-angular";
import {IonicJasmine} from "../../shared/ionic-jasmine";

describe("LoaderService", () => {

  let loadingController: LoadingController;

  let loaderService: LoaderService;

  let loading: Loading;

  beforeEach(() => {
    loadingController = IonicJasmine.loadingController();

    loaderService = new LoaderService(loadingController);

    loading = jasmine.createSpyObj('loading', ['dismiss', 'present']);
    (<jasmine.Spy>loading.dismiss).and.returnValue(Promise.resolve());
    (<jasmine.Spy>loading.present).and.returnValue(Promise.resolve());
    (<jasmine.Spy>loadingController.create).and.returnValue(loading);
  });

  it("should create a loader", (done: DoneFn) => {
    loaderService.createLoader()
      .then(() => {
        expect(loading.present).toHaveBeenCalledTimes(1);
        expect(loaderService['loading']).toBeDefined();
        done();
      });
  });

  it("should dismiss a loader", (done: DoneFn) => {
    loaderService['loading'] = loading;

    loaderService.dismissLoader()
      .then(() => {
        expect(loading.dismiss).toHaveBeenCalledTimes(1);
        expect(loaderService['loading']).toBeNull();
        done();
      });
  });

  it("should create a delayed loader", (done: DoneFn) => {
    loaderService.createLoader({delayBeforeShowingInMillis: 500})
      .then(() => {
        expect(loading.present).not.toHaveBeenCalled();
        setTimeout(() => {
          expect(loading.present).toHaveBeenCalledTimes(1);
          expect(loaderService['loading']).toBeDefined();
          done();
        }, 500)
      });
  });

  it("should dismiss a delayed loader before it's shown", (done: DoneFn) => {
    loaderService.createLoader({delayBeforeShowingInMillis: 500})
      .then(() => loaderService.dismissLoader())
      .then(() => {
        expect(loading.present).not.toHaveBeenCalled();
        setTimeout(() => {
          expect(loading.present).not.toHaveBeenCalled();
          expect(loading.dismiss).toHaveBeenCalledTimes(1);
          expect(loaderService['loading']).toBeNull();
          done();
        }, 500);
      });
  });
});
