import {Injectable} from "@angular/core";
import {Loading, LoadingController, LoadingOptions} from "ionic-angular";

@Injectable()
export class LoaderService {

  private delayedLoaderTimeout: any;
  private loading: Loading;

  constructor(private readonly loadingController: LoadingController) {
  }

  createLoader(loaderOptions: LoaderOptions = {}): Promise<any> {

    if (this.loading) {
      return this.dismissLoader();
    }

    const opts: LoadingOptions = {
      content: loaderOptions.content,
      enableBackdropDismiss: true
    };

    if (loaderOptions.delayBeforeShowingInMillis) {
      this.loading = this.loadingController.create(opts);
      this.delayedLoaderTimeout = setTimeout(() => {
        if (this.loading) {
          this.loading.present();
        }
      }, loaderOptions.delayBeforeShowingInMillis);
      return Promise.resolve();

    } else {
      this.loading = this.loadingController.create(opts);
      return this.loading.present();
    }
  }

  dismissLoader(): Promise<any> {

    const promise = this.loading ? this.loading.dismiss() : Promise.resolve();
    this.loading = null;

    if (this.delayedLoaderTimeout) {
      clearTimeout(this.delayedLoaderTimeout);
      this.delayedLoaderTimeout = null;
    }

    return promise;
  }
}

export interface LoaderOptions {
  content?: string;
  delayBeforeShowingInMillis?: number;
}
