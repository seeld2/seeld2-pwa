import {NgModule} from "@angular/core";
import {UtilsModule} from "../utils/utils.module";
import {LoaderService} from "./loader.service";
import {PlatformService} from "./platform.service";
import {SeeldCordovaPluginProvider} from "./seeld-cordova-plugin.provider";

@NgModule({
  imports: [
    UtilsModule
  ],
  providers: [
    LoaderService,
    PlatformService,
    SeeldCordovaPluginProvider,
  ]
})
export class MobileModule {
}
