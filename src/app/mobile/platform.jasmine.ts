import {Objects} from "../utils/objects";
import {LoaderService} from "./loader.service";
import {PlatformService} from "./platform.service";
import {SeeldCordovaPluginProvider} from "./seeld-cordova-plugin.provider";

export class PlatformJasmine { // TODO Rename to MobileJasmine

  static loaderService() {
    return jasmine.createSpyObj('loaderService', Objects.propertyNamesOf(new LoaderService(null)));
  }

  static platformService() {
    return jasmine.createSpyObj('platformService', Objects.propertyNamesOf(new PlatformService(null, null)));
  }

  static seeldCordovaPluginProvider() {
    return jasmine.createSpyObj('seeldCordovaPluginProvider', Objects.propertyNamesOf(new SeeldCordovaPluginProvider()));
  }
}
