import {Events, Platform} from "ionic-angular";
import {PlatformService} from "./platform.service";
import {IonicJasmine} from "../../shared/ionic-jasmine";

describe("Platforms", () => {

  const APP_LOCATION: any = {host: 'localhost'};
  const WEB_LOCATION: any = {host: 'live.app.be'};

  let events: Events;
  let platform: Platform;

  let platforms: PlatformService;

  beforeEach(() => {
    events = IonicJasmine.events();
    platform = IonicJasmine.platform();

    platforms = new PlatformService(events, platform);
  });

  it("should indicate whether the platform is a browser, either desktop or mobile", () => {

    platforms['location'] = WEB_LOCATION;
    expect(platforms.isBrowser()).toEqual(true);

    platforms['location'] = APP_LOCATION;
    expect(platforms.isBrowser()).toEqual(false);
  });

  it("should indicate if the platform is a mobile browser (or a device emulation inside Chrome Dev Tools)", () => {

    platforms['location'] = WEB_LOCATION;
    (<jasmine.Spy>platform.is).and.returnValue(true);
    expect(platforms.isMobileBrowser()).toBe(true);
    expect((<jasmine.Spy>platform.is).calls.argsFor(0)[0]).toEqual('mobile');
    expect((<jasmine.Spy>platform.is).calls.argsFor(1)[0]).toEqual('mobileweb');

    platforms['location'] = WEB_LOCATION;
    (<jasmine.Spy>platform.is).and.returnValue(false);
    expect(platforms.isMobileBrowser()).toBe(false);
  });

  it("should indicate whether the platform is an Android device or not", () => {

    platforms['location'] = WEB_LOCATION;
    expect(platforms.isAndroidApp()).toEqual(false);
    expect(platform.is).not.toHaveBeenCalled();

    (platform.is as jasmine.Spy).calls.reset();

    platforms['location'] = APP_LOCATION;
    (<jasmine.Spy>platform.is).and.returnValue(false);
    expect(platforms.isAndroidApp()).toEqual(false);
    expect(platform.is).toHaveBeenCalledWith('android');

    (platform.is as jasmine.Spy).calls.reset();

    platforms['location'] = APP_LOCATION;
    (<jasmine.Spy>platform.is).and.returnValue(true);
    expect(platforms.isAndroidApp()).toEqual(true);
    expect(platform.is).toHaveBeenCalledWith('android');
  });
});
