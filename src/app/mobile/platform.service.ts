import {Injectable} from "@angular/core";
import {Events, Platform} from "ionic-angular";
import {Logger} from "../utils/logging/logger";
import {MobileEvent} from "./mobile.event";

/**
 * An injectable component that rationalizes and centralizes interaction with the Ionic-detected platforms.
 *
 * This component must be able to detect the following contexts:
 * - locally-served web app on desktop browser. platforms: core | served from http://localhost:8100
 * - locally-served web app on mobile browser. platforms: mobile, android, mobileweb | served from http://localhost:8100
 * - mobile app run on connected device. served from: http://localhost
 * - mobile app run on emulator
 * - live web app on desktop browser. platforms: core | served from https://app.seeld.io
 * - live web app on mobile browser. platforms: mobile, android, mobileweb | served from https://app.seeld.io
 * - mobile app on live server
 */
@Injectable()
export class PlatformService {

  private readonly logger: Logger = Logger.for("PlatformService");

  private location = window.location;
  private runningInBackground = false;

  constructor(private readonly events: Events,
              private readonly platform: Platform) {
  }

  getPlatformProperties(): string[] {
    return this.platform.platforms();
  }

  isAndroidApp(): boolean {
    /*
      Don't be tempted to add "!this.isMobileBrowser()" here: the App considers itself as a mobile browser too.
     */
    return !this.isBrowser() && this.platform.is('android');
  }

  isBrowser(): boolean {
    // If host is strictly 'localhost' then it can only be the App
    // (Cordova apps serve from http://localhost, without port)
    return this.location['host'] !== 'localhost';
  }

  isMobileBrowser(): boolean {
    return this.platform.is('mobile') && this.platform.is('mobileweb');
  }

  isRunningInBackground(): boolean {
    return this.runningInBackground;
  }

  ready(): Promise<string> {
    return this.platform.ready();
  }

  watchIfAppIsRunningInBackground(onResume) {
    document.addEventListener("pause", () => {
      this.logger.trace('pause');
      this.runningInBackground = true;
      this.events.publish(MobileEvent.SENT_TO_BACKGROUND);
    });
    document.addEventListener("resume", () => {
      this.logger.trace('resume');
      this.runningInBackground = false;
      this.events.publish(MobileEvent.RESUMED_FROM_BACKGROUND);
      onResume();
    });
  }
}
