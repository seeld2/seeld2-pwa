import {Refresher} from "ionic-angular";

export class Refreshers {

  static closeAfterDelay(refresher: Refresher) {
    const timeoutId = setTimeout(() => {
      if (refresher) {
        refresher.complete();
      }
      clearTimeout(timeoutId);
    }, 200 + Math.round((Math.random() * 800.0)));
  }
}
