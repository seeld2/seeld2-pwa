import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {Logger} from "../utils/logging/logger";

declare var SeeldCordovaPlugin: any;

export class SeeldCordovaPluginProvider {

  private readonly logger: Logger = Logger.for('SeeldCordovaPluginProvider');

  private _seeldCordovaPluginInstance;

  seeldCordovaPlugin(): Observable<any> {
    if (this._seeldCordovaPluginInstance) {
      return of(this._seeldCordovaPluginInstance);
    }
    return from(this.instantiateSeeldCordovaPlugin());
  }

  private instantiateSeeldCordovaPlugin(attempts: number = 10): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this._seeldCordovaPluginInstance = new SeeldCordovaPlugin();
        resolve(this._seeldCordovaPluginInstance);
      } catch (error) {
        if (attempts >= 0) {
          setTimeout(() => resolve(this.instantiateSeeldCordovaPlugin(attempts - 1)), 500);
        } else {
          this.logger.error("Could not obtain Seeld's Cordova Plugin", error);
          reject();
        }
      }
    });
  }
}
