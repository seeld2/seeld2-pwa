import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class BoxAddressesRequest implements Serializable {

  static of(boxAddresses: string[]): BoxAddressesRequest {
    return new BoxAddressesRequest(boxAddresses);
  }

  private constructor(private readonly _boxAddresses: string[]) {
  }

  asObject(): any {
    return {boxAddresses: this._boxAddresses};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
