import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {map, mergeMap, tap} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {config} from "../app.config";
import {PlatformService} from "../mobile/platform.service";
import {SeeldCordovaPluginProvider} from "../mobile/seeld-cordova-plugin.provider";
import {SessionService} from "../sessions/session.service";
import {UiNotifications} from "../uinotifications/ui-notifications";
import {Logger} from "../utils/logging/logger";
import {RemoteNotificationsService} from "./remote-notifications.service";
import {UnreadMessagesPoller} from "./unread-messages-poller";

export class DeviceRemoteNotificationsService extends RemoteNotificationsService {

  private readonly deviceLogger: Logger = Logger.for('DeviceRemoteNotificationsService');

  static build(platformService: PlatformService,
               seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
               sessionService: SessionService,
               uiNotifications: UiNotifications,
               unreadMessagesPoller: UnreadMessagesPoller): Observable<DeviceRemoteNotificationsService> {
    return of(new DeviceRemoteNotificationsService(
      platformService, seeldCordovaPluginProvider, sessionService, uiNotifications, unreadMessagesPoller));
  }

  private _seeldCordovaPlugin;

  constructor(private readonly platformService: PlatformService,
              private readonly seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
              sessionService: SessionService,
              uiNotifications: UiNotifications,
              unreadMessagesPoller: UnreadMessagesPoller) {
    super(sessionService, uiNotifications, unreadMessagesPoller);
  }

  registerBoxAddresses(readFromBoxAddresses: string[]) {
    super.registerBoxAddresses(readFromBoxAddresses);

    this.initSeeldCordovaPlugin()
      .pipe(
        tap(() => {
          this._seeldCordovaPlugin.notificationsWatch(
            environment.api.server.address,
            readFromBoxAddresses,
            () => this.deviceLogger.info("Registered box addresses on SeeldCordovaPlugin"),
            (e) => this.deviceLogger.error("Error while starting SeeldCordovaPlugin", e)
          );
        })
      )
      .subscribe();
  }

  protected handleNewMessages() {
    this._seeldCordovaPlugin.notificationsDisplayNotification(
      () => {
        if (this.platformService.isRunningInBackground()) {
          return;
        }
        setTimeout(() => this._seeldCordovaPlugin.notificationsCloseNotification(
          () => {
          },
          () => {
          }
        ), config.ui.notifications.duration);
      },
      (e) => this.deviceLogger.error("Error while attempting to display a notification on device", e)
    );
  }

  stopListening(): Observable<boolean> {
    return super.stopListening()
      .pipe(
        mergeMap(() => this.initSeeldCordovaPlugin()),
        mergeMap(() => {
          return new Observable(subscriber => {
            this._seeldCordovaPlugin.notificationsStopWatching(
              () => {
                subscriber.next();
                subscriber.complete();
              },
              (e) => this.deviceLogger.error("Error while stopping SeeldCordovaPlugin", e)
            );
          });
        })
      );
  }

  private initSeeldCordovaPlugin(): Observable<any> {
    return this.seeldCordovaPluginProvider.seeldCordovaPlugin()
      .pipe(
        map((seeldCordovaPlugin) => this._seeldCordovaPlugin = seeldCordovaPlugin)
      );
  }
}
