import {NotificationType} from "./notification-type";

export class Notification {

  constructor(public type: NotificationType) {
  }

  static fromJson(json: string): Notification {
    const object: any = JSON.parse(json);
    return new Notification(object.type);
  }
}
