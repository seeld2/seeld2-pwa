import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {MobileModule} from "../mobile/mobile.module";
import {SessionsModule} from "../sessions/sessions.module";
import {UiNotificationsModule} from "../uinotifications/ui-notifications.module";
import {UtilsModule} from "../utils/utils.module";
import {RemoteNotificationsServiceProvider} from "./remote-notifications-service-provider.service";

@NgModule({
  imports: [
    HttpClientModule,
    MobileModule,
    SessionsModule,
    UiNotificationsModule,
    UtilsModule
  ],
  providers: [
    RemoteNotificationsServiceProvider
  ]
})
export class NotificationsModule {
}
