import {SeeldCordovaPluginProvider} from "../mobile/seeld-cordova-plugin.provider";
import {RemoteNotificationsServiceProvider} from "./remote-notifications-service-provider.service";
import {PlatformJasmine} from "../mobile/platform.jasmine";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {CoreJasmine} from "../../core/core-jasmine";
import {UiNotificationsJasmine} from "../uinotifications/ui-notifications.jasmine";
import {RemoteNotificationsService} from "./remote-notifications.service";

describe("RemoteNotificationsServiceProvider", () => {

  let platforms;
  let seeldCordovaPluginProvider: SeeldCordovaPluginProvider;
  let sessionService;
  let uiNotifications;
  let unreadMessagesPoller;

  let remoteNotificationsServiceProvider;

  beforeEach(() => {
    platforms = PlatformJasmine.platformService();
    seeldCordovaPluginProvider = PlatformJasmine.seeldCordovaPluginProvider();
    sessionService = SessionsJasmine.sessionService();
    uiNotifications = UiNotificationsJasmine.uiNotifications();
    unreadMessagesPoller = CoreJasmine.unreadMessagesPollerService();

    remoteNotificationsServiceProvider = new RemoteNotificationsServiceProvider(
      platforms, seeldCordovaPluginProvider, sessionService, uiNotifications, unreadMessagesPoller);
  });

  it("should provide an instance of a WebRemoteNotificationsService if the platform is a browser", (done: DoneFn) => {

    (<jasmine.Spy>platforms.isBrowser).and.returnValue(true);

    remoteNotificationsServiceProvider.obtain().subscribe((service: RemoteNotificationsService) => {
      expect(service.constructor.name).toEqual('WebRemoteNotificationsService');
      done();
    });
  });

  it("should provide an instance of a DeviceRemoteNotificationsService if the platform is not a browser", (done: DoneFn) => {

    (<jasmine.Spy>platforms.isBrowser).and.returnValue(false);

    remoteNotificationsServiceProvider.obtain().subscribe((service: RemoteNotificationsService) => {
      expect(service.constructor.name).toEqual('DeviceRemoteNotificationsService');
      done();
    });
  });
});
