import {Injectable} from "@angular/core";
import {PlatformService} from "../mobile/platform.service";
import {Observable} from "rxjs";
import {SeeldCordovaPluginProvider} from "../mobile/seeld-cordova-plugin.provider";
import {RemoteNotificationsService} from "./remote-notifications.service";
import {of} from "rxjs/observable/of";
import {map} from "rxjs/operators";
import {WebRemoteNotificationsService} from "./web-remote-notifications.service";
import {DeviceRemoteNotificationsService} from "./device-remote-notifications.service";
import {SessionService} from "../sessions/session.service";
import {UiNotifications} from "../uinotifications/ui-notifications";
import {UnreadMessagesPoller} from "./unread-messages-poller";

@Injectable()
export class RemoteNotificationsServiceProvider {

  private instance: RemoteNotificationsService;

  constructor(private readonly platformService: PlatformService,
              private readonly seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
              private readonly sessionService: SessionService,
              private readonly uiNotifications: UiNotifications,
              private readonly unreadMessagesPoller: UnreadMessagesPoller) { // TODO Remove this dependency
  }

  obtain(): Observable<RemoteNotificationsService> {

    if (this.instance) {
      return of(this.instance);
    }

    const instance: Observable<RemoteNotificationsService> = this.platformService.isBrowser()
      ? WebRemoteNotificationsService.build(this.sessionService, this.uiNotifications, this.unreadMessagesPoller)
      : DeviceRemoteNotificationsService.build(this.platformService, this.seeldCordovaPluginProvider,
        this.sessionService, this.uiNotifications, this.unreadMessagesPoller);

    return instance.pipe(
      map((remoteNotificationsService: RemoteNotificationsService) => {
        this.instance = remoteNotificationsService;
        return this.instance;
      })
    );
  }
}
