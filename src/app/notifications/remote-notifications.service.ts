import {RxStomp} from "@stomp/rx-stomp";
import {environment} from "../../environments/environment";
import {config} from "../app.config";
import {SessionService} from "../sessions/session.service";
import {Logger} from "../utils/logging/logger";
import {BoxAddressesRequest} from "./box-addresses-request";

import * as SockJS from 'sockjs-client';
import {map} from "rxjs/operators";
import {Notification} from "./notification";
import {NotificationType} from "./notification-type";
import {UnreadMessagesPoller} from "./unread-messages-poller";
import {Duration} from "luxon";
import {UiNotifications} from "../uinotifications/ui-notifications";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";

/**
 * This service acts as the central hub for listening and reacting to server notifications, such as "new messages" or
 * "new contact request".
 */
export abstract class RemoteNotificationsService {

  private readonly logger: Logger = Logger.for('RemoteNotificationsService');

  private client: RxStomp;
  private timeoutId: any = null;

  protected constructor(protected readonly sessionService: SessionService,
                        private readonly uiNotifications: UiNotifications,
                        private readonly unreadMessagesPoller: UnreadMessagesPoller) { // TODO Remove this dependency
  }

  initialize() {

    if (!this.client) {
      this.initStompClient();
      this.watchForNotifications();
      this.attemptClientActivation();
    }

    this.timeoutId = this.startRefreshingBoxAddressesRegistrationRecurrently();
  }

  private initStompClient() {

    this.client = new RxStomp();
    this.client.configure({
      webSocketFactory: () => new SockJS(config.wsapi.connectionUrl),
      debug: (msg: string) => {
        // this.logger.debug(msg);
      }
    });

    // (Re-)register box addresses as soon as we're connected
    this.client.connected$.subscribe(() => {
      this.logger.info("This client is connected to receive notifications!");
      this.registerBoxAddressesInSession();
    });

    this.client.webSocketErrors$.subscribe((event: Event) => {
      this.logger.warn("Got a web socket error", event);
    });
  }

  private watchForNotifications() {
    this.client.watch(config.wsapi.destination.notification)
      .pipe(
        map((message: any) => {
          return Notification.fromJson(message.body);
        }))
      .subscribe((notification: Notification) => {
        if (notification.type === NotificationType.NEW_MSG) {
          this.unreadMessagesPoller.pollUnreadMessages()
            .subscribe(() => {
              this.handleNewMessages();
            })
        }
      });
  }

  private attemptClientActivation() {
    this.client.activate();

    setTimeout(() => {
      if (!this.client || !this.client.connected()) {
        this.handleWebSocketError();
      }
    }, environment.notifications.connectionTimeout);
  }

  private handleWebSocketError() {

    // TODO Here we shouldn't fall back to polling, nor show a message. Just send an event that will show a "No Internet Connection"

    const nextAttempt: number = Duration.fromMillis(environment.notifications.reconnectionAttemptDelay).as("minutes");
    this.logger.warn("Can't connect to real-time notifications... Switching back to polling. " +
      `Will make another attempt in ${nextAttempt} minutes.`);
    this.uiNotifications.showWarning("It looks like Seeld can't receive real-time notifications... " +
      "We'll switch to polling to check for unread messages for the moment. " +
      `We'll try again connecting to real-time notifications in ${nextAttempt} minutes.`);

    this.client.deactivate();
    this.unreadMessagesPoller.switchToFallbackInterval();

    setTimeout(() => {
      this.logger.info("Attempting to reconnect to receive notifications...");
      this.attemptClientActivation();
      this.unreadMessagesPoller.switchToNormalInterval();

    }, environment.notifications.reconnectionAttemptDelay);
  }

  private startRefreshingBoxAddressesRegistrationRecurrently(): any {

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    return setTimeout(() => {
      this.registerBoxAddressesInSession();

      this.timeoutId = this.startRefreshingBoxAddressesRegistrationRecurrently();
    }, environment.notifications.refresh.interval);
  }

  registerBoxAddresses(readFromBoxAddresses: string[]) {
    this.client.publish({
      destination: config.wsapi.endpoint.notifications.register,
      body: BoxAddressesRequest.of(readFromBoxAddresses).toJson()
    });
  }

  registerBoxAddressesInSession() {
    if (this.sessionService.isSessionInitialized()) {
      this.logger.trace("registerBoxAddressesInSession()");
      this.registerBoxAddresses(this.sessionService.session().readFromBoxAddresses());
    }
  }

  stopListening(): Observable<boolean> {
    if (this.client && this.client.connected() && this.client.active) {
      clearTimeout(this.timeoutId);
      this.client.deactivate();
    }
    return of(true);
  }

  protected abstract handleNewMessages();
}
