import {Observable} from "rxjs";

export abstract class UnreadMessagesPoller {

  abstract pollUnreadMessages(): Observable<number>;

  abstract schedulePolling();

  abstract switchToFallbackInterval();

  abstract switchToNormalInterval();
}
