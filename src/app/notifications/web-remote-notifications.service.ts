import {RemoteNotificationsService} from "./remote-notifications.service";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {UnreadMessagesPoller} from "./unread-messages-poller";
import {SessionService} from "../sessions/session.service";
import {UiNotifications} from "../uinotifications/ui-notifications";

export class WebRemoteNotificationsService extends RemoteNotificationsService {

  static build(sessionService: SessionService,
               uiNotifications: UiNotifications,
               unreadMessagesPoller: UnreadMessagesPoller): Observable<WebRemoteNotificationsService> {
    return of(new WebRemoteNotificationsService(sessionService, uiNotifications, unreadMessagesPoller));
  }

  constructor(sessionService: SessionService,
              uiNotifications: UiNotifications,
              unreadMessagesPoller: UnreadMessagesPoller) {
    super(sessionService, uiNotifications, unreadMessagesPoller);
  }

  protected handleNewMessages() {
    // No need: new messages will be notified by unread messages' poller
  }
}
