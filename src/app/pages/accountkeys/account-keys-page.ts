import {Component} from '@angular/core';
import {AlertController, NavParams, ViewController} from "ionic-angular";
import {ArmoredKeyPair} from "../../crypto/openpgp/armored-key-pair";
import {PlatformService} from "../../mobile/platform.service";
import {AccountKeys} from "../../../core/account-keys/account-keys";
import {SessionService} from "../../sessions/session.service";

@Component({
  selector: 'page-account-keys',
  templateUrl: 'account-keys.html',
})
export class AccountKeysPage {

  private readonly filenameSuffix: string = '.seeld.txt';

  constructor(private accountKeys: AccountKeys,
              private alertCtrl: AlertController,
              private navParams: NavParams,
              private platforms: PlatformService,
              private sessionService: SessionService,
              private viewCtrl: ViewController) {
  }

  // noinspection JSUnusedGlobalSymbols
  ionViewDidEnter() {
    if (this.navParams.get('accountKeysWarning')) {
      this.alertCtrl.create({
        buttons: [{
          text: 'Got it!',
          handler: () => {
            this.saveToFile();
            this.viewCtrl.dismiss();
          }
        }],
        enableBackdropDismiss: false,
        message: 'Before you go ahead make sure you write down that password (in a password manager, for example) and backup your keys somewhere safe.<br/><strong>Do NOT lose your account keys and password: if you do, your account and messages will be lost!</strong>'
      }).present();
    }
  }

  accountKeysImported(armoredKeyPair: ArmoredKeyPair) {
    this.viewCtrl.dismiss(armoredKeyPair);
  }

  close() {
    this.viewCtrl.dismiss();
  }

  // TODO This could and should be tested!
  saveToFile() {
    let filename = 'New Account' + this.filenameSuffix;

    const pseudo = this.sessionService.session().pseudo();
    if (pseudo) {
      filename = pseudo + this.filenameSuffix;
    }

    if (this.platforms.isAndroidApp()) {
      this.accountKeys.saveToFileAndroid(filename);
    } else {
      this.accountKeys.saveToFileBrowser(filename);
    }
  }
}
