import {Component, ElementRef, EventEmitter, Output, ViewChild} from "@angular/core";
import {ArmoredKeyPair} from "../../crypto/openpgp/armored-key-pair";
import {FileChooser} from "@ionic-native/file-chooser";
import {File} from "@ionic-native/file";
import {FilePath} from "@ionic-native/file-path";
import {PlatformService} from "../../mobile/platform.service";
import {UiNotifications} from "../../uinotifications/ui-notifications";

@Component({
  selector: 'import-account-keys',
  templateUrl: 'import-account-keys.html'
})
export class ImportAccountKeys {

  @ViewChild('accountKeysFileInput') accountKeysFileInput: ElementRef;

  @Output() imported = new EventEmitter();

  constructor(private file: File,
              private fileChooser: FileChooser,
              private filePath: FilePath,
              private notifications: UiNotifications,
              private platforms: PlatformService) {
  }

  // noinspection JSMethodCanBeStatic
  openFileUploader() {
    if (this.platforms.isAndroidApp()) {
      this.importAccountKeysAndroid();
    } else {
      document.getElementById('accountKeysFileInput').click();
    }
  }

  importAccountKeysBrowser() {
    const fileInput: HTMLInputElement = this.accountKeysFileInput.nativeElement;

    if (fileInput.files.length > 0) {

      try {
        // TODO Extract this to its own shared/File class
        const reader: FileReader = new FileReader();

        reader.onload = (event: Event) => {
          const armoredKeyPair: ArmoredKeyPair = JSON.parse(event.target['result']);
          // TODO This would become a passed function
          this.notifyAccountKeysSuccessfullyImported();
          this.imported.emit(armoredKeyPair);
        };
        reader.onabort = (abort) => {
          console.error('Abort event: ', abort);
          throw new Error('Account Keys import was aborted'); // TODO This would become a passed function
        };
        reader.onerror = (error: ErrorEvent) => {
          console.error('Error event: ', error);
          throw new Error('Error while reading the file: ' + error.message); // TODO This would become a passed function
        };

        reader.readAsText(fileInput.files[0]);

      } catch (error) {
        console.error(error);
      }
    }
  }

  private importAccountKeysAndroid() {
    this.fileChooser.open()
      .then((uri: string) => {
        return this.filePath.resolveNativePath(uri);
      })
      .then((nativePath: string) => {
        const fileIndex = nativePath.lastIndexOf('/');
        const path = nativePath.substring(0, fileIndex + 1);
        const file = nativePath.substring(fileIndex + 1);

        return this.file.readAsText(path, file);
      })
      .then((content: string) => {
        const armoredKeyPair: ArmoredKeyPair = JSON.parse(content);
        this.notifyAccountKeysSuccessfullyImported();
        this.imported.emit(armoredKeyPair);
      })
      .catch(error => {
        console.log(error);
      });
  }

  private notifyAccountKeysSuccessfullyImported() {
    this.notifications.showSuccess('Account Keys successfully imported!');
  }
}
