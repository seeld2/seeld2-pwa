import {ConnectPage} from "./connect-page";
import {Loading, LoadingController, NavParams} from "ionic-angular";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {ProfilesService} from "../../profiles/profiles.service";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {Profile} from "../../profiles/profile";
import {of} from "rxjs/observable/of";
import {UiNotificationsJasmine} from "../../uinotifications/ui-notifications.jasmine";
import {ProfilesJasmine} from "../../profiles/profiles.jasmine";
import {ContactsService} from "../../contacts/contacts.service";
import {ContactsJasmine} from "../../contacts/contacts.jasmine";

describe("ConnectPage", () => {

  const BOX_ADDRESS: string = 'box address';
  const SYSTEM_PUBLIC_KEYS: string = 'system public keys';
  const USERNAME: string = 'username';

  let contactsService: ContactsService;
  let loadingCtrl: LoadingController;
  let navParams: NavParams;
  let notifications: UiNotifications;
  let profiles: ProfilesService;

  let connectPage: ConnectPage;

  let loading: Loading;

  beforeEach(() => {
    contactsService = ContactsJasmine.contactsService();
    loadingCtrl = IonicJasmine.loadingController();
    navParams = jasmine.createSpyObj('navParams', ['get']);
    notifications = UiNotificationsJasmine.uiNotifications();
    profiles = ProfilesJasmine.profilesService();

    connectPage = new ConnectPage(contactsService, loadingCtrl, notifications, profiles, navParams);

    (<jasmine.Spy>navParams.get).and.returnValue(false);

    loading = jasmine.createSpyObj('loading', ['dismiss', 'present']);
    (<jasmine.Spy>loadingCtrl.create).and.returnValue(loading);
  });

  describe(", when tapping the button to connect to a person,", () => {

    it("should prevent the user from connecting to an undefined profile", (done: DoneFn) => {
      connectPage['profile'] = undefined;
      connectPage.connectToPersonTapped().add(() => {
        expect(loadingCtrl.create).not.toHaveBeenCalled();
        done();
      });
    });

    it("should prevent the user from connecting to an empty profile", (done: DoneFn) => {
      connectPage['profile'] = Profile.empty();
      connectPage.connectToPersonTapped().add(() => {
        expect(loadingCtrl.create).not.toHaveBeenCalled();
        done();
      });
    });

    it("should send a connection request to a profile", (done: DoneFn) => {
      connectPage.profile = Profile.build(BOX_ADDRESS, SYSTEM_PUBLIC_KEYS, USERNAME);

      (<jasmine.Spy>contactsService.requestContactWith).and.returnValue(of(true));

      connectPage.connectToPersonTapped().add(() => {

        expect(loadingCtrl.create).toHaveBeenCalledTimes(1);
        expect(loading.present).toHaveBeenCalledTimes(1);
        expect(contactsService.requestContactWith).toHaveBeenCalledWith(connectPage.profile);
        expect(notifications.showSuccess).toHaveBeenCalledTimes(1);
        expect(loading.dismiss).toHaveBeenCalledTimes(1);

        done();
      });
    });

    it("should always close the loader", (done: DoneFn) => {
      connectPage.profile = Profile.build(BOX_ADDRESS, SYSTEM_PUBLIC_KEYS, USERNAME);

      (<jasmine.Spy>contactsService.requestContactWith).and.returnValue(of({}));

      connectPage.connectToPersonTapped().add(() => {

        expect(loadingCtrl.create).toHaveBeenCalledTimes(1);
        expect(loading.present).toHaveBeenCalledTimes(1);
        expect(contactsService.requestContactWith).toHaveBeenCalledWith(connectPage.profile);
        // expect(notifications.showSuccess).not.toHaveBeenCalled();
        expect(loading.dismiss).toHaveBeenCalledTimes(1);

        done();
      });
    });
  });

  describe(", when searching a profile,", () => {

    beforeEach(() => {
      connectPage.model.username = USERNAME;
    });

    it("should prevent the user from searching for a profile he's already connected with", (done: DoneFn) => {

      (<jasmine.Spy>profiles.isNotConnectedTo).and.returnValue(false);

      connectPage.searchTapped().add(() => {

        expect(profiles.isNotConnectedTo).toHaveBeenCalledWith(USERNAME);

        done();
      });
    });

    it("should fetch the profile with the specified pseudo", (done: DoneFn) => {
      const profile: Profile = Profile.build(BOX_ADDRESS, SYSTEM_PUBLIC_KEYS, USERNAME);

      (<jasmine.Spy>profiles.isNotConnectedTo).and.returnValue(true);
      (<jasmine.Spy>profiles.fetchDetails).and.returnValue(of(profile));

      connectPage.searchTapped().add(() => {

        expect(profiles.isNotConnectedTo).toHaveBeenCalledWith(USERNAME);
        expect(loadingCtrl.create).toHaveBeenCalledTimes(1);
        expect(loading.present).toHaveBeenCalledTimes(1);
        expect(profiles.fetchDetails).toHaveBeenCalledWith(USERNAME);
        expect(notifications.showWarning).not.toHaveBeenCalled();

        done();
      });
    });

    it("should show a warning if the specified pseudo does not match any profile", (done: DoneFn) => {

      (<jasmine.Spy>profiles.isNotConnectedTo).and.returnValue(true);
      (<jasmine.Spy>profiles.fetchDetails).and.returnValue(of(Profile.empty()));

      connectPage.searchTapped().add(() => {

        expect(profiles.isNotConnectedTo).toHaveBeenCalledWith(USERNAME);
        expect(loadingCtrl.create).toHaveBeenCalledTimes(1);
        expect(loading.present).toHaveBeenCalledTimes(1);
        expect(profiles.fetchDetails).toHaveBeenCalledWith(USERNAME);
        expect(notifications.showWarning).toHaveBeenCalled();

        done();
      });
    });
  });
});
