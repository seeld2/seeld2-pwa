import {Component, ViewChild} from '@angular/core';
import {Validation} from "../../../shared/angular/validation/validation";
import {ConnectFormModel} from "./connect-form-model";
import {LoadingController, NavParams} from "ionic-angular";
import {catchError, finalize} from "rxjs/operators";
import {collapse} from "../../../shared/angular/animations/collapse";
import {ProfilesService} from "../../profiles/profiles.service";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {of} from "rxjs/observable/of";
import {Profile} from "../../profiles/profile";
import {Logger} from "../../utils/logging/logger";
import {Subscription} from "rxjs";
import {ContactsService} from "../../contacts/contacts.service";

@Component({
  selector: 'page-connect',
  templateUrl: 'connect.html',
  animations: [collapse.trigger]
})
export class ConnectPage {

  private readonly logger: Logger = Logger.for('ConnectPage');

  @ViewChild('connectForm') connectForm;

  disable = {
    connectButton: false,
    searchButton: false
  };
  display = {
    profile: 'hide'
  };
  firstTime: boolean = false;
  model: ConnectFormModel = {
    username: ''
  };
  profile: Profile;

  constructor(private readonly contactsService: ContactsService,
              private readonly loadingCtrl: LoadingController,
              private readonly notifications: UiNotifications,
              private readonly profiles: ProfilesService,
              readonly navParams: NavParams) {

    const firstTime: boolean = navParams.get('firstTime');
    if (firstTime) {
      this.firstTime = firstTime;
    }
  }

  connectToPersonTapped(): Subscription {

    if (!this.profile || this.profile.isEmpty()) {
      return Subscription.EMPTY;
    }

    this.disable.connectButton = true;
    this.disable.searchButton = true;

    const loading = this.loadingCtrl.create({content: 'Encrypting and sending connection request...'});
    loading.present();

    return this.contactsService.requestContactWith(this.profile)
      .subscribe(
        () => {
          this.clearProfile();
          this.notifications.showSuccess(`Sent connection request to "${this.model.username}"!`);
          this.model.username = '';
        },
        error => this.logger.error(`Error while requesting contact with ${this.profile.pseudo()}`, error),
        () => {
          loading.dismiss();
          this.disable.connectButton = false;
          this.disable.searchButton = false;
        }
      );
  }

  forceValidationOf(fieldName: string) {
    Validation.force(this.connectForm.controls[fieldName]);
    Validation.refreshIonItem(fieldName);
  }

  searchTapped(): Subscription {

    if (this.profiles.isNotConnectedTo(this.model.username)) {

      this.disable.searchButton = true;

      const loading = this.loadingCtrl.create({content: 'Searching ...'});
      loading.present();

      this.clearProfile();

      return this.profiles.fetchDetails(this.model.username)
        .pipe(
          catchError((error: any) => {
            if (error.status && error.status === 404) {
              return of(Profile.empty())
            }
            throw error;
          }),
          finalize(() => {
            loading.dismiss();
            this.disable.searchButton = false;
          })
        )
        .subscribe((profile: Profile) => {
          this.profile = profile;
          if (this.profile.isEmpty()) {
            this.notifications.showWarning(`No "${this.model.username}" was found`);
          } else {
            this.showProfile();
          }
        });
    }

    return Subscription.EMPTY;
  }

  private clearProfile() {
    this.display.profile = 'hide';

  }

  private showProfile() {
    this.display.profile = 'show';
  }
}
