import {Component} from "@angular/core";
import {AlertController, NavParams, ViewController} from "ionic-angular";
import {Contact} from "../../user/contact/contact";
import {ContactActions} from "./contact-actions";

@Component({
  selector: 'page-contacts-action',
  templateUrl: 'contact-actions.html'
})
export class ContactActionsPage {

  private readonly contact: Contact;

  constructor(private readonly alertController: AlertController,
              private readonly navParams: NavParams,
              private readonly viewController: ViewController) {
    this.contact = this.navParams.data;
  }

  deleteContact() {
    this.alertController.create({
      buttons: [
        {
          cssClass: 'danger',
          handler: () => {
            this.viewController.dismiss(ContactActions.REMOVE_CONTACT);
          },
          text: "Yes"
        },
        {
          handler: () => {
            this.viewController.dismiss(ContactActions.NONE);
          },
          text: "No"
        }
      ],
      enableBackdropDismiss: false,
      message: `Are you sure you want to remove ${this.contact.description()} from your contacts?`,
      title: "Delete contact"
    }).present();
  }
}
