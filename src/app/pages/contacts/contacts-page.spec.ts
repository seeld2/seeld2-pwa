import {ContactsPage} from "./contacts-page";
import {PlatformService} from "../../mobile/platform.service";
import {Events, LoadingController, NavController, NavParams, PopoverController} from "ionic-angular";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {SessionService} from "../../sessions/session.service";
import {UserEventsService} from "../../userevents/user-events.service";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {Session} from "../../sessions/session";
import {Contact} from "../../user/contact/contact";
import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";
import {of} from "rxjs/observable/of";
import {UserEventsJasmine} from "../../userevents/user-events.jasmine";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {UiNotificationsJasmine} from "../../uinotifications/ui-notifications.jasmine";
import {PlatformJasmine} from "../../mobile/platform.jasmine";
import {ContactsService} from "../../contacts/contacts.service";
import {ContactsJasmine} from "../../contacts/contacts.jasmine";
import {ContactRequest} from "../../contacts/request/contact-request";
import {ContactRequestResponseHandlingResult} from "../../contacts/request/contact-request-response-handling-result";
import {LoaderService} from "../../mobile/loader.service";
import {CoreJasmine} from "../../../core/core-jasmine";

describe("ContactsPage", () => {

  const BOBBY: string = 'bobby';
  const CAROL: string = 'carol';
  const EVENT_ID_1: string = 'eventid1';
  const EVENT_ID_2: string = 'eventid2';

  let platformService: PlatformService;
  let contactsService: ContactsService;
  let events: Events;
  let loaderService: LoaderService;
  let loadingCtrl: LoadingController;
  let navCtrl: NavController;
  let uiNotifications: UiNotifications;
  let popoverController: PopoverController;
  let sessionService: SessionService;
  let userEventsService: UserEventsService;
  let navParams: NavParams;

  let page: ContactsPage

  let session: Session;

  beforeEach(() => {

    platformService = PlatformJasmine.platformService();
    contactsService = ContactsJasmine.contactsService();
    events = IonicJasmine.events();
    loaderService = CoreJasmine.loaderService();
    loadingCtrl = IonicJasmine.loadingController();
    navCtrl = jasmine.createSpyObj('navCtrl', ['push']);
    uiNotifications = UiNotificationsJasmine.uiNotifications();
    popoverController = null;
    sessionService = SessionsJasmine.sessionService();
    userEventsService = UserEventsJasmine.userEventsService();
    navParams = jasmine.createSpyObj('navParams', ['get']);

    page = new ContactsPage(platformService, contactsService, events, loaderService, loadingCtrl,
      navCtrl, uiNotifications, popoverController, sessionService, userEventsService, navParams);

    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);
  });

  describe(", when handling a contact request event,", () => {

    it("should reject the request silently if the requester is already a contact", () => {

      (<jasmine.Spy>session.hasContactWithPseudo).and.returnValue(true);
      (<jasmine.Spy>contactsService.rejectContactRequestSilently).and.returnValue(of({}));

      const userEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_1, UserEventType.CONTACT_REQUEST);
      page['handleContactRequestEvent'](userEvent);

      expect(session.hasContactWithPseudo).toHaveBeenCalledWith(BOBBY);
      expect(contactsService.rejectContactRequestSilently).toHaveBeenCalled();
    });

    it("should reject the request silently if it's a duplicate of a request that's already listed on the page", () => {
      const userEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_1, UserEventType.CONTACT_REQUEST);
      page['contactRequestEvents'] = [{contactRequest: ContactRequest.fromUserEvent(userEvent), eventId: EVENT_ID_1}];

      (<jasmine.Spy>session.hasContactWithPseudo).and.returnValue(false);
      (<jasmine.Spy>contactsService.rejectContactRequestSilently).and.returnValue(of({}));

      const duplicateUserEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_2, UserEventType.CONTACT_REQUEST);
      page['handleContactRequestEvent'](duplicateUserEvent);

      expect(contactsService.rejectContactRequestSilently).toHaveBeenCalled();
    });

    it("should prevent the request from being listed more than once on the page", () => {
      const userEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_1, UserEventType.CONTACT_REQUEST);
      page['contactRequestEvents'] = [{contactRequest: ContactRequest.fromUserEvent(userEvent), eventId: EVENT_ID_1}];

      (<jasmine.Spy>session.hasContactWithPseudo).and.returnValue(false);

      const duplicateUserEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_1, UserEventType.CONTACT_REQUEST);
      page['handleContactRequestEvent'](duplicateUserEvent);

      expect(page['contactRequestEvents'].length).toEqual(1);
    });

    it("should list the request on page", () => {
      const userEvent: UserEvent = UserEvent
        .build({requesterUsername: BOBBY}, EVENT_ID_1, UserEventType.CONTACT_REQUEST);
      page['contactRequestEvents'] = [{contactRequest: ContactRequest.fromUserEvent(userEvent), eventId: EVENT_ID_1}];

      (<jasmine.Spy>session.hasContactWithPseudo).and.returnValue(false);

      const newerEvent: UserEvent = UserEvent
        .build({requesterUsername: CAROL}, EVENT_ID_2, UserEventType.CONTACT_REQUEST);
      page['handleContactRequestEvent'](newerEvent);

      expect(page['contactRequestEvents'].length).toEqual(2);
    });
  });

  describe(", when  handling a contact request response event,", () => {

    const ACCEPTER_DISPLAY_NAME: string = "accepterDisplayName";
    const ACCEPTER_PUBLIC_KEYS: string = "accepterPublicKeys";
    const ACCEPTER_PSEUDO: string = "accepterPseudo";
    const ACCEPTER_WRITE_TO: string = "accepterWriteTo";
    const REQUESTER_WRITE_TO: string = "requesterWriteTo";
    const SECRET_KEY: string = "secretKey";
    const SILENTLY: boolean = true;

    it("should handle an accepted contact request response", (done: DoneFn) => {

      (<jasmine.Spy>contactsService.handleContactRequestResponse).and.returnValue(of({
        hasAcceptedContactRequest: true,
        notify: true,
        profileContactDescription: ""
      } as ContactRequestResponseHandlingResult));

      (<jasmine.Spy>contactsService.syncContactsToRemote).and.returnValue(of({}));
      (<jasmine.Spy>session.contactsAsArraySortedByDescription).and.returnValue([aContact(CAROL), aContact(BOBBY)]);

      const userEvent: UserEvent = UserEvent
        .build({
          accepterDisplayName: ACCEPTER_DISPLAY_NAME,
          accepterPublicKeys: ACCEPTER_PUBLIC_KEYS,
          accepterUsername: ACCEPTER_PSEUDO,
          accepterWriteTo: ACCEPTER_WRITE_TO,
          requesterWriteTo: REQUESTER_WRITE_TO,
          silently: SILENTLY,
          secretKey: SECRET_KEY,
        }, EVENT_ID_1, UserEventType.CONTACT_REQUEST_RESPONSE);
      page['handleContactRequestResponseEvent'](userEvent).add(() => {

        expect(uiNotifications.showSuccess).toHaveBeenCalled();
        expect(userEventsService.fetchAndPublishNewUserEvents).toHaveBeenCalled();
        done()
      });
    });

    it("should handle a rejected contact request response", (done: DoneFn) => {

      (<jasmine.Spy>contactsService.handleContactRequestResponse).and.returnValue(of({
        hasAcceptedContactRequest: false,
        notify: true,
        profileContactDescription: ""
      } as ContactRequestResponseHandlingResult));

      (<jasmine.Spy>contactsService.syncContactsToRemote).and.returnValue(of({}));
      (<jasmine.Spy>session.contactsAsArraySortedByDescription).and.returnValue([aContact(CAROL), aContact(BOBBY)]);

      const userEvent: UserEvent = UserEvent
        .build({
          accepterDisplayName: ACCEPTER_DISPLAY_NAME,
          accepterPublicKeys: ACCEPTER_PUBLIC_KEYS,
          accepterUsername: ACCEPTER_PSEUDO,
          accepterWriteTo: REQUESTER_WRITE_TO,
          requesterWriteTo: REQUESTER_WRITE_TO,
          silently: SILENTLY,
          secretKey: SECRET_KEY,
        }, EVENT_ID_1, UserEventType.CONTACT_REQUEST_RESPONSE);
      page['handleContactRequestResponseEvent'](userEvent).add(() => {

        expect(uiNotifications.showWarning).toHaveBeenCalled();
        expect(userEventsService.fetchAndPublishNewUserEvents).toHaveBeenCalled();
        done()
      });
    });
  });

  it("should handle a contact request remove event", (done: DoneFn) => {

    (<jasmine.Spy>contactsService.handleContactRemove).and.returnValue(of(true));

    (<jasmine.Spy>contactsService.syncContactsToRemote).and.returnValue(of({}));
    (<jasmine.Spy>session.contactsAsArraySortedByDescription).and.returnValue([aContact(CAROL), aContact(BOBBY)]);

    const userEvent: UserEvent = UserEvent.build({pseudo: CAROL}, EVENT_ID_1, UserEventType.CONTACT_REMOVE);
    page['handleContactRemoveEvent'](userEvent).add(() => {

      expect(contactsService.handleContactRemove).toHaveBeenCalled()
      expect(userEventsService.fetchAndPublishNewUserEvents).toHaveBeenCalled();
      done()
    });
  });

  it("should refresh the page", (done: DoneFn) => {
    const carol: Contact = aContact(CAROL);
    const bobby: Contact = aContact(BOBBY);

    (<jasmine.Spy>contactsService.syncContactsToRemote).and.returnValue(of({}));
    (<jasmine.Spy>session.contactsAsArraySortedByDescription).and.returnValue([carol, bobby]);

    page['refresh']().add(() => {
      expect(userEventsService.fetchAndPublishNewUserEvents).toHaveBeenCalled();
      expect(page['contacts']).toEqual([bobby, carol]);
      expect(page['refreshedAtLeastOnce']).toBe(true);
      done();
    });
  });

  function aContact(pseudo: string): Contact {
    return Contact.with(pseudo, null, null, null, null, null);
  }
});
