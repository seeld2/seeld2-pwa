import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  Events,
  LoadingController,
  NavController,
  NavParams,
  Popover,
  PopoverController,
  Refresher
} from 'ionic-angular';
import {Observable, Subscription} from "rxjs";
import {forkJoin} from "rxjs/observable/forkJoin";
import {of} from "rxjs/observable/of";
import {finalize, mergeMap} from "rxjs/operators";
import {ConversationEvent} from "../../conversations/conversation-event";
import {config} from "../../app.config";
import {AppEvent} from "../../app.event";
import {ContactsService} from "../../contacts/contacts.service";
import {ContactRemove} from "../../contacts/remove/contact-remove";
import {ContactRemoveEvent} from "../../contacts/remove/contact-remove-event";
import {ContactRequest} from "../../contacts/request/contact-request";
import {ContactRequestEvent} from "../../contacts/request/contact-request-event";
import {ContactRequestResponse} from "../../contacts/request/contact-request-response";
import {ContactRequestResponseEvent} from "../../contacts/request/contact-request-response-event";
import {ContactRequestResponseHandlingResult} from "../../contacts/request/contact-request-response-handling-result";
import {LoaderService} from "../../mobile/loader.service";
import {PlatformService} from "../../mobile/platform.service";
import {Refreshers} from "../../mobile/refreshers";
import {SessionService} from "../../sessions/session.service";
import {Tab} from "../../tab";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {Contact} from "../../user/contact/contact";
import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEventsService} from "../../userevents/user-events.service";
import {Task} from "../../utils/execution/task";
import {TaskExecutor} from "../../utils/execution/task-executor";
import {Logger} from "../../utils/logging/logger";
import {ConnectPage} from "../connect/connect-page";
import {ConversationPage} from "../conversations/conversation/conversation-page";
import {ContactActions} from "./contact-actions";
import {ContactActionsPage} from "./contact-actions.page";

@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage implements OnInit, OnDestroy {

  private readonly logger: Logger = Logger.for("ContactsPage");
  private readonly taskExecutor: TaskExecutor = TaskExecutor.create();

  private isRefreshing: boolean = false;
  private refreshedAtLeastOnce: boolean = false;

  contactRequestEvents: ContactRequestEvent[] = []; // TODO Turn this into an entity that encapsulates events!
  contacts: Contact[] = []; // TODO Turn this into an entity that encapsulates contacts!
  disable = {
    refreshButton: false
  };
  selectedContacts: any = {};
  selectionMode: boolean;

  constructor(public readonly platforms: PlatformService,
              private readonly contactsService: ContactsService,
              private readonly events: Events,
              private readonly loaderService: LoaderService,
              private readonly loadingCtrl: LoadingController,
              private readonly navCtrl: NavController,
              private readonly uiNotifications: UiNotifications,
              private readonly popoverController: PopoverController,
              private readonly sessionService: SessionService,
              private readonly userEventsService: UserEventsService,
              navParams: NavParams) {

    const recipients: string[] = navParams.get('recipients');

    this.selectionMode = recipients !== undefined;

    if (recipients) {
      recipients.forEach((recipient: string) => this.selectedContacts[recipient] = true);
    }
  }

  ngOnInit(): void {

    this.events.subscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST), (userEvent: UserEvent) => {
      this.taskExecutor.queue(Task.subscription(this, 100, this.handleContactRequestEvent, [userEvent]));
      this.taskExecutor.executeWhileNext();
    });

    this.events.subscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST_RESPONSE), (userEvent: UserEvent) => {
      this.taskExecutor.queue(Task.subscription(this, 100, this.handleContactRequestResponseEvent, [userEvent]));
      this.taskExecutor.executeWhileNext();
    });

    this.events.subscribe(UserEvent.topicFor(UserEventType.CONTACT_REMOVE), (userEvent: UserEvent) => {
      this.taskExecutor.queue(Task.subscription(this, 10, this.handleContactRemoveEvent, [userEvent]));
      this.taskExecutor.executeWhileNext();
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST));
    this.events.unsubscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST_RESPONSE));
    this.events.unsubscribe(UserEvent.topicFor(UserEventType.CONTACT_REMOVE));
  }

  // noinspection JSUnusedGlobalSymbols
  ionViewDidEnter() {
    this.taskExecutor.queue(Task.subscription(this, 1, this.refresh));
    this.taskExecutor.executeWhileNext();
  }

  ionViewDidLeave() {
    if (this.selectionMode) {
      this.events.publish(ConversationEvent.UPDATE_RECIPIENTS, this.selectedContacts);
    }
  }

  // ACTIONS

  connectTapped() {
    const firstTime: boolean = this.contacts.length === 0;
    this.navCtrl.push(ConnectPage, {firstTime: firstTime});
  }

  contactRequestAccepted(eventId: string) {

    const loading = this.loadingCtrl.create({content: 'Encrypting and accepting connection request'});
    loading.present();

    const contactRequestEvent = this.contactRequestEvents
      .find((contactRequestEvent: ContactRequestEvent) => contactRequestEvent.eventId === eventId);

    if (contactRequestEvent) {
      this.contactsService.acceptContactRequest(contactRequestEvent)
        .pipe(
          mergeMap(acceptedEventId => {
            // Reject all other eventual contact requests for that pseudo: we don't need them anymore, since user has accepted!
            const observables: Observable<any>[] = this.contactRequestEvents
              .filter(event => event.eventId !== acceptedEventId)
              .filter(event => event.contactRequest.requesterPseudo() === contactRequestEvent.contactRequest.requesterPseudo())
              .map((event: ContactRequestEvent) => this.contactsService.rejectContactRequestSilently(event));
            return forkJoin(observables);
          }),
          finalize(() => loading.dismiss())
        )
        .subscribe(
          () => {},
          (error) => this.logger.error(error),
          () => {
            this.uiNotifications.showSuccess('Contact request accepted');

            this.taskExecutor.queue(Task.subscription(this, 100, this.refresh));
            this.taskExecutor.executeWhileNext();
          }
        );
    }
  }

  contactRequestRejected(eventId: string) {

    const loading = this.loadingCtrl.create({content: 'Encrypting and rejecting connection request'});
    loading.present();

    const contactRequest = this.contactRequestEvents
      .find((contactRequest: ContactRequestEvent) => contactRequest.eventId === eventId);

    if (contactRequest) {
      this.contactsService.rejectContactRequest(contactRequest)
        .pipe(
          finalize(() => {
            loading.dismiss();
          })
        )
        .subscribe(() => {
          this.uiNotifications.showSuccess('Contact request rejected');
          this.refreshRequested();
        });
    }
  }

  moreOptionsTapped(event: Event, contact: Contact) {
    const popover: Popover = this.popoverController.create(ContactActionsPage, contact);
    popover.onDidDismiss((option) => {
      if (option) {
        switch (option) {
          case ContactActions.REMOVE_CONTACT:
            this.contactsService.deleteContact(contact)
              .subscribe(() => {
                this.taskExecutor.queue(Task.subscription(this, 100, this.refresh));
                this.taskExecutor.executeWhileNext();
                // this.refresh();

                this.uiNotifications.showInfo(`${contact.description()} has been removed from your contacts`);
              });
        }
      }
    });
    popover.present({ev: event});
  }

  refreshRequested(refresher?: Refresher) {
    this.taskExecutor.queue(Task.subscription(this, 1, this.refresh, [refresher]));
    this.taskExecutor.executeWhileNext();
    // this.refresh(refresher);
  }

  switchToConversationsTabTapped() {
    this.events.publish(AppEvent.TABS_SWITCH, Tab.CONVERSATIONS);
  }

  writeTo(contact: Contact) {
    this.navCtrl.push(ConversationPage, {contact: contact, previousPage: 'ContactsPage'});
  }

  // PUBLIC

  isFirstTime(): boolean {
    return this.refreshedAtLeastOnce && !this.isRefreshing && this.contacts.length === 0;
  }

  // PRIVATE

  private endRefresh(refresher: Refresher) {
    this.loaderService.dismissLoader();
    if (refresher) {
      Refreshers.closeAfterDelay(refresher);
    }

    this.isRefreshing = false;
    this.refreshedAtLeastOnce = true;

    const timeoutId = setTimeout(() => {
      this.disable.refreshButton = false;
      clearTimeout(timeoutId);
    }, 250);
  }

  private handleContactRemoveEvent(userEvent: UserEvent): Subscription {
    const contactRemoveEvent: ContactRemoveEvent = {
      contactRemove: ContactRemove.fromUserEvent(userEvent),
      eventId: userEvent.eventId()
    }

    return this.contactsService.handleContactRemove(contactRemoveEvent)
      .subscribe((removed: boolean) => {
        if (removed) {
          this.uiNotifications.showWarning(`${contactRemoveEvent.contactRemove.pseudo()} removed you from his contacts`);

          this.taskExecutor.queue(Task.subscription(this, 100, this.refresh));
          this.taskExecutor.executeWhileNext();
          // this.refresh();
        }
      });
  }

  private handleContactRequestEvent(userEvent: UserEvent): Subscription {

    const receivedContactRequestEvent: ContactRequestEvent = {
      contactRequest: ContactRequest.fromUserEvent(userEvent),
      eventId: userEvent.eventId()
    };

    const contactRequestPseudo = receivedContactRequestEvent.contactRequest.requesterPseudo();

    // If the contact request is already a contact, reject the contact request silently
    if (this.sessionService.session().hasContactWithPseudo(contactRequestPseudo)) {
      return this.contactsService.rejectContactRequestSilently(receivedContactRequestEvent).subscribe();
    }

    // If the contact request for that pseudo is already listed AND the event IDs differ, then reject the contact request silently
    const existingRequestIndex: number = this.contactRequestEvents
      .findIndex((event: ContactRequestEvent) =>
        event.contactRequest.requesterPseudo() === contactRequestPseudo && event.eventId !== receivedContactRequestEvent.eventId);
    if (existingRequestIndex >= 0) {
      return this.contactsService.rejectContactRequestSilently(receivedContactRequestEvent).subscribe();
    }

    // If the contact request for that pseudo is NOT already listed, then add it to the list of contactRequestEvents
    const sameRequestIndex: number = this.contactRequestEvents
      .findIndex((event: ContactRequestEvent) => event.eventId === receivedContactRequestEvent.eventId);
    if (sameRequestIndex === -1) {
      this.contactRequestEvents.push(receivedContactRequestEvent);
    }

    return of({}).subscribe();
  }

  private handleContactRequestResponseEvent(userEvent: UserEvent): Subscription {

    const receivedContactRequestResponseEvent: ContactRequestResponseEvent = {
      contactRequestResponse: ContactRequestResponse.fromUserEvent(userEvent),
      eventId: userEvent.eventId()
    };

    return this.contactsService.handleContactRequestResponse(receivedContactRequestResponseEvent)
      .subscribe((result: ContactRequestResponseHandlingResult) => {
        if (result.hasAcceptedContactRequest) {
          this.uiNotifications.showSuccess(`${result.profileContactDescription} accepted your contact request!`);
        } else if (result.notify) {
          this.uiNotifications.showWarning(`${result.profileContactDescription} rejected your contact request!`);
        }
        this.refresh();
      });
  }

  private refresh(refresher?: Refresher): Subscription {
    this.logger.trace(`refresh()`);

    if (!this.isRefreshing) {
      this.startRefresh();

      // Refresh contact requests events
      this.contactRequestEvents.splice(0);

      // Refresh contacts
      return this.contactsService.syncContactsToRemote()
        .subscribe(
          () => {
            this.contacts.splice(0);
            this.sessionService.session().contactsAsArraySortedByDescription()
              .sort((contact1: Contact, contact2: Contact) => contact1.description().localeCompare(contact2.description()))
              .forEach((contact: Contact) => this.contacts.push(contact));
          },
          // error
          (error) => {
            this.logger.error("Something unexpected went wrong while refreshing", error);
            this.endRefresh(refresher);
          },
          // complete
          () => {
            this.endRefresh(refresher);
            this.userEventsService.fetchAndPublishNewUserEvents();
          }
        );
    }
  }

  private startRefresh() {
    this.disable.refreshButton = true;
    this.isRefreshing = true;

    this.loaderService.createLoader({delayBeforeShowingInMillis: config.ui.loading.delayBeforeShowingInMillis});
  }
}
