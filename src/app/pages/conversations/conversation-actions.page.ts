import {Component} from "@angular/core";
import {AlertController, ViewController} from "ionic-angular";
import {ConversationActions} from "./conversation-actions";

@Component({
  selector: 'page-conversation-actions',
  templateUrl: 'conversation-actions.html'
})
export class ConversationActionsPage {

  constructor(private readonly alertController: AlertController,
              private readonly viewController: ViewController) {
  }

  deleteConversation() {
    this.alertController.create({
      buttons: [
        {
          cssClass: 'danger',
          handler: () => {
            this.viewController.dismiss(ConversationActions.DELETE_CONVERSATION)
          },
          text: "Yes, I'm sure"
        },
        {
          handler: () => {
            this.viewController.dismiss()
          },
          text: "No, don't delete"
        }
      ],
      enableBackdropDismiss: false,
      message: "The conversation will be permanently deleted from the server and this client, " +
        "but cached versions will remain on your other devices and on your contacts' devices",
      title: "DELETE CONVERSATION"
    }).present();
  }
}
