import {File} from "../../../../shared/files/file";

export interface ConversationFormModel {
  title: string;
  message: string;
  attachment: File
}
