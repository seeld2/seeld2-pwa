import {ElementRef, NgZone} from "@angular/core";
import {AlertController, Events, NavController, NavParams, PopoverController} from "ionic-angular";
import {of} from "rxjs/observable/of";
import {CoreJasmine} from "../../../../core/core-jasmine";
import {Files} from "../../../../shared/files/files";
import {IonicJasmine} from "../../../../shared/ionic-jasmine";
import {ConversationsJasmine} from "../../../conversations/conversations.jasmine";
import {ConversationsService} from "../../../conversations/conversations.service";
import {Message} from "../../../conversations/messages/message";
import {MessageTestBuilder as aMessage} from "../../../conversations/messages/message.test-builder";
import {MessagesService} from "../../../conversations/messages/messages.service";
import {PlatformJasmine} from "../../../mobile/platform.jasmine";
import {PlatformService} from "../../../mobile/platform.service";
import {SessionService} from "../../../sessions/session.service";
import {SessionTestBuilder as aSession} from "../../../sessions/session.test-builder";
import {SessionsJasmine} from "../../../sessions/sessions.jasmine";
import {TimeService} from "../../../time/time.service";
import {UiNotifications} from "../../../uinotifications/ui-notifications";
import {UiNotificationsJasmine} from "../../../uinotifications/ui-notifications.jasmine";
import {Contact} from "../../../user/contact/contact";
import {ContactKey} from "../../../user/contact/contact-key";
import {ContactTestBuilder as aContact} from "../../../user/contact/contact.test-builder";
import {Contacts} from "../../../user/contact/contacts";
import {ConversationPage} from "./conversation-page";

describe("ConversationPage", () => {

  const CONVERSATION_ID: string = "CONVERSATION_ID";
  const MESSAGE_ID_1: string = "messageId1";
  const MESSAGE_ID_2: string = "messageId2";
  const MESSAGE_ID_3: string = "messageId3";

  let platforms: PlatformService;
  let ngZone: NgZone;
  let alertController: AlertController;
  let conversations: ConversationsService;
  let events: Events;
  let files: Files;
  let messagesService: MessagesService;
  let navCtrl: NavController;
  let notifications: UiNotifications;
  let popoverController: PopoverController;
  let sessionService: SessionService;
  let timeService: TimeService;
  let navParams: NavParams;

  let conversationPage: ConversationPage;

  beforeEach(() => {
    platforms = PlatformJasmine.platformService();
    ngZone = jasmine.createSpyObj('ngZone', ['run']);
    alertController = IonicJasmine.alertController();
    conversations = ConversationsJasmine.conversationsService();
    events = IonicJasmine.events();
    files = CoreJasmine.files();
    messagesService = CoreJasmine.messages();
    navCtrl = CoreJasmine.navController();
    notifications = UiNotificationsJasmine.uiNotifications();
    navParams = new NavParams({});
    sessionService = SessionsJasmine.sessionService();
    timeService = new TimeService(null);

    conversationPage = new ConversationPage(platforms, ngZone, alertController, conversations, events, files,
      messagesService, navCtrl, notifications, popoverController, sessionService, timeService, navParams);

    conversationPage.attachmentFileInput = {
      nativeElement: jasmine.createSpyObj('nativeElement', ['dispatchEvent'])
    } as ElementRef;
  });

  describe(", when receiving an event to update the To's", () => {

    beforeEach(() => {
      (<jasmine.Spy>sessionService.session).and.returnValue(aSession.with()
        .contacts(Contacts.fromArrayOfContacts([
          contact('alreadyPresent'), contact('toAdd'), contact('toRemove'), contact('notPresent')]))
        .asIs());
    });

    it("should add the contacts marked as selected and missing from the current recipients", () => {

      conversationPage['to'] = [contact('alreadyPresent'), contact('toRemove')];

      conversationPage['refreshTosUsingSelectedContacts']({
        'alreadyPresent': true,
        'toAdd': true,
        'toRemove': true,
        'notPresent': false
      });

      const toUsernames: string[] = conversationPage['to'].map((contact: Contact) => contact.pseudo());
      expect(toUsernames.length).toEqual(3);
      expect(toUsernames).toContain('alreadyPresent');
      expect(toUsernames).toContain('toAdd');
      expect(toUsernames).toContain('toRemove');
    });

    it("should remove the contacts marked as unselected and present in the current recipients", () => {

      conversationPage['to'] = [contact('alreadyPresent'), contact('toRemove')];

      conversationPage['refreshTosUsingSelectedContacts']({
        'alreadyPresent': true,
        'toAdd': false,
        'toRemove': false,
        'notPresent': false
      });

      const toUsernames: string[] = conversationPage['to'].map((contact: Contact) => contact.pseudo());
      expect(toUsernames.length).toEqual(1);
      expect(toUsernames).toContain('alreadyPresent');
    });

    function contact(username: string) {
      return Contact.with(username, null, null, null, ContactKey.with(null, null, null), null);
    }
  });

  // it("should refresh the view to have a sync'ed list of the conversation's messages", (done: DoneFn) => {
  //
  //   const conversation: Conversation = aConversation.with().conversationId(CONVERSATION_ID).noBlanks();
  //
  //   conversationPage['conversation'] = conversation;
  //   conversationPage['messages'] = [aMessageWithMessageId("M1", 100)];
  //
  //   (<jasmine.Spy>messagesService.listMessages).and.returnValue(of(aMessageWithMessageId("M2", 200)));
  //   (<jasmine.Spy>conversations.markConversationAsRead).and.returnValue(of(true));
  //
  //   conversationPage['refresh']().add(() => {
  //
  //     expect(messagesService.listMessages).toHaveBeenCalledWith(conversation, 0, config.messages.pageSize);
  //     expect(conversations.markConversationAsRead).toHaveBeenCalledWith(conversationPage['conversation']);
  //
  //     const messages: Message[] = conversationPage['messages'];
  //     expect(messages.length).toEqual(2);
  //
  //     expect(conversationPage['conversation'].message().messageId()).toEqual("M2");
  //
  //     done();
  //   });
  // });

  describe(", when tapping on the attach button,", () => {

    describe(", on a browser", () => {

      it("should trigger the opening of the file input that allows uploading attachments", () => {

        (platforms.isAndroidApp as jasmine.Spy).and.returnValue(false);

        conversationPage.attachTapped();

        expect(conversationPage.attachmentFileInput.nativeElement.dispatchEvent)
          .toHaveBeenCalledWith(jasmine.any(MouseEvent));
      });

      it("should load the file to be sent with the message", () => {

        conversationPage['attachmentFileInputSelected']();

        expect(files.readHtmlFileInputAsBytes).toHaveBeenCalledWith(conversationPage.attachmentFileInput.nativeElement,
          jasmine.any(Function), jasmine.any(Function), jasmine.any(Function));
      });
    });
  });

  describe(", when deleting selected messages is tapped,", () => {

    it("should delete the selected messages", (done: DoneFn) => {

      const message1: Message = aMessage.with().messageId(MESSAGE_ID_1).asIs();
      const message2: Message = aMessage.with().messageId(MESSAGE_ID_2).asIs();
      const message3: Message = aMessage.with().messageId(MESSAGE_ID_3).asIs();
      conversationPage['messages'] = [message3, message2, message1];

      conversationPage['selectedMessages'][MESSAGE_ID_1] = true;
      conversationPage['selectedMessages'][MESSAGE_ID_3] = false;

      (<jasmine.Spy>messagesService.deleteMessages).and.returnValue(of({}));

      conversationPage['deleteSelectedTapped']().add(() => {
        expect(messagesService.deleteMessages).toHaveBeenCalledWith([message1]);
        const messages: Message[] = conversationPage['messages'];
        expect(messages.length).toEqual(2);
        expect(messages[0].messageId()).toEqual(MESSAGE_ID_3);
        expect(messages[1].messageId()).toEqual(MESSAGE_ID_2);
        done();
      });
    });

    it("should stop if no messages were selected for deletion", (done: DoneFn) => {
      conversationPage['deleteSelectedTapped']().add(() => {
        done();
      });
    });
  });

  describe(", when tapping on the send button,", () => {

    it("should add the message to the list of conversation's messages", (done: DoneFn) => {

      conversationPage.conversationForm = {form: {valid: true}};
      conversationPage['disable']['sendButton'] = false;
      conversationPage['to'] = [aContact.with().noBlanks()];

      conversationPage.model = {title: 'title', message: 'message', attachment: null};

      (<jasmine.Spy>messagesService.sendMessage).and.returnValue(of(aMessageWithIdAndSendDate("test", 1)));
      (<jasmine.Spy>navCtrl.getPrevious).and.returnValue({name: ''});

      conversationPage.sendTapped().add(() => {

        expect(events.publish).toHaveBeenCalledTimes(2);
        expect(messagesService.sendMessage).toHaveBeenCalled();

        expect(conversationPage['messages'].length).toBe(1);

        done();
      });
    });

    it("should prevent the sending if something is missing for the message to be valid", (done: DoneFn) => {

      conversationPage.conversationForm = {form: {valid: false}};
      conversationPage['disable']['sendButton'] = true;
      conversationPage['to'] = [];

      conversationPage.sendTapped().add(() => {
        expect(conversationPage['messages'].length).toBe(0);
        done();
      });
    });
  });

  describe(", when checking if the message can be sent,", () => {

    it("should confirm that message can be sent", () => {
      conversationPage.conversationForm = {form: {valid: true}};
      conversationPage['disable']['sendButton'] = false;
      conversationPage['to'] = [aContact.with().noBlanks()];
      expect(conversationPage.canSend()).toEqual(true);
    });

    it("should negate that message can be sent when the form is invalid", () => {
      conversationPage.conversationForm = {form: {valid: false}};
      conversationPage['disable']['sendButton'] = false;
      conversationPage['to'] = [aContact.with().noBlanks()];
      expect(conversationPage.canSend()).toEqual(false);
    });

    it("should negate that message can be sent when no recipients are specified", () => {
      conversationPage.conversationForm = {form: {valid: true}};
      conversationPage['disable']['sendButton'] = false;
      conversationPage['to'] = [];
      expect(conversationPage.canSend()).toEqual(false);
    });

    it("should negate that message can be sent when the send button is disabled", () => {
      conversationPage.conversationForm = {form: {valid: true}};
      conversationPage['disable']['sendButton'] = true;
      conversationPage['to'] = [aContact.with().noBlanks()];
      expect(conversationPage.canSend()).toEqual(false);
    });
  });

  describe(", when handling a message after a send or a refresh,", () => {

    it("should add the latest message at the end if it's the latest one", () => {

      conversationPage['messages'] = [aMessageWithIdAndSendDate("1", 1000), aMessageWithIdAndSendDate("2", 2000)];

      conversationPage['handleMessage'](aMessageWithIdAndSendDate("3", 3000), false);

      expect(conversationPage['messages'].length).toBe(3);
      expect(conversationPage['messages'].length).toBe(3);
      expect(conversationPage['messages'][2].messageId()).toEqual("3");
    });

    it("should insert the message at the right position if it's not the latest one", () => {

      conversationPage['messages'] = [aMessageWithIdAndSendDate("1", 1), aMessageWithIdAndSendDate("3", 3)];

      conversationPage['handleMessage'](aMessageWithIdAndSendDate("2", 2), false);

      expect(conversationPage['messages'].length).toBe(3);
      expect(conversationPage['messages'][1].messageId()).toEqual("2");
    });
  });

  // describe(", when refreshing,", () => {
  //
  //   it("should list the refreshed messages", (done: DoneFn) => {
  //
  //     // const firstMessage: Message = new Message("title", "content1", "alice", 1, null, null, 1, "c1", "m1");
  //     const firstMessage: Message = aMessage.with().title("title").content("content1").sender("alice").order(1).conversationId("c1").and.messageId("m1").asIs();
  //
  //     conversationPage['conversation'] = Conversation.with("c1", firstMessage, true);
  //
  //     // const latestMessage = new Message("title", "content2", "alice", 2, null, null, 1, "c1", "m2");
  //     const latestMessage = aMessage.with().title("title").content("content2").sender("alice").order(2).conversationId("c1").and.messageId("m2").asIs();
  //
  //     (<jasmine.Spy>messagesService.listMessages).and.returnValue(of(latestMessage));
  //     (<jasmine.Spy>conversations.markConversationAsRead).and.returnValue(of(true));
  //
  //     conversationPage['refresh']().add(() => {
  //
  //       expect(conversations.markConversationAsRead).toHaveBeenCalledWith(Conversation.with("c1", latestMessage, true));
  //
  //       done();
  //     });
  //   });
  // });

  function aMessageWithIdAndSendDate(id: string, sendDate: number): Message {
    // return new Message(null, null, null, sendDate, null, null, 1, null, id);
    return aMessage.with().sendDate(sendDate).order(1).and.messageId(id).asIs();
  }

  function aMessageWithMessageId(messageId: string, sendDate: number): Message {
    // return new Message(null, null, null, sendDate, null, null, 1, null, messageId);
    return aMessage.with().sendDate(sendDate).order(1).and.messageId(messageId).asIs();
  }
});
