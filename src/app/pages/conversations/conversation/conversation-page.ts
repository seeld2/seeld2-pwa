import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {
  AlertController,
  Content,
  Events,
  NavController,
  NavParams,
  Popover,
  PopoverController,
  Refresher,
  ScrollEvent
} from 'ionic-angular';
import {Subscription} from "rxjs";
import {environment} from "../../../../environments/environment";
import {Validation} from "../../../../shared/angular/validation/validation";
import {File} from "../../../../shared/files/file";
import {FileMetadata} from "../../../../shared/files/file-metadata";
import {Files} from "../../../../shared/files/files";
import {config} from "../../../app.config";
import {AppEvent} from "../../../app.event";
import {Conversation} from "../../../conversations/conversation";
import {ConversationEvent} from "../../../conversations/conversation-event";
import {ConversationsService} from "../../../conversations/conversations.service";
import {Message} from "../../../conversations/messages/message";
import {MessagesService} from "../../../conversations/messages/messages.service";
import {MobileEvent} from "../../../mobile/mobile.event";
import {PlatformService} from "../../../mobile/platform.service";
import {Refreshers} from "../../../mobile/refreshers";
import {NotificationEvent} from "../../../notifications/notification.event";
import {Session} from "../../../sessions/session";
import {SessionService} from "../../../sessions/session.service";
import {TimeService} from "../../../time/time.service";
import {UiNotifications} from "../../../uinotifications/ui-notifications";
import {Contact} from "../../../user/contact/contact";
import {Contacts} from "../../../user/contact/contacts";
import {Task} from "../../../utils/execution/task";
import {TaskExecutor} from "../../../utils/execution/task-executor";
import {Logger} from "../../../utils/logging/logger";
import {ContactsPage} from "../../contacts/contacts-page";
import {ConversationFormModel} from "./conversation-form-model";
import {MessageActions} from "./message-actions";
import {MessageActionsPage} from "./message-actions.page";

@Component({
  selector: 'page-conversation',
  templateUrl: 'conversation.html',
})
export class ConversationPage {

  private readonly logger: Logger = Logger.for("ConversationPage");
  private readonly messageEditorTextareaFieldName = 'message';
  private readonly taskExecutor = TaskExecutor.create();

  @ViewChild('attachmentFileInput') attachmentFileInput: ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild('conversationForm') conversationForm;

  private conversation: Conversation;
  private previousPage: string;
  private refreshing: boolean = false;
  private scroll: any = {
    lastFetchedMessagesAmount: 0,
    page: 0,
    pageSize: config.messages.pageSize,
  };
  private scrollToBottomTimeout;

  attachmentDataUrl: string;
  deletionMode: boolean = false;
  disable = {
    attachment: !environment.features.attachment,
    refreshButton: false,
    sendButton: false,
    titleField: false
  };
  messages: Message[] = [];
  model: ConversationFormModel = {
    title: '',
    message: '',
    attachment: undefined
  };
  selectedMessages = {};
  to: Contact[] = [];

  constructor(public readonly platforms: PlatformService,
              public readonly zone: NgZone,
              private readonly alertController: AlertController,
              private readonly conversationsService: ConversationsService,
              private readonly events: Events,
              private readonly files: Files,
              private readonly messagesService: MessagesService,
              private readonly navCtrl: NavController,
              private readonly notifications: UiNotifications,
              private readonly popoverController: PopoverController,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService,
              navParams: NavParams) {

    if (navParams.get('contact')) {
      this.to.push(navParams.get('contact'));
    }

    if (navParams.get('conversation')) {
      this.conversation = navParams.get('conversation');
      this.refreshTosFromLastConversationMessage();
    }

    if (navParams.get('previousPage')) {
      this.previousPage = navParams.get('previousPage');
    }

    this.refreshTitle();
  }

  ionViewWillEnter() {
    this.events.subscribe(MobileEvent.RESUMED_FROM_BACKGROUND, () => {
      this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0])).executeWhileNext();
    });

    this.events.subscribe(NotificationEvent.DETECTED_NEW_MESSAGES, () => {
      this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPageFromStorage, [0])).executeWhileNext();
    });

    this.events.subscribe(ConversationEvent.UPDATE_RECIPIENTS, (selectedContacts: any) => {
      this.refreshTosUsingSelectedContacts(selectedContacts);
      this.refreshTitle();
    });
  }

  ionViewDidEnter() {
    this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0, true])).executeWhileNext();
  }

  ionViewWillLeave() {
    this.events.unsubscribe(MobileEvent.RESUMED_FROM_BACKGROUND);
    this.events.unsubscribe(NotificationEvent.DETECTED_NEW_MESSAGES);
    this.events.unsubscribe(ConversationEvent.UPDATE_RECIPIENTS);
  }

  // ACTIONS

  attachmentFileInputSelected() {
    const fileInput: HTMLInputElement = this.attachmentFileInput.nativeElement;

    this.files.readHtmlFileInputAsBytes(
      fileInput,
      (event: Event) => {
        this.model.attachment = File.of(event.target['result'], FileMetadata.of(fileInput));
        this.attachmentDataUrl = this.model.attachment.dataUrl();
      },
      (errorEvent: ErrorEvent) => {
        this.logger.error("Error while reading the attachment as bytes", errorEvent);
      },
      (event: Event) => {
        this.logger.error("User aborted reading the attachment as bytes", event);
      });
  }

  attachTapped() {
    if (this.platforms.isAndroidApp()) {
      this.loadFileInAndroid();
    } else {
      this.attachmentFileInput.nativeElement.dispatchEvent(new MouseEvent('click'));
    }
  }

  private loadFileInAndroid() {

  }

  deleteSelectedTapped(): Subscription {
    const selectedMessages: string[] = Object.keys(this.selectedMessages);
    const messagesToDelete: Message[] = selectedMessages
      .filter((messageId: string) => {
        return this.selectedMessages[messageId];
      })
      .map((messageId: string) => {
        return this.messages.find((message: Message) => message.messageId() === messageId);
      });

    if (messagesToDelete.length === 0) {
      return Subscription.EMPTY
    }

    if (this.messages.length - messagesToDelete.length < 1) {
      this.alertController.create({
        buttons: [{text: "Ok"}],
        enableBackdropDismiss: false,
        message: "You cannot delete all messages left for a conversation. Please try to delete the whole conversation instead.",
        title: "Delete message"
      }).present();
      return;
    }

    this.startRefreshing();
    this.events.publish(AppEvent.ONGOING_PROCESS, true);

    return this.messagesService.deleteMessages(messagesToDelete)
      .subscribe(
        () => {
          this.notifications.showSuccess("Messages deleted!");
          messagesToDelete.forEach((messageToDelete: Message) => {
            const index: number = this.messages
              .findIndex((message: Message) => message.messageId() === messageToDelete.messageId());
            this.messages.splice(index, 1);
          });

          const latestMessage: Message = this.messages[this.messages.length - 1];
          this.conversation = Conversation.with(latestMessage.conversationId(), latestMessage);

          this.endRefreshing();
          this.deletionMode = false;
          this.events.publish(AppEvent.ONGOING_PROCESS, false);
        },
        // Error
        (error) => {
          this.logger.error("Error while deleting messages", error);
          this.endRefreshing();
          this.deletionMode = false;
          this.events.publish(AppEvent.ONGOING_PROCESS, false);
        }
      );
  }

  isScrolling(event: ScrollEvent) {
    if (!event) {
      return;
    }
    this.zone.run(() => {
      const hasMessagesToFetch: boolean = this.scroll.lastFetchedMessagesAmount > 0;

      if (hasMessagesToFetch && !this.refreshing && event.directionY === 'up'
        && event.scrollTop < event.contentHeight) {
        this.scroll.page++;
        this.logger.debug(`Refreshing page ${this.scroll.page} due to scrolling`);
        this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [this.scroll.page])).executeWhileNext();
      }
    });
  }

  messageEditorTextAreaBlurred() {
    Validation.force(this.conversationForm.controls[this.messageEditorTextareaFieldName]);
    Validation.refreshIonItem(this.messageEditorTextareaFieldName);
  }

  messageRecipientsTapped() {
    const recipients: string[] = this.to.map((contact: Contact) => contact.pseudo());
    this.navCtrl.push(ContactsPage, {recipients: recipients});
  }

  moreOptionsTapped(event: Event) {
    const popover: Popover = this.popoverController.create(MessageActionsPage);
    popover.onDidDismiss((option) => {
      switch (option) {
        case MessageActions.SWITCH_TO_DELETION_MODE:
          this.deletionMode = true;
          break;
      }
    });
    popover.present({ev: event});
  }

  refreshTapped(refresher?: Refresher) {
    if (this.conversation) {
      this.messages = [];
      this.messagesService.clearCache(this.conversation.conversationId());
      this.scroll.page = 0;
      this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0, true, refresher])).executeWhileNext();
    }
  }

  sendTapped(): Subscription {

    if (!this.canSend()) {
      return Subscription.EMPTY;
    }

    this.startRefreshing();
    this.disable.titleField = true;
    this.events.publish(AppEvent.ONGOING_PROCESS, true);

    this.notifications.showInfo('Encrypting message...');

    return this.messagesService.sendMessage(this.model.title, this.model.message, this.to, this.conversation)
      .subscribe(
        (sentMessage: Message) => {

          this.notifications.showSuccess('Message sent!');

          this.handleMessage(sentMessage, true);
        },

        // error
        ((error) => {
          this.logger.error("Error while sending the message", error);

          this.endRefreshing();
          this.disable.titleField = false;

          this.events.publish(AppEvent.ONGOING_PROCESS, false);

        }),

        // complete
        () => {
          this.model.message = "";

          this.endRefreshing();

          this.events.publish(AppEvent.ONGOING_PROCESS, false);

          // If coming from ContactsPage, we return to the ConversationsPage
          this.logger.debug(`Previous page is "${this.previousPage}"`)
          if (this.previousPage && this.previousPage === 'ContactsPage') {
            this.navCtrl.popToRoot();
              // TODO Temporarily disabled as it doesn't fully work on production (the conversation doesn't appear and user is forced to refresh to see the new conversation)
              // .then(() => {
              //   this.logger.trace(`Publishing ${AppEvent.TABS_SWITCH} event for tab conversation`);
              //   return this.events.publish(AppEvent.TABS_SWITCH, Tab.CONVERSATIONS);
              // });
          }
        }
      );
  }

  // PUBLIC

  canSend(): boolean {
    return this.conversationForm.form.valid && this.to.length > 0 && !this.disable.sendButton;
  }

  // TODO Test
  describeSender(message: Message) {
    const pseudo: string = message.sender();
    const session: Session = this.sessionService.session();
    if (session.pseudo() === pseudo) {
      return session.loggedUserDescription();
    }
    const contact: Contact = session.contact(pseudo);
    if (!contact) {
      return pseudo;
    }
    return session.pseudo() === pseudo ? session.loggedUserDescription() : session.contact(pseudo).description();
  }

  isAGroupConversation(message: Message): boolean {
    return message.participantsAmount() > 2;
  }

  isANewConversation(): boolean {
    return !this.conversation && this.messages.length === 0;
  }

  messageAuthor(message: Message): string {
    return this.messagesService.describeSenderOf(message);
  }

  messageTos(message: Message): string {
    return this.messagesService.describeRecipientsOf(message);
  }

  // noinspection JSMethodCanBeStatic
  sendDateFormatted(message: Message): string {
    return message.sendDate ? this.timeService.formatDateTime(message.sendDate()) : 'sending...';
  }

  sendDateFormattedToTheMillisecond(message: Message): string {
    return message.sendDate ? this.timeService.formatDateTimeMillis(message.sendDate()) : 'sending...';
  }

  senderIsLoggedUser(sender: string): boolean {
    return this.sessionService.session().pseudo() === sender;
  }

  // PRIVATE

  private endRefreshing(refresher: Refresher = null) {
    if (refresher) {
      Refreshers.closeAfterDelay(refresher);
    }

    this.selectedMessages = {};

    const timeoutId = setTimeout(() => {
      this.disable.refreshButton = false;
      this.disable.sendButton = false;
      clearTimeout(timeoutId);
      this.refreshing = false;
    }, 250);
  }

  // TODO Test
  private fetchPage(page: number, scrollToBottom = false, refresher?: Refresher): Subscription {
    this.logger.trace(`fetchPage(${page}, ${scrollToBottom}, ${refresher !== undefined})`);

    if (!this.conversation) {
      return Subscription.EMPTY;
    }

    this.startRefreshing();

    return this.messagesService.fetchPage(this.conversation, page, this.scroll.pageSize).subscribe(
    (message: Message) => this.handleMessage(message, scrollToBottom),
      // error
      (error) => {
        this.endRefreshing(refresher);
        this.logger.error(`Something unexpected went wrong while refreshing page ${page}`, error);
      },
      // complete
      () => {
        this.handleMessagesListUpdateComplete();
        this.endRefreshing(refresher);
        this.logger.debug(`Done retrieving page ${this.scroll.page}`);
      }
    );
  }

  // TODO Test
  private fetchPageFromStorage(page: number, scrollToBottom = false): Subscription {
    this.logger.trace(`fetchPageFromStorage(${page}, ${scrollToBottom})`);

    return this.messagesService
      .fetchPageFromStorage(this.conversation.conversationId(), page, this.scroll.pageSize).subscribe(
        (message: Message) => this.handleMessage(message, scrollToBottom),
        // error
        (error) => {
          this.logger.error("Couldn't fetch page from storage", error);
          this.endRefreshing();
        },
        // complete
        () => {
          this.handleMessagesListUpdateComplete();
          this.endRefreshing();
          this.logger.debug(`Done retrieving page ${this.scroll.page} from storage`);
        },
      );
  }

  private handleMessage = (message: Message, scrollToBottom) => {
    if (message) {

      const index: number = this.messages.findIndex((m: Message) => m.messageId() === message.messageId())
      if (index === -1) {

        const insertionIndex: number = this.messages.findIndex((m: Message) => m.sendDate() > message.sendDate());
        if (insertionIndex >= 0) {
          this.messages.splice(insertionIndex, 0, message);
        } else {
          this.messages.push(message);
        }

        this.messages.sort((a: Message, b: Message) => b.compareTo(a));

        const latestMessage: Message = this.messages[this.messages.length - 1];
        this.conversation = Conversation.with(latestMessage.conversationId(), latestMessage);

        if (scrollToBottom) {
          this.scrollToBottom();
        }
        this.scroll.lastFetchedMessagesAmount++;
      }
    }
  };

  private handleMessagesListUpdateComplete() {
    this.logger.trace(`handleMessagesListUpdateComplete()`);
    // Update the conversation to the latest message,
    // so that when marking the conversation as read it will be updated with the latest message
    this.conversation.setMessage(this.messages[this.messages.length - 1]);

    // This not only marks the conversation as read: it also updates the conversation's message
    this.conversationsService.markConversationAsRead(this.conversation).subscribe();
  }

  private refreshTitle() {
    if (this.conversation && this.conversation.message()) {
      this.model.title = this.conversation.message().title();
    } else if (!this.model.title && this.to.length > 0) {
      this.model.title = this.sessionService
        .session().loggedUserDescription() + "," + Contacts.fromArrayOfContacts(this.to).describeContacts();
    }
  }

  private refreshTosFromLastConversationMessage() {
    this.to = this.messagesService
      .getRecipientsOf(this.conversation.message(), [this.sessionService.session().pseudo()]);
  }

  private refreshTosUsingSelectedContacts(selectedContacts: any) {
    const contacts: Contact[] = this.sessionService.session().contactsAsArraySortedByDescription();

    Object.keys(selectedContacts).forEach((selectedUsername: string) => {

      const toContactsIndex: number = this.to.findIndex((to: Contact) => to.pseudo() === selectedUsername);

      if (selectedContacts[selectedUsername] && toContactsIndex === -1) { // Add the selected contacts to the recipients
        this.to.push(contacts.find((contact: Contact) => contact.pseudo() === selectedUsername));

      } else if (!selectedContacts[selectedUsername] && toContactsIndex >= 0) { // Remove the unselected contacts from the recipients
        this.to.splice(toContactsIndex);
      }
    });
  }

  private scrollToBottom(): Promise<boolean> {
    if (this.scrollToBottomTimeout) {
      clearTimeout(this.scrollToBottomTimeout);
    }
    return new Promise((resolve) => {
      this.scrollToBottomTimeout = setTimeout(() => {
        try {
          this.content.scrollToBottom().then(() => resolve());
        } catch (e) {
          // This may happen if the user already left the page. No action taken: if we don't scroll to bottom it's not a big deal...
        }
      }, 500);
    });
  }

  private startRefreshing() {
    this.refreshing = true;
    this.disable.refreshButton = true;
    this.disable.sendButton = true;
    this.scroll.lastFetchedMessagesAmount = 0;
  }
}
