import {Component} from "@angular/core";
import {ViewController} from "ionic-angular";
import {MessageActions} from "./message-actions";

@Component({
  selector: 'page-message-actions',
  templateUrl: 'message-actions.html'
})
export class MessageActionsPage {

  constructor(private readonly viewController: ViewController) {
  }

  switchMessageDeletion() {
    this.viewController.dismiss(MessageActions.SWITCH_TO_DELETION_MODE);
  }
}
