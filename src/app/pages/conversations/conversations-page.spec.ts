import {NgZone} from "@angular/core";
import {AlertController, Events, NavController, PopoverController} from "ionic-angular";
import {CoreJasmine} from "../../../core/core-jasmine";
import {AngularJasmine} from "../../../shared/angular-jasmine";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {ApiRemote} from "../../api-remote.service";
import {Conversation} from "../../conversations/conversation";
import {ConversationsJasmine} from "../../conversations/conversations.jasmine";
import {ConversationsService} from "../../conversations/conversations.service";
import {Message} from "../../conversations/messages/message";
import {MessagesService} from "../../conversations/messages/messages.service";
import {UnreadMessagesPollerService} from "../../conversations/messages/unread/unread-messages-poller.service";
import {PlatformJasmine} from "../../mobile/platform.jasmine";
import {PlatformService} from "../../mobile/platform.service";
import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {SessionService} from "../../sessions/session.service";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {TimeService} from "../../time/time.service";
import {UserEventsJasmine} from "../../userevents/user-events.jasmine";
import {UserEventsService} from "../../userevents/user-events.service";
import {ConversationsPage} from "./conversations-page";

describe("ConversationsPage", () => {

  const CONVERSATION_ID_1: string = "cid1";
  const CONVERSATION_ID_2: string = "cid2";
  const MESSAGE_ID_1: string = "mid1";
  const MESSAGE_ID_2: string = "mid2";

  let platforms: PlatformService;
  let ngZone: NgZone;
  let alertController: AlertController;
  let apiRemote: ApiRemote;
  let conversationsService: ConversationsService;
  let events: Events;
  let messagesService: MessagesService;
  let navCtrl: NavController;
  let popoverController: PopoverController;
  let seeldCordovaPluginProvider: SeeldCordovaPluginProvider;
  let sessionService: SessionService;
  let timeService: TimeService;
  let unreadMessagesPollerService: UnreadMessagesPollerService;
  let userEventsService: UserEventsService;

  let conversationsPage: ConversationsPage;

  beforeEach(() => {
    platforms = PlatformJasmine.platformService();
    ngZone = AngularJasmine.ngZone();
    alertController = null;
    apiRemote = null;
    conversationsService = ConversationsJasmine.conversationsService();
    events = IonicJasmine.events();
    messagesService = CoreJasmine.messages();
    popoverController = IonicJasmine.popoverController();
    seeldCordovaPluginProvider = PlatformJasmine.seeldCordovaPluginProvider();
    sessionService = SessionsJasmine.sessionService();
    timeService = new TimeService(null);
    unreadMessagesPollerService = null;
    userEventsService = UserEventsJasmine.userEventsService();

    conversationsPage = new ConversationsPage(platforms, ngZone, alertController, apiRemote, conversationsService,
      events, messagesService, navCtrl, popoverController, seeldCordovaPluginProvider, sessionService,
      timeService, unreadMessagesPollerService, userEventsService);
  });

  describe(", when handling a conversation,", () => {

    it("should add the conversation to the list of displayed conversations", () => {
      conversationsPage['conversations'] = [aConversationWithMessage(CONVERSATION_ID_1, MESSAGE_ID_1, 1)];
      conversationsPage['handleConversation'](aConversationWithMessage(CONVERSATION_ID_2, MESSAGE_ID_2, 2));
      expect(conversationsPage['conversations'].length).toEqual(2);
    });

    it("should prevent duplicate conversations from being added", () => {
      conversationsPage['conversations'] = [aConversationWithMessage(CONVERSATION_ID_1, MESSAGE_ID_1, 1)];
      conversationsPage['handleConversation'](aConversationWithMessage(CONVERSATION_ID_1, MESSAGE_ID_2, 2));
      expect(conversationsPage['conversations'].length).toEqual(1);
    });

    it("should ensure the conversations are sorted (latest on top)", () => {
      conversationsPage['conversations'] = [aConversationWithMessage(CONVERSATION_ID_1, MESSAGE_ID_1, 111)];

      conversationsPage['handleConversation'](aConversationWithMessage(CONVERSATION_ID_2, MESSAGE_ID_2, 222));

      expect(conversationsPage['conversations'][0].conversationId()).toEqual(CONVERSATION_ID_2)
      expect(conversationsPage['conversations'][1].conversationId()).toEqual(CONVERSATION_ID_1)
    });
  });

  // TODO Replace this with MessageTestBuilder and ConversationTestBuilder
  function aConversationWithMessage(conversationId: string, messageId: string, sendDate: number) {
    const message: Message = Message.with(null, null, null, sendDate, null, null, {}, 1, conversationId, messageId);
    return Conversation.with(conversationId, message, true);
  }
});
