import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {
  AlertController,
  Events,
  NavController,
  Popover,
  PopoverController,
  Refresher,
  ScrollEvent
} from 'ionic-angular';
import {Subscription} from "rxjs";
import {map} from "rxjs/operators";
import {ApiRemote} from "../../api-remote.service";
import {config} from "../../app.config";
import {AppEvent} from "../../app.event";
import {Conversation} from "../../conversations/conversation";
import {ConversationsService} from "../../conversations/conversations.service";
import {Message} from "../../conversations/messages/message";
import {MessagesService} from "../../conversations/messages/messages.service";
import {UnreadMessagesPollerService} from "../../conversations/messages/unread/unread-messages-poller.service";
import {MobileEvent} from "../../mobile/mobile.event";
import {PlatformService} from "../../mobile/platform.service";
import {Refreshers} from "../../mobile/refreshers";
import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {NotificationEvent} from "../../notifications/notification.event";
import {Session} from "../../sessions/session";
import {SessionService} from "../../sessions/session.service";
import {Tab} from "../../tab";
import {TimeService} from "../../time/time.service";
import {Contact} from "../../user/contact/contact";
import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";
import {UserEventsService} from "../../userevents/user-events.service";
import {Task} from "../../utils/execution/task";
import {TaskExecutor} from "../../utils/execution/task-executor";
import {Logger} from "../../utils/logging/logger";
import {ConversationActions} from "./conversation-actions";
import {ConversationActionsPage} from "./conversation-actions.page";
import {ConversationPage} from "./conversation/conversation-page";

@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html',
})
export class ConversationsPage implements OnInit, OnDestroy {

  private readonly logger = Logger.for("ConversationsPage");
  private readonly taskExecutor = TaskExecutor.create();

  private hasRefreshedAtLeastOnce = false;
  private isRefreshing = false;
  private scroll = {
    lastFetchedConversationsAmount: 0,
    page: 0,
    pageSize: config.conversations.pageSize,
  };

  batteryIsOptimized = false;
  conversations: Conversation[] = [];
  disable = {
    refreshButton: false
  };
  newMobileVersion: string = undefined;
  notify = {
    contacts: false
  };

  constructor(public readonly platforms: PlatformService,
              public readonly zone: NgZone,
              private readonly alertController: AlertController,
              private readonly apiRemote: ApiRemote,
              private readonly conversationsService: ConversationsService,
              private readonly events: Events,
              private readonly messagesService: MessagesService,
              private readonly navCtrl: NavController,
              private readonly popoverController: PopoverController,
              private readonly seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
              private readonly sessionService: SessionService,
              private readonly timeService: TimeService,
              private readonly unreadMessagesPollerService: UnreadMessagesPollerService,
              private readonly userEventsService: UserEventsService) {
  }

  ngOnInit(): void {
    let androidApp: boolean = this.platforms.isAndroidApp();
    this.logger.info(`Checking for new versions... (AndroidApp: ${androidApp})`);
    if (androidApp) {
      this.apiRemote.versions().subscribe(versions => {
        const serverMobileVersion = versions.mobileVersion();
        if (!serverMobileVersion) {
          this.logger.info('No version specified remotely');
          return;
        }
        let serverMobileVersionIsGreaterThanCurrentVersion: Boolean = versions.isGreaterThan(config.version);
        this.logger.info(`Found remote mobile version: ${serverMobileVersion} . Current version: ${config.version} . Update available: ${serverMobileVersionIsGreaterThanCurrentVersion}`);
        if (serverMobileVersion && serverMobileVersionIsGreaterThanCurrentVersion) {
          this.newMobileVersion = serverMobileVersion;
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.events.unsubscribe(MobileEvent.RESUMED_FROM_BACKGROUND);
    this.events.unsubscribe(NotificationEvent.DETECTED_NEW_MESSAGES);
    this.events.unsubscribe(AppEvent.TABS_SWITCH);
    this.events.unsubscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST));
  }

  // noinspection JSUnusedGlobalSymbols
  ionViewDidEnter() {
    this.events.subscribe(MobileEvent.RESUMED_FROM_BACKGROUND, () => {
      this.logger.trace(`Received ${MobileEvent.RESUMED_FROM_BACKGROUND} event`);
      this.initializeOptimizationState();
      this.refreshUserEvents();
      this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0])).executeWhileNext();
    });

    this.events.subscribe(NotificationEvent.DETECTED_NEW_MESSAGES, () => {
      this.logger.trace(`Received ${NotificationEvent.DETECTED_NEW_MESSAGES} event`);
      this.refreshUserEvents();
      this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPageFromStorage, [0])).executeWhileNext();
    });

    this.events.subscribe(AppEvent.TABS_SWITCH, (tabIndex) => {
      this.logger.trace(`Received ${AppEvent.TABS_SWITCH} event with tab index ${tabIndex}`);
      if (tabIndex === Tab.CONVERSATIONS) {
        this.refreshUserEvents();
        // Refresh of conversations is already done by ionViewDidEnter()
      }
    });

    this.events.subscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST), () => {
      this.notify.contacts = true;
    });

    this.unreadMessagesPollerService.schedulePolling();
    this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0])).executeWhileNext();
    this.initializeOptimizationState();
  }

  // ACTIONS

  batteryOptimizationAlertTapped() {
    this.alertController.create({
      buttons: [
        {
          handler: () => {
            this.seeldCordovaPluginProvider.seeldCordovaPlugin()
              .pipe(
                map((seeldCordovaPlugin) => seeldCordovaPlugin.batteryShowOptimization(
                  () => this.batteryIsOptimized = false,
                  () => {
                  }
                ))
              )
              .subscribe();
          },
          text: "Proceed"
        },
        {text: "Cancel"}
      ],
      enableBackdropDismiss: false,
      message: "In order to work around Google's push notifications system, we need you to whitelist Seeld in the" +
        " Battery Optimization page so that it is NOT optimized by Android. This will allow our notifications system" +
        " to keep on running in the background and notify you when messages arrive.",
      title: "Enable mobile App notifications"
    }).present();
  }

  conversationTapped(conversation) {
    this.navCtrl.push(ConversationPage, {conversation: conversation});
  }

  moreOptionsTapped(event: Event, conversationToDelete: Conversation) {
    const popover: Popover = this.popoverController.create(ConversationActionsPage);
    popover.onDidDismiss((option) => {
      switch (option) {
        case ConversationActions.DELETE_CONVERSATION:
          this.startRefresh();
          this.conversationsService.deleteConversation(conversationToDelete).subscribe(
            () => {
              const indexOfConversationToDelete: number = this.conversations.findIndex((conversation: Conversation) => {
                return conversation.conversationId() === conversationToDelete.conversationId();
              });
              this.conversations.splice(indexOfConversationToDelete, 1);
              this.endRefresh();
            }
          );
          break;
      }
    });
    popover.present({ev: event});
  }

  newConversationTapped() {
    this.navCtrl.push(ConversationPage);
  }

  refreshRequested(refresher?: Refresher) {
    this.conversations = [];
    this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [0, refresher])).executeWhileNext();
  }

  scrolling(event: ScrollEvent) {
    if (!event) {
      return;
    }
    this.zone.run(() => {
      const hasConversationsToFetch: boolean = this.scroll.lastFetchedConversationsAmount > 0;

      if (hasConversationsToFetch && !this.isRefreshing && event.directionY === 'down'
        && event.scrollTop > ((this.scroll.page + 1) * (event.contentTop + event.contentHeight))) {
        this.scroll.page++;
        this.logger.debug(`Refreshing page ${this.scroll.page} due to scrolling`);
        this.taskExecutor.queue(Task.subscription(this, 10, this.fetchPage, [this.scroll.page])).executeWhileNext();
      }
    });
  }

  switchToContactsTabTapped() {
    this.events.publish(AppEvent.TABS_SWITCH, Tab.CONTACTS);
  }

  // PUBLIC

  isFirstTime() {
    /* We check if the page has refreshed at least once, otherwise the "welcome" message appears briefly before the
    app is able to populate the conversations' array */
    return this.hasRefreshedAtLeastOnce && !this.isRefreshing && this.conversations.length === 0;
  }

  // TODO Test me
  lastSenderDescription(conversation: Conversation): string {
    const pseudo: string = conversation.message().sender();
    const session: Session = this.sessionService.session();
    if (session.pseudo() === pseudo) {
      return session.loggedUserDescription();
    }
    const contact: Contact = session.contact(pseudo);
    if (!contact) {
      return pseudo;
    }
    return session.contact(pseudo).description();
  }

  loggedUserDescription(): string {
    return this.sessionService.session().loggedUserDescription();
  }

  participantsDescription(message: Message): string {
    return this.messagesService.describeParticipantsOf(message, [this.sessionService.session().pseudo()]);
  }

  // noinspection JSMethodCanBeStatic
  sendDateFormatted(millis: number): string {
    return this.timeService.formatDate(millis);
  }

  // PRIVATE

  private endRefresh(refresher: Refresher = null) {
    if (refresher) {
      Refreshers.closeAfterDelay(refresher);
    }

    this.isRefreshing = false;
    this.hasRefreshedAtLeastOnce = true;

    const timeoutId = setTimeout(() => {
      this.disable.refreshButton = false;
      clearTimeout(timeoutId);
    }, 250);
  }

  // TODO Test
  private fetchPage(page: number, refresher ?: Refresher): Subscription {
    this.logger.trace(`fetchPage(${page}, ${refresher !== undefined})`);

    this.startRefresh();
    this.scroll.lastFetchedConversationsAmount = 0;

    return this.conversationsService.fetchPage(page, this.scroll.pageSize).subscribe(
      (conversation: Conversation) => {
        this.handleConversation(conversation);
        this.scroll.lastFetchedConversationsAmount++;
      },
      // error
      (error) => {
        this.logger.error(`Something unexpected went wrong while fetching page ${this.scroll.page}`, error);
        this.endRefresh(refresher);
      },
      // complete
      () => {
        this.endRefresh(refresher);
        this.logger.debug(`Done retrieving page ${this.scroll.page}`);
      }
    );
  }

  // TODO Test
  private fetchPageFromStorage(page: number, refresher?: Refresher): Subscription {
    this.logger.trace(`fetchPageFromStorage(${page}, ${refresher !== undefined})`);

    return this.conversationsService.fetchPageFromStorage(page, this.scroll.pageSize).subscribe(
      (conversation: Conversation) => {
        this.handleConversation(conversation);
        this.scroll.lastFetchedConversationsAmount++;
      },
      (error) => {
        this.logger.error("Couldn't fetch page from storage", error);
        this.endRefresh(refresher);
      },
      () => {
        this.endRefresh(refresher);
        this.logger
          .debug(`Done retrieving page ${this.scroll.page} from storage`);
      },
    );
  }

  private handleConversation = (conversation: Conversation) => {
    const index: number = this.conversations
      .findIndex((c: Conversation) => c.conversationId() === conversation.conversationId());
    if (index === -1) {
      this.conversations.push(conversation);
    } else if (!conversation.equals(this.conversations[index])) {
      this.conversations[index] = conversation;
    }
    this.conversations.sort((a: Conversation, b: Conversation) => b.compareTo(a));
  };

  private initializeOptimizationState() {
    this.logger.trace('initializeOptimizationState()');
    if (this.platforms.isAndroidApp()) {
      this.seeldCordovaPluginProvider.seeldCordovaPlugin()
        .pipe(
          map((seeldCordovaPlugin) => seeldCordovaPlugin.batteryIsOptimized(
            (isOptimized) => {
              this.logger.debug(`App is battery-optimized: ${isOptimized}`);
              return this.batteryIsOptimized = isOptimized === "true";
            },
            (e) => this.logger.error(e)
          ))
        )
        .subscribe();
    }
  }

  private refreshUserEvents() {
    this.notify.contacts = false;
    this.userEventsService.fetchAndPublishNewUserEvents();
  }

  private startRefresh() {
    this.disable.refreshButton = true;
    this.isRefreshing = true;
  }
}
