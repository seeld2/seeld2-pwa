import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, LoadingController, NavParams, ViewController} from 'ionic-angular';
import {LoginFormModel} from "./login-form-model";
import {Page} from "../page";
import {LoginParams} from "../../login/login-params";
import {LoginService} from "../../login/login.service";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {collapse} from "../../../shared/angular/animations/collapse";
import {Logger} from "../../utils/logging/logger";
import {PlatformService} from "../../mobile/platform.service";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  animations: [collapse.trigger]
})
export class LoginPage implements OnInit {

  private readonly logger: Logger = Logger.for("LoginPage");

  @ViewChild('loginForm') loginForm;

  display = {
    disclaimer: 'hide',
    login: 'show'
  };
  disable = {
    loginButton: false
  };
  hide = {
    closeButton: true,
  };
  model: LoginFormModel = new LoginFormModel();
  passphraseField: any = {
    icon: 'eye',
    type: 'password'
  };

  constructor(private readonly alertCtrl: AlertController,
              private readonly loadingCtrl: LoadingController,
              private readonly loginService: LoginService,
              private readonly navParams: NavParams,
              private readonly notifications: UiNotifications,
              private readonly platforms: PlatformService,
              private readonly viewCtrl: ViewController) {
  }

  ngOnInit(): void {
    const loginParams: LoginParams = this.navParams.get('loginParams');

    this.hide.closeButton = loginParams.forceLogin;
  }

  // ACTIONS

  closeButtonTapped() {
    this.viewCtrl.dismiss();
  }

  disclaimerCloseTapped() {
    this.display.disclaimer = 'hide';
    this.display.login = 'show';
  }

  disclaimerTapped() {
    this.display.disclaimer = 'show';
    this.display.login = 'hide';
  }

  loginTapped() {
    this.disable.loginButton = true;

    const loading = this.loadingCtrl.create({content: 'Logging in...'});
    loading.present();

    this.loginService.performLogin(this.model.username, this.model.passphrase)

      .subscribe(
        // next
        (result: boolean) => {
          if (result) {
            this.viewCtrl.dismiss(Page.CONVERSATIONS);
          } else {
            this.notifications.showError('Invalid pseudo or passphrase');
          }
        },
        // error
        (error: any) => {
          this.logger.error(error);

          loading.dismiss();
          this.disable.loginButton = false;

          if (error.status && error.status === 401) {
            this.alertCtrl.create({
              buttons: [{
                text: 'Okay!'
              }],
              enableBackdropDismiss: true,
              message: 'Oops, either we could not find your pseudo or your passphrase is incorrect... Please try again!'
            }).present();
          }
        },
        // complete
        () => {
          loading.dismiss();
          this.disable.loginButton = false;
        }
      );
  }

  registerTapped() {
    this.viewCtrl.dismiss(Page.REGISTER);
  }

  // PUBLIC

  isBrowser(): boolean {
    return this.platforms.isBrowser();
  }

  togglePassphraseFieldVisibility() {
    if (this.passphraseField.type === 'password') {
      this.passphraseField.type = 'text';
      this.passphraseField.icon = 'eye-off';
    } else {
      this.passphraseField.type = 'password';
      this.passphraseField.icon = 'eye';
    }
  }
}
