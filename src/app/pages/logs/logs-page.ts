import {Component, OnDestroy, OnInit} from '@angular/core';
import {ViewController} from 'ionic-angular';
import {ClipboardService} from "ngx-clipboard";
import {UiNotifications} from "../../uinotifications/ui-notifications";
import {Logger} from "../../utils/logging/logger";
import {LoggerEntry} from "../../utils/logging/logger-entry";
import {LoggingLevel} from "../../utils/logging/logging-level";

@Component({
  selector: 'page-logs',
  templateUrl: 'logs.html',
})
export class LogsPage implements OnDestroy, OnInit {

  // private static readonly SHOW_LOGS_FROM_LEVEL: LoggingLevel = LoggingLevel.INFO;

  entries: any = {};
  loggingLevel: LoggingLevel = Logger.level();
  timestamps: string[] = [];

  private loggingLevelIntervalId: any;

  constructor(private readonly clipboardService: ClipboardService,
              private readonly uiNotifications: UiNotifications,
              private readonly viewCtrl: ViewController) {
  }

  ngOnInit(): void {

    this.displayListOfEntries();

    this.loggingLevelIntervalId = setInterval(() => {
      if (Logger.level() !== this.loggingLevel) {
        Logger.setLevel(this.loggingLevel);
        this.displayListOfEntries();
      }
    }, 250);
  }

  ngOnDestroy(): void {
    clearInterval(this.loggingLevelIntervalId);
  }

  // ACTIONS

  closeButtonTapped() {
    this.viewCtrl.dismiss();
  }

  copyLogsToClipboardTapped() {
    const loggerEntriesAsText: string = Logger.listEntries()
      .map((loggerEntry: LoggerEntry) => `${loggerEntry.meta()} ${this.entryAsText(loggerEntry)}`)
      .reduce((accumulator, line) => accumulator + line + '\n', "");
    if (this.clipboardService.copyFromContent(loggerEntriesAsText)) {
      this.uiNotifications.showSuccess("Logs copied to clipboard!");
    }
  }

  // PRIVATE

  private displayListOfEntries() {
    this.entries = {};
    this.timestamps = [];
    Logger.listEntries()
      .filter((entry: LoggerEntry) => entry.getLevel() >= Logger.level())
      .forEach((entry: LoggerEntry) => {
        const timestamp: string = entry.meta();
        const entryText: string = this.entryAsText(entry);
        if (this.entries[timestamp]) {
          this.entries[timestamp] += `<br>${entryText}`;
        } else {
          this.entries[timestamp] = entryText;
          this.timestamps.push(timestamp);
        }
      });
  }

  private entryAsText(entry: LoggerEntry) {
    return entry.getEntry().constructor.name === 'String'
      ? entry.getEntry()
      : JSON.stringify(entry.getEntry());
  }
}
