import {Component, ViewChild} from '@angular/core';
import {Content, Loading, LoadingController, ViewController} from 'ionic-angular';
import {RegistrationFormModel} from "./registration-form-model";
import {Validation} from "../../../shared/angular/validation/validation";
import {finalize} from "rxjs/operators";
import {RegistrationsService} from "../../registrations/registrations.service";
import {Page} from "../page";

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('registrationForm') registrationForm;
  @ViewChild(Content) content: Content;

  enableRegisterButton: boolean = true;
  model: RegistrationFormModel = new RegistrationFormModel();
  passphraseField: any = {
    icon: 'eye',
    type: 'password'
  };
  registrationAvailable: boolean = undefined;

  constructor(private loadingController: LoadingController,
              private registrations: RegistrationsService,
              private viewCtrl: ViewController) {
  }

  // noinspection JSUnusedGlobalSymbols
  ionViewDidEnter() {
    this.registrations.isRegistrationAvailable()
      .subscribe((availability: boolean) => {
        return this.registrationAvailable = availability;
      });
  }

  // ACTIONS

  backButtonTapped() {
    this.viewCtrl.dismiss(Page.LOGIN);
  }

  registerTapped() {

    this.enableRegisterButton = false;

    const loading: Loading = this.loadingController.create({
      content: "Generating your account.<br/>Please be patient..."
    });
    loading.present();

    this.registrations.register(this.model.username.toLowerCase(), this.model.displayName, this.model.passphrase)
      .pipe(
        finalize(() => {
          loading.dismiss();
          this.enableRegisterButton = true;
        })
      )
      .subscribe(() => this.viewCtrl.dismiss(Page.CONVERSATIONS));
  }

  scrollToBottom() {
    this.content.scrollToBottom();
  }

  // PUBLIC

  forceValidationOf(fieldName: string) {
    Validation.force(this.registrationForm.controls[fieldName]);
    Validation.refreshIonItem(fieldName);
  }

  forceValidationOfPassphrase() {
    this.forceValidationOf('passphrase');
    if (this.model.passphraseRepeated) {
      this.forceValidationOf('passphraseRepeated');
    }
  }

  refreshValidationOfPseudo() {
    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      Validation.refreshIonItem('pseudo');
    }, 500);
  }

  togglePassphraseFieldVisibility() {
    if (this.passphraseField.type === 'password') {
      this.passphraseField.type = 'text';
      this.passphraseField.icon = 'eye-off';
    } else {
      this.passphraseField.type = 'password';
      this.passphraseField.icon = 'eye';
    }
  }

  // PRIVATE
}
