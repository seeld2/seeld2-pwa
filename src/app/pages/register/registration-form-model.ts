export class RegistrationFormModel {
  constructor(
    public username: string = '',
    public displayName: string = '',
    public passphrase: string = '',
    public passphraseRepeated: string = '') {
  }
}
