import {Directive, Input} from "@angular/core";
import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, NgModel, ValidationErrors} from "@angular/forms";
import {RegistrationsRemote} from "../../registrations/registrations.remote";

@Directive({
  selector: '[usernameIsAvailable]',
  providers: [{
    provide: NG_ASYNC_VALIDATORS,
    useExisting: UsernameIsAvailableValidator,
    multi: true
  }]
})
export class UsernameIsAvailableValidator implements AsyncValidator {
  @Input() usernameIsAvailable: NgModel;

  private timeout;

  constructor(private registrationsRemote: RegistrationsRemote) {
  }

  validate(control: AbstractControl): Promise<ValidationErrors | null> {

    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    return !control.value ? Promise.resolve(null) : new Promise(
      (resolve) => {

        const typedUsername = control.value;

        this.timeout = setTimeout(() => {

          this.registrationsRemote.fetchUsernameAvailability(typedUsername)
            .subscribe(availability => {
              resolve(availability.available ? null : {'usernameIsAvailable': {available: false}})
            });

        }, 250);
      }
    );
  }
}
