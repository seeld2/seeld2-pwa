import {MainTabsPage} from "./main-tabs-page";
import {Events} from "ionic-angular";
import {PlatformService} from "../../mobile/platform.service";
import {SessionService} from "../../sessions/session.service";
import {UserEventsService} from "../../userevents/user-events.service";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {UserEventsJasmine} from "../../userevents/user-events.jasmine";
import {SessionsJasmine} from "../../sessions/sessions.jasmine";
import {PlatformJasmine} from "../../mobile/platform.jasmine";

describe("MainTabsPage", () => {

  let events: Events;
  let platformService: PlatformService;
  let sessionService: SessionService;
  let userEventsService: UserEventsService;

  let page: MainTabsPage;

  beforeEach(() => {
    events = IonicJasmine.events();
    platformService = PlatformJasmine.platformService();
    sessionService = SessionsJasmine.sessionService();
    userEventsService = UserEventsJasmine.userEventsService();

    page = new MainTabsPage(events, platformService, sessionService, userEventsService);
  });

  it("should indicate if the tabs at the bottom must appear or not", () => {

    (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(false);
    expect(page.mustTabsShow()).toBe(false);

    (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(true);
    (<jasmine.Spy>platformService.isBrowser).and.returnValue(true);
    (<jasmine.Spy>platformService.isMobileBrowser).and.returnValue(false);
    expect(page.mustTabsShow()).toBe(false);

    (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(true);
    (<jasmine.Spy>platformService.isBrowser).and.returnValue(false);
    (<jasmine.Spy>platformService.isMobileBrowser).and.returnValue(false);
    expect(page.mustTabsShow()).toBe(true);

    (<jasmine.Spy>sessionService.isSessionInitialized).and.returnValue(true);
    (<jasmine.Spy>platformService.isBrowser).and.returnValue(false);
    (<jasmine.Spy>platformService.isMobileBrowser).and.returnValue(true);
    expect(page.mustTabsShow()).toBe(true);
  });
});
