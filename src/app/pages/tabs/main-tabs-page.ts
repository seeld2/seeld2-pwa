import {Component, OnInit, ViewChild} from '@angular/core';
import {Logger} from "../../utils/logging/logger";
import {ConversationsPage} from "../conversations/conversations-page";
import {ContactsPage} from "../contacts/contacts-page";
import {Events, Tabs} from "ionic-angular";
import {PlatformService} from "../../mobile/platform.service";
import {AppEvent} from "../../app.event";
import {SessionService} from "../../sessions/session.service";
import {UserEventsService} from "../../userevents/user-events.service";
import {UserEvent} from "../../userevents/user-event";
import {UserEventType} from "../../userevents/user-event-type";

@Component({
  selector: 'page-main-tabs',
  templateUrl: 'main-tabs.html',
})
export class MainTabsPage implements OnInit {

  private readonly logger: Logger = Logger.for("MainTabsPage");

  @ViewChild('mainTabs') mainTabs: Tabs;

  contactsTab: any = ContactsPage;
  conversationsTab: any = ConversationsPage;
  notify: any = {
    contacts: false
  };

  constructor(private readonly events: Events,
              private readonly platformService: PlatformService,
              private readonly sessionService: SessionService,
              private readonly userEventsService: UserEventsService) {
  }

  ngOnInit(): void {

    this.events.subscribe(AppEvent.TABS_SWITCH, (tabIndex) => {
      this.logger.trace(`Received ${AppEvent.TABS_SWITCH} event with tab index ${tabIndex}`);
      this.mainTabs.select(tabIndex);
    });

    this.events.subscribe(UserEvent.topicFor(UserEventType.CONTACT_REQUEST), () => {
      this.notify.contacts = true;
    });

    this.mainTabs.ionChange.subscribe(() => {
      this.refreshUserEvents();
    });
  }

  ionViewDidEnter() {
    this.refreshUserEvents();
  }

  mustTabsShow(): boolean {
    return this.sessionService.isSessionInitialized()
      && (!this.platformService.isBrowser() || this.platformService.isMobileBrowser());
  }

  private refreshUserEvents() {
    if (this.mustTabsShow()) {
      this.notify.contacts = false;
      this.userEventsService.fetchAndPublishNewUserEvents();
    }
  }
}
