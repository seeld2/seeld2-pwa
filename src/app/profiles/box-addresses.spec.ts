import {BoxAddresses} from "./box-addresses";

describe("BoxAddresses", () => {

  const BOX_ADDRESS_1: string = "boxAddress1";
  const BOX_ADDRESS_2: string = "boxAddress2";

  it("should build an instance from an array of box addresses", () => {
    const boxAddresses: BoxAddresses = BoxAddresses.of([BOX_ADDRESS_1, BOX_ADDRESS_2]);
    expect(boxAddresses['_boxAddresses']).toContain(BOX_ADDRESS_1);
    expect(boxAddresses['_boxAddresses']).toContain(BOX_ADDRESS_2);
  });

  it("should provide a plain JavaScript object of itself", () => {
    const boxAddresses: BoxAddresses = BoxAddresses.of([BOX_ADDRESS_1, BOX_ADDRESS_2]);
    const object: any = boxAddresses.toObject();
    expect(object).toEqual({boxAddresses: [BOX_ADDRESS_1, BOX_ADDRESS_2]});
  });
});
