import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class BoxAddresses implements Serializable {

  static of(boxAddresses: string[]): BoxAddresses {
    return new BoxAddresses(boxAddresses);
  }

  private constructor(private readonly _boxAddresses: string[]) {
  }

  asObject(): any {
    return {boxAddresses: this._boxAddresses};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
