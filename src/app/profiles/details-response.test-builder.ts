import {DetailsResponse} from "./profiles-remote";

export class DetailsResponseTestBuilder {

  public static readonly BOX_ADDRESS: string = "box address";
  public static readonly SYSTEM_PUBLIC_KEYS: string = "system public keys";
  public static readonly USERNAME: string = "username";

  static with(): DetailsResponseTestBuilder {
    return new DetailsResponseTestBuilder();
  }

  static withDefaultValues(): DetailsResponse {
    return DetailsResponseTestBuilder.with().noBlanks();
  }

  and = this;

  private _boxAddress: string;
  private _systemPublicKeys: string;
  private _username: string;

  private constructor() {
  }

  asIs(): DetailsResponse {
    return <DetailsResponse>{
      boxAddress: this._boxAddress,
      systemPublicKeys: this._systemPublicKeys,
      username: this._username
    };
  }

  noBlanks(): DetailsResponse {
    const boxAddress: string = this._boxAddress ? this._boxAddress : DetailsResponseTestBuilder.BOX_ADDRESS;
    const systemPublicKeys: string = this._systemPublicKeys ? this._systemPublicKeys : DetailsResponseTestBuilder.SYSTEM_PUBLIC_KEYS;
    const username: string = this._username ? this._username : DetailsResponseTestBuilder.USERNAME;
    return <DetailsResponse>{boxAddress: boxAddress, systemPublicKeys: systemPublicKeys, username: username};
  }

  boxAddress(boxAddress: string): DetailsResponseTestBuilder {
    this._boxAddress = boxAddress;
    return this;
  }

  systemPublicKeys(systemPublicKeys: string): DetailsResponseTestBuilder {
    this._systemPublicKeys = systemPublicKeys;
    return this;
  }

  username(username: string): DetailsResponseTestBuilder {
    this._username = username;
    return this;
  }
}
