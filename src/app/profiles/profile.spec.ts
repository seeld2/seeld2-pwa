import {Profile} from "./profile";

describe("Profile", () => {

  const BOX_ADDRESS: string = "box address";
  const SYSTEM_PUBLIC_KEYS: string = "system public keys";
  const USERNAME: string = "username";

  it("should indicate if it's empty or not", () => {
    let profile: Profile;

    profile = Profile.empty();
    expect(profile.isEmpty()).toBe(true);

    profile = Profile.build(BOX_ADDRESS, null, null);
    expect(profile.isEmpty()).toBe(false);

    profile = Profile.build(null, SYSTEM_PUBLIC_KEYS, null);
    expect(profile.isEmpty()).toBe(false);

    profile = Profile.build(null, null, USERNAME);
    expect(profile.isEmpty()).toBe(false);
  });
});
