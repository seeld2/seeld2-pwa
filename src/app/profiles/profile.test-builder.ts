import {Profile} from "./profile";

export class ProfileTestBuilder {

  public static readonly BOX_ADDRESS: string = "boxAddress";
  public static readonly PSEUDO: string = "pseudo";
  public static readonly SYSTEM_PUBLIC_KEYS: string = "systemPublicKeys";

  static with(): ProfileTestBuilder {
    return new ProfileTestBuilder();
  }

  and = this;

  private _boxAddress: string
  private _pseudo: string
  private _systemPublicKeys: string

  private constructor() {
  }

  asIs(): Profile {
    return Profile.build(this._boxAddress, this._systemPublicKeys, this._pseudo);
  }

  noBlanks(): Profile {
    return Profile.build(
      this._boxAddress ? this._boxAddress : ProfileTestBuilder.BOX_ADDRESS,
      this._systemPublicKeys ? this._systemPublicKeys : ProfileTestBuilder.SYSTEM_PUBLIC_KEYS,
      this._pseudo ? this._pseudo : ProfileTestBuilder.PSEUDO
    );
  }

  boxAddress(boxAddress: string): ProfileTestBuilder {
    this._boxAddress = boxAddress;
    return this;
  }

  pseudo(pseudo: string): ProfileTestBuilder {
    this._pseudo = pseudo;
    return this;
  }

  systemPublicKeys(systemPublicKeys: string): ProfileTestBuilder {
    this._systemPublicKeys = systemPublicKeys;
    return this;
  }
}
