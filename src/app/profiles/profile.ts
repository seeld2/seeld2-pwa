import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class Profile implements Serializable {

  static build(boxAddress: string, systemPublicKeys: string, pseudo: string) {
    return new Profile(boxAddress, systemPublicKeys, pseudo);
  }

  static empty() {
    return new Profile(null, null, null);
  }

  static fromJson(json: string): Profile {
    const object: any = JSON.parse(json);
    return new Profile(object.boxAddress, object.systemPublicKeys, object.username);
  }

  private constructor(private readonly _boxAddress: string,
                      private readonly _systemPublicKeys: string,
                      private readonly _pseudo: string) {
  }

  asObject(): any {
    return {
      boxAddress: this._boxAddress,
      systemPublicKeys: this._systemPublicKeys,
      username: this._pseudo
    };
  }

  boxAddress(): string {
    return this._boxAddress;
  }

  isEmpty(): boolean {
    return this._boxAddress === null && this._systemPublicKeys === null && this._pseudo === null;
  }

  pseudo(): string {
    return this._pseudo;
  }

  systemPublicKeys(): string {
    return this._systemPublicKeys;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
