import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {AesRequest} from "../crypto/aes/aes-request";
import {AngularJasmine} from "../../shared/angular-jasmine";
import {config} from "../app.config";
import {AES} from "../crypto/aes/aes";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {ContactTestBuilder as aContact} from "../user/contact/contact.test-builder";
import {Contacts} from "../user/contact/contacts";
import {DeletedContacts} from "../user/contact/deleted-contacts";
import {KeyPair} from "../user/key/key-pair";
import {Keys} from "../user/key/keys";
import {KeysTestBuilder as someKeys} from "../user/key/keys.test-builder";
import {UserPayload} from "../user/user-payload";
import {UserPayloadTestBuilder, UserPayloadTestBuilder as aUserPayload} from "../user/user-payload.test.builder";
import {
  DetailsResponseTestBuilder,
  DetailsResponseTestBuilder as aDetailsResponse
} from "./details-response.test-builder";
import {Profile} from "./profile";
import {PayloadResponse, ProfilesRemote} from "./profiles-remote";

describe("ProfilesRemote", () => {

  const ENCRYPTED_DATA: Uint8Array = new Uint8Array([1, 2, 3]);
  const LOGGED_PSEUDO: string = "logged pseudo";
  const LOGGED_PSEUDO_AES_AS_TEXT: string = "loggedpseudoencryptedastext";
  const LOGGED_PSEUDO_PRIVATE_KEYS: string = 'loggedPseudoPrivateKeys';
  const LOGGED_PSEUDO_PUBLIC_KEYS: string = 'loggedPseudoPublicKeys';
  const LOGGED_PSEUDO_WRITE_TO_BOX_ADDRESSES: string[] = ['boxaddress1', 'boxaddress2'];
  const SECRET_KEY: string = "secretKey";
  const USER_PAYLOAD_PGP: string = "userPayloadPgp";

  let exchangeAes: AES;
  let httpClient: HttpClient;
  let openPgp: OpenPgp;
  let sessionService: SessionService;

  let profilesRemote: ProfilesRemote;

  let session: Session;

  beforeEach(() => {
    exchangeAes = CryptoJasmine.exchangeAes();
    httpClient = AngularJasmine.httpClient();
    openPgp = CryptoJasmine.openPgp();
    sessionService = SessionsJasmine.sessionService();

    profilesRemote = new ProfilesRemote(httpClient, sessionService);

    (<jasmine.Spy>sessionService.exchangeAes).and.returnValue(of(exchangeAes));
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);

    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);
    (<jasmine.Spy>session.pseudo).and.returnValue(LOGGED_PSEUDO);
  });

  xit("should delete a whole profile", (done: DoneFn) => {

    (<jasmine.Spy>exchangeAes.encryptAsText).and.returnValue(of(LOGGED_PSEUDO_AES_AS_TEXT));

    (<jasmine.Spy>session.writeToBoxAddresses).and.returnValue(LOGGED_PSEUDO_WRITE_TO_BOX_ADDRESSES);
    (<jasmine.Spy>exchangeAes.encrypt).and.returnValue(of(ENCRYPTED_DATA));

    (<jasmine.Spy>httpClient.put).and.returnValue(of({}));

    profilesRemote.deleteProfile()
      .subscribe((result: boolean) => {

        expect(exchangeAes.encryptAsText).toHaveBeenCalledWith(LOGGED_PSEUDO);
        expect(exchangeAes.encrypt).toHaveBeenCalledWith(JSON.stringify({boxAddresses: LOGGED_PSEUDO_WRITE_TO_BOX_ADDRESSES}));
        expect(httpClient.put).toHaveBeenCalledWith(
          `${config.api.profiles}/${LOGGED_PSEUDO_AES_AS_TEXT}/state/deleted`,
          <AesRequest>{payload: Array.from(ENCRYPTED_DATA)});

        expect(result).toBe(true);

        done();
      });
  });

  it("should fetch the details for a given pseudo", (done: DoneFn) => {

    (<jasmine.Spy>httpClient.get).and.returnValue(of(aDetailsResponse.withDefaultValues()));

    profilesRemote.fetchDetails(DetailsResponseTestBuilder.USERNAME).subscribe((profile: Profile) => {
      expect(profile.boxAddress()).toEqual(DetailsResponseTestBuilder.BOX_ADDRESS);
      expect(profile.pseudo()).toEqual(DetailsResponseTestBuilder.USERNAME);
      expect(profile.systemPublicKeys()).toEqual(DetailsResponseTestBuilder.SYSTEM_PUBLIC_KEYS);
      done();
    });
  });

  it("should fetch the logged user's own payload", (done: DoneFn) => {

    const userPayload: UserPayload = aUserPayload.withDefaultSystemKeyPair();

    (<jasmine.Spy>sessionService.pseudo).and.returnValue(LOGGED_PSEUDO);
    (<jasmine.Spy>exchangeAes.encryptAsText).and.returnValue(of(LOGGED_PSEUDO_AES_AS_TEXT));
    const payloadResponse: PayloadResponse = {payload: USER_PAYLOAD_PGP};
    (<jasmine.Spy>httpClient.get).and.returnValue(of(payloadResponse));
    (<jasmine.Spy>sessionService.systemPublicKeys).and.returnValue(UserPayloadTestBuilder.SYSTEM_PUBLIC_KEYS);
    (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(userPayload.toJson()));

    profilesRemote.fetchOwnPayload().subscribe((fetchedUserPayload: UserPayload) => {

      expect(exchangeAes.encryptAsText).toHaveBeenCalledWith(LOGGED_PSEUDO);
      expect(httpClient.get).toHaveBeenCalledWith(`${config.api.profiles}/${LOGGED_PSEUDO_AES_AS_TEXT}/payload`);
      expect(openPgp.decrypt).toHaveBeenCalledWith(USER_PAYLOAD_PGP, UserPayloadTestBuilder.SYSTEM_PUBLIC_KEYS);

      expect(fetchedUserPayload).toEqual(userPayload);

      done();
    });
  });

  it("should update the profile's payload", (done: DoneFn) => {

    const contacts: Contacts = Contacts.fromArrayOfContacts([
      aContact.with().pseudo(LOGGED_PSEUDO).connectedSince(134679).asIs()
    ]);

    const deletedContacts: DeletedContacts = DeletedContacts.fromObject({
      carol: {readFrom: "carolReadFrom", secretKey: "carolSecretKey"}
    });

    const keys: Keys = someKeys.with()
      .keyPairByKeyId(
        LOGGED_PSEUDO,
        KeyPair.with(Keys.SYSTEM_KEYS_TYPE, LOGGED_PSEUDO_PUBLIC_KEYS, LOGGED_PSEUDO_PRIVATE_KEYS))
      .asIs();

    (<jasmine.Spy>sessionService.pseudo).and.returnValue(LOGGED_PSEUDO);
    (<jasmine.Spy>exchangeAes.encryptAsText).and.returnValue(of(LOGGED_PSEUDO_AES_AS_TEXT));

    (<jasmine.Spy>openPgp.encrypt).and.returnValue(of(USER_PAYLOAD_PGP));

    (<jasmine.Spy>httpClient.put).and.returnValue(of({}));

    profilesRemote.updatePayload(contacts, deletedContacts, keys, SECRET_KEY).subscribe(() => {

      expect(exchangeAes.encryptAsText).toHaveBeenCalledWith(LOGGED_PSEUDO);

      expect(openPgp.encrypt).toHaveBeenCalledWith('' +
      '{"contacts":{"logged pseudo":{"pseudo":"' + LOGGED_PSEUDO + '","connectedSince":134679}},' +
      '"deletedContacts":{"carol":{"readFrom":"carolReadFrom","secretKey":"carolSecretKey"}},' +
      '"keys":{"' + LOGGED_PSEUDO + '":{"type":"ecc.x25519","publicKeys":"' + LOGGED_PSEUDO_PUBLIC_KEYS + '","privateKeys":"' + LOGGED_PSEUDO_PRIVATE_KEYS + '"}},' +
      '"secretKey":"' + SECRET_KEY + '"}');

      expect(httpClient.put).toHaveBeenCalledWith(
        `${config.api.profiles}/${LOGGED_PSEUDO_AES_AS_TEXT}/payload`,
        `{"payload":"${USER_PAYLOAD_PGP}"}`);

      done();
    });
  });
});
