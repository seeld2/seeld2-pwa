import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {forkJoin} from "rxjs/observable/forkJoin";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../app.config";
import {AES} from "../crypto/aes/aes";
import {SessionService} from "../sessions/session.service";
import {Contacts} from "../user/contact/contacts";
import {DeletedContacts} from "../user/contact/deleted-contacts";
import {Keys} from "../user/key/keys";
import {UserPayload} from "../user/user-payload";
import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";
import {Profile} from "./profile";

@Injectable()
export class ProfilesRemote {

  constructor(private httpClient: HttpClient,
              private sessionService: SessionService) {
  }

  deleteProfile(): Observable<boolean> {

    const aesPseudoUrlFriendlyObs: Observable<string> = this.sessionService.exchangeAes()
      .pipe(
        mergeMap((exchangeAes: AES) => {
          const pseudo: string = this.sessionService.session().pseudo();
          return exchangeAes.encryptAsText(pseudo);
        }),
        map((aesUsername: string) => {
          return AES.urlFriendly(aesUsername);
        })
      );

    return aesPseudoUrlFriendlyObs
      .pipe(
        mergeMap((aesUsernameUrlFriendly) => {
          return this.httpClient.delete(`${config.api.profiles}/${aesUsernameUrlFriendly}/state/deleted`);
        }),
        map(() => {
          return true;
        })
      );
  }

  fetchDetails(pseudo: string): Observable<Profile> {
    return this.httpClient.get(`${config.api.profiles}/${pseudo}`)
      .pipe(
        map((response: DetailsResponse) => Profile
          .build(response.boxAddress, response.systemPublicKeys, response.username))
      );
  }

  fetchOwnPayload(): Observable<UserPayload> {
    return this.sessionService.exchangeAes()
      .pipe(
        mergeMap((exchangeAes: AES) => exchangeAes.encryptAsText(this.sessionService.pseudo())),
        mergeMap((aesPseudo: string) => this.httpClient.get(`${config.api.profiles}/${AES.urlFriendly(aesPseudo)}/payload`)),
        mergeMap((payloadResponse: PayloadResponse) => this.sessionService.openPgp()
          .decrypt(payloadResponse.payload, this.sessionService.systemPublicKeys())),
        map((json: string) => UserPayload.fromJson(json))
      );
  }

  /**
   * Updates the payload of the logged username, using the specified payload contacts, keys and secretKeys.
   */
  updatePayload(contacts: Contacts, deletedContacts: DeletedContacts, keys: Keys, secretKey: string): Observable<any> {

    const aesPseudoObs: Observable<string> = this.sessionService.exchangeAes()
      .pipe(
        mergeMap((exchangeAes: AES) => exchangeAes.encryptAsText(this.sessionService.pseudo())),
        map((aesUsername: string) => AES.urlFriendly(aesUsername))
      );

    const json: string = UserPayload.with(
      contacts,
      deletedContacts,
      this.sessionService.displayName(),
      keys,
      this.sessionService.picture(),
      secretKey)
      .toJson();
    const userPayloadPgpObs: Observable<string> = this.sessionService.openPgp().encrypt(json);

    return forkJoin([aesPseudoObs, userPayloadPgpObs])
      .pipe(
        mergeMap(([aesPseudo, userPayloadPgp]) => {
          return this.httpClient
            .put(`${config.api.profiles}/${aesPseudo}/payload`, PayloadRequest.with(userPayloadPgp).toJson());
        })
      );
  }
}

export interface DetailsResponse {
  boxAddress: string;
  systemPublicKeys: string;
  username: string;
}

export class PayloadRequest implements Serializable {

  static with(payload: string): PayloadRequest {
    return new PayloadRequest(payload);
  }

  private constructor(private readonly _payload: string) {
  }

  asObject(): any {
    return {payload: this._payload};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export interface PayloadResponse {
  payload: string;
}
