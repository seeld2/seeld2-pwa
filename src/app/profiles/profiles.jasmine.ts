import {Objects} from "../utils/objects";
import {ProfilesService} from "./profiles.service";
import {ProfilesRemote} from "./profiles-remote";

export class ProfilesJasmine {

  static profilesService() {
    return jasmine.createSpyObj("profilesService", Objects.propertyNamesOf(new ProfilesService(null, null, null, null)));
  }

  static profilesRemote() {
    return jasmine.createSpyObj('profilesRemote', Objects.propertyNamesOf(new ProfilesRemote(null, null)));
  }
}
