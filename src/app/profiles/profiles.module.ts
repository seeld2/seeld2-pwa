import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {SessionsModule} from "../sessions/sessions.module";
import {ProfilesService} from "./profiles.service";
import {CryptoModule} from "../crypto/crypto.module";
import {UserModule} from "../user/user.module";
import {ProfilesRemote} from "./profiles-remote";
import {UtilsModule} from "../utils/utils.module";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    SessionsModule,
    UserModule,
    UtilsModule
  ],
  providers: [
    ProfilesRemote,
    ProfilesService
  ]
})
export class ProfilesModule {
}
