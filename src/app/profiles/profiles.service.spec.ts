import {LoaderService} from "../mobile/loader.service";
import {PlatformJasmine} from "../mobile/platform.jasmine";
import {ProfilesService} from "./profiles.service";
import {ProfilesRemote} from "./profiles-remote";
import {SessionService} from "../sessions/session.service";
import {Session} from "../sessions/session";
import {Storage} from "@ionic/storage";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {of} from "rxjs/observable/of";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {ProfilesJasmine} from "./profiles.jasmine";
import {UserPayloadTestBuilder as aUserPayload} from "../user/user-payload.test.builder";
import {UserPayload} from "../user/user-payload";
import {ContactTestBuilder as aContact} from "../user/contact/contact.test-builder";

describe("ProfilesService", () => {

  const LOGGED_USER_PSEUDO: string = "logged user pseudo"

  let loaderService: LoaderService;
  let profilesRemote: ProfilesRemote;
  let sessionService: SessionService;
  let storage: Storage;

  let profilesService: ProfilesService;

  let session: Session;

  beforeEach(() => {
    loaderService = PlatformJasmine.loaderService();
    profilesRemote = ProfilesJasmine.profilesRemote();
    sessionService = SessionsJasmine.sessionService();
    storage = IonicJasmine.storage();

    profilesService = new ProfilesService(loaderService, profilesRemote, sessionService, storage);

    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);
  });

  it("should delete a user profile", (done: DoneFn) => {

    (<jasmine.Spy>loaderService.createLoader).and.returnValue(of({}));
    (<jasmine.Spy>profilesRemote.deleteProfile).and.returnValue(of(true));
    (<jasmine.Spy>storage.clear).and.returnValue(Promise.resolve());
    (<jasmine.Spy>loaderService.dismissLoader).and.returnValue(of({}));

    profilesService.deleteProfile()
      .subscribe(() => {
        done();
      });
  });

  describe(", when checking if the specified pseudo is not connect to the logged user,", () => {

    beforeEach(() => {
      (<jasmine.Spy>session.pseudo).and.returnValue(LOGGED_USER_PSEUDO);
    });

    it("should return true if the submitted pseudo is not a connection of the logged user", () => {
      (<jasmine.Spy>session.contactsAsArray).and.returnValue([aContact.with().pseudo("bobby").asIs()]);
      expect(profilesService.isNotConnectedTo("carol")).toBe(true);
    });

    it("should return false if the submitted pseudo is already a connection of the logged user", () => {
      (<jasmine.Spy>session.contactsAsArray).and.returnValue([aContact.with().pseudo("bobby").asIs()]);
      expect(profilesService.isNotConnectedTo("bobby")).toBe(false);
    });

    it("should return false if the submitted pseudo is actually the logged user", () => {
      expect(profilesService.isNotConnectedTo(LOGGED_USER_PSEUDO)).toBe(false);
    })
  });

  it("should sync the local session's contacts to the ones from the remote payload", (done: DoneFn) => {

    const contact = aContact.with().pseudo(LOGGED_USER_PSEUDO).asIs();
    const userPayload: UserPayload = aUserPayload.with().defaultSystemKeyPair().and.contact(contact).asIs();

    (<jasmine.Spy>profilesRemote.fetchOwnPayload).and.returnValue(of(userPayload));
    (<jasmine.Spy>sessionService.refreshSessionContacts).and.returnValue(of({}));

    profilesService.syncSessionContactsToRemote().subscribe(() => {

      expect(sessionService.refreshSessionContacts).toHaveBeenCalledWith(userPayload.contacts());

      done();
    });
  });
});
