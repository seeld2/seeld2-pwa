import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {LoaderService} from "../mobile/loader.service";
import {ProfilesRemote} from "./profiles-remote";
import {SessionService} from "../sessions/session.service";
import {Profile} from "./profile";
import {Contact} from "../user/contact/contact";
import {mergeMap} from "rxjs/operators";
import {Storage} from "@ionic/storage";
import {UserPayload} from "../user/user-payload";

@Injectable()
export class ProfilesService {

  constructor(private readonly loaderService: LoaderService,
              private readonly profilesRemote: ProfilesRemote,
              private readonly sessionService: SessionService,
              private readonly storage: Storage) {
  }

  deleteProfile(): Observable<any> {
    return from(this.loaderService.createLoader())
      .pipe(
        mergeMap(() => this.profilesRemote.deleteProfile()),
        mergeMap(() => this.storage.clear()),
        mergeMap(() => this.loaderService.dismissLoader())
      );
  }

  fetchDetails(pseudo: string): Observable<Profile> {
    return this.profilesRemote.fetchDetails(pseudo);
  }

  isNotConnectedTo(pseudo: string): boolean {
    if (pseudo === this.sessionService.session().pseudo()) {
      return false;
    }

    const indexOfContactWithPseudo: number = this.sessionService.session().contactsAsArray()
      .findIndex((contact: Contact) => contact.pseudo() === pseudo);
    return indexOfContactWithPseudo === -1;
  }

  syncSessionContactsToRemote(): Observable<any> {
    return this.profilesRemote.fetchOwnPayload()
      .pipe(
        mergeMap((userPayload: UserPayload) => this.sessionService.refreshSessionContacts(userPayload.contacts()))
      );
  }

  updatePayload(): Observable<any> {
    return this.profilesRemote.updatePayload(
      this.sessionService.session().contacts(),
      this.sessionService.session().deletedContacts(),
      this.sessionService.session().keys(),
      this.sessionService.session().secretKey());
  }
}
