import {Storage} from "@ionic/storage";
import {of} from "rxjs/observable/of";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {LoaderService} from "../mobile/loader.service";
import {PlatformJasmine} from "../mobile/platform.jasmine";
import {SessionTestBuilder as aSession} from "../sessions/session.test-builder";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {Contact} from "../user/contact/contact";
import {ContactKey} from "../user/contact/contact-key";
import {Contacts} from "../user/contact/contacts";
import {Profile} from "./profile";
import {ProfilesRemote} from "./profiles-remote";
import {ProfilesJasmine} from "./profiles.jasmine";
import {ProfilesService} from "./profiles.service";

describe('Profiles', () => { // TODO Merge with profiles.service.spec.ts

  const CONNECTED_USERNAME: string = 'connected';
  const LOGGED_USERNAME: string = 'logged username';
  const UNKNOWN_USERNAME: string = 'UNKNOWN_USERNAME';

  let loaderService: LoaderService;
  let profilesRemote: ProfilesRemote;
  let sessionService: SessionService;
  let storage: Storage;

  let profiles: ProfilesService;

  beforeEach(() => {
    loaderService = PlatformJasmine.loaderService();
    profilesRemote = ProfilesJasmine.profilesRemote();
    sessionService = SessionsJasmine.sessionService();
    storage = IonicJasmine.storage();

    profiles = new ProfilesService(loaderService, profilesRemote, sessionService, storage);

    (<jasmine.Spy>sessionService.session).and.returnValue(aSession.with()
      .contacts(Contacts.fromArrayOfContacts([aContact(CONNECTED_USERNAME)])).and.pseudo(LOGGED_USERNAME).asIs());
  });

  it("should fetch the details of a given user", (done: DoneFn) => {

    const profile: Profile = Profile.build('boxAddress', 'systemPublicKeys', CONNECTED_USERNAME);

    (<jasmine.Spy>profilesRemote.fetchDetails).and.returnValue(of(profile));

    profiles.fetchDetails(CONNECTED_USERNAME).subscribe((details: Profile) => {

      expect(profilesRemote.fetchDetails).toHaveBeenCalledWith(CONNECTED_USERNAME);

      expect(details).toBeDefined();
      expect(details.pseudo()).toEqual(CONNECTED_USERNAME);

      done();
    });
  });

  describe(", when checking if a user is connected to the logged user,", () => {

    it("should confirm the user is not connected to a given contact's username", () => {
      expect(profiles.isNotConnectedTo(UNKNOWN_USERNAME)).toEqual(true);
    });

    it("should confirm the user is already connected to a given contact's username", () => {
      expect(profiles.isNotConnectedTo(CONNECTED_USERNAME)).toEqual(false);
    });

    it("should confirm the user is already connected to himself", () => {
      expect(profiles.isNotConnectedTo(LOGGED_USERNAME)).toEqual(false);
    });
  });

  function aContact(username: string): Contact {
    return Contact.with(username, null, null, null, ContactKey.with(null, null, null), null);
  }
});
