export class RegistrationRequest {

  displayName: string;
  kdf: RegistrationKdf;
  payload: string;
  picture: string;
  srp: RegistrationSrp;
  systemKeys: RegistrationSystemKeys;
  username: string;
}

export interface RegistrationKdf {
  type: string;
  salt: string;
}

export interface RegistrationSrp {
  type: string;
  salt: string;
  verifier: string;
}

export interface RegistrationSystemKeys {
  type: string;
  publicKeys: string;
  privateKeys: string;
}
