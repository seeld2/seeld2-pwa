import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {CryptoModule} from "../crypto/crypto.module";
import {LoginModule} from "../login/login.module";
import {SessionsModule} from "../sessions/sessions.module";
import {UserModule} from "../user/user.module";
import {UtilsModule} from "../utils/utils.module";
import {RegistrationsRemote} from "./registrations.remote";
import {RegistrationsService} from "./registrations.service";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    LoginModule,
    SessionsModule,
    UserModule,
    UtilsModule
  ],
  providers: [
    RegistrationsRemote,
    RegistrationsService
  ]
})
export class RegistrationsModule {
}
