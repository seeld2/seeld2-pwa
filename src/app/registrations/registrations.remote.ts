import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {config} from "../app.config";
import {RegistrationRequest} from "./registration-request";

@Injectable()
export class RegistrationsRemote {

  constructor(private httpClient: HttpClient) {
  }

  fetchRegistrationAvailability(): Observable<any> {
    return this.httpClient.get(`${config.api.registrations}/availability`);
  }

  fetchUsernameAvailability(username: string): Observable<any> {
    return this.httpClient.get<any[]>(`${config.api.registrations}/${username}/availability`);
  }

  submitRegistration(registrationRequest: RegistrationRequest): Observable<any> {
    return this.httpClient.post(`${config.api.registrations}/`, registrationRequest);
  }
}
