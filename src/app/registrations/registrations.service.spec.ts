import {RegistrationsService} from "./registrations.service";
import {LoginService} from "../login/login.service";
import {RegistrationsRemote} from "./registrations.remote";
import {RegistrationRequest} from "./registration-request";
import {of} from "rxjs/observable/of";
import {SessionService} from "../sessions/session.service";
import {CoreJasmine} from "../../core/core-jasmine";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {LoginJasmine} from "../login/login.jasmine";

describe("Registration", () => {

  const DISPLAY_NAME = 'displayName';
  const PASSPHRASE = 'passphrase';
  const USERNAME = 'username';

  let login: LoginService;
  let registrationsRemote: RegistrationsRemote;
  let sessionService: SessionService;

  let registrations: RegistrationsService;

  beforeEach(() => {
    login = LoginJasmine.loginService();
    registrationsRemote = CoreJasmine.registrationsRemote();
    sessionService = SessionsJasmine.sessionService();

    registrations = new RegistrationsService(login, registrationsRemote, sessionService);
  });

  it("should register a new user", (done: DoneFn) => {

    (sessionService.clearAndRemoveSession as jasmine.Spy).and.returnValue(of(true));
    (registrationsRemote.submitRegistration as jasmine.Spy).and.returnValue(of({}));
    (login.performLogin as jasmine.Spy).and.returnValue(of(true));

    try {

      registrations.register(USERNAME, DISPLAY_NAME, PASSPHRASE).subscribe((registration: RegistrationRequest) => {

        expect(sessionService.clearAndRemoveSession).toHaveBeenCalledTimes(1);
        expect(registrationsRemote.submitRegistration).toHaveBeenCalledTimes(1);

        expect(registration).toBeDefined();
        expect(registration.displayName).toBeUndefined();
        expect(registration.payload).toBeDefined();
        expect(registration.username).toEqual(USERNAME);
        expect(registration.kdf).toBeUndefined();
        expect(registration.srp).toBeUndefined();

        done();

      });
    } catch (e) {
      fail(e);
    }
  }, 100000);
});
