import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../app.config";
import {Hmac} from "../crypto/hmac/hmac";
import {KDF} from "../crypto/kdf/kdf";
import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {OpenPgpUserId} from "../crypto/openpgp/open-pgp-user-id";
import {SRP} from "../crypto/srp/srp";
import {LoginService} from "../login/login.service";
import {SessionService} from "../sessions/session.service";
import {Keys} from "../user/key/keys";
import {UserPayload} from "../user/user-payload";
import {RegistrationKdf, RegistrationRequest, RegistrationSrp, RegistrationSystemKeys} from "./registration-request";
import {RegistrationsRemote} from "./registrations.remote";

@Injectable()
export class RegistrationsService {

  private kdf: KDF;
  private openPgp: OpenPgp;

  constructor(private login: LoginService,
              private registrationsRemote: RegistrationsRemote,
              private sessionService: SessionService) {
    this.kdf = new KDF({
      N: config.kdf.N,
      r: config.kdf.r,
      p: config.kdf.p,
      dkLen: config.kdf.dkLen,
      encoding: config.kdf.encoding
    });
  }

  isRegistrationAvailable(): Observable<boolean> {

    return this.registrationsRemote.fetchRegistrationAvailability()
      .pipe(
        mergeMap(availability => {
          return of(availability.available);
        })
      );
  }

  register(username: string, displayName: string, passphrase: string): Observable<RegistrationRequest> {
    const registrationRequest: RegistrationRequest = new RegistrationRequest();

    username = username.toLowerCase();

    // registrationRequest.displayName = displayName;
    registrationRequest.username = username;

    registrationRequest.kdf = RegistrationsService.generateKdf();
    registrationRequest.srp = RegistrationsService.generateSrp(username, passphrase);

    const userId: OpenPgpUserId = {
      email: username + config.pgp.userIdDomain,
      name: username
    };

    return this.sessionService.clearAndRemoveSession()
      .pipe(
        mergeMap(() => this.kdf.deriveKey(passphrase, registrationRequest.kdf.salt)),

        mergeMap((derivedPassphrase) => this.initializeOpenPgpWithNewKeys(userId, derivedPassphrase)),

        mergeMap((armoredKeyPair: ArmoredKeyPair) => {
          registrationRequest.systemKeys = {
            type: Keys.SYSTEM_KEYS_TYPE,
            publicKeys: armoredKeyPair.publicKeys,
            privateKeys: armoredKeyPair.privateKeys
          } as RegistrationSystemKeys;

          const secretKey: string = Hmac.generateSecretKey();
          const json: string = UserPayload.withoutContacts(displayName, null, armoredKeyPair, secretKey).toJson();
          return this.openPgp.encrypt(json);
        }),

        mergeMap((encryptedPayload: string) => {
          registrationRequest.payload = encryptedPayload;

          return this.registrationsRemote.submitRegistration(registrationRequest);
        }),

        mergeMap(() => this.login.performLogin(username, passphrase)),

        map(() => {
          passphrase = undefined; // For security reasons, we explicitly remove these
          registrationRequest.kdf = undefined;
          registrationRequest.srp = undefined;
          return registrationRequest;
        })
      );
  }

  private initializeOpenPgpWithNewKeys(userId: any, passphrase: string): Observable<ArmoredKeyPair> {
    let armoredKeyPair: ArmoredKeyPair;

    return from(OpenPgp.generateKeys(userId, passphrase))
      .pipe(
        mergeMap((akp: ArmoredKeyPair) => {
          armoredKeyPair = akp;

          return OpenPgp.fromArmoredKeyPair(armoredKeyPair, passphrase);
        }),
        map((o: OpenPgp) => {
          this.openPgp = o;

          return armoredKeyPair;
        })
      );
  }

  private static generateKdf(): RegistrationKdf {
    return {type: 'scrypt', salt: SRP.generateSalt()} as RegistrationKdf;
  }

  private static generateSrp(username: string, passphrase: string): RegistrationSrp {
    const srpSalt: string = SRP.generateSalt();
    return {
      type: 'srp6a',
      salt: srpSalt,
      verifier: SRP.generateVerifier(username, passphrase, srpSalt)
    } as RegistrationSrp;
  }
}
