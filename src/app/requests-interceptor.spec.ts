import {HttpEvent, HttpHandler, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {config} from "./app.config";
import {RequestsInterceptor} from "./requests-interceptor";
import {Session} from "./sessions/session";
import {SessionService} from "./sessions/session.service";
import {SessionsJasmine} from "./sessions/sessions.jasmine";

describe("RequestsInterceptor", () => {

  const GET: string = 'GET';
  const POST: string = 'POST';
  const PUT: string = 'PUT';

  const AUTH: string = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJiMmM0MTIwOC0wNzAxLTQ3MWItOTYwMy00MGQ3MjkyMDI5MTIiLCJzdWIiOiJ0ZXN0MDEiLCJleHAiOjE1NjgyMzIwMjZ9.HJlnRYZ1vAGRe9unuoUFOTXToK0zqaZrLhejMOxx2RULTUMdcu7zYmoe3-Kuo70hiHsfHroKiZX62oyRr7ymFQ";

  let sessionService: SessionService;

  let requestsInterceptor: RequestsInterceptor;

  beforeEach(() => {
    sessionService = SessionsJasmine.sessionService();

    requestsInterceptor = new RequestsInterceptor(sessionService);

    (<jasmine.Spy>sessionService.session).and.returnValue(aSession(AUTH));
  });

  it("should send credentials for methods GET on secured request URL", (done: DoneFn) => {
    expectCredentialsToBeSent(httpRequest('/api/secured/url', GET), done);
  });

  it("should send credentials for methods POST on secured request URL", (done: DoneFn) => {
    expectCredentialsToBeSent(httpRequest('/api/secured/url', POST), done);
  });

  it("should send credentials for methods PUT on secured request URL", (done: DoneFn) => {
    expectCredentialsToBeSent(httpRequest('/api/secured/url', PUT), done);
  });

  it("should not send credentials for PUT on '/api/conversations'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(config.api.conversations, PUT), done);
  });

  it("should not send credentials for PUT on '/api/conversations/id'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.conversations}/ids`, PUT), done);
  });

  it("should not send credentials for PUT on '/api/conversations/conversationId/messages'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.conversations}/conversationId/messages`, PUT), done);
  });

  it("should not send credentials for PUT on '/api/conversations/conversationId/state/deleted'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.conversations}/conversationId/state/deleted`, PUT), done);
  });

  it("should not send credentials for PUT on '/api/conversations/conversationId/messages/messageId/state/deleted'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.conversations}/conversationId/messages/messageId/state/deleted`, PUT), done);
  });

  it("should not send credentials for PUT on '/api/events'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(config.api.events, PUT), done);
  });

  it("should send credentials for POST on '/api/events'", (done: DoneFn) => {
    expectCredentialsToBeSent(httpRequest(config.api.events, POST), done);
  });

  it("should not send credentials for PUT on '/api/events/eventId/state/deleted'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.events}/eventId/state/deleted`, PUT), done);
  });

  it("should not send credentials for POST on '/api/login'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(config.api.login, POST), done);
  });

  it("should not send credentials for GET on '/api/login/challenge'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(config.api.login + '/challenge/username', GET), done);
  });

  it("should not send credentials for PUT on '/api/messages/new'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.messages}/new`, PUT), done);
  });

  it("should not send credentials for PUT on '/api/messages/new/timeline/latest'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.messages}/new/timeline/latest`, PUT), done);
  });

  it("should not send credentials for POST on '/api/registrations'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(config.api.registrations, POST), done);
  });

  it("should not send credentials for GET on '/api/registrations/availability'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.registrations}/availability`, GET), done);
  });

  it("should not send credentials for GET on '/api/registrations/username/availability'", (done: DoneFn) => {
    expectCredentialsNotToBeSent(httpRequest(`${config.api.registrations}/username/availability`, GET), done);
  });

  function aSession(auth: string) {
    return Session.build(auth, null, null, null, null, null, null, null, null, null, null);
  }

  function expectCredentialsNotToBeSent(req: HttpRequest<any>, done: DoneFn) {
    let next: FakeHttpHandler = new FakeHttpHandler();
    requestsInterceptor.intercept(req, next)
      .subscribe(() => {
        expect(next.handledReq.headers.get('Authorization')).toBe(null);
        done();
      });
  }

  function expectCredentialsToBeSent(req: HttpRequest<any>, done: DoneFn) {
    let next: FakeHttpHandler = new FakeHttpHandler();
    requestsInterceptor.intercept(req, next)
      .subscribe(() => {
        expect(next.handledReq.headers.get('Authorization')).toContain(AUTH);
        done();
      });
  }

  function httpRequest (url: string, method: string, presentHeaders: any[] = []) {
    return {
      clone: (req) => req,
      headers: {
        keys: () => presentHeaders
      },
      method: method,
      url: url
    } as HttpRequest<any>;
  }
});

class FakeHttpHandler extends HttpHandler {

  handledReq: HttpRequest<any>;

  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    this.handledReq = req;
    return of(null);
  }
}
