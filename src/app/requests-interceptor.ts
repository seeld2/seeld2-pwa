import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {environment} from "../environments/environment";
import {config} from "./app.config";
import {SessionService} from "./sessions/session.service";
import {AntPathMatcher} from "./utils/ant-path-matcher";
import {Logger} from "./utils/logging/logger";

@Injectable()
export class RequestsInterceptor implements HttpInterceptor {

  private readonly logger: Logger = Logger.for("RequestsInterceptor");

  private urlExceptionsAntPathMatcher: AntPathMatcher;

  constructor(private sessionService: SessionService) {
    this.urlExceptionsAntPathMatcher = AntPathMatcher.for(config.api.credentials.exceptions.map((e) => e.url));
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const requiresAuthToken = this.requiresAuthToken(req);

    const httpRequest = req.clone({
      headers: this.getHeaders(req, requiresAuthToken),
      method: req.method,
      url: req.url,
      withCredentials: true
    });

    this.logger.debug(`url: ${httpRequest.url} | urlWithParams: ${httpRequest.urlWithParams} | method: ${httpRequest.method} | withCredentials: ${httpRequest.withCredentials}`);

    return next.handle(httpRequest);
  }

  private getHeaders(req: HttpRequest<any>, requiresAuthToken: boolean): HttpHeaders {

    const headers = {};

    req.headers.keys().forEach((name: string) => {
      headers[name] = req.headers.get(name);
    });

    if (requiresAuthToken) {
      headers['Authorization'] = 'Bearer ' + this.sessionService.session().auth();
    }

    if (!headers['Accept']) {
      headers['Accept'] = 'application/json';
    }
    if (!headers['Content-Type']) {
      headers['Content-Type'] = 'application/json';
    }

    headers['Api-Ver'] = (config.api.version).toString();

    return new HttpHeaders(headers);
  }

  private isAPathThatRequiresCredentials(req: HttpRequest<any>, method: string): boolean {
    const matchingAntPath: string = this.urlExceptionsAntPathMatcher.match(RequestsInterceptor.rootlessUrl(req));
    if (!matchingAntPath) {
      return true;
    }
    const antPaths: any[] = config.api.credentials.exceptions
      .filter((exception: any) => exception.url === matchingAntPath && exception.method === method);
    return antPaths.length === 0;
  }

  private requiresAuthToken(req: HttpRequest<any>): boolean {
    const methodRequiresCredentials: boolean = RequestsInterceptor.isAMethodThatRequiresCredentials(req);
    const pathRequiresCredentials: boolean = this.isAPathThatRequiresCredentials(req, req.method);
    return methodRequiresCredentials && pathRequiresCredentials;
  }

  private static isAMethodThatRequiresCredentials(req: HttpRequest<any>) {
    return config.api.credentials.methods.indexOf(req.method) > -1;
  }

  private static rootlessUrl(req: HttpRequest<any>) {
    return req.url.replace(environment.api.server.address, '');
  }
}
