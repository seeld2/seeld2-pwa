import {HmacHashesByBoxAddress} from "./hmac-hashes-by-box-address";

describe("HmacHashesByBoxAddress", () => {

  const BOX_ADDRESS_1: string = "boxAddress1";
  const BOX_ADDRESS_2: string = "boxAddress2";
  const BOX_ADDRESS_3: string = "boxAddress3";
  const BOX_ADDRESS_4: string = "boxAddress4";
  const HMAC_HASH_1: string = "hmacHash1";
  const HMAC_HASH_2: string = "hmacHash2";
  const HMAC_HASH_3: string = "hmacHash3";
  const HMAC_HASH_4: string = "hmacHash4";

  it("should merge an instance with another one", () => {
    const map1: any = {};
    map1[BOX_ADDRESS_1] = HMAC_HASH_1;
    map1[BOX_ADDRESS_2] = HMAC_HASH_2;
    const hmacHashesByBoxAddress1: HmacHashesByBoxAddress = HmacHashesByBoxAddress.with(map1);
    expect(Object.keys(hmacHashesByBoxAddress1.asObject()).length).toEqual(2);

    const map2: any = {};
    map2[BOX_ADDRESS_3] = HMAC_HASH_3;
    map2[BOX_ADDRESS_4] = HMAC_HASH_4;
    const hmacHashesByBoxAddress2: HmacHashesByBoxAddress = HmacHashesByBoxAddress.with(map2);
    expect(Object.keys(hmacHashesByBoxAddress2.asObject()).length).toEqual(2);

    const result: HmacHashesByBoxAddress = hmacHashesByBoxAddress1.mergeWith(hmacHashesByBoxAddress2);

    expect(Object.keys(hmacHashesByBoxAddress1.asObject()).length).toEqual(4);
    expect(Object.keys(hmacHashesByBoxAddress2.asObject()).length).toEqual(2);

    const resultObject: any = result.asObject();
    expect(Object.keys(resultObject).length).toEqual(4);
    expect(resultObject[BOX_ADDRESS_1]).toEqual(HMAC_HASH_1);
    expect(resultObject[BOX_ADDRESS_2]).toEqual(HMAC_HASH_2);
    expect(resultObject[BOX_ADDRESS_3]).toEqual(HMAC_HASH_3);
    expect(resultObject[BOX_ADDRESS_4]).toEqual(HMAC_HASH_4);
  });
});
