import {HmacHashesByBoxAddress} from "./hmac-hashes-by-box-address";

export class HmacHashesByBoxAddressTestBuilder {

  static with(): HmacHashesByBoxAddressTestBuilder {
    return new HmacHashesByBoxAddressTestBuilder();
  }

  static withoutEntries(): HmacHashesByBoxAddress {
    return HmacHashesByBoxAddress.with({});
  }

  and = this;

  private readonly _hmacHashesByBoxAddress: any = {};

  private constructor() {
  }

  asIs(): HmacHashesByBoxAddress {
    return HmacHashesByBoxAddress.with(this._hmacHashesByBoxAddress);
  }

  entry(boxAddress: string, hmacHash: string): HmacHashesByBoxAddressTestBuilder {
    this._hmacHashesByBoxAddress[boxAddress] = hmacHash;
    return this;
  }
}
