import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class HmacHashesByBoxAddress implements Serializable {

  static empty(): HmacHashesByBoxAddress {
    return new HmacHashesByBoxAddress({});
  }

  static with(hmacHashesByBoxAddress: any): HmacHashesByBoxAddress {
    return !hmacHashesByBoxAddress
      ? new HmacHashesByBoxAddress({})
      : new HmacHashesByBoxAddress(hmacHashesByBoxAddress);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: any) {
  }

  asObject(): any {
    return this._hmacHashesByBoxAddress;
  }

  isEmpty(): boolean {
    return Object.keys(this._hmacHashesByBoxAddress).length === 0;
  }

  mergeWith(other: HmacHashesByBoxAddress): HmacHashesByBoxAddress {
    const boxAddresses: string[] = Object.keys(other._hmacHashesByBoxAddress);
    boxAddresses.forEach((boxAddress: string) =>
      this._hmacHashesByBoxAddress[boxAddress] = other._hmacHashesByBoxAddress[boxAddress]);
    return this;
  }

  set(boxAddress: string, hmacHash: string = null) {
    this._hmacHashesByBoxAddress[boxAddress] = hmacHash;
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
