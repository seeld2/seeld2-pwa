import {DeletedContacts} from "../user/contact/deleted-contacts";
import {SessionTestBuilder as aSession} from "./session.test-builder";
import {SessionService} from "./session.service";
import {Session} from "./session";
import {Contact} from "../user/contact/contact";
import {of} from "rxjs/observable/of";
import {CoreJasmine} from "../../core/core-jasmine";
import {Events} from "ionic-angular";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {SessionsJasmine} from "./sessions.jasmine";
import {SessionStorageService} from "./storage/session-storage.service";
import {LoginResponse} from "../login/login-response";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {UserPayload} from "../user/user-payload";
import {SessionEvent} from "./session-event";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {ContactKey} from "../user/contact/contact-key";
import {Keys} from "../user/key/keys";
import {Contacts} from "../user/contact/contacts";

describe("SessionService", () => {

  const AUTH: string = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiIyM2ZhMzFmMi1hNTI2LTQ3ZjEtOTBlOS1hM2MzYjc4MDAwMWMiLCJzdWIiOiJ0ZXN0MDEiLCJleHAiOjE1Njc5NjkxODF9.-yU9NvIBjLm6dNDkRWdnYbYyJSoWNwPywqnPwvX3bL3sYwOa91AbYYvLKuC3qW8zvITnj-wTl0CyyLYXvJSnkg";
  const CONTACTS: Contacts = Contacts.fromArrayOfContacts([aContact('bob'), aContact('carol')]);
  const M2: string = "a87ba8d58968584cbe7c16d7f9bcc147a913864e6e67c50498706a3e1dab0317";
  const PAYLOAD: string = `-----BEGIN PGP MESSAGE-----
Version: OpenPGP.js v4.5.1
Comment: https://openpgpjs.org

wV4D8sBELXO/Qi0SAQdAyB5/48Ga5yfbZ1Enz/4H9RmzzMhFBQPC/2ylG3p3
o0cwLCkuH3vv8pOlGYRKLfa33NR+VXWYjsAAgnctvu7jdhL9Iz5JfJf621UP
1vi0O9D/0sReAeQOIp+2whk8JBT2GYRgtIc9vx9WmhPbWy+7k42UKbYU7hvx
JeGMmtrRVF0Y0BarhxGYjSNAKDKQhFRX5YgqoTh0kP9nHfezZgek4PwN3GQ0
t9OIZq/gyjvUZdMAyHmkEWWwneRm4GbjTyJgepkjKzt+AYkNlie3AluMSlcb
PEu1iMaPkeVIPKw9UJ6RYc8GMYlQuRrfkDdoWsrCRRvxy8A6WzjBUEv9f6Ui
n5DbC9YKiq6Yyr3q45gW0imHmWqWgizIPAJS8C60UGRycDoM8RImKERXZMhh
rMUjRlPLCEv70NGtIu94BCyeCHAqCF55r78nYw/XC8vZcjUyeptkp8TgLWJc
0Lls5SIx9pqnu/cUpLw/F3bCVz+6AvEaCnzjuiWxlpWOBhKU6w8Gka6OZU4I
yQi72Uz9caYh+bVKx5djcS9j3atmfVsLTwPuoWCJxmHFbQ0nORUiYbc3WdW3
e68EP3Peibt2LCuxj1fDfbBDPDNJp3I5llRlky3IgsTrlIQhJpb4jetxjjPB
8prN2FUaN/xs6nZRoR2z7h55dBpVPg58NT/wnEF6mogb27GTGxq70AGJqPOS
7l8SyDDvn63/L0L3X6k2pvx7lp7bL6cYhNpROIxYPYdKMcZrsUQ9ZG7/7Mo0
Eht6yCDO5Qyg7u/SiC8qM9Z/o6dEFx6Q8RUh6KPXCKw9OCyx9nWIluK387d6
CcQxUmJKQccwkedCKxr9W4k9FdDqS+XqKQFwjCetOiKgCWXD7JK3K/PCSp1C
xV0kvQfTjgfnYIGHjS0fDmxy1aNMuLIGqDSavnCiuC7Cdn/W3lwn7vK7l1cD
xR0UOdRrCPe1TMwAGmwY7U1Bs9zc+FGwlQbqNWz3g224xOB9HRukfcJYaJ63
axzvZcPpD83LavARXB+QGANQebvBp9smj6bWe62c2Zh2DSfB75TeGgKY2ihX
aC7Ovken6DVE2GwOJSIYpgkxb5/TikLt4RVr5DgyOO2YdcNPtaCbeaC5m1Kv
Ljq7F6IaArYK6u2ryUtMgjkMwY3XpafKgrxLuui2/IbQXrbUXPJDnoa7Yr1g
RcSKSWNbRWP0/m+lVnE/9glCichtJXCRtFuzjjSHXScqR3ZGKBGgk7OyvUNb
Yz0H0LCrsv4wZOtUMesD+GGlefza4gjc6gme7KIuJWZgjj5ubWbT2Qax+aQM
Kgt5FC+W9SRKO09AbLgNy7RHXc+vTMWDRNIGddLng1/tLQJNpfcTASGOQRQc
BgeHGLhf7jGfxWUAKb0/9K8A5vociRFwbf9TPDyr4Wk2F1ibPGhVjOANNOJ8
touCdeEn0fJY3X93bo+rzY+Hhb31azEMalpthdQHL/+kaz34qeqMtrGily3M
tXl2NvzOKgkNZy9L5ZbX30z4mI2HSjXTLS5J7Mp41rES4afqn6KONJeI0euX
Po42WcFqlUoIXqo4HUAUAjmLiF6iAnd/hejxbQXdAWSd5vCCaVvIGXKa3T8G
gIW4mWDw1hphm0V3OnGgKpDBH6GfyQ52V5AE7x2PyFCWhRi5e+000so9cRm0
GIBtxP8e68t6sik793C/yFMgNGEyj/nNJUz8R5XHxFmxZEOENaziwOkuOEmi
ScB4uGiYVVf4fHvoGiVYa9RJm0ssMt9KjhY4nvw+4MW3pdeTdeaS2U26Ja7i
uAA3X2nnbLy//s0kIdAeIUa6Qul1Nw5RzgFB8+9K6UyjZuIKu/47hXruakHA
2U4f2HLfATw9PP7RFW8=
=vFsx
-----END PGP MESSAGE-----
`;
  const PROFILE: number[] = [150, 225, 5, 195, 145, 53, 86, 48, 72, 250, 209, 199, 165, 84, 108, 37, 197, 56, 118, 107, 42, 117, 73, 103, 205, 250, 52, 7, 10, 146, 106, 128, 170, 12, 255, 254, 155, 244, 220, 67, 92, 87, 170, 144, 129, 21, 149, 222, 178, 161, 179, 35, 80, 45, 63, 203, 83, 9, 92, 34, 127, 49, 179, 144, 155, 220, 91, 150, 174, 235, 103, 74, 223, 177, 188, 163, 218, 126, 125, 94, 151, 230, 205, 166, 227, 153, 140, 172, 21, 72, 222, 136, 156, 22, 40, 169, 121, 91, 13, 135, 252, 131, 213, 158, 174, 66, 165, 146, 31, 24, 88, 2, 213, 139, 82, 167, 233, 214, 23, 172, 99, 235, 50, 87, 88, 204, 170, 51, 217, 140, 127, 126, 125, 235, 13, 208, 46, 41, 12, 91, 238, 165, 169, 155, 97, 155, 90, 108, 248, 102, 57, 224, 122, 194, 183, 124, 200, 68, 234, 63, 121, 55, 44, 145, 29, 94, 23, 191, 26, 95, 223, 135, 4, 47, 171, 209, 213, 199, 144, 156, 189, 204, 6, 228, 34, 138, 88, 227, 6, 169, 30, 181, 25, 183, 69, 1, 106, 56, 173, 113, 18, 109, 212, 103, 128, 2, 182, 34, 223, 188, 122, 223, 154, 75, 222, 212, 30, 112, 100, 221, 166, 227, 221, 189, 220, 160, 177, 143, 224, 35, 186, 191, 232, 39, 71, 48, 132, 157, 215, 64, 233, 157, 189, 131, 18, 235, 180, 26, 30, 156, 84, 7, 200, 210, 206, 45, 45, 147, 133, 221, 74, 166, 113, 163, 225, 0, 21, 96, 101, 46, 40, 210, 90, 179, 14, 95, 64, 107, 90, 92, 94, 192, 134, 251, 58, 220, 163, 121, 68, 199, 208, 207, 190, 221, 152, 117, 45, 53, 93, 180, 71, 67, 100, 111, 183, 72, 223, 169, 239, 241, 143, 87, 87, 74, 169, 203, 54, 244, 124, 252, 180, 15, 8, 148, 10, 19, 100, 185, 226, 101, 169, 28, 9, 38, 210, 82, 41, 216, 215, 106, 89, 24, 100, 49, 150, 180, 203, 165, 56, 22, 101, 59, 162, 121, 36, 113, 37, 219, 215, 97, 229, 219, 209, 43, 161, 240, 64, 75, 2, 125, 128, 10, 210, 244, 13, 51, 137, 91, 7, 162, 89, 28, 224, 107, 221, 155, 217, 28, 119, 243, 129, 45, 35, 202, 201, 65, 118, 176, 159, 192, 82, 100, 81, 222, 214, 53, 149, 26, 99, 167, 199, 111, 188, 192, 125, 196, 182, 45, 131, 226, 103, 136, 235, 3, 2, 143, 27, 42, 236, 1, 69, 153, 210, 11, 243, 154, 100, 189, 181, 148, 122, 1, 176, 37, 57, 88, 78, 86, 116, 61, 113, 65, 197, 159, 79, 107, 152, 19, 243, 44, 206, 248, 176, 93, 57, 198, 51, 155, 245, 109, 147, 85, 217, 222, 181, 116, 173, 37, 242, 198, 206, 115, 25, 105, 141, 98, 156, 111, 38, 151, 111, 70, 49, 76, 218, 254, 104, 156, 235, 104, 218, 24, 165, 162, 246, 105, 122, 187, 69, 104, 189, 23, 178, 144, 58, 87, 32, 112, 35, 53, 110, 106, 136, 247, 108, 220, 176, 247, 86, 189, 83, 193, 248, 110, 121, 206, 115, 96, 162, 183, 178, 178, 192, 35, 230, 47, 104, 227, 134, 52, 129, 103, 18, 93, 99, 185, 13, 155, 200, 209, 45, 147, 21, 61, 78, 191, 152, 17, 15, 11, 142, 245, 221, 242, 99, 179, 46, 11, 127, 214, 5, 70, 48, 244, 68, 130, 220, 37, 214, 10, 93, 1, 242, 171, 17, 21, 79, 26, 169, 228, 87, 177, 82, 27, 142, 99, 47, 71, 38, 167, 44, 201, 22, 146, 96, 120, 45, 97, 202, 73, 252, 81, 203, 221, 41, 99, 141, 45, 216, 115, 206, 248, 115, 131, 119, 231, 249, 32, 212, 254, 22, 136, 142, 141, 90, 87, 47, 183, 202, 132, 56, 252, 138, 34, 216, 127, 22, 2, 254, 40, 46, 185, 52, 237, 111, 145, 171, 162, 152, 71, 214, 91, 152, 173, 215, 104, 98, 236, 110, 141, 106, 192, 255, 100, 114, 80, 170, 27, 206, 55, 221, 214, 50, 151, 147, 42, 218, 142, 17, 160, 153, 166, 149, 170, 45, 131, 109, 79, 226, 176, 47, 181, 199, 129, 206, 62, 42, 60, 151, 39, 138, 254, 219, 44, 135, 134, 160, 237, 151, 139, 30, 157, 175, 72, 175, 174, 51, 43, 29, 43, 69, 34, 76, 85, 24, 152, 100, 212, 49, 28, 41, 149, 77, 22, 85, 6, 37, 187, 8, 160, 25, 215, 224, 41, 160, 27, 120, 195, 8, 99, 64, 130, 187, 193, 167, 124, 20, 162, 177, 109, 157, 79, 34, 105, 122, 60, 88, 78, 57, 65, 87, 5, 162, 160, 52, 64, 214, 155, 253, 210, 6, 122, 75, 91, 157, 34, 112, 206, 47, 152, 31, 17, 203, 224, 161, 37, 31, 172, 59, 187, 14, 44, 188, 133, 232, 0, 32, 247, 119, 199, 10, 162, 21, 16, 171, 91, 191, 16, 220, 106, 29, 78, 189, 249, 87, 35, 240, 55, 58, 107, 48, 174, 38, 3, 204, 8, 90, 182, 153, 131, 77, 64, 115, 176, 113, 92, 113, 232, 121, 161, 30, 128, 94, 234, 115, 129, 166, 166, 152, 52, 45, 202, 89, 207, 242, 31, 134, 175, 161, 201, 131, 180, 228, 196, 158, 88, 156, 103, 98, 106, 93, 215, 10, 42, 226, 219, 112, 98, 225, 137, 189, 200, 61, 160, 187, 171, 189, 98, 229, 168, 148, 224, 179, 107, 96, 113, 13, 118, 51, 191, 55, 8, 166, 148, 100, 40, 255, 13, 44, 246, 154, 134, 170, 131, 47, 243, 185, 85, 44, 143, 175, 219, 62, 167, 62, 95, 209, 187, 203, 77, 36, 88, 169, 66, 217, 24, 166, 19, 116, 208, 190, 151, 43, 41, 199, 44, 42, 139, 119, 250, 62, 32, 30, 159, 58, 169, 126, 164, 56, 38, 139, 85, 171, 117, 121, 214, 1, 219, 27, 11, 242, 20, 177, 107, 225, 65, 163, 182, 29, 131, 161, 1, 23, 62, 40, 28, 70, 62, 52, 26, 111, 169, 68, 94, 171, 23, 119, 147, 216, 245, 89, 226, 120, 168, 197, 65, 101, 216, 12, 66, 15, 77, 77, 69, 32, 100, 107, 70, 36, 121, 60, 75, 145, 225, 247, 102, 181, 2, 26, 204, 175, 71, 92, 107, 109, 250, 230, 82, 251, 63, 239, 72, 134, 251, 98, 207, 124, 151, 118, 123, 59, 223, 11, 29, 96, 65, 184, 76, 200, 52, 250, 120, 145, 219, 223, 91, 67, 69, 38, 164, 180, 211, 47, 206, 186, 114, 178, 207, 249, 140, 240, 210, 20, 88, 86, 208, 197, 159, 28, 75, 116, 21, 227, 132, 70, 92, 1, 217, 46, 80, 96, 115, 177, 50, 112, 181, 183, 6, 16, 196, 37, 149, 54, 228, 78, 114, 231, 127, 135, 101, 188, 47, 134, 56, 41, 79, 39, 240, 132, 90, 41, 29, 179, 171, 243, 27, 27, 189, 66, 100, 26, 44, 11, 254, 219, 95, 184, 196, 138, 80, 173, 95, 145, 99, 55, 195, 140, 111, 50, 157, 192, 24, 202, 201, 8, 195, 182, 217, 222, 186, 251, 22, 127, 131, 225, 11, 23, 124, 127, 250, 102, 71, 110, 145, 99, 137, 126, 134, 190, 223, 99, 75, 123, 79, 161, 242, 245, 193, 20, 6, 29, 233, 95, 237, 1, 51, 45, 236, 227, 182, 148, 4, 179, 251, 247, 217, 22, 177, 61, 8, 102, 165, 147, 61, 254, 78, 70, 104, 157, 147, 133, 180, 155, 248, 12, 10, 229, 12, 1, 132, 55, 161, 197, 120, 169, 17, 254, 109, 234, 220, 102, 34, 124, 126, 156, 115, 0, 253, 23, 119, 177, 161, 25, 95, 82, 108, 229, 88, 166, 155, 193, 198, 122, 243, 21, 222, 166, 213, 43, 160, 42, 146, 63, 38, 247, 162, 140, 13, 112, 50, 0, 25, 108, 117, 22, 138, 56, 221, 100, 140, 135, 207, 165, 99, 107, 191, 22, 41, 110, 132, 110, 44, 7, 187, 54, 31, 120, 149, 124, 134, 234, 175, 192, 216, 5, 129, 179, 34, 118, 157, 33, 73, 18, 75, 147, 37, 138, 205, 50, 62, 224, 39, 254, 87, 219, 69, 122, 198, 247, 38, 107, 231, 63, 45, 118, 244, 74, 116, 180, 6, 175, 16, 110, 189, 32, 66, 202, 197, 9, 192, 219, 118, 40, 54, 209, 197, 72, 158, 34, 225, 165, 98, 47, 73, 75, 139, 234, 215, 255, 84, 104, 137, 60, 0, 82, 59, 214, 122, 241, 37, 64, 150, 130, 232, 24, 123, 148, 245, 133, 254, 102, 213, 39, 82, 56, 51, 23, 148, 151, 143, 238, 94, 32, 71, 21, 99, 142, 147, 102, 124, 139, 193, 18, 113, 75, 216, 54, 194, 185, 6, 175, 155, 41, 202, 20, 76, 172, 110, 225, 72, 212, 12, 172, 158, 91, 197, 173, 127, 138, 37, 140, 219, 26, 107, 248, 86, 225, 241, 14, 85, 96, 22, 35, 104, 57, 249, 59, 241, 154, 231, 24, 253, 183, 228, 68, 210, 146, 207, 84, 122, 119, 207, 43, 130, 223, 224, 80, 122, 244, 171, 43, 100, 40, 57, 157, 205, 24, 134, 225, 242, 116, 205, 130, 162, 64, 127, 68, 92, 101, 76, 196, 10, 224, 168, 139, 135, 163, 89, 137, 232, 169, 248, 103, 162, 31, 203, 142, 97, 111, 255, 50, 194, 5, 92, 174, 158, 159, 145, 220, 135, 13, 127, 210, 75, 200, 184, 245, 219, 93, 247, 241, 165, 138, 115, 24, 20, 54, 193, 87, 59, 233, 59, 230, 249, 251, 71, 70, 101, 198, 23, 24, 82, 60, 224, 156, 23, 180, 223, 43, 39, 120, 104, 215, 107, 32, 235, 190, 98, 13, 231, 39, 88, 127, 193, 235, 130, 16, 26, 34, 105, 114, 118, 172, 216, 116, 212, 217, 115, 125, 170, 208, 143, 230, 63, 116, 47, 131, 251, 108, 88, 192, 167, 133, 139, 74, 157, 218, 106, 54, 179, 72, 12, 15, 104, 12, 238, 27, 199, 162, 169, 202, 111, 144, 4, 244, 99, 245, 236, 214, 122, 11, 156, 204, 70, 17, 241, 223, 200, 27, 197, 161, 165, 41, 103, 62, 113, 76, 215, 174, 244, 85, 151, 221, 207, 2, 153, 237, 106, 240, 15, 138, 213, 48, 151, 134, 30, 230, 64, 248, 188, 91, 101, 187, 118, 28, 119, 240, 41, 199, 5, 239, 67, 170, 184, 252, 68, 136, 115, 32, 118, 82, 239, 66, 156, 149, 130, 79, 191, 226, 72, 87, 220, 186, 170, 235, 249, 109, 169, 69, 236, 139, 80, 39, 181, 235, 92, 79, 7, 181, 174, 156, 152, 141, 252, 102, 232, 140, 101, 161, 91, 145, 225, 16, 179, 27, 46, 184, 4, 30, 64, 171, 224, 21, 30, 104, 95, 159, 89, 192, 44, 223, 196, 207, 155, 232, 80, 48, 198, 124, 39, 0, 97, 11, 192, 122, 220, 157, 80, 119, 11, 137, 225, 239, 26, 232, 225, 116, 43, 20, 48, 255, 250, 107, 235, 45, 34, 111, 100, 90, 95, 10, 25, 241, 174, 255, 209, 84, 255, 242, 9, 92, 211, 102, 91, 141, 1, 191, 214, 61, 15, 225, 103, 187, 185, 240, 165, 5, 47, 40, 81, 222, 225, 119, 200, 250, 126, 138, 198, 227, 167, 242, 194, 233, 17, 247, 140, 159, 115, 174, 14, 131, 73, 158, 23, 6, 15, 200, 194, 63, 17, 83, 151, 44, 236, 90, 37, 165, 126, 171, 10, 189, 164, 168, 5, 43, 145, 251, 9, 4, 231, 86, 151, 103, 184, 70, 212, 67, 192, 195, 187, 95, 94, 213, 21, 161, 208, 153, 229, 16, 46, 125, 1, 221, 178, 120, 161, 246, 87, 118, 128, 52, 124, 151, 203, 158, 221, 67, 82, 60, 253, 62, 205, 96, 152, 238, 163, 93, 55, 40, 227, 23, 32, 1, 224, 23, 45, 160, 101, 75, 118, 59, 111, 87, 140, 147, 13, 37, 205, 106, 150, 102, 28, 149, 33, 37, 158, 100, 215, 105, 175, 158, 70, 45, 171, 233, 184, 135, 187, 254, 107, 63, 133, 164, 228, 7, 38, 91, 246, 48, 1, 197, 186, 78, 252, 85, 200, 215, 19, 186, 80, 48, 207, 58, 120, 147, 56, 34, 192, 245, 242, 113, 34, 73, 151, 113, 87, 230, 228, 111, 29, 173, 164, 50, 16, 53, 122, 40, 53, 148, 66, 127, 145, 182, 102, 231, 142, 38, 177, 1, 31, 216, 57, 173, 133, 120, 59, 166, 49, 246, 50, 224, 81, 32, 150, 0, 31, 121, 128, 219, 117, 128, 49, 29, 58, 226, 209, 4, 159, 210, 137, 12, 134, 20, 236, 24, 64, 213, 41, 221, 62, 61, 235, 162, 213, 148, 87, 198, 0, 160, 175, 240, 2, 176, 124, 206, 99, 223, 139, 20, 46, 190, 190, 158, 233, 137, 236, 61, 119, 97, 167, 83, 31, 30, 115, 0, 227, 129, 66, 171, 60, 183, 17, 31, 82, 32, 254, 69, 142, 125, 216, 134, 182, 44, 122, 43, 13, 235, 15, 208, 168, 72, 38, 255, 198, 12, 22, 233, 67, 150, 75, 182, 213, 90, 85, 50, 222, 120, 16, 197, 6, 233, 162, 30, 85, 196, 89, 106, 145, 197, 218, 178, 78, 92, 7, 164, 92, 251, 246, 113, 1, 8, 236, 33, 219, 36, 235, 215, 93, 229, 8, 196, 0, 59, 83, 38, 39, 229, 164, 252, 36, 85, 230, 35, 198, 103, 176, 14, 181, 212, 36, 166, 100, 184, 227, 187, 45, 157, 233, 140, 178, 102, 24, 57, 108, 131, 147, 156, 109, 192, 57, 141, 114, 97, 25, 152, 69, 14, 123, 129, 218, 98, 122, 119, 137, 156, 221, 116, 41, 54, 96, 186, 43, 68, 32, 241, 70, 94, 103, 152, 39, 64, 94, 66, 145, 156, 187, 199, 219, 221, 58, 9, 226, 21, 226, 109, 81, 225, 182, 229, 235, 48, 221, 228, 48, 240, 220, 201, 218, 156, 98, 117, 7, 81, 109, 63, 18, 4, 132, 46, 140, 140, 149, 77, 7, 34, 210, 75, 178, 48, 180, 201, 223, 239, 249, 98, 102, 255, 1, 133, 83, 167, 44, 215, 15, 207, 148, 233, 77, 57, 97, 106, 185, 114, 2, 198, 224, 231, 50, 185, 30, 253, 3, 75, 110, 20, 51, 156, 31, 112, 233, 155, 176, 240, 47, 71, 111, 108, 8, 175, 211, 55, 84, 238, 176, 134, 30, 129, 21, 148, 122, 236, 1, 72, 241, 85, 46, 120, 10, 131, 70, 235, 12, 35, 229, 85, 52, 156, 245, 175, 50, 120, 216, 41, 70, 19, 100, 172, 249, 88, 159, 113, 125, 229, 87, 229, 76, 88, 238, 112, 22, 236, 246, 3, 197, 16, 11, 217, 24, 17, 147, 29, 182, 14, 86, 172, 24, 114, 13, 135, 53, 150, 162, 132, 135, 52, 117, 216, 218, 173, 74, 251, 212, 224, 114, 20, 239, 178, 26, 63, 240, 26, 186, 118, 199, 200, 238, 211, 200, 33, 11, 35, 84, 41, 215, 56, 161, 144, 220, 191, 9, 145, 21, 61, 71, 144, 75, 151, 89, 7, 238, 139, 252, 110, 42, 149, 26, 12, 22, 30, 180, 171, 236, 121, 17, 93, 19, 8, 33, 149, 12, 186, 228, 181, 253, 112, 250, 31, 127, 20, 243, 33, 24, 52, 162, 39, 153, 155, 175, 82, 177, 114, 11, 218, 28, 216, 131, 94, 205, 217, 187, 191, 157, 208, 1, 176, 112, 65, 29, 40, 202, 37, 134, 227, 116, 34, 76, 252, 243, 200, 184, 141, 241, 191, 60, 17, 73, 251, 52, 48, 69, 203, 253, 243, 146, 146, 68, 36, 200, 40, 220, 180, 38, 220, 250, 171, 168, 115, 54, 42, 146, 10, 68, 143, 44, 218, 87, 168, 62, 102, 94, 3, 169, 255, 71, 1, 55, 159, 214, 138, 129, 154, 65, 146, 39, 54, 69, 192, 241, 248, 130, 52, 155, 49, 132, 244, 188, 240, 219, 158, 126, 241, 135, 186, 153, 219, 249, 17, 166, 231, 52, 158, 38, 40, 87, 64, 0, 178, 133, 199, 229, 118, 184, 90, 109, 215, 68, 233, 57, 22, 174, 246, 244, 130, 180, 43, 142, 75, 52, 153, 80, 86, 33, 44, 63, 233, 100, 14, 119, 144, 57, 226, 34, 160, 79, 243, 86, 23];
  const SECRET_KEY: string = "secretKey";
  const SESSION_KEY: string = "8301ebc9bce6ce8ded910e724cc7e9af414249ea034ab8a4d2dbdce443e3a5d5";
  const SYSTEM_BOX_ADDRESS: string = "system box address";

  let events: Events;

  let sessionService: SessionService;

  let openPgp: OpenPgp;
  let session: Session;
  let sessionStorageService: SessionStorageService;

  beforeEach(() => {
    events = IonicJasmine.events();
    sessionService = new SessionService(events, null);

    sessionStorageService = CoreJasmine.sessionStorageService();
    sessionService['_sessionStorageService'] = sessionStorageService;

    openPgp = CryptoJasmine.openPgp();
    sessionService['_openPgp'] = openPgp;

    session = aSession.with().boxAddress(SYSTEM_BOX_ADDRESS).secretKey(SECRET_KEY).and.contacts(CONTACTS).noBlanks();
    sessionService['_session'] = session;
  });

  it("should add a contact to the session and storage", (done: DoneFn) => {

    const session: Session = aSession.with().contacts(Contacts.fromArrayOfContacts([aContact('bob'), aContact('carol')])).noBlanks();
    sessionService['_session'] = session;

    const contact: Contact = aContact('alice');

    (<jasmine.Spy>sessionStorageService.storeSession).and.returnValue(of(true));

    sessionService.addContact(contact)
      .subscribe((result: boolean) => {

        expect(sessionStorageService.storeSession).toHaveBeenCalled();

        expect(result).toEqual(true);
        const storedContactPseudos: string[] = session.contactsAsArray().map((contact: Contact) => contact.pseudo());
        expect(storedContactPseudos.length).toEqual(3);
        expect(storedContactPseudos).toContain('alice');

        done();
      });
  });

  it("should clear a session in-memory and on storage", (done: DoneFn) => {

    (<jasmine.Spy>sessionStorageService.clearSession).and.returnValue(of(true));

    sessionService.clearAndRemoveSession()
      .subscribe((result: boolean) => {

        expect(sessionStorageService.clearSession).toHaveBeenCalled();

        expect(result).toEqual(true);
        expect(sessionService.session()).toBeUndefined();

        done();
      });
  });

  describe(", when requesting the HMAC hash for a box address,", () => {

    it("should generate the hash for the user's system box address", (done: DoneFn) => {
      sessionService.hmacHashForBoxAddress(SYSTEM_BOX_ADDRESS).subscribe((hmacHash: string) => {
        expect(hmacHash).toEqual("REg5fp2vpWzdDhG9A+ShZrkD6MFi1CTGMzXjrK51MOY4MBmOwRr0dvRl67oFH4R4n9YN67SxbmBgdpA1P3396A==");
        done();
      });
    });

    it("should generate the hash for the contact's \"read from\" box address", (done: DoneFn) => {
      session = SessionsJasmine.session();
      sessionService['_session'] = session;

      (<jasmine.Spy>session.systemBoxAddress).and.returnValue(SYSTEM_BOX_ADDRESS);
      (<jasmine.Spy>session.hmacHashForBoxAddress).and.returnValue(of("hmac hash"));

      const contactBoxAddress: string = "contact box address";
      sessionService.hmacHashForBoxAddress(contactBoxAddress).subscribe((hmacHash: string) => {
        expect(session.hmacHashForBoxAddress).toHaveBeenCalledWith(contactBoxAddress)
        expect(hmacHash).toEqual("hmac hash");
        done();
      });
    });
  });

  it("should initialize a new session", (done: DoneFn) => {

    const loginResponse: LoginResponse = {auth: AUTH, m2: M2, payload: PAYLOAD, profile: PROFILE};

    const userPayload: UserPayload = UserPayload
      .with(Contacts.empty(), DeletedContacts.empty(), '', Keys.with({}), '', SECRET_KEY);

    (<jasmine.Spy>sessionStorageService.clearSession).and.returnValue(of(true));
    (<jasmine.Spy>openPgp.decrypt).and.returnValue(of(userPayload.toJson()));
    (<jasmine.Spy>sessionStorageService.unlock).and.returnValue(of(sessionStorageService));
    (<jasmine.Spy>sessionStorageService.storeSession).and.returnValue(of(true));

    sessionService.initializeNewSession('alice', 'alicePassphrase', loginResponse, SESSION_KEY)
      .subscribe((result: boolean) => {

        expect(events.publish).toHaveBeenCalledWith(SessionEvent.INITIALIZED);

        expect(result).toEqual(true);

        done();
      });
  });

  it("should check if session is initialized", () => {

    sessionService['_openPgp'] = openPgp;
    sessionService['_session'] = session;
    expect(sessionService.isSessionInitialized()).toEqual(true);

    sessionService['_openPgp'] = null;
    sessionService['_session'] = session;
    expect(sessionService.isSessionInitialized()).toEqual(false);

    sessionService['_openPgp'] = openPgp;
    sessionService['_session'] = null;
    expect(sessionService.isSessionInitialized()).toEqual(false);
  });

  describe(", when refreshing the session contacts,", () => {

    it("should update and store the session", (done: DoneFn) => {

      sessionService['_session'] = aSession.with()
        .contacts(Contacts.fromArrayOfContacts([aContact('bob', 'Bob'), aContact('carol', 'Carol')])).noBlanks();

      (<jasmine.Spy>sessionStorageService.storeSession).and.returnValue(of(true));

      const newContacts: Contacts = Contacts
        .fromArrayOfContacts([aContact('bob', 'Bobby'), aContact('carol', 'Caroline')]);
      sessionService.refreshSessionContacts(newContacts).subscribe(() => {

        const refreshedSession = sessionService['_session'];
        expect(sessionStorageService.storeSession).toHaveBeenCalledWith(refreshedSession);

        expect(refreshedSession.contacts().areEqualTo(newContacts)).toBe(true);

        done();
      });
    });

    it("should prevent updating the session when no contacts have actually been changed", (done: DoneFn) => {

      const contacts: Contacts = Contacts.fromArrayOfContacts([aContact('bob', 'Bob'), aContact('carol', 'Carol')]);
      sessionService['_session'] = aSession.with().contacts(contacts).noBlanks();

      const newContacts: Contacts = Contacts.fromArrayOfContacts([aContact('bob', 'Bob'), aContact('carol', 'Carol')]);
      sessionService.refreshSessionContacts(newContacts).subscribe(() => {

        expect(sessionStorageService.storeSession).not.toHaveBeenCalled();
        expect(sessionService['_session'].contacts().areEqualTo(contacts)).toBe(true);
        done();
      });
    });
  });

  it("should remove a contact from the session and storage", (done: DoneFn) => {

    const session = aSession.with().contacts(Contacts.fromArrayOfContacts([aContact('bob'), aContact('carol')])).noBlanks();
    sessionService['_session'] = session;

    (<jasmine.Spy>sessionStorageService.storeSession).and.returnValue(of(true));

    const contact: Contact = aContact('carol');
    sessionService.deleteContact(contact).subscribe(() => {

      expect(sessionStorageService.storeSession).toHaveBeenCalled();

      const storedContactPseudos: string[] = session.contactsAsArray().map((contact: Contact) => contact.pseudo());
      expect(storedContactPseudos.length).toEqual(1);
      expect(storedContactPseudos).not.toContain('carol');

      done();
    });
  });

  describe(", when restoring a stored session,", () => {

    it("should return true if the session exists", (done: DoneFn) => {

      (<jasmine.Spy>sessionStorageService.isSessionStored).and.returnValue(of(true));
      (<jasmine.Spy>sessionStorageService.unlock).and.returnValue(of(sessionStorageService));
      (<jasmine.Spy>sessionStorageService.retrieveSession).and.returnValue(of(session));

      sessionService.restoreSession()
        .subscribe((result: boolean) => {
          expect(events.publish).toHaveBeenCalledWith(SessionEvent.INITIALIZED);
          expect(result).toEqual(true);
          done();
        });
    });

    it("should return false if no session is stored", (done: DoneFn) => {

      (<jasmine.Spy>sessionStorageService.isSessionStored).and.returnValue(of(false));

      sessionService.restoreSession()
        .subscribe((result: boolean) => {
          expect(events.publish).not.toHaveBeenCalled();
          expect(result).toEqual(false);
          done();
        });
    });
  });

  it("should update a contact and store the update in the session", () => {
    sessionService['_session'] = aSession.with().contacts(Contacts.fromArrayOfContacts([aContact('bob', 'oldReadFrom')])).noBlanks();
    expect(sessionService['_session'].contact('bob').readFrom()).toEqual('oldReadFrom');

    sessionService.updateContact(aContact('bob', 'newReadFrom'));

    expect(sessionService['_session'].contact('bob').readFrom()).toEqual('newReadFrom');
  });

  function aContact(pseudo: string, readFrom: string = null): any {
    return Contact.with(pseudo, null, readFrom, null, ContactKey.with(null, null, null), null);
  }
});
