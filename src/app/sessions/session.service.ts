import {Injectable} from "@angular/core";
import {Events} from "ionic-angular";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {map, mergeMap, tap} from "rxjs/operators";
import {config} from "../app.config";
import {AES, BlockCipherMode} from "../crypto/aes/aes";
import {KDF} from "../crypto/kdf/kdf";
import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {LoginProfile} from "../login/login-profile";
import {LoginResponse} from "../login/login-response";
import {Contact} from "../user/contact/contact";
import {Contacts} from "../user/contact/contacts";
import {DeletedContact} from "../user/contact/deleted-contact";
import {Keys} from "../user/key/keys";
import {UserPayload} from "../user/user-payload";
import {Logger} from "../utils/logging/logger";
import {HmacHashesByBoxAddress} from "./hmac-hashes-by-box-address";
import {Session} from "./session";
import {SessionEvent} from "./session-event";
import {SessionStorageProvider} from "./storage/session-storage.provider";
import {SessionStorageService} from "./storage/session-storage.service";

// TODO Remove all "shortcuts" to session methods. This is a service. It's okay to pass a Session object and ask methods to rely on that for their calls!
@Injectable()
export class SessionService {

  private readonly _kdf: KDF;
  private readonly logger: Logger = Logger.for("SessionService");

  private _exchangeAes: AES;
  private _openPgp: OpenPgp;
  private _session: Session;
  private _sessionStorageService: SessionStorageService;

  constructor(private readonly events: Events,
              private readonly sessionStorageProvider: SessionStorageProvider) {
    this._kdf = new KDF({
      N: config.kdf.N,
      r: config.kdf.r,
      p: config.kdf.p,
      dkLen: config.kdf.dkLen,
      encoding: config.kdf.encoding
    });
  }

  /**
   * Adds the specified contact to the list of contacts in session and in storage.<br>
   * If the contact already exists in the list, then it is updated.
   */
  addContact(contact: Contact): Observable<any> {
    this._session.addContact(contact);
    return this.storeSession();
  }

  /**
   * Clears session's <b>in-memory and persisted</b> artifacts to be able to have a clean session.
   */
  clearAndRemoveSession(): Observable<boolean> {

    this.logger.trace("clearAndRemoveSession()");

    this._session = undefined;

    return this.initSessionStorageService()
      .pipe(
        mergeMap(() => {
          return this._sessionStorageService.clearSession();
        })
      );
  }

  contact(pseudo: string): Contact {
    return this._session.contact(pseudo);
  }

  /**
   * @param pseudos If an array of pseudos is specified, only those contacts are returned.
   *                Pseudos not matching a session contact are ignored.
   */
  contactsAsArray(pseudos: string[] = null): Contact[] {
    return this._session.contactsAsArray(pseudos);
  }

  contactWithReadFromBoxAddress(readFromBoxAddress: string): Contact {
    return this._session.contactWithReadFromBoxAddress(readFromBoxAddress);
  }

  /**
   * Removes the specified contact from the list of contacts in session and persists the modified session.
   */
  deleteContact(contact: Contact): Observable<any> {
    this._session.deleteContact(contact);
    return this.storeSession();
  }

  deletedContactsAsArray(pseudos: string[] = null): DeletedContact[] {
    return this._session.deletedContactsAsArray(pseudos);
  }

  displayName(): string {
    return this._session.displayName();
  }

  exchangeAes(): Observable<AES> {
    return this._exchangeAes
      ? of(this._exchangeAes)
      : AES.fromJwk(this._session.exchangeKey())
        .pipe(
          tap((aes: AES) => this._exchangeAes = aes),
          map(() => this._exchangeAes)
        );
  }

  /**
   * @deprecated User sessionService.session().hmacHashesByDeletedReadFromBoxAddress()
   */
  hmacHashesByDeletedReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {
    return this._session.hmacHashesByDeletedReadFromBoxAddresses(pseudos);
  }

  /**
   * @param pseudos If an array of pseudos is specified, only the hmac hashes for those pseudos are returned.
   *                Pseudos not matching a session contact are ignored.
   * @deprecated Use sessionService.session().hmacHashesByReadFromBoxAddresses()
   */
  hmacHashesByReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {
    return this._session.hmacHashesByReadFromBoxAddresses(pseudos);
  }

  hmacHashForBoxAddress(boxAddress: string): Observable<string> {
    if (boxAddress === this._session.systemBoxAddress()) {
      return this._session.hmacHashOfSystemBoxAddress();
    }
    return this._session.hmacHashForBoxAddress(boxAddress);
  }

  /**
   * @deprecated User sessionService.session().hmacHashOfSystemBoxAddress()
   */
  hmacHashOfSystemBoxAddress(): Observable<string> {
    return this._session.hmacHashOfSystemBoxAddress();
  }

  /**
   * Initializes a new session. Typically used after logging in against the server.
   */
  initializeNewSession(pseudo: string, passphrase: string, loginResponse: LoginResponse, sessionKey: string):
    Observable<boolean> {

    let derivedKdfKey: string;
    let loginProfile: LoginProfile;
    let userPayload: UserPayload;

    return this.clearAndRemoveSession()
      .pipe(
        mergeMap(() => {
          return AES.fromHexKey(sessionKey, BlockCipherMode.GCM);
        }),

        mergeMap((sessionAes: AES) => {
          return sessionAes.decrypt(new Uint8Array(loginResponse.profile));
        }),

        mergeMap((loginProfileJson: string) => {
          loginProfile = LoginProfile.fromJson(loginProfileJson);

          return this._kdf.deriveKey(passphrase, loginProfile.credentials.kdf.salt);
        }),

        mergeMap((derivedKey: string) => {
          derivedKdfKey = derivedKey;

          return this.initOpenPgp(
            {
              publicKeys: loginProfile.credentials.systemKeys.publicKeys,
              privateKeys: loginProfile.credentials.systemKeys.privateKeys
            },
            derivedKdfKey
          );
        }),

        mergeMap((openPgp: OpenPgp) => {
          this._openPgp = openPgp;

          return this._openPgp.decrypt(loginResponse.payload, loginProfile.credentials.systemKeys.publicKeys);
        }),

        mergeMap((userPayloadJson: string) => {
          userPayload = UserPayload.fromJson(userPayloadJson);

          this._session = Session.build(
            loginResponse.auth,
            loginProfile.boxAddress,
            userPayload.contacts(),
            userPayload.deletedContacts(),
            derivedKdfKey,
            userPayload.displayName() ? userPayload.displayName() : loginProfile.displayName,
            AES.jsonWebKeyOf(loginProfile.exchangeKey, BlockCipherMode.GCM),
            userPayload.keys(),
            userPayload.picture() ? userPayload.picture() : loginProfile.picture,
            pseudo,
            userPayload.secretKey());

          return this.initSessionStorageService()
            .pipe(
              mergeMap(() => {
                const deviceStorageKey: JsonWebKey = AES.jsonWebKeyOf(loginProfile.deviceStorageKey, BlockCipherMode.GCM);
                return this._sessionStorageService.unlock(deviceStorageKey);
              }),

              mergeMap((sessionStorageService: SessionStorageService) => {
                return sessionStorageService.storeSession(this._session);
              })
            );
        }),

        map(() => {

          derivedKdfKey = undefined;
          loginProfile = undefined;
          loginResponse = undefined;
          passphrase = undefined;
          sessionKey = undefined;
          userPayload = undefined;

          this.events.publish(SessionEvent.INITIALIZED);

          return true;
        })
      );
  }

  isSessionInitialized(): boolean {
    return !!this._kdf && !!this._openPgp && !!this._session;
  }

  openPgp(): OpenPgp {
    return this._openPgp;
  }

  picture(): string {
    return this._session.picture();
  }

  pseudo(): string {
    return this._session.pseudo();
  }

  publicKeysForKeyId(keyId: string) {
    return this._session.armoredKeyPair(keyId).publicKeys;
  }

  /**
   * Refreshes the session's contacts.<br>
   * If any change is detected in the contacts, the session will also be persisted.
   */
  refreshSessionContacts(contacts: Contacts): Observable<any> {
    if (contacts.areEqualTo(this._session.contacts())) {
      return of({});
    }

    this._session = Session.build(
      this._session.auth(),
      this._session.systemBoxAddress(),
      contacts,
      this._session.deletedContacts(),
      this._session.derivedKdfKey(),
      this._session.displayName(),
      this._session.exchangeKey(),
      this._session.keys(),
      this._session.picture(),
      this._session.pseudo(),
      this._session.secretKey());
    return this.storeSession();
  }

  /**
   * Restores and resumes the persisted session.
   * Returns false if no session was persisted or if the persisted session could not be restored.
   */
  restoreSession(): Observable<boolean> {

    return this.initSessionStorageService()
      .pipe(
        mergeMap(() => {
          return this._sessionStorageService.isSessionStored()
        }),

        mergeMap((isStored: boolean) => {

          if (isStored) {

            return this._sessionStorageService.unlock()
              .pipe(
                mergeMap(() => {
                  return this._sessionStorageService.retrieveSession();
                }),

                mergeMap((session: Session) => {
                  this._session = session;

                  const keyPair: ArmoredKeyPair = this._session.armoredKeyPair(Keys.systemKeysId());

                  return this.initOpenPgp(keyPair, this._session.derivedKdfKey());
                }),

                tap((openPgp: OpenPgp) => {
                  this._openPgp = openPgp;
                }),

                map(() => {

                  this.events.publish(SessionEvent.INITIALIZED);

                  return true;
                })
              );

          } else {
            return of(false);
          }
        })
      );
  }

  session(): Session {
    return this._session;
  }

  sessionStorageService(): SessionStorageService {
    return this._sessionStorageService;
  }

  /**
   * @deprecated Use sessionService.sessionStorageService.storageAes()
   */
  storageAes(): Observable<AES> {
    return this._sessionStorageService.storageAes();
  }

  /**
   * @deprecated Use sessionService.session().boxAddress()
   */
  systemBoxAddress(): string {
    return this._session.systemBoxAddress();
  }

  systemPublicKeys(): string {
    return this._session.systemPublicKeys();
  }

  /**
   * Updates the specified contact in the session and persists the modified session.
   */
  updateContact(contact: Contact): Observable<any> {
    this._session.updateContact(contact);
    return this.storeSession();
  }

  updateSession(session: Session): Observable<any> {
    this._session = session;
    return this.storeSession()
  }

  /* PRIVATE */

  private initOpenPgp(armoredKeyPair: ArmoredKeyPair, passphrase: string): Observable<OpenPgp> {
    return this._openPgp
      ? of(this._openPgp)
      : OpenPgp.fromArmoredKeyPair(armoredKeyPair, passphrase)
        .pipe(
          tap((openPgp: OpenPgp) => this._openPgp = openPgp),
          map(() => this._openPgp)
        );
  }

  private initSessionStorageService(): Observable<SessionStorageService> {
    return this._sessionStorageService
      ? of(this._sessionStorageService)
      : this.sessionStorageProvider.obtain()
        .pipe(
          tap((sessionStorageService: SessionStorageService) => this._sessionStorageService = sessionStorageService),
          map(() => this._sessionStorageService)
        );
  }

  private storeSession(): Observable<any> {
    return this.initSessionStorageService()
      .pipe(
        mergeMap(() => this._sessionStorageService.storeSession(this._session))
      );
  }
}
