import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {Contact} from "../user/contact/contact";
import {ContactTestBuilder as aContact} from "../user/contact/contact.test-builder";
import {Contacts} from "../user/contact/contacts";
import {DeletedContact} from "../user/contact/deleted-contact";
import {DeletedContacts} from "../user/contact/deleted-contacts";
import {KeyPair} from "../user/key/key-pair";
import {Keys} from "../user/key/keys";
import {Session} from "./session";
import {SessionTestBuilder, SessionTestBuilder as aSession} from "./session.test-builder";

describe("Session", () => {

  const AUTH: string = 'auth';
  const BOX_ADDRESS: string = 'boxAddress';
  const CONNECTED_SINCE: number = 1615889725321;
  const CONTACTS: Contacts = Contacts.fromArrayOfContacts([
    aContact.with().pseudo('bob').and.connectedSince(CONNECTED_SINCE).noBlanks(),
    aContact.with().pseudo('carol').and.connectedSince(CONNECTED_SINCE).noBlanks()
  ]);
  const DELETED_CONTACTS = DeletedContacts.fromObject({
    'david': {
      readFrom: 'davidReadFrom',
      secretKey: 'davidSecretKey'
    }
  });
  const DERIVED_KDF_KEY: string = 'derivedKdfKey';
  const DISPLAY_NAME: string = 'displayName';
  const EXCHANGE_KEY: JsonWebKey = {
    kty: 'oct',
    k: 'VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I',
    alg: 'A256GCM'
  };
  const KEYS: Keys = Keys.fromObject({keyId: {type: 'ecc', publicKeys: 'publicKeys', privateKeys: 'privateKeys'}});
  const PICTURE: any = {};
  const PSEUDO: string = 'pseudo';
  const SECRET_KEY: string = 'secretKey';

  let session: Session;

  describe(", at construction,", () => {

    it("should build a new instance", () => {
      session = aSession.with().noBlanks();
      expect(session).toBeDefined();
      Object.keys(session).forEach(field => expect(session[field]).toBeDefined());
    });

    it("should build a new instance from a JSON string", () => {
      session = Session.fromJson('{' +
        '"auth":"auth",' +
        '"boxAddress":"boxAddress",' +
        '"contacts":{' +
        '"bob":{"pseudo":"bob","displayName":"display name","readFrom":"read from","writeTo":"write to","writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":"secret key","connectedSince":1615889725321},' +
        '"carol":{"pseudo":"carol","displayName":"display name","readFrom":"read from","writeTo":"write to","writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":"secret key","connectedSince":1615889725321}},' +
        '"deletedContacts":{"david":{"readFrom":"davidReadFrom","secretKey":"davidSecretKey"}},' +
        '"derivedKdfKey":"derivedKdfKey",' +
        '"displayName":"displayName",' +
        '"exchangeKey":{"kty":"oct","k":"VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I","alg":"A256GCM"},' +
        '"keys":{"keyId":{"type":"ecc","publicKeys":"publicKeys","privateKeys":"privateKeys"}},' +
        '"picture":{},' +
        '"pseudo":"pseudo"}');
      expect(session.auth()).toEqual(AUTH);
      expect(session.systemBoxAddress()).toEqual(BOX_ADDRESS);
      expect(session.contacts().areEqualTo(CONTACTS)).toBe(true);
      expect(session.deletedContacts()).toEqual(DELETED_CONTACTS);
      expect(session.derivedKdfKey()).toEqual(DERIVED_KDF_KEY);
      expect(session.displayName()).toEqual(DISPLAY_NAME);
      expect(session.exchangeKey()).toEqual(EXCHANGE_KEY);
      expect(session.keys().toJson()).toEqual(KEYS.toJson());
      expect(session.picture()).toEqual(PICTURE);
      expect(session.pseudo()).toEqual(PSEUDO);
    });

    it("should build a new instance from an object", () => {
      session = Session.fromObject({
        auth: AUTH,
        boxAddress: BOX_ADDRESS,
        contacts: CONTACTS.toObject(),
        deletedContacts: DELETED_CONTACTS.toObject(),
        derivedKdfKey: DERIVED_KDF_KEY,
        displayName: DISPLAY_NAME,
        exchangeKey: EXCHANGE_KEY,
        keys: KEYS.toObject(),
        picture: PICTURE,
        pseudo: PSEUDO
      });
      expect(session.auth()).toEqual(AUTH);
      expect(session.systemBoxAddress()).toEqual(BOX_ADDRESS);
      expect(session.contacts().areEqualTo(CONTACTS)).toBe(true);
      expect(session.deletedContacts()).toEqual(DELETED_CONTACTS);
      expect(session.derivedKdfKey()).toEqual(DERIVED_KDF_KEY);
      expect(session.displayName()).toEqual(DISPLAY_NAME);
      expect(session.exchangeKey()).toEqual(EXCHANGE_KEY);
      expect(session.keys().toJson()).toEqual(KEYS.toJson());
      expect(session.picture()).toEqual(PICTURE);
      expect(session.pseudo()).toEqual(PSEUDO);
    });
  });

  it("should add a contact to this session's contact", () => {

    const contacts: Contacts = Contacts.fromArrayOfContacts([
      aContact.with().pseudo('bob').noBlanks(),
      aContact.with().pseudo('carol').noBlanks()
    ]);
    session = aSession.with().contacts(contacts).noBlanks();
    expect(Object.keys(session['_contacts'].contactsAsMapOfContactsByPseudo()).length).toEqual(2);

    const newContact: Contact = aContact.with().pseudo("danny").noBlanks();
    session.addContact(newContact);
    expect(Object.keys(session['_contacts'].contactsAsMapOfContactsByPseudo()).length).toEqual(3);
    expect(session['_contacts'].contact('danny')).toEqual(newContact);
  });

  it("should return an identified pair of keys as an ArmoredKeyPair", () => {
    const session = aSession.with().keys(KEYS).noBlanks();

    const keyPair: ArmoredKeyPair = session.armoredKeyPair("keyId");

    expect(keyPair).toBeDefined();
    expect(keyPair.publicKeys).toEqual(KEYS.keyPairWithKeyId("keyId").publicKeys());
    expect(keyPair.privateKeys).toEqual(KEYS.keyPairWithKeyId("keyId").privateKeys());
  });

  describe(", when getting contacts", () => {

    beforeEach(() => {
      session = aSession.with().noBlanks();
    });

    it("should return the specified contact", () => {
      session = aSession.with().contacts(CONTACTS).noBlanks();
      const contact: Contact = session.contact('carol');
      expect(contact).toBeDefined();
      expect(contact.pseudo()).toEqual('carol');
    });

    it("should return all of the logged user's contacts", () => {

      session = aSession.with().contacts(CONTACTS).noBlanks();

      const contacts: Contact[] = session.contactsAsArray();

      expect(contacts.length).toEqual(2);
      const contactsPseudos = contacts.map((contact: Contact) => contact.pseudo());
      expect(contactsPseudos).toContain('bob');
      expect(contactsPseudos).toContain('carol');
    });

    it("should return the contacts specified by the passed usernames", () => {
      session = aSession.with().contacts(CONTACTS).noBlanks();
      const contacts: Contact[] = session.contactsAsArray(['bob']);
      expect(contacts.length).toEqual(1);
      expect(contacts[0].pseudo()).toEqual('bob');
    });

    it("should ignore unknown contacts in the array of passed usernames", () => {

      session = aSession.with().contacts(CONTACTS).noBlanks();

      const contacts: Contact[] = session.contactsAsArray(['bob', 'unknown']);

      expect(contacts.length).toEqual(1);
      const contactsPseudos = contacts.map((contact: Contact) => contact.pseudo());
      expect(contactsPseudos).toContain('bob');
      expect(contactsPseudos).not.toContain('unknown');
    });
  });

  it("should delete a contact from this session's contacts", () => {
    const carol: Contact = aContact.with().pseudo('carol').noBlanks();
    const bob: Contact = aContact.with().pseudo('bob').noBlanks();
    session = aSession.with()
      .contacts(Contacts.fromArrayOfContacts([bob, carol])).deletedContacts(DeletedContacts.empty()).noBlanks();
    expect(session.contacts().contactsSortedByDescription().length).toEqual(2);
    expect(session.deletedContacts().deletedContacts().length).toEqual(0);

    session.deleteContact(carol);

    expect(session.contacts().contactsSortedByDescription().length).toEqual(1);
    expect(session.contacts().contact('carol')).toBeUndefined();
    expect(session.deletedContacts().deletedContacts().length).toEqual(1);
  });

  describe(", when fetching deleted contacts,", () => {

    it("should return all deleted contacts", () => {
      session = aSession.with().deletedContacts(DeletedContacts.fromObject({
        'david.1': {readFrom: 'davidReadFrom', secretKey: 'davidSecretKey'},
        'emmet.1': {readFrom: 'emmetReadFrom', secretKey: 'emmetSecretKey'},
      })).noBlanks();
      const deletedContacts: DeletedContact[] = session.deletedContactsAsArray();
      expect(deletedContacts.length).toEqual(2);
    });

    it("should return the deleted contacts from the specified pseudos", () => {
      session = aSession.with().deletedContacts(DeletedContacts.fromObject({
        'david.1': {readFrom: 'davidReadFrom', secretKey: 'davidSecretKey'},
        'emmet.1': {readFrom: 'emmetReadFrom', secretKey: 'emmetSecretKey'},
      })).noBlanks();
      const deletedContacts: DeletedContact[] = session.deletedContactsAsArray(["emmet"]);
      expect(deletedContacts.length).toEqual(1);
    });
  });

  it("should indicate whether the logged user has a contact with a given pseudo or not", () => {
    session = aSession.with().contacts(CONTACTS).noBlanks();
    expect(session.hasContactWithPseudo("bob")).toBe(true);
    expect(session.hasContactWithPseudo("nobodyscontact")).toBe(false);
  });

  describe(", when generating the HMAC hash for a given box address,", () => {

    it("should return a HMAC hash for a contact matching the specified  \"read from\" box address", (done: DoneFn) => {

      const session: Session = aSession.with()
        .contacts(Contacts.fromArrayOfContacts([
          aContact.with().pseudo("david").readFrom("davidReadFrom").and.secretKey("davidSecretKey").noBlanks(),
          aContact.with().pseudo("emmet").readFrom("emmetReadFrom").and.secretKey("emmetSecretKey").noBlanks()
        ]))
        .asIs();

      session.hmacHashForBoxAddress("davidReadFrom").subscribe((hmacHash: string) => {
        expect(hmacHash).toEqual("dwRvuja3OY+TaQ3UGNJbEGffpBWOjnTbgQVN6BcO2hduloWwdSPY+wQLsej1/2B4RoGMSY5EH+1jg5X9v9h1tA==");
        done();
      });
    });

    it("should return the hash for a deleted contact matching the specified  \"read from\" box address", (done: DoneFn) => {

      const session: Session = aSession.with()
        .contacts(Contacts.fromArrayOfContacts([
          aContact.with().pseudo("emmet").readFrom("emmetReadFrom").and.secretKey("emmetSecretKey").noBlanks()
        ]))
        .deletedContacts(DeletedContacts.fromObject({
          "david.1": {readFrom: "davidReadFrom", secretKey: "davidSecretKey"}
        }))
        .asIs()

      session.hmacHashForBoxAddress("davidReadFrom").subscribe((hmacHash: string) => {
        expect(hmacHash).toEqual("dwRvuja3OY+TaQ3UGNJbEGffpBWOjnTbgQVN6BcO2hduloWwdSPY+wQLsej1/2B4RoGMSY5EH+1jg5X9v9h1tA==");
        done();
      });
    });
  });

  describe(", when generating a HMAC hash from the logged user's system box address,", () => {

    it("should return the HMAC hash of the system box address", (done: DoneFn) => {

      const session: Session = aSession.with().boxAddress(BOX_ADDRESS).secretKey(SECRET_KEY).noBlanks();

      session.hmacHashOfSystemBoxAddress().subscribe((hash: string) => {
        expect(hash).toEqual('jI2+vk7JmSs1OrUaEcBfTovEDzdY2D+bg+yhZrRbW84ajYB0AhVVEtQyNMhHn7tHgvlYQ+n9tqD2h3OA6K7ytg==');
        done();
      });
    });

    it("should return null when session has no secret key", (done: DoneFn) => {

      const session: Session = aSession.with().boxAddress(BOX_ADDRESS).asIs();

      session.hmacHashOfSystemBoxAddress().subscribe((hash: string) => {
        expect(hash).toBeNull();
        done();
      });
    });
  });

  describe(", when describing the logged user,", () => {

    beforeEach(() => {
      session = aSession.with().noBlanks();
    });

    it("should return the display name is present,", () => {
      const description: string = session.loggedUserDescription();
      expect(description).toEqual(DISPLAY_NAME);
    });

    it("should return the pseudo if the display name is absent,", () => {
      session = aSession.with().pseudo(PSEUDO).and.displayName(null).asIs();
      const description: string = session.loggedUserDescription();
      expect(description).toEqual(PSEUDO);
    })
  });

  it("should return all the box addresses to be used when fetching received messages", () => {
    const contacts: Contacts = Contacts
      .fromArrayOfContacts([
        aContact.with().pseudo('bob').and.readFrom('readFromBob').noBlanks(),
        aContact.with().pseudo('carol').and.readFrom('readFromCarol').noBlanks()]);
    session = aSession.with().contacts(contacts).noBlanks();

    const boxAddresses: string[] = session.readFromBoxAddresses();

    expect(boxAddresses.length).toEqual(3);
    expect(boxAddresses).toContain(BOX_ADDRESS);
    expect(boxAddresses).toContain('readFromBob');
    expect(boxAddresses).toContain('readFromCarol');
  });

  it("should retrieve the system public keys", () => {
    const keyPairsByKeyId: any = {};
    keyPairsByKeyId[`${Keys.SYSTEM_IDENTIFIER}.${Keys.SYSTEM_KEYS_TYPE}`] = KeyPair
      .with(Keys.SYSTEM_KEYS_TYPE, 'systemPublicKeys', 'systemPrivateKeys');
    const keys: Keys = Keys.with(keyPairsByKeyId);
    session = aSession.with().keys(keys).noBlanks();


    const systemPublicKeys: string = session.systemPublicKeys();

    expect(systemPublicKeys).toEqual("systemPublicKeys");
  });

  it("should represent the session as a JSON string", () => {
    session = aSession.with().auth(AUTH).boxAddress(BOX_ADDRESS).contacts(CONTACTS).deletedContacts(DELETED_CONTACTS)
      .derivedKdfKey(DERIVED_KDF_KEY).displayName(DISPLAY_NAME).exchangeKey(EXCHANGE_KEY).keys(KEYS).picture(PICTURE)
      .pseudo(PSEUDO).and.secretKey(SessionTestBuilder.SECRET_KEY).asIs();
    expect(session.toJson()).toEqual('{' +
      '"auth":"auth",' +
      '"boxAddress":"boxAddress",' +
      '"contacts":{' +
      '"bob":{"pseudo":"bob","displayName":"display name","readFrom":"read from","writeTo":"write to","writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":"secret key","connectedSince":1615889725321},' +
      '"carol":{"pseudo":"carol","displayName":"display name","readFrom":"read from","writeTo":"write to","writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":"secret key","connectedSince":1615889725321}},' +
      '"deletedContacts":{"david":{"readFrom":"davidReadFrom","secretKey":"davidSecretKey"}},' +
      '"derivedKdfKey":"derivedKdfKey",' +
      '"displayName":"displayName",' +
      '"exchangeKey":{"kty":"oct","k":"VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I","alg":"A256GCM"},' +
      '"keys":{"keyId":{"type":"ecc","publicKeys":"publicKeys","privateKeys":"privateKeys"}},' +
      '"picture":{},' +
      '"pseudo":"pseudo","secretKey":"secretKey"}');
  });

  it("should update the specified contact", () => {
    const contacts: Contacts = Contacts.fromArrayOfContacts([
      aContact.with().pseudo('bob').readFrom('readFromBob').and.writeTo('writeToBobOld').noBlanks()
    ]);
    session = aSession.with().contacts(contacts).noBlanks();

    const newContact: Contact = aContact.with().pseudo('bob').readFrom('readFromBob').and.writeTo('writeToBobNew').noBlanks();
    session.updateContact(newContact);

    const updatedContact: Contact = session.contact('bob');
    expect(updatedContact.writeTo()).toEqual('writeToBobNew');
  });

  it("should return all the box addresses to be used when posting written messages", () => {
    const contacts: Contacts = Contacts.fromArrayOfContacts([
      aContact.with().pseudo('bob').readFrom('readFromBob').and.writeTo('writeToBob').noBlanks(),
      aContact.with().pseudo('carol').readFrom('readFromCarol').and.writeTo('writeToCarol').noBlanks()
    ]);
    session = aSession.with().contacts(contacts).noBlanks();

    const boxAddresses: string[] = session.writeToBoxAddresses();

    expect(boxAddresses.length).toEqual(2);
    expect(boxAddresses).toContain('writeToBob');
    expect(boxAddresses).toContain('writeToCarol');
  });
});
