import {Contacts} from "../user/contact/contacts";
import {DeletedContacts} from "../user/contact/deleted-contacts";
import {Keys} from "../user/key/keys";
import {Session} from "./session";

export class SessionTestBuilder {

  public static readonly AUTH: string = "auth";
  public static readonly BOX_ADDRESS: string = "boxAddress";
  public static readonly CONTACTS: Contacts = Contacts.empty();
  public static readonly DELETED_CONTACTS = DeletedContacts.empty();
  public static readonly DERIVED_KDF_KEY: string = "derivedKdfKey";
  public static readonly DISPLAY_NAME: string = "displayName";
  public static readonly EXCHANGE_KEY: JsonWebKey = <JsonWebKey>{
    kty: 'oct',
    k: 'VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I',
    alg: 'A256GCM'
  };
  public static readonly KEYS: Keys = Keys.systemKeysWith("publicKeys", "privateKeys");
  public static readonly PICTURE: any = "";
  public static readonly PSEUDO: string = "pseudo";
  public static readonly SECRET_KEY: string = "secretKey";

  static with(): SessionTestBuilder {
    return new SessionTestBuilder();
  }

  and = this;

  private _auth: string;
  private _boxAddress: string;
  private _contacts: Contacts;
  private _deletedContacts: DeletedContacts;
  private _derivedKdfKey: string;
  private _displayName: string;
  private _exchangeKey: JsonWebKey;
  private _keys: Keys;
  private _picture: any;
  private _pseudo: string;
  private _secretKey: string;

  private constructor() {
  }

  asIs(): Session {
    return Session.build(
      this._auth,
      this._boxAddress,
      this._contacts,
      this._deletedContacts,
      this._derivedKdfKey,
      this._displayName,
      this._exchangeKey,
      this._keys,
      this._picture,
      this._pseudo,
      this._secretKey
    );
  }

  noBlanks(): Session {
    return Session.build(
      this._auth ? this._auth : SessionTestBuilder.AUTH,
      this._boxAddress ? this._boxAddress : SessionTestBuilder.BOX_ADDRESS,
      this._contacts ? this._contacts : SessionTestBuilder.CONTACTS,
      this._deletedContacts ? this._deletedContacts : SessionTestBuilder.DELETED_CONTACTS,
      this._derivedKdfKey ? this._derivedKdfKey : SessionTestBuilder.DERIVED_KDF_KEY,
      this._displayName ? this._displayName : SessionTestBuilder.DISPLAY_NAME,
      this._exchangeKey ? this._exchangeKey : SessionTestBuilder.EXCHANGE_KEY,
      this._keys ? this._keys : SessionTestBuilder.KEYS,
      this._picture ? this._picture : SessionTestBuilder.PICTURE,
      this._pseudo ? this._pseudo : SessionTestBuilder.PSEUDO,
      this._secretKey ? this._secretKey : SessionTestBuilder.SECRET_KEY
    );
  }

  auth(auth: string): SessionTestBuilder {
    this._auth = auth;
    return this;
  }

  boxAddress(boxAddress: string): SessionTestBuilder {
    this._boxAddress = boxAddress;
    return this;
  }

  contacts(contacts: Contacts): SessionTestBuilder {
    this._contacts = contacts;
    return this;
  }

  deletedContacts(deletedContacts: DeletedContacts): SessionTestBuilder {
    this._deletedContacts = deletedContacts;
    return this;
  }

  derivedKdfKey(derivedKdfKey: string): SessionTestBuilder {
    this._derivedKdfKey = derivedKdfKey;
    return this;
  }

  displayName(displayName: string): SessionTestBuilder {
    this._displayName = displayName;
    return this;
  }

  exchangeKey(exchangeKey: JsonWebKey): SessionTestBuilder {
    this._exchangeKey = exchangeKey;
    return this;
  }

  keys(keys: Keys): SessionTestBuilder {
    this._keys = keys;
    return this;
  }

  picture(picture: any): SessionTestBuilder {
    this._picture = picture;
    return this;
  }

  pseudo(pseudo: string): SessionTestBuilder {
    this._pseudo = pseudo;
    return this;
  }

  secretKey(secretKey: string): SessionTestBuilder {
    this._secretKey = secretKey;
    return this;
  }
}
