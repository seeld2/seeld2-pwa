import {of} from "rxjs/observable/of";
import {map, mergeMap} from "rxjs/operators";
import {Hmac} from "../crypto/hmac/hmac";
import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {Contact} from "../user/contact/contact";
import {DeletedContact} from "../user/contact/deleted-contact";
import {DeletedContacts} from "../user/contact/deleted-contacts";
import {Keys} from "../user/key/keys";
import {Contacts} from "../user/contact/contacts";
import {Serializable} from "../utils/serialization/serializable";
import {KeyPair} from "../user/key/key-pair";
import {Serializer} from "../utils/serialization/serializer";
import {HmacHashesByBoxAddress} from "./hmac-hashes-by-box-address";
import {Observable} from "rxjs/Observable";

export class Session implements Serializable {

  static build(auth: string,
               boxAddress: string,
               contacts: Contacts,
               deletedContacts: DeletedContacts,
               derivedKdfKey: string,
               displayName: string,
               exchangeKey: JsonWebKey,
               keys: Keys,
               picture: any,
               pseudo: string,
               secretKey: string): Session {
    return new Session(
      auth,
      boxAddress,
      contacts,
      deletedContacts ? deletedContacts : DeletedContacts.empty(),
      derivedKdfKey,
      displayName,
      exchangeKey,
      keys,
      picture,
      pseudo,
      secretKey);
  }

  static fromJson(json: string): Session {
    return Session.fromObject(JSON.parse(json));
  }

  static fromObject(object: any): Session {
    return Session.build(
      object.auth,
      object.boxAddress,
      Contacts.fromObject(object.contacts),
      DeletedContacts.fromObject(object.deletedContacts),
      object.derivedKdfKey,
      object.displayName,
      object.exchangeKey,
      Keys.fromObject(object.keys),
      object.picture,
      object.pseudo,
      object.secretKey);
  }

  private _hmacCryptoKey: CryptoKey;

  private constructor(private readonly _auth: string,
                      private readonly _boxAddress: string,
                      private readonly _contacts: Contacts,
                      private readonly _deletedContacts: DeletedContacts,
                      private readonly _derivedKdfKey: string,
                      private readonly _displayName: string,
                      private readonly _exchangeKey: JsonWebKey,
                      private readonly _keys: Keys,
                      private readonly _picture: any,
                      private readonly _pseudo: string,
                      private readonly _secretKey: string) {
  }

  addContact(contact: Contact) {
    this._contacts.setContact(contact);
  }

  /**
   * Returns this session's private and public keys as an ArmoredKeyPair.
   *
   * @param keyId The ID of the keys to be retrieved.
   */
  armoredKeyPair(keyId: string): ArmoredKeyPair {
    const keyPair: KeyPair = this._keys.keyPairWithKeyId(keyId);

    return keyPair ? {publicKeys: keyPair.publicKeys(), privateKeys: keyPair.privateKeys()} : undefined;
  }

  asObject(): any {
    return {
      auth: this._auth,
      boxAddress: this._boxAddress,
      contacts: this._contacts.toObject(),
      deletedContacts: this._deletedContacts.toObject(),
      derivedKdfKey: this._derivedKdfKey,
      displayName: this._displayName,
      exchangeKey: this._exchangeKey,
      keys: this._keys.toObject(),
      picture: this._picture,
      pseudo: this._pseudo,
      secretKey: this._secretKey
    };
  }

  auth(): string {
    return this._auth;
  }

  contact(pseudo: string): Contact {
    return this._contacts.contact(pseudo);
  }

  contacts(): Contacts {
    return this._contacts;
  }

  /**
   * @param pseudos If an array of pseudos is specified, only those contacts are returned.
   *                Pseudos not matching a session contact are ignored.
   */
  contactsAsArray(pseudos: string[] = null): Contact[] {
    if (pseudos) {
      return pseudos
        .map((pseudo: string) => this._contacts.contact(pseudo))
        .filter((contact: Contact) => !!contact);
    } else {
      return this._contacts.contacts();
    }
  }

  /**
   * @param pseudos If an array of pseudos is specified, only those contacts are returned.
   *                Pseudos not matching a session contact are ignored.
   */
  contactsAsArraySortedByDescription(pseudos: string[] = null): Contact[] {
    if (pseudos) {
      return pseudos
        .map((pseudo: string) => this._contacts.contact(pseudo))
        .filter((contact: Contact) => !!contact);
    } else {
      return this._contacts.contactsSortedByDescription();
    }
  }

  contactWithReadFromBoxAddress(readFromBoxAddress: string): Contact {
    return this._contacts.contactWithReadFromBoxAddress(readFromBoxAddress);
  }

  deleteContact(contact: Contact) {
    this._contacts.delete(contact.pseudo());
    this._deletedContacts.add(contact);
  }

  deletedContacts(): DeletedContacts {
    return this._deletedContacts;
  }

  deletedContactsAsArray(pseudos: string[] = null): DeletedContact[] {
    return this._deletedContacts.deletedContacts(pseudos);
  }

  derivedKdfKey(): string {
    return this._derivedKdfKey;
  }

  displayName(): string {
    return this._displayName;
  }

  exchangeKey(): JsonWebKey {
    return this._exchangeKey;
  }

  hasContactWithPseudo(pseudo: string): boolean {
    return !!this._contacts.contact(pseudo);
  }

  hmacHashesByDeletedReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {
    return this._deletedContacts.hmacHashesByReadFromBoxAddresses(pseudos);
  }

  /**
   * @param pseudos If an array of pseudos is specified, only the hmac hashes for those pseudos are returned.
   *                Pseudos not matching a session contact are ignored.
   */
  hmacHashesByReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {
    return this._contacts.hmacHashesByReadFromBoxAddresses(pseudos);
  }

  hmacHashForBoxAddress(boxAddress: string): Observable<string> {
    const contact: Contact = this._contacts.contactWithReadFromBoxAddress(boxAddress);
    if (contact) {
      return contact.hmacHash(boxAddress);
    }
    const deletedContact: DeletedContact = this._deletedContacts.deletedContactWithReadFromBoxAddress(boxAddress);
    if (deletedContact) {
      return deletedContact.hmacHash()
    }
    return of(null);
  }

  hmacHashOfSystemBoxAddress(): Observable<string> {
    if (!this._secretKey) {
      return of(null);
    }
    return this.hmacCryptoKey()
      .pipe(
        mergeMap((hmacKey: CryptoKey) => {
          return Hmac.hash(this._boxAddress, hmacKey);
        })
      );
  }

  keys(): Keys {
    return this._keys;
  }

  loggedUserDescription(): string {
    const displayName: string = this._displayName;
    return displayName && displayName.trim().length > 0 ? displayName : this._pseudo;
  }

  picture(): any { // TODO The type of picture will have to be precised. Once we have some...
    return this._picture;
  }

  pseudo(): string {
    return this._pseudo;
  }

  readFromBoxAddresses(): string[] {
    const readFromBoxAddresses: string[] = this.contactsAsArray().map((contact: Contact) => contact.readFrom());
    return readFromBoxAddresses.concat(this._boxAddress);
  }

  secretKey(): string {
    return this._secretKey;
  }

  systemBoxAddress(): string {
    return this._boxAddress;
  }

  /**
   * Returns the systems public keys: the public keys generated by default for each system user.
   */
  systemPublicKeys(): string {
    return this._keys.systemKeyPair().publicKeys();
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  updateContact(contact: Contact) {
    this._contacts.setContact(contact);
  }

  /**
   * Returns all the "writeTo" box addresses of the logged user's contacts.
   */
  writeToBoxAddresses(): string[] {
    return this.contactsAsArray().map((contact: Contact) => contact.writeTo());
  }

  private hmacCryptoKey(): Observable<CryptoKey> {
    if (this._hmacCryptoKey) {
      return of(this._hmacCryptoKey);
    }
    return Hmac.keyFor(this._secretKey)
      .pipe(
        map((hmacCryptoKey: CryptoKey) => {
          this._hmacCryptoKey = hmacCryptoKey;
          return this._hmacCryptoKey;
        })
      );
  }
}
