import {Objects} from "../utils/objects";
import {Session} from "./session";
import {SessionService} from "./session.service";

export class SessionsJasmine {

  static session() {
    return jasmine.createSpyObj('session', Objects
      .propertyNamesOf(Session.build(null, null, null, null, null, null, null, null, null, null, null)));
  }

  static sessionService() {
    return jasmine.createSpyObj('sessionService', Objects.propertyNamesOf(new SessionService(null, null)));
  }

  static sessionWebStorageService() {
    return jasmine.createSpyObj('sessionWebStorageService', ['storageAes']);
  }
}
