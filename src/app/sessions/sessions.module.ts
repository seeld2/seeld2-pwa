import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {StorageServiceModule} from "ngx-webstorage-service";
import {MobileModule} from "../mobile/mobile.module";
import {SessionService} from "./session.service";
import {SessionStorageProvider} from "./storage/session-storage.provider";
import {CryptoModule} from "../crypto/crypto.module";
import {UtilsModule} from "../utils/utils.module";
import {UserModule} from "../user/user.module";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    MobileModule,
    StorageServiceModule,
    UserModule,
    UtilsModule,
  ],
  providers: [
    SessionService,
    SessionStorageProvider
  ]
})
export class SessionsModule {
}
