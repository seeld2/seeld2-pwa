import {PlatformJasmine} from "../../mobile/platform.jasmine";
import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {SessionStorageService} from "./session-storage.service";
import {SessionDeviceStorageService} from "./session-device-storage.service";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {Session} from "../session";
import {Storage} from "@ionic/storage";
import {of} from "rxjs/observable/of";
import {CryptoJasmine} from "../../crypto/crypto.jasmine";
import {Contacts} from "../../user/contact/contacts";
import {Keys} from "../../user/key/keys";

describe("SessionDeviceStorageService", () => {

  const DEVICE_STORAGE_KEY = {
    kty: 'oct',
    k: 'VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I',
    alg: 'A256GCM'
  };
  const DEVICE_STORAGE_KEY_ENCRYPTED: string = 'vieurh-lkxzé156==';
  const SESSION_ENCRYPTED: string = 'nsopr4e9821/jvb==';

  let seeldCordovaPluginProvider: SeeldCordovaPluginProvider;
  let storage: Storage;

  let sessionStorageService: SessionStorageService;

  let session: Session;
  let storageAes_;

  beforeEach(() => {

    seeldCordovaPluginProvider = PlatformJasmine.seeldCordovaPluginProvider();
    storage = IonicJasmine.storage();

    sessionStorageService = new SessionDeviceStorageService(seeldCordovaPluginProvider, storage);

    storageAes_ = CryptoJasmine.storageAes();
    sessionStorageService['storageAes_'] = storageAes_;
    sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

    sessionStorageService['_seeldCordovaPlugin'] = {};
    sessionStorageService['deleteAlias'] = () => of(true);

    session = Session.build(null, null, Contacts.empty(), null, null, null, null, Keys.with({}), null, null, null);
  });

  it("should clear the storage and the keystore alias", (done: DoneFn) => {

    (<jasmine.Spy>storage.remove).and.returnValue(Promise.resolve());

    sessionStorageService.clearSession()
      .subscribe((result: boolean) => {

        expect((<jasmine.Spy>storage.remove).calls.argsFor(0)[0]).toEqual(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY);
        expect((<jasmine.Spy>storage.remove).calls.argsFor(1)[0]).toEqual(SessionStorageService.SESSION_STORAGE_KEY);

        expect(result).toEqual(true);

        done();
      });
  });

  it("should tell if a session is stored or not", (done: DoneFn) => {

    (<jasmine.Spy>storage.get).and.returnValues(
      Promise.resolve(SESSION_ENCRYPTED), Promise.resolve(DEVICE_STORAGE_KEY_ENCRYPTED));

    sessionStorageService.isSessionStored()
      .subscribe((result: boolean) => {

        expect((<jasmine.Spy>storage.get).calls.argsFor(0)[0]).toEqual(SessionStorageService.SESSION_STORAGE_KEY);
        expect((<jasmine.Spy>storage.get).calls.argsFor(1)[0]).toEqual(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY);

        expect(result).toEqual(true);

        done();
      });
  });

  describe(", when retrieving a session,", () => {

    it("should return the stored session if this session storage service was unlocked first", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(SESSION_ENCRYPTED));
      (<jasmine.Spy>storageAes_.decryptFromText).and.returnValue(of(session.toJson()));

      sessionStorageService.retrieveSession()
        .subscribe((result: Session) => {

          expect(storage.get).toHaveBeenCalledWith(SessionStorageService.SESSION_STORAGE_KEY);
          expect(storageAes_.decryptFromText).toHaveBeenCalledWith(SESSION_ENCRYPTED);

          expect(result).toEqual(session);

          done();
        });
    });

    it("should throw an error if this session storage service was not unlocked beforehand", (done: DoneFn) => {

      sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

      sessionStorageService.retrieveSession()
        .subscribe(
          () => {},
          (() => done())
        );
    });
  });

  describe(", when storing a session,", () => {

    it("should return true if the session was successfully stored", (done: DoneFn) => {

      (<jasmine.Spy>storageAes_.encryptAsText).and.returnValue(of(SESSION_ENCRYPTED));
      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve(true));

      sessionStorageService.storeSession(session)
        .subscribe((result: boolean) => {

          expect(storageAes_.encryptAsText).toHaveBeenCalledWith(session.toJson());
          expect(storage.set).toHaveBeenCalledWith(SessionStorageService.SESSION_STORAGE_KEY, SESSION_ENCRYPTED);

          expect(result).toEqual(true);

          done();
        });
    });

    it("should throw an error if this session storage service was not unlocked beforehand", (done: DoneFn) => {

      sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

      sessionStorageService.retrieveSession()
        .subscribe(
          () => {},
          (() => done())
        );
    });
  });

  describe(", when unlocking the session storage service,", () => {

    beforeEach(() => {
      (<jasmine.Spy>seeldCordovaPluginProvider.seeldCordovaPlugin).and.returnValue(of({}));

      sessionStorageService['deviceStorageKey_'] = null;

      sessionStorageService['decryptWithKeystoreSecretKey'] = () => of(JSON.stringify(DEVICE_STORAGE_KEY));
      sessionStorageService['encryptWithKeystoreSecretKey'] = () => of(DEVICE_STORAGE_KEY_ENCRYPTED);
    });

    it("should store the device storage key if it is specified", (done: DoneFn) => {

      (<jasmine.Spy>storage.set).and.returnValue(Promise.resolve());

      sessionStorageService.unlock(DEVICE_STORAGE_KEY)
        .subscribe((result: SessionStorageService) => {

          expect(storage.set)
            .toHaveBeenCalledWith(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY, DEVICE_STORAGE_KEY_ENCRYPTED);

          expect(result).toBeDefined()
          expect(sessionStorageService['deviceStorageKey_']).toEqual(DEVICE_STORAGE_KEY);

          done();
        });
    });

    it("should retrieve the device storage key from storage if not specified at call", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.resolve(DEVICE_STORAGE_KEY_ENCRYPTED));

      sessionStorageService.unlock()
        .subscribe((result: SessionStorageService) => {

          expect(storage.get).toHaveBeenCalledWith(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY);

          expect(result).toBeDefined()
          expect(sessionStorageService['deviceStorageKey_']).toEqual(DEVICE_STORAGE_KEY);

          done();
        });
    });

    it("should throw an error if no device storage key was passed and none was retrieved from storage", (done: DoneFn) => {

      (<jasmine.Spy>storage.get).and.returnValue(Promise.reject(null));

      sessionStorageService.unlock()
        .subscribe(
          () => {},
          (() => done())
        );
    });
  });
});
