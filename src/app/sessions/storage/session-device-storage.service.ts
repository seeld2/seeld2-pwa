import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {SessionStorageService} from "./session-storage.service";
import {Observable} from "rxjs";
import {Session} from "../session";
import {of} from "rxjs/observable/of";
import {Logger} from "../../utils/logging/logger";
import {from} from "rxjs/observable/from";
import {map, mergeMap} from "rxjs/operators";
import {Storage} from "@ionic/storage";
import {AES} from "../../crypto/aes/aes";

export class SessionDeviceStorageService extends SessionStorageService {

  static build(seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
               storage: Storage): Observable<SessionDeviceStorageService> {
    return of(new SessionDeviceStorageService(seeldCordovaPluginProvider, storage));
  }

  private readonly alias: string = "seeld"; // TODO Move to config
  private readonly deviceLogger: Logger = Logger.for("SessionDeviceStorageService");

  private _seeldCordovaPlugin;

  constructor(private readonly seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
              private readonly storage: Storage) {
    super();
  }

  clearSession(): Observable<boolean> {
    return this.deleteAlias()
      .pipe(
        mergeMap(() => from(this.storage.remove(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY))),
        mergeMap(() => from(this.storage.remove(SessionStorageService.SESSION_STORAGE_KEY))),
        mergeMap(() => of(true))
      );
  }

  isSessionStored(): Observable<boolean> {

    let hasSession: boolean;
    let hasDeviceStorageKey: boolean;

    return from(this.storage.get(SessionStorageService.SESSION_STORAGE_KEY))
      .pipe(
        mergeMap((result: string) => {
          hasSession = !!result;

          return from(this.storage.get(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY));
        }),
        mergeMap((result: string) => {
          hasDeviceStorageKey = !!result;

          return of(hasSession && hasDeviceStorageKey);
        })
      );
  }

  retrieveSession(): Observable<Session> {

    this.checkStorageServiceIsUnlocked();

    return this.storageAes()
      .pipe(
        mergeMap(() => {
          return from(this.storage.get(SessionStorageService.SESSION_STORAGE_KEY));
        }),

        mergeMap((encrypted: string) => {
          return this.storageAes_.decryptFromText(encrypted);
        }),

        map((unencrypted: string) => {
          return Session.fromJson(unencrypted);
        })
      );
  }

  storeSession(session: Session): Observable<boolean> {

    this.checkStorageServiceIsUnlocked();

    return this.storageAes()
      .pipe(
        mergeMap((aes: AES) => {
          return aes.encryptAsText(session.toJson());
        }),

        mergeMap((encrypted: string) => {
          return from(this.storage.set(SessionStorageService.SESSION_STORAGE_KEY, encrypted));
        }),

        mergeMap(() => of(true))
      );
  }

  unlock(deviceStorageKey?: JsonWebKey): Observable<SessionStorageService> {

    let obs: Observable<JsonWebKey>;
    if (deviceStorageKey) {
      obs = this.encryptWithKeystoreSecretKey(JSON.stringify(deviceStorageKey))
        .pipe(
          mergeMap((encryptedDeviceStorageKey: string) => {
            return from(this.storage.set(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY, encryptedDeviceStorageKey));
          }),

          map(() => {
            return deviceStorageKey;
          })
        );
    } else {
      const promise = this.storage.get(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY)
        .catch(() => { // Empty catch is necessary for the error to be thrown...
        });
      obs = from(promise)
        .pipe(
          mergeMap((encryptedDeviceStorageKey: string) => {

            if (!encryptedDeviceStorageKey) {
              this.deviceLogger.error("No device storage key specified or retrieved from storage!");
              throw new Error(this.deviceLogger.lastEntry().getEntry());
            }

            return this.decryptWithKeystoreSecretKey(encryptedDeviceStorageKey);
          }),

          map((unencryptedText: string) => {
            return JSON.parse(unencryptedText);
          })
        );
    }

    return this.initSeeldCordovaPlugin()
      .pipe(
        mergeMap(() => {
          return obs;
        }),

        map((dsk: JsonWebKey) => {
          this.deviceStorageKey_ = dsk;
          return this;
        })
      );
  }

  private initSeeldCordovaPlugin(): Observable<any> {
    return this.seeldCordovaPluginProvider.seeldCordovaPlugin()
      .pipe(
        map((seeldCordovaPlugin) => this._seeldCordovaPlugin = seeldCordovaPlugin)
      );
  }

  private decryptWithKeystoreSecretKey(encrypted: string): Observable<string> {
    return this.initSeeldCordovaPlugin()
      .pipe(
        mergeMap(() => {
          return new Observable(subscriber => {
            this._seeldCordovaPlugin.keystoreDecryptFromBase64(
              this.alias,
              encrypted,
              (unencrypted: string) => {
                subscriber.next(unencrypted);
                subscriber.complete();
              },
              (error) => {
                this.deviceLogger.error("Could not decrypt", error);
                subscriber.error("Could not decrypt");
                subscriber.complete();
              }
            );
          });
        })
      );
  }

  private encryptWithKeystoreSecretKey(text: string): Observable<string> {
    return this.initSeeldCordovaPlugin()
      .pipe(
        mergeMap(() => {
          return new Observable(subscriber => {
            this._seeldCordovaPlugin.keystoreEncryptToBase64(
              this.alias,
              text,
              (encrypted: string) => {
                subscriber.next(encrypted);
                subscriber.complete();
              },
              (error) => {
                this.deviceLogger.error("Could not encrypt", error);
                subscriber.error("Could not encrypt");
                subscriber.complete();
              }
            );
          });
        })
      );
  }

  private deleteAlias(): Observable<boolean> {
    return this.initSeeldCordovaPlugin()
      .pipe(
        mergeMap(() => {
          return new Observable(subscriber => {
            // noinspection TypeScriptValidateJSTypes
            this._seeldCordovaPlugin.keystoreDeleteAlias(
              this.alias,
              () => {
                subscriber.next(true);
                subscriber.complete();
              },
              (error) => {
                this.deviceLogger.error("Could not clear secure storage", error);
                subscriber.next(false);
                subscriber.complete();
              }
            );
          });
        })
      );
  }
}
