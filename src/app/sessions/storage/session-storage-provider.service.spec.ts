import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {SessionStorageProvider} from "./session-storage.provider";
import {PlatformService} from "../../mobile/platform.service";
import {StorageService} from "ngx-webstorage-service";
import {CoreJasmine} from "../../../core/core-jasmine";
import {IonicJasmine} from "../../../shared/ionic-jasmine";
import {SessionStorageService} from "./session-storage.service";
import {SessionWebStorageService} from "./session-web-storage.service";
import {Storage} from "@ionic/storage";
import {PlatformJasmine} from "../../mobile/platform.jasmine";

describe("SessionStorageProvider", () => {

  let platforms: PlatformService;
  let seeldCordovaPluginProvider: SeeldCordovaPluginProvider;
  let storage: Storage;
  let storageService: StorageService;

  let sessionStorageProvider: SessionStorageProvider;

  beforeEach(() => {
    platforms = PlatformJasmine.platformService();
    seeldCordovaPluginProvider = PlatformJasmine.seeldCordovaPluginProvider();
    storage = IonicJasmine.storage();
    storageService = CoreJasmine.storageService();

    sessionStorageProvider = new SessionStorageProvider(platforms, seeldCordovaPluginProvider, storage, storageService);
  })

  it("should return a SessionDeviceStorageService on mobile device platforms", (done: DoneFn) => {

    (<jasmine.Spy>platforms.isBrowser).and.returnValue(false);

    sessionStorageProvider.obtain()
      .subscribe((result: SessionStorageService) => {

        expect(result).toBeDefined();
        expect(result.constructor.name).toEqual("SessionDeviceStorageService");

        done();
      });
  });

  it("should return a SessionWebStorageService on web platforms", (done: DoneFn) => {

    (<jasmine.Spy>platforms.isBrowser).and.returnValue(true);

    sessionStorageProvider.obtain()
      .subscribe((result: SessionStorageService) => {

        expect(result).toBeDefined();
        expect(result.constructor.name).toEqual("SessionWebStorageService");

        done();
      });
  });

  it("should return the cached storage service instance if there's one", (done: DoneFn) => {

    const cachedService: SessionStorageService = new SessionWebStorageService(storageService);
    sessionStorageProvider['instance'] = cachedService;

    sessionStorageProvider.obtain()
      .subscribe((result: SessionStorageService) => {

        expect(result).toEqual(cachedService);

        done();
      });
  });
});
