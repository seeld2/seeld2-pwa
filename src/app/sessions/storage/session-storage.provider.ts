import {Inject, Injectable} from "@angular/core";
import {PlatformService} from "../../mobile/platform.service";
import {SESSION_STORAGE, StorageService} from "ngx-webstorage-service";
import {SeeldCordovaPluginProvider} from "../../mobile/seeld-cordova-plugin.provider";
import {SessionStorageService} from "./session-storage.service";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {tap} from "rxjs/operators";
import {SessionWebStorageService} from "./session-web-storage.service";
import {SessionDeviceStorageService} from "./session-device-storage.service";
import {Storage} from "@ionic/storage";

@Injectable()
export class SessionStorageProvider {

  private instance: SessionStorageService;

  constructor(private readonly platforms: PlatformService,
              private readonly seeldCordovaPluginProvider: SeeldCordovaPluginProvider,
              private readonly storage: Storage,
              @Inject(SESSION_STORAGE) private readonly storageService: StorageService) {
  }

  obtain(): Observable<SessionStorageService> {

    if (this.instance) {
      return of(this.instance);
    }

    const instance: Observable<SessionStorageService> = this.platforms.isBrowser()
      ? SessionWebStorageService.build(this.storageService)
      : SessionDeviceStorageService.build(this.seeldCordovaPluginProvider, this.storage);

    return instance.pipe(
      tap((sessionStorageService: SessionStorageService) => {
        this.instance = sessionStorageService;
      })
    );
  }
}
