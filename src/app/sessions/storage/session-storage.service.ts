import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {map} from "rxjs/operators";
import {AES} from "../../crypto/aes/aes";
import {Logger} from "../../utils/logging/logger";
import {Session} from "../session";

/**
 * This service is in charge of storing and retrieving a Session from a web or mobile storage.
 */
export abstract class SessionStorageService {

  static readonly DEVICE_STORAGE_KEY_STORAGE_KEY: string = 'DSK';
  static readonly SESSION_STORAGE_KEY: string = 'SESSION';

  private readonly logger: Logger = Logger.for("SessionStorageService");

  protected deviceStorageKey_: JsonWebKey;
  protected storageAes_: AES;

  public storageAes(): Observable<AES> {
    return this.storageAes_
      ? of(this.storageAes_)
      : AES.fromJwk(this.deviceStorageKey_)
        .pipe(
          map((aes: AES) => this.storageAes_ = aes),
          map(() => this.storageAes_)
        );
  }

  abstract clearSession(): Observable<boolean>;

  abstract isSessionStored(): Observable<boolean>;

  abstract retrieveSession(): Observable<Session>;

  abstract storeSession(session: Session): Observable<boolean>

  abstract unlock(deviceStorageKey?: JsonWebKey): Observable<SessionStorageService>;

  protected checkStorageServiceIsUnlocked() {
    if (!this.deviceStorageKey_) {
      this.logger.error("This instance of SessionStorageService must be unlocked before retrieving a session");
      throw new Error(this.logger.lastEntry().getEntry());
    }
  }
}
