import {SessionStorageService} from "./session-storage.service";
import {SessionWebStorageService} from "./session-web-storage.service";
import {CoreJasmine} from "../../../core/core-jasmine";
import {Session} from "../session";
import {of} from "rxjs/observable/of";
import {CryptoJasmine} from "../../crypto/crypto.jasmine";
import {Contacts} from "../../user/contact/contacts";
import {Keys} from "../../user/key/keys";

describe("SessionWebStorageService", () => {

  const DEVICE_STORAGE_KEY = {
    kty: 'oct',
    k: 'VqGsw5eHtgUVblUDRglutJCdnnU8_MOPng1CA5CVB7I',
    alg: 'A256GCM'
  };
  const SESSION_ENCRYPTED: number[] = [9, 5, 1];

  let storageService;

  let sessionStorageService: SessionStorageService;

  let session: Session;
  let storageAes_;

  beforeEach(() => {
    storageService = CoreJasmine.storageService();

    sessionStorageService = new SessionWebStorageService(storageService);

    storageAes_ = CryptoJasmine.storageAes();
    sessionStorageService['storageAes_'] = storageAes_;
    sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

    session = Session.build(null, null, Contacts.fromObject({}), null, null, null, null, Keys.fromObject({}), null, null, null);
  });

  it("should clear the storage", (done: DoneFn) => {

    sessionStorageService.clearSession()
      .subscribe((result: boolean) => {

        expect(storageService.clear).toHaveBeenCalled();

        expect(result).toEqual(true);

        done();
      });
  });

  it("should tell if a session is stored or not", (done: DoneFn) => {

    (<jasmine.Spy>storageService.has).and.returnValues(true, true);

    sessionStorageService.isSessionStored()
      .subscribe((result: boolean) => {

        expect(result).toEqual(true);

        done();
      });
  });

  describe(", when retrieving a session,", () => {

    it("should return the stored session if this session storage service was unlocked first", (done: DoneFn) => {

      (<jasmine.Spy>storageService.get).and.returnValue(SESSION_ENCRYPTED);
      (<jasmine.Spy>storageAes_.decrypt).and.returnValue(of(session.toJson()));

      sessionStorageService.retrieveSession()
        .subscribe((result: Session) => {

          expect(storageService.get).toHaveBeenCalledWith(SessionStorageService.SESSION_STORAGE_KEY);

          expect(result).toEqual(session);

          done();
        });
    });

    it("should throw an error if this session storage service was not unlocked beforehand", (done: DoneFn) => {

      sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

      sessionStorageService.retrieveSession()
        .subscribe(
          () => {},
          (() => done())
        );
    });
  });

  describe(", when storing a session,", () => {

    it("should return true if the session was successfully stored", (done: DoneFn) => {

      (<jasmine.Spy>storageAes_.encrypt).and.returnValue(of(new Uint8Array(SESSION_ENCRYPTED)));
      (<jasmine.Spy>storageService.set).and.returnValue(of(true));

      sessionStorageService.storeSession(session)
        .subscribe((result: boolean) => {

          expect(storageAes_.encrypt).toHaveBeenCalledWith(session.toJson());
          expect(storageService.set).toHaveBeenCalledWith(SessionStorageService.SESSION_STORAGE_KEY, SESSION_ENCRYPTED);

          expect(result).toEqual(true);

          done();
        });
    });

    it("should throw an error if this session storage service was not unlocked beforehand", (done: DoneFn) => {

      sessionStorageService['deviceStorageKey_'] = DEVICE_STORAGE_KEY;

      sessionStorageService.retrieveSession()
        .subscribe(
          () => {},
          (() => done())
        );
    });
  });

  describe(", when unlocking the session storage service,", () => {

    beforeEach(() => {
      sessionStorageService['deviceStorageKey_'] = null;
    });

    it("should store the device storage key if it is specified", (done: DoneFn) => {

      sessionStorageService.unlock(DEVICE_STORAGE_KEY)
        .subscribe((result: SessionStorageService) => {

          expect(storageService.set)
            .toHaveBeenCalledWith(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY, DEVICE_STORAGE_KEY);

          expect(result).toBeDefined()
          expect(sessionStorageService['deviceStorageKey_']).toEqual(DEVICE_STORAGE_KEY);

          done();
        });
    });

    it("should retrieve the device storage key from storage if not specified at call", (done: DoneFn) => {

      (<jasmine.Spy>storageService.get).and.returnValue(DEVICE_STORAGE_KEY);

      sessionStorageService.unlock()
        .subscribe((result: SessionStorageService) => {

          expect(storageService.get).toHaveBeenCalledWith(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY);

          expect(result).toBeDefined()
          expect(sessionStorageService['deviceStorageKey_']).toEqual(DEVICE_STORAGE_KEY);

          done();
        });
    });

    it("should throw an error if no device storage key was passed and none was retrieved from storage", (done: DoneFn) => {

      (<jasmine.Spy>storageService.get).and.returnValue(null);

      try {
        sessionStorageService.unlock()
          .subscribe(() => {
          });
      } catch (e) {
        done();
      }
    });
  });
});
