import {SessionStorageService} from "./session-storage.service";
import {StorageService} from "ngx-webstorage-service";
import {Observable} from "rxjs";
import {map, mergeMap, tap} from "rxjs/operators";
import {Logger} from "../../utils/logging/logger";
import {of} from "rxjs/observable/of";
import {Session} from "../session";
import {AES} from "../../crypto/aes/aes";

export class SessionWebStorageService extends SessionStorageService {

  static build(storageService: StorageService): Observable<SessionWebStorageService> {
    return of(new SessionWebStorageService(storageService));
  }

  private readonly webLogger: Logger = Logger.for("SessionWebStorageService");

  constructor(private readonly storageService: StorageService) {
    super();
  }

  clearSession(): Observable<boolean> {
    this.storageService.clear();
    return of(true);
  }

  isSessionStored(): Observable<boolean> {
    return of(this.storageService.has(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY)
      && this.storageService.has(SessionStorageService.SESSION_STORAGE_KEY));
  }

  retrieveSession(): Observable<Session> {

    this.checkStorageServiceIsUnlocked();

    return this.storageAes()
      .pipe(
        mergeMap((aes: AES) => {
          const encrypted = this.storageService.get(SessionStorageService.SESSION_STORAGE_KEY);
          return aes.decrypt(new Uint8Array(encrypted));
        }),

        map((unencrypted: string) => {
          return Session.fromJson(unencrypted);
        })
      );
  }

  storeSession(session: Session): Observable<boolean> {

    this.checkStorageServiceIsUnlocked();

    return this.storageAes()
      .pipe(
        mergeMap((aes: AES) => {
          return aes.encrypt(session.toJson());
        }),

        tap((encrypted: Uint8Array) => {
          this.storageService.set(SessionStorageService.SESSION_STORAGE_KEY, Array.from(encrypted));
        }),

        mergeMap(() => of(true))
      );
  }

  unlock(deviceStorageKey?: JsonWebKey): Observable<SessionWebStorageService> {

    if (deviceStorageKey) {
      this.storageService.set(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY, deviceStorageKey);

    } else {
      deviceStorageKey = this.storageService.get(SessionStorageService.DEVICE_STORAGE_KEY_STORAGE_KEY);

      if (!deviceStorageKey) {
        this.webLogger.error("No device storage key specified or retrieved from storage!");
        throw new Error(this.webLogger.lastEntry().getEntry());
      }
    }

    this.deviceStorageKey_ = deviceStorageKey;

    return of(this);
  }
}
