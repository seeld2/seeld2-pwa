import {Objects} from "../utils/objects";
import {TimeService} from "./time.service";

export class TimeJasmine {

  static timeService() {
    return jasmine.createSpyObj('timeService', Objects.propertyNamesOf(new TimeService(null)));
  }
}
