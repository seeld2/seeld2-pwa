import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {UtilsModule} from "../utils/utils.module";
import {TimeService} from "./time.service";

@NgModule({
  imports: [
    HttpClientModule,
    UtilsModule
  ],
  providers: [
    TimeService
  ]
})
export class TimeModule {

}
