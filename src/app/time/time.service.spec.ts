import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {AngularJasmine} from "../../shared/angular-jasmine";
import {Pong} from "./pong";
import {TimeService} from "./time.service";
import * as moment from "moment";

describe("TimeService", () => {

  const DATE_IN_MILLIS: number = 1614935274532;

  let httpClient: HttpClient;

  let timeService: TimeService

  beforeEach(() => {
    httpClient = AngularJasmine.httpClient();
    timeService = new TimeService(httpClient);
  });

  it("should set up the time skew", (done: DoneFn) => {

    const serverMillis: number = moment().valueOf() + 10000;
    (<jasmine.Spy>httpClient.get).and.returnValue(of(<Pong>{epoch: serverMillis}));

    timeService.adjust().add(() => {
      expect(timeService['skew']).toBeGreaterThan(0);
      done();
    });
  });

  it("should format a date in milliseconds to a human-readable date", () => {

    expect(timeService.formatDate(DATE_IN_MILLIS)).toEqual("05/03/2021");
  });

  it("should format a date in milliseconds to a human-readable date and time", () => {
    expect(timeService.formatDateTime(DATE_IN_MILLIS)).toEqual("05/03/2021 10:07");
  });

  it("should format a date in milliseconds to a human-readable date, time and milliseconds", () => {
    expect(timeService.formatDateTimeMillis(DATE_IN_MILLIS)).toEqual("05/03/2021 10:07:54.532");
  });
});
