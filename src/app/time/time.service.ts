import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

import * as moment from "moment";
import {Subscription} from "rxjs";
import {of} from "rxjs/observable/of";
import {mergeMap} from "rxjs/operators";
import {config} from "../app.config";
import {Pong} from "./pong";

@Injectable()
export class TimeService {

  private skew: number = 0;

  constructor(private readonly httpClient: HttpClient) {
  }

  adjust(): Subscription {
    const localNow: number = moment().valueOf();
    return this.httpClient.get(`${config.api.root}`)
      .pipe(
        mergeMap((pong: Pong) => {
          const serverNow: number = pong.epoch;
          this.skew = serverNow - localNow;
          return of(this);
        })
      )
      .subscribe();
  }

  formatDate(millis: number): string {
    return moment(millis).format(config.moment.format.date);
  }

  formatDateTime(millis: number): string {
    return moment(millis).format(config.moment.format.dateTime);
  }

  formatDateTimeMillis(millis: number): string {
    return moment(millis).format(config.moment.format.dateTimeMillis);
  }

  nowAdjusted(): number {
    return moment().valueOf() + this.skew;
  }
}
