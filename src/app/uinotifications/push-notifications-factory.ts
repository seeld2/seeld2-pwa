import {Injectable} from "@angular/core";
import {PushNotifications} from "./push-notifications.service";

@Injectable()
export class PushNotificationsFactory {

  constructor() {
  }

  build(): Promise<PushNotifications> {
    return new Promise<PushNotifications>((resolve, reject) => {

      if ('Notification' in window) {
        const pushNotifications: PushNotifications = new PushNotifications();
        pushNotifications.init()
          .then((permission: string) => {
            if (permission === PushNotifications.GRANTED) {
              resolve(pushNotifications);
            } else {
              reject('Push notifications denied');
            }
          });

      } else {
        reject('Push notifications not supported');
      }
    });
  }
}
