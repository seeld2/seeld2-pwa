import {PushNotifications} from "./push-notifications.service";

describe("PushNotifications", () => {

  const TEXT = 'text';
  const TITLE = 'title';

  let notification;
  const pushNotifications: PushNotifications = new PushNotifications();

  beforeEach(() => {
    notification = jasmine.createSpyObj('Notification', ['requestPermission']);
    notification.permission = 'default';

    pushNotifications['notification'] = notification;
  });

  describe(", when initialising,", () => {

    it("should return 'granted' if the user granted push notifications", (done: DoneFn) => {

      (notification.requestPermission as jasmine.Spy).and.returnValue(Promise.resolve('granted'));

      pushNotifications.init().then((result: string) => {
        expect(result).toEqual('granted');
        done();
      });
    });

    it("should return 'denied' if the user denied push notifications", (done: DoneFn) => {

      (notification.requestPermission as jasmine.Spy).and.returnValue(Promise.resolve('denied'));

      pushNotifications.init().then((result: string) => {
        expect(result).toEqual('denied');
        done();
      });
    });
  });

  describe(", when pushing notifications,", () => {

    it("should return a notification if the user granted push notification", () => {
      notification.permission = 'granted';
      pushNotifications['notification'] = notification;

      const pushedNotification: Notification = pushNotifications.push(TITLE, TEXT);

      expect(pushedNotification).toBeDefined();
      expect(pushedNotification.title).toEqual(TITLE);
      expect(pushedNotification.body).toEqual(TEXT);
    });

    it("should return null if the user didn't grant push notification", () => {
      notification.permission = 'denied';
      pushNotifications['notification'] = notification;

      expect(pushNotifications.push(TITLE, TEXT)).toBeNull();
    });
  });
});
