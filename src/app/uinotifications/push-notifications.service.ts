import {Logger} from "../utils/logging/logger";

declare var Notification: any;

/**
 * A service for handling web push notifications, which appear on the user's desktop.
 */
export class PushNotifications {

  public static readonly DEFAULT: string = 'default';
  public static readonly GRANTED: string = 'granted';

  private readonly logger: Logger = Logger.for('PushNotifications');

  private notification = Notification; // This is to allow mocking this Web API when testing...

  constructor() {
  }

  /**
   * Initialises this service by requesting permission (if needed) to the user in order to display push notifications.
   *
   * @returns A Promise that resolves into the permission obtained after completing initialization
   */
  init(): Promise<string> {
    return new Promise<any>((resolve) => {

      const currentPermission = this.notification.permission;

      if (currentPermission === PushNotifications.DEFAULT) {
        this.notification.requestPermission()
          .then((result: string) => {
            this.logger
              .info(`After requesting the permission to push notifications, the obtained permission is: ${result}`);
            resolve(result);
          })
          .catch(reason => this.logger.info("Did not get the permission to push notifications on this device", reason));

      } else {
        this.logger.info(`Currently the permission to push notifications is: ${currentPermission}`);
        resolve(currentPermission);
      }
    });
  }

  /**
   * Pushes a notification. If permission is granted, the notification will be shown.
   *
   * @param title The title of the notification
   * @param text The text inside the notification box
   * @param icon (optional) An icon to be shown in the notification box
   * @returns The pushed notification
   */
  push(title: string, text: string, icon?: string): Notification {
    if (this.notification.permission === PushNotifications.GRANTED) {
      return new Notification(title, {
        body: text,
        icon: icon
      });
    } else {
      return null;
    }
  }
}
