import {Objects} from "../utils/objects";
import {UiNotifications} from "./ui-notifications";
import {PushNotificationsFactory} from "./push-notifications-factory";

export class UiNotificationsJasmine {

  static pushNotifications() {
    return jasmine.createSpyObj('pushNotifications', ['canUsePushNotifications', 'haveBeenGranted', 'init', 'push']);
  }

  static pushNotificationsFactory() {
    return jasmine
      .createSpyObj('pushNotificationsFactory', Objects.propertyNamesOf(new PushNotificationsFactory()));
  }

  static uiNotifications() {
    return jasmine
      .createSpyObj('uiNotifications', Objects.propertyNamesOf(new UiNotifications(null, null, null)));
  }
}
