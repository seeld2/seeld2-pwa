import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {MobileModule} from "../mobile/mobile.module";
import {PushNotificationsFactory} from "./push-notifications-factory";
import {UiNotifications} from "./ui-notifications";
import {UtilsModule} from "../utils/utils.module";

@NgModule({
  imports: [
    BrowserModule,
    MobileModule,
    UtilsModule
  ],
  providers: [
    PushNotificationsFactory,
    UiNotifications
  ]
})
export class UiNotificationsModule {
}
