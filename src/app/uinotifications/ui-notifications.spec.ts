import {UiNotifications} from "./ui-notifications";
import {PlatformService} from "../mobile/platform.service";
import {PushNotifications} from "./push-notifications.service";
import {ToastController} from "ionic-angular";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {PushNotificationsFactory} from "./push-notifications-factory";
import {UiNotificationsJasmine} from "./ui-notifications.jasmine";
import {PlatformJasmine} from "../mobile/platform.jasmine";

describe("UI Notifications", () => {

  const TEXT: string = 'text';

  let platforms: PlatformService;
  let pushNotificationsFactory: PushNotificationsFactory;
  let toastCtrl: ToastController;

  let uiNotifications: UiNotifications;

  let pushNotifications: PushNotifications;

  beforeEach(() => {
    platforms = PlatformJasmine.platformService();
    pushNotificationsFactory = UiNotificationsJasmine.pushNotificationsFactory();
    toastCtrl = IonicJasmine.toastCtrl();

    uiNotifications = new UiNotifications(platforms, pushNotificationsFactory, toastCtrl);

    pushNotifications = UiNotificationsJasmine.pushNotifications();
  });

  describe(", when initializing", () => {

    it("should initialize push notifications if the platform is a browser and push notifications can be used",
      (done: DoneFn) => {

        (platforms.isBrowser as jasmine.Spy).and.returnValue(true);
        (<jasmine.Spy>pushNotificationsFactory.build).and.returnValue(Promise.resolve(pushNotifications));

        uiNotifications.initialize()
          .subscribe((result: boolean) => {
            expect(result).toEqual(true);

            expect(uiNotifications['pushNotifications']).toBeDefined();
            done();
          });
      });
  });

  describe(", when showing a notification on device,", () => {

    beforeEach(() => {
      (platforms.isBrowser as jasmine.Spy).and.returnValue(true);
      (toastCtrl.create as jasmine.Spy).and.returnValue(IonicJasmine.toast());
    });

    it("should push the notification on a browser when permission has been granted", () => {

      uiNotifications['pushNotifications'] = pushNotifications;

      uiNotifications.showOnDevice(TEXT);

      expect(pushNotifications.push).toHaveBeenCalledWith(jasmine.any(String), TEXT);
      expect(toastCtrl.create).toHaveBeenCalledWith(jasmine.objectContaining({cssClass: 'toast-info'}));
    });

    it("should show in-app info message on a browser when permission to push notifications has not been granted", () => {

      uiNotifications['pushNotifications'] = null;

      uiNotifications.showOnDevice(TEXT);

      expect(toastCtrl.create).toHaveBeenCalledWith(jasmine.objectContaining({cssClass: 'toast-info'}));
    });
  });
});
