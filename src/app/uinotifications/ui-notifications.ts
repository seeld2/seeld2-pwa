import {Injectable} from "@angular/core";
import {PushNotifications} from "./push-notifications.service";
import {PlatformService} from "../mobile/platform.service";
import {ToastController} from "ionic-angular";
import {config} from "../app.config";
import {PushNotificationsFactory} from "./push-notifications-factory";
import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";

/**
 * Provides a single point-of-entry for notifications.
 * The way the user is notified depends on the device.
 */
@Injectable()
export class UiNotifications {

  private pushNotifications: PushNotifications;

  constructor(private platforms: PlatformService,
              private pushNotificationsFactory: PushNotificationsFactory,
              private toastCtrl: ToastController) {
  }

  /**
   * Sets up the UI notifications service by initializing push notifications if available.
   */
  initialize(): Observable<boolean> {

    if (this.platforms.isBrowser()) {

      return from(this.pushNotificationsFactory.build()
        .then((pushNotifications: PushNotifications) => {
          this.pushNotifications = pushNotifications;
          return true;
        })
        .catch(() => {
          this.pushNotifications = null;
          return false;
        }));
    }
  }

  showError(message: string) {
    this.showToast(message, 'error');
  }

  showInfo(message: string) {
    this.showToast(message, 'info');
  }

  showOnDevice(text: string) {
    if (this.platforms.isBrowser()) {
      if (this.pushNotifications) {
        this.pushNotifications.push(config.ui.notifications.title, text);
      }
    }

    this.showInfo(text); // Always show info inside app
  }

  showSuccess(message: string) {
    this.showToast(message, 'success');
  }

  showWarning(message: string) {
    this.showToast(message, 'warning');
  }

  private showToast(message: string, level: string) {

    // if (this.platforms.isAndroidApp()) {
    //   this.toast.showWithOptions({
    //     duration: config.ui.notifications.duration,
    //     message: message,
    //     position: 'bottom',
    //     styling: {
    //       cornerRadius: 10,
    //       backgroundColor: UiNotifications.TOAST_COLORS[level],
    //       textColor: '#454545'
    //     }
    //   }).subscribe((result) => {
    //     if (result && result.event === 'touch') {
    //       // noinspection JSIgnoredPromiseFromCall
    //       this.toast.hide();
    //     }
    //   });
    //
    // } else {
      // noinspection JSIgnoredPromiseFromCall
      this.toastCtrl.create({
        closeButtonText: '\u274C',
        cssClass: 'toast-' + level.toLowerCase(),
        duration: config.ui.notifications.duration,
        message: message,
        position: 'bottom',
        showCloseButton: true
      }).present();
    // }
  }

  // private static readonly TOAST_COLORS: any = {
  //   'error': '#ee4266',
  //   'info': '#e8e5f8',
  //   'success': '#bddb9c',
  //   'warning': '#ffbd8c'
  // };
}
