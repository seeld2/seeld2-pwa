import {ContactKey} from "./contact-key";

describe("ContactKey", () => {

  const ID: string = "id";
  const PUBLIC_KEYS: string = "publicKeys";
  const TYPE: string = "type";

  it("should build an instance from an object representing a ContactKey", () => {
    const contactKey: ContactKey = ContactKey.fromObject({id: ID, publicKeys: PUBLIC_KEYS, type: TYPE});
    expect(contactKey['_id']).toEqual(ID);
    expect(contactKey['_type']).toEqual(TYPE);
    expect(contactKey['_publicKeys']).toEqual(PUBLIC_KEYS);
  });

  it("should build an instance based on properties", () => {
    const contactKey: ContactKey = ContactKey.with(ID, TYPE, PUBLIC_KEYS);
    expect(contactKey['_id']).toEqual(ID);
    expect(contactKey['_type']).toEqual(TYPE);
    expect(contactKey['_publicKeys']).toEqual(PUBLIC_KEYS);
  });

  it("should provide a serialized representation of itself", () => {
    const object: any = ContactKey.with(ID, TYPE, PUBLIC_KEYS).toObject();
    expect(object.id).toEqual(ID);
    expect(object.type).toEqual(TYPE);
    expect(object.publicKeys).toEqual(PUBLIC_KEYS);
  });
});
