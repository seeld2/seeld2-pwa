import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class ContactKey implements Serializable {

  static fromObject(object: any): ContactKey {
    return new ContactKey(object.id, object.type, object.publicKeys);
  }

  static with(id: string, type: string, publicKeys: string): ContactKey {
    return new ContactKey(id, type, publicKeys);
  }

  private constructor(private _id: string,
                      private _type: string,
                      private _publicKeys: string) {
  }

  asObject(): any {
    return {
      id: this._id,
      type: this._type,
      publicKeys: this._publicKeys
    };
  }

  publicKeys(): string {
    return this._publicKeys;
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
