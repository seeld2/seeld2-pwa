import * as moment from "moment";
import {Contact} from "./contact";
import {ContactKey} from "./contact-key";

describe("Contact", () => {

  const PSEUDO: string = "username";
  const CONNECTED_SINCE: number = moment().valueOf();
  const DISPLAY_NAME: string = "display name";
  const READ_FROM: string = "r-e-a-d-f-r-o-m";
  const SECRET_KEY: string = 'secret key';
  const WRITE_TO: string = "w-r-i-t-e-t-o";
  const WRITE_WITH: ContactKey = ContactKey.with("contactKeyId", "contactKeyType", "contactKeyPublicKeys");

  beforeEach(() => {
  });

  describe(", when building a new Contact,", () => {

    it("should instantiate a new contact from properties", () => {
      const contact: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY);
      expect(contact).toBeDefined();
      expect(contact.connectedSinceFormatted()).toBeDefined();
    });

    it("should instantiate an existing contact, which has a 'connected since' value", () => {

      const contact: Contact = Contact
        .with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY, CONNECTED_SINCE);

      expect(contact).toBeDefined();
      expect(contact.connectedSince()).toEqual(CONNECTED_SINCE);
    });

    it("should return a new contact from an object representing a contact", () => {

      const object: any = {
        pseudo: PSEUDO,
        displayName: DISPLAY_NAME,
        readFrom: READ_FROM,
        writeTo: WRITE_TO,
        writeWith: {id: "contactKeyId", type: "contactKeyType", publicKeys: "contactKeyPublicKeys"},
        secretKey: SECRET_KEY,
        connectedSince: CONNECTED_SINCE
      };

      const contact: Contact = Contact.fromObject(object);

      expect(contact.pseudo()).toEqual(PSEUDO);
      expect(contact.displayName()).toEqual(DISPLAY_NAME);
      expect(contact.readFrom()).toEqual(READ_FROM);
      expect(contact.writeTo()).toEqual(WRITE_TO);
      expect(contact.writeWith()).toEqual(WRITE_WITH);
      expect(contact.secretKey()).toEqual(SECRET_KEY);
      expect(contact.connectedSince()).toEqual(CONNECTED_SINCE);
    });
  });

  it("should provide a serialized representation of itself", () => {
    const contact: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY, CONNECTED_SINCE);
    const object: any = contact.toObject();
    expect(object.pseudo).toEqual(PSEUDO);
    expect(object.displayName).toEqual(DISPLAY_NAME);
    expect(object.readFrom).toEqual(READ_FROM);
    expect(object.writeTo).toEqual(WRITE_TO);
    expect(object.writeWith).toEqual({id: "contactKeyId", type: "contactKeyType", publicKeys: "contactKeyPublicKeys"});
    expect(object.secretKey).toEqual(SECRET_KEY);
    expect(object.connectedSince).toEqual(CONNECTED_SINCE);
  });

  it("should describe the contact using either its display name or username", () => {

    const contact1: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY);
    expect(contact1.description()).toEqual(DISPLAY_NAME);

    const contact2: Contact = Contact.with(PSEUDO, '', READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY);
    expect(contact2.description()).toEqual(PSEUDO);
  });

  describe(", when generating a HMAC hash,", () => {

    it("should generate the hash using the supplied text if the contact has a secret key", (done: DoneFn) => {
      const contact: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY);
      contact.hmacHash(READ_FROM).subscribe((hmacHash: string) => {
        expect(hmacHash)
          .toEqual("2+GJFb60eMwnK+JsvMf8B15Bro/Y/L/VeJnLaSFBrjyG3Vf/ys5ngFihToWokZwGlPY8q8os6UMBRqbQlCyC+A==");
        done();
      });
    });

    it("should return 'null' if the contact has no secret key to generate the hash", (done: DoneFn) => {
      const contact: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, null);
      contact.hmacHash(READ_FROM).subscribe((hmacHash: string) => {
        expect(hmacHash).toBeNull();
        done();
      });
    });

    it("should cache the crypto key", (done: DoneFn) => {
      const contact: Contact = Contact.with(PSEUDO, DISPLAY_NAME, READ_FROM, WRITE_TO, WRITE_WITH, SECRET_KEY);
      contact.hmacHash(READ_FROM).subscribe(() => {
        expect(contact['_hmacCryptoKey']).toBeDefined();
        done();
      });
    });
  });
});
