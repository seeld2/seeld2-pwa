import {ContactKey} from "./contact-key";
import {Contact} from "./contact";

export class ContactTestBuilder {

  public static readonly PSEUDO: string = "pseudo";
  public static readonly DISPLAY_NAME: string = "display name";
  public static readonly READ_FROM: string = "read from";
  public static readonly WRITE_TO: string = "write to";
  public static readonly WRITE_WITH: ContactKey = ContactKey.with(null, null, null);
  public static readonly SECRET_KEY: string = "secret key";

  static with(): ContactTestBuilder {
    return new ContactTestBuilder();
  }

  and = this;

  private _pseudo: string;
  private _displayName: string;
  private _readFrom: string;
  private _writeTo: string;
  private _writeWith: ContactKey;
  private _secretKey: string;
  private _connectedSince: number;

  private constructor() {
  }

  asIs(): Contact {
    return Contact.with(
      this._pseudo,
      this._displayName,
      this._readFrom,
      this._writeTo,
      this._writeWith,
      this._secretKey,
      this._connectedSince
    );
  }

  noBlanks(): Contact {
    return Contact.with(
      this._pseudo ? this._pseudo : ContactTestBuilder.PSEUDO,
      this._displayName ? this._displayName : ContactTestBuilder.DISPLAY_NAME,
      this._readFrom ? this._readFrom : ContactTestBuilder.READ_FROM,
      this._writeTo ? this._writeTo : ContactTestBuilder.WRITE_TO,
      this._writeWith ? this._writeWith : ContactTestBuilder.WRITE_WITH,
      this._secretKey ? this._secretKey : ContactTestBuilder.SECRET_KEY,
      this._connectedSince
    );
  }

  connectedSince(connectedSince: number): ContactTestBuilder {
    this._connectedSince = connectedSince;
    return this;
  }

  displayName(displayName: string): ContactTestBuilder {
    this._displayName = displayName;
    return this;
  }

  pseudo(pseudo: string): ContactTestBuilder {
    this._pseudo = pseudo;
    return this;
  }

  readFrom(readFrom: string): ContactTestBuilder {
    this._readFrom = readFrom;
    return this;
  }

  secretKey(secretKey: string): ContactTestBuilder {
    this._secretKey = secretKey;
    return this;
  }

  writeTo(writeTo: string): ContactTestBuilder {
    this._writeTo = writeTo;
    return this;
  }

  writeWith(writeWith: ContactKey): ContactTestBuilder {
    this._writeWith = writeWith;
    return this;
  }
}
