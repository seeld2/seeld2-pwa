import * as moment from "moment";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../../app.config";
import {Hmac} from "../../crypto/hmac/hmac";
import {Random} from "../../crypto/random/random";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";
import {ContactKey} from "./contact-key";

declare var base64js: any;
declare var TextEncoder: any;

/**
 * A contact as stored in the logged user's payload.
 */
export class Contact implements Serializable {

  static fromObject(object: any) {
    return new Contact(
      object.pseudo ? object.pseudo : object.username,
      object.displayName,
      object.readFrom,
      object.writeTo,
      ContactKey.fromObject(object.writeWith),
      object.secretKey,
      object.connectedSince);
  }

  static generateSecretKey(): string {
    return Random.generateBase64String(64);
  }

  static with(pseudo: string,
              displayName: string,
              readFrom: string,
              writeTo: string,
              writeWith: ContactKey,
              secretKey: string,
              connectedSince?: number) {
    return new Contact(pseudo, displayName, readFrom, writeTo, writeWith, secretKey, connectedSince);
  }

  private _hmacCryptoKey: CryptoKey;

  private constructor(private readonly _pseudo: string,
                      private readonly _displayName: string,
                      private readonly _readFrom: string,
                      private readonly _writeTo: string,
                      private readonly _writeWith: ContactKey,
                      private readonly _secretKey: string,
                      private readonly _connectedSince?: number) {
    if (!this._connectedSince) {
      this._connectedSince = moment().valueOf();
    }
  }

  asObject(): any {
    return {
      pseudo: this._pseudo,
      displayName: this._displayName,
      readFrom: this._readFrom,
      writeTo: this._writeTo,
      writeWith: this._writeWith,
      secretKey: this._secretKey,
      connectedSince: this._connectedSince
    };
  }

  connectedSince(): number {
    return this._connectedSince;
  }

  connectedSinceFormatted(): string {
    return moment(this._connectedSince).format(config.moment.format.date);
  }

  /**
   * The description is either the contact's display name if it has one, or its username otherwise.
   */
  description(): string {
    return this._displayName ? this._displayName : this._pseudo;
  }

  displayName(): string {
    return this._displayName;
  }

  hmacHash(text: string): Observable<string> {
    if (!this._secretKey) {
      return of(null);
    }

    return this.hmacHashKey()
      .pipe(
        mergeMap((cryptoKey: CryptoKey) => {
          const encodedData = new TextEncoder().encode(text);
          return window.crypto.subtle.sign("HMAC", cryptoKey, encodedData);
        }),
        map((encodedHashedData: ArrayBuffer) => {
          return base64js.fromByteArray(new Uint8Array(encodedHashedData));
        })
      );
  }

  pseudo(): string {
    return this._pseudo;
  }

  readFrom(): string {
    return this._readFrom;
  }

  secretKey(): string {
    return this._secretKey;
  }

  toObject(): any {
    return Serializer.toObject(this);
  }

  /**
   * Returns the box address to be used to write to this contact.
   */
  writeTo(): string {
    return this._writeTo;
  }

  writeWith(): ContactKey {
    return this._writeWith;
  }

  private hmacHashKey(): Observable<CryptoKey> {
    if (this._hmacCryptoKey) {
      return of(this._hmacCryptoKey);
    }

    return Hmac.keyFor(this._secretKey)
      .pipe(
        map((hmacCryptoKey: CryptoKey) => {
          this._hmacCryptoKey = hmacCryptoKey;
          return this._hmacCryptoKey;
        })
      );
  }
}
