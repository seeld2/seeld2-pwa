import {Contacts} from "./contacts";
import {Contact} from "./contact";
import {ContactKey} from "./contact-key";
import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";

describe("Contacts", () => {

  const ALICE_PSEUDO: string = "alice";
  const ALICE_DISPLAY_NAME: string = "Alice";
  const BOBBY_PSEUDO: string = "bobby";

  it("should build an instance from an array of Contact instances", () => {
    const contacts: Contacts = Contacts.fromArrayOfContacts([aContact(ALICE_PSEUDO), aContact(BOBBY_PSEUDO)]);
    expect(contacts['_contacts'][ALICE_PSEUDO]).toBeDefined();
    expect(contacts['_contacts'][ALICE_PSEUDO].constructor.name).toEqual('Contact');
    expect(contacts['_contacts'][BOBBY_PSEUDO]).toBeDefined();
    expect(contacts['_contacts'][BOBBY_PSEUDO].constructor.name).toEqual('Contact');
  });

  it("should build an instance from a map of Contacts", () => {

    const mapOfContacts: any = {};
    mapOfContacts[ALICE_PSEUDO] = aContact(ALICE_PSEUDO);
    mapOfContacts[BOBBY_PSEUDO] = aContact(BOBBY_PSEUDO);

    const contacts: Contacts = Contacts.of(mapOfContacts);

    expect(contacts['_contacts'][ALICE_PSEUDO]).toBeDefined();
    expect(contacts['_contacts'][ALICE_PSEUDO].pseudo()).toEqual(ALICE_PSEUDO);
    expect(contacts['_contacts'][BOBBY_PSEUDO]).toBeDefined();
    expect(contacts['_contacts'][BOBBY_PSEUDO].pseudo()).toEqual(BOBBY_PSEUDO);
  });

  it("should build an instance from a map of JavaScript objects representing contacts", () => {

    const object: any = {};
    object[ALICE_PSEUDO] = aContactObject(ALICE_PSEUDO);
    object[BOBBY_PSEUDO] = aContactObject(BOBBY_PSEUDO);

    const contacts: Contacts = Contacts.fromObject(object);

    expect(contacts['_contacts'][ALICE_PSEUDO].pseudo()).toEqual(ALICE_PSEUDO);
    expect(contacts['_contacts'][BOBBY_PSEUDO].pseudo()).toEqual(BOBBY_PSEUDO);
  });

  it("should return the contact with the specified pseudo", () => {
    const aliceContact: Contact = aContact(ALICE_PSEUDO);
    const bobbyContact: Contact = aContact(BOBBY_PSEUDO);
    const contacts: Contacts = Contacts.fromArrayOfContacts([aliceContact, bobbyContact]);
    expect(contacts.contact(ALICE_PSEUDO)).toEqual(aliceContact);
  });

  it("should return the contacts as an array", () => {

    const contacts: Contacts = Contacts.of({
      zzz: aContact("zzz", "BBB"),
      aaa: aContact("aaa", "AAA"),
      ccc: aContact("ccc", "")
    });

    const arrayOfContacts: Contact[] = contacts.contacts();

    expect(arrayOfContacts.length).toEqual(3);
    expect(arrayOfContacts[0].pseudo()).toEqual("zzz");
    expect(arrayOfContacts[1].pseudo()).toEqual("aaa");
    expect(arrayOfContacts[2].pseudo()).toEqual("ccc");
  });

  it("should sort the contacts by their description", () => {

    const contacts: Contacts = Contacts.of({
      zzz: aContact("zzz", "BBB"),
      aaa: aContact("aaa", "AAA"),
      ccc: aContact("ccc", "")
    });

    const sorted = contacts.contactsSortedByDescription();

    expect(sorted[0].pseudo()).toEqual("aaa");
    expect(sorted[1].pseudo()).toEqual("zzz");
    expect(sorted[2].pseudo()).toEqual("ccc");
  });

  it("should describe an array of contacts", () => {

    const mapOfContacts: any = {};
    mapOfContacts[ALICE_PSEUDO] = aContact(ALICE_PSEUDO, ALICE_DISPLAY_NAME);
    mapOfContacts[BOBBY_PSEUDO] = aContact(BOBBY_PSEUDO, "");
    const contacts: Contacts = Contacts.of(mapOfContacts);

    const contactsDescription: string = contacts.describeContacts();

    expect(contactsDescription).toEqual(`${ALICE_DISPLAY_NAME},${BOBBY_PSEUDO}`);
  });

  describe(", when checking whether two Contacts containers are equal", () => {

    it("should return true if both containers contain the same contacts", () => {
      const these: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob'), aContact('carol', 'Carol')]);
      const others: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob'), aContact('carol', 'Carol')]);
      expect(these.areEqualTo(others)).toBe(true);
    });

    it("should return false if the number of contacts differ in the containers", () => {
      const these: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob'), aContact('carol', 'Carol')]);
      const others: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob')]);
      expect(these.areEqualTo(others)).toBe(false);
    });

    it("should return false if the contacts (their pseudos) differ in the containers", () => {
      const these: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob'), aContact('carol', 'Carol')]);
      const others: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob'), aContact('danny', 'Danny')]);
      expect(these.areEqualTo(others)).toBe(false);
    });

    it("should return false if the contacts inside differ", () => {
      const these: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'Bob')]);
      const others: Contacts = Contacts.fromArrayOfContacts([aContact('bobby', 'The other Bob')]);
      expect(these.areEqualTo(others)).toBe(false);
    });
  });

  it("should provide the contact having the specified 'read from' box address'", () => {

    const contacts: Contacts = Contacts.of({
      contactA: aContactWithReadFromAndSecretKey("contactA", "readFromA", "secretKeyA"),
      contactB: aContactWithReadFromAndSecretKey("contactB", "readFromB", "secretKeyB")
    });

    const contact: Contact = contacts.contactWithReadFromBoxAddress("readFromB");

    expect(contact.pseudo()).toEqual("contactB");
  });

  describe(", when fetching contacts' hmac hashes by the \"read from\" box address,", () => {

    it("should provide all of the contacts' hashes by \"read from\" box addresses", (done: DoneFn) => {

      const contacts: Contacts = Contacts.of({
        contactA: aContactWithReadFromAndSecretKey("contactA", "readFromA", "aSecretKey"),
        contactB: aContactWithReadFromAndSecretKey("contactB", "readFromB", "bSecretKey")
      });

      contacts.hmacHashesByReadFromBoxAddresses().subscribe((hmacHashesByBoxAddress: HmacHashesByBoxAddress) => {
        const object = hmacHashesByBoxAddress.asObject();

        expect(object['readFromA'])
          .toEqual('usY9MMyN1RZAYlkDWIHZBa4zQT/CiU95H8KDM4goTuRcA7SXan12d/kYdY38DAiXu+0CQeamkCIB0HsnoXs9Cg==');
        expect(object['readFromB'])
          .toEqual('9v8BPjseNQxUFbBpHDx8wU/Pkfn6FnTh/WXBeGXFU05AKmddqxxC6l9ARIDdmaWqpKyZH+8zRvqvBaYZJZqZEQ==');

        done();
      });
    });

    it("should provide only the hashes for the specified \"read from\" box addresses", (done: DoneFn) => {

      const contacts: Contacts = Contacts.of({
        contactA: aContactWithReadFromAndSecretKey("contactA", "readFromA", "aSecretKey"),
        contactB: aContactWithReadFromAndSecretKey("contactB", "readFromB", "bSecretKey")
      });

      contacts.hmacHashesByReadFromBoxAddresses(["contactB"])
        .subscribe((hmacHashesByBoxAddress: HmacHashesByBoxAddress) => {
          const object = hmacHashesByBoxAddress.asObject();
          expect(object['readFromB'])
            .toEqual('9v8BPjseNQxUFbBpHDx8wU/Pkfn6FnTh/WXBeGXFU05AKmddqxxC6l9ARIDdmaWqpKyZH+8zRvqvBaYZJZqZEQ==');
          done();
        });
    });
  });

  it("should extract all the pseudos from an array of contacts", () => {

    const mapOfContacts: any = {};
    mapOfContacts[ALICE_PSEUDO] = aContact(ALICE_PSEUDO, ALICE_DISPLAY_NAME);
    mapOfContacts[BOBBY_PSEUDO] = aContact(BOBBY_PSEUDO, "");
    const contacts: Contacts = Contacts.of(mapOfContacts);

    const pseudos: string[] = contacts.pseudos();

    expect(pseudos.length).toEqual(2);
    expect(pseudos).toContain(ALICE_PSEUDO);
    expect(pseudos).toContain(BOBBY_PSEUDO);
  });

  it("should set a contact", () => {
    const contacts: Contacts = Contacts.fromArrayOfContacts([aContact(ALICE_PSEUDO)]);
    expect(Object.keys(contacts['_contacts']).length).toEqual(1);

    const contactsToAdd = aContact(BOBBY_PSEUDO);
    contacts.setContact(contactsToAdd);
    expect(Object.keys(contacts['_contacts']).length).toEqual(2);
    expect(contacts['_contacts'][BOBBY_PSEUDO]).toEqual(contactsToAdd);
  });

  it("should provide a serialized representation of itself", () => {

    const mapOfContacts: any = {};
    mapOfContacts[ALICE_PSEUDO] = aContact(ALICE_PSEUDO);
    mapOfContacts[BOBBY_PSEUDO] = aContact(BOBBY_PSEUDO);
    const contacts: Contacts = Contacts.of(mapOfContacts);

    const object: any = contacts.toObject();

    expect(Object.keys(object).length).toEqual(2);
    expect(object[ALICE_PSEUDO].pseudo).toEqual(ALICE_PSEUDO);
    expect(object[ALICE_PSEUDO].writeWith.constructor.name).toEqual('Object');
    expect(object[BOBBY_PSEUDO].pseudo).toEqual(BOBBY_PSEUDO);
    expect(object[BOBBY_PSEUDO].writeWith.constructor.name).toEqual('Object');
  });

  function aContact(pseudo: string, displayName: string = null): Contact {
    return Contact.with(pseudo, displayName, null, null, ContactKey.with(null, null, null), null, 123456789);
  }

  function aContactObject(pseudo: string): any {
    return {
      pseudo: pseudo,
      displayName: null,
      readFrom: null,
      writeTo: null,
      writeWith: {id: null, type: null, publicKeys: null},
      secretKey: null,
      connectedSince: 123456789
    };
  }

  function aContactWithReadFromAndSecretKey(pseudo: string, readFrom: string, secretKey: string) {
    return Contact.with(pseudo, pseudo, readFrom, null, ContactKey.with(null, null, null), secretKey, 123456789);
  }
});
