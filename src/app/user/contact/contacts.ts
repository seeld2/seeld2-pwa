import {Contact} from "./contact";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";
import * as _ from 'lodash';
import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";
import {Observable} from "rxjs/Observable";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {map, mergeMap, toArray} from "rxjs/operators";
import {forkJoin} from "rxjs/observable/forkJoin";

export class Contacts implements Serializable {

  static empty(): Contacts {
    return Contacts.of({});
  }

  static fromArrayOfContacts(contacts: Contact[]): Contacts {
    const mapOfContacts: any = {};
    contacts.forEach((contact: Contact) => mapOfContacts[contact.pseudo()] = contact);
    return Contacts.of(mapOfContacts);
  }

  static fromObject(object: any): Contacts {
    const contacts: any = {};
    Object.keys(object).forEach((pseudo: string) => contacts[pseudo] = Contact.fromObject(object[pseudo]));
    return Contacts.of(contacts);
  }

  static of(mapOfContactsByPseudo: any): Contacts {
    return new Contacts(mapOfContactsByPseudo);
  }

  private constructor(private readonly _contacts: any) {
  }

  areEqualTo(otherContacts: Contacts) {
    return _.isEqual(this.toObject(), otherContacts.toObject())
  }

  asObject(): any {
    return this._contacts;
  }

  contact(pseudo: string): Contact {
    return this._contacts[pseudo];
  }

  contacts(): Contact[] {
    return Object.keys(this._contacts).map((pseudo: string) => this._contacts[pseudo]);
  }

  contactsAsMapOfContactsByPseudo(): any {
    return this._contacts;
  }

  /**
   * Naturally sorts contacts by ascending description.
   *
   * @see {@link Contact.description} for the content of what is being sorted
   */
  contactsSortedByDescription(): Contact[] {
    return this.contacts()
      .sort((pc1: Contact, pc2: Contact) => pc1.description().localeCompare(pc2.description()));
  }

  contactWithReadFromBoxAddress(readFromBoxAddress: string): Contact {
    return this.contacts().find((contact: Contact) => contact.readFrom() === readFromBoxAddress);
  }

  delete(pseudo: string) {
    delete this._contacts[pseudo];
  }

  /**
   * Returns a string that describes the specified contacts.
   */
  describeContacts(): string {
    return Object.keys(this._contacts)
      .map((pseudo: string) => this._contacts[pseudo].description())
      .join(',');
  }

  /**
   * @param pseudos If an array of pseudos is specified, only the hmac hashes for those pseudos are returned.
   *                Pseudos not matching a session contact are ignored.
   */
  hmacHashesByReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {

    const contacts: Contact[] = pseudos
      ? this.contactsWithPseudos(pseudos)
      : this.contacts();

    return from(contacts)
      .pipe(
        mergeMap((contact: Contact) => forkJoin([
          of(contact.readFrom()),
          contact.hmacHash(contact.readFrom())
        ])),
        toArray(),
        map(arrayOfResultPairs => {
          const hmacHashesByBoxAddress: any = {};
          arrayOfResultPairs.forEach(resultPair => hmacHashesByBoxAddress[resultPair[0]] = resultPair[1]);
          return HmacHashesByBoxAddress.with(hmacHashesByBoxAddress);
        })
      );
  }

  pseudos(): string[] {
    return Object.keys(this._contacts)
      .map((pseudo: string) => this._contacts[pseudo].pseudo());
  }

  setContact(contact: Contact) {
    return this._contacts[contact.pseudo()] = contact;
  }

  toObject(): any {
    return Serializer.toObject(this);
  }

  private contactsWithPseudos(pseudos: string[]): Contact[] {
    return Object.keys(this._contacts)
      .filter((pseudo: string) => pseudos.indexOf(pseudo) >= 0)
      .map((pseudo: string) => this._contacts[pseudo]);
  }
}
