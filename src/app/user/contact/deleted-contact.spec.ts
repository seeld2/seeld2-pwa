import {Serializer} from "../../utils/serialization/serializer";
import {DeletedContact} from "./deleted-contact";

describe("DeletedContact", () => {

  const READ_FROM: string = "readFrom";
  const SECRET_KEY: string = "secretKey";

  it("should provide an instance from a plain object", () => {
    const deletedContact: DeletedContact = DeletedContact.fromObject({readFrom: READ_FROM, secretKey: SECRET_KEY});
    expect(deletedContact['_readFrom']).toEqual(READ_FROM);
    expect(deletedContact['_secretKey']).toEqual(SECRET_KEY);
  });

  it("should express itself as a plain object", () => {
    const deletedContact: DeletedContact = DeletedContact.fromObject({readFrom: READ_FROM, secretKey: SECRET_KEY});
    const object: any = Serializer.toObject(deletedContact);
    expect(object).toEqual({readFrom: READ_FROM, secretKey: SECRET_KEY});
  });

  it("should build an instance using its values", () => {
    const deletedContact: DeletedContact = DeletedContact.with(READ_FROM, SECRET_KEY);
    expect(deletedContact['_readFrom']).toEqual(READ_FROM);
    expect(deletedContact['_secretKey']).toEqual(SECRET_KEY);
  });

  describe(", when generating a HMAC hash,", () => {

    it("should generate the hash the supplied text if the contact has a secret key", (done: DoneFn) => {
      const deletedContact: DeletedContact = DeletedContact.with(READ_FROM, SECRET_KEY);
      deletedContact.hmacHash().subscribe((hmacHash: string) => {
        expect(hmacHash)
          .toEqual("Kvo0Zxao89r/xT4rHrP2V5WJXTmifvz9J/SXkj8/LN4tSHODxQkKyR9ist+QgLMfsV1dXOsGiCT4+MsALHDMYg==");
        done();
      });
    });

    it("should return 'null' if the contact has no secret key to generate the hash", (done: DoneFn) => {
      const deletedContact: DeletedContact = DeletedContact.with(READ_FROM, null);
      deletedContact.hmacHash().subscribe((hmacHash: string) => {
        expect(hmacHash).toBeNull();
        done();
      });
    });

    it("should cache the crypto key", (done: DoneFn) => {
      const deletedContact: DeletedContact = DeletedContact.with(READ_FROM, SECRET_KEY);
      deletedContact.hmacHash().subscribe(() => {
        expect(deletedContact['_hmacCryptoKey']).toBeDefined();
        done();
      });
    });
  });
});
