import {DeletedContact} from "./deleted-contact";

export class DeletedContactTestBuilder {

  public static readonly READ_FROM: string = "readFrom";
  public static readonly SECRET_KEY: string = "secretKey";

  static with(): DeletedContactTestBuilder {
    return new DeletedContactTestBuilder();
  }

  and = this;

  private _readFrom: string;
  private _secretKey: string;

  private constructor() {
  }

  asIs(): DeletedContact {
    return DeletedContact.with(this._readFrom, this._secretKey);
  }

  noBlanks(): DeletedContact {
    return DeletedContact.with(
      this._readFrom ? this._readFrom : DeletedContactTestBuilder.READ_FROM,
      this._secretKey ? this._secretKey : DeletedContactTestBuilder.SECRET_KEY
    );
  }

  readFrom(readFrom: string): DeletedContactTestBuilder {
    this._readFrom = readFrom;
    return this;
  }

  secretKey(secretKey: string): DeletedContactTestBuilder {
    this._secretKey = secretKey;
    return this;
  }
}
