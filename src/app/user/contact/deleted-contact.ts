import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {map, mergeMap} from "rxjs/operators";
import {Hmac} from "../../crypto/hmac/hmac";
import {Serializable} from "../../utils/serialization/serializable";

declare var base64js: any;
declare var TextEncoder: any;

export class DeletedContact implements Serializable {

  static fromObject(object: any): DeletedContact {
    return new DeletedContact(object.readFrom, object.secretKey);
  }

  static with(readFrom: string, secretKey: string) {
    return new DeletedContact(readFrom, secretKey);
  }

  private _hmacCryptoKey: CryptoKey;

  private constructor(private readonly _readFrom: string,
                      private readonly _secretKey: string) {
  }

  asObject(): any {
    return {readFrom: this._readFrom, secretKey: this._secretKey};
  }

  hmacHash(): Observable<string> {
    if (!this._secretKey) {
      return of(null);
    }

    return this.hmacHashKey()
      .pipe(
        mergeMap((cryptoKey: CryptoKey) => {
          const encodedData = new TextEncoder().encode(this._readFrom);
          return window.crypto.subtle.sign("HMAC", cryptoKey, encodedData);
        }),
        map((encodedHashedData: ArrayBuffer) => {
          return base64js.fromByteArray(new Uint8Array(encodedHashedData));
        })
      );
  }

  readFrom(): string {
    return this._readFrom;
  }

  private hmacHashKey(): Observable<CryptoKey> {
    if (this._hmacCryptoKey) {
      return of(this._hmacCryptoKey);
    }

    return Hmac.keyFor(this._secretKey)
      .pipe(
        map((hmacCryptoKey: CryptoKey) => {
          this._hmacCryptoKey = hmacCryptoKey;
          return this._hmacCryptoKey;
        })
      );
  }
}
