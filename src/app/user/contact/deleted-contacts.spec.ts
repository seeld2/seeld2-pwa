import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";
import {ContactTestBuilder as aContact} from "./contact.test-builder";
import {DeletedContact} from "./deleted-contact";
import {DeletedContacts} from "./deleted-contacts";

describe("DeletedContacts", () => {

  const CAROL_PSEUDO: string = "carol";
  const CAROL_READ_FROM: string = "carolReadFrom";
  const CAROL_SECRET_KEY: string = "carolSecretKey";

  const DAVID_PSEUDO: string = "david";
  const DAVID_READ_FROM: string = "davidReadFrom";
  const DAVID_SECRET_KEY: string = "davidSecretKey";

  it("should provide an empty instance", () => {
    const deletedContacts: DeletedContacts = DeletedContacts.empty();
    expect(deletedContacts['_deletedContacts']).toEqual({});
  });

  it("should provide an instance from an object", () => {
    const object: any = {"carol": {readFrom: CAROL_READ_FROM, secretKey: CAROL_SECRET_KEY}};
    const deletedContacts: DeletedContacts = DeletedContacts.fromObject(object);
    expect(deletedContacts['_deletedContacts']).toBeDefined();
    expect(deletedContacts['_deletedContacts'][CAROL_PSEUDO]).toBeDefined();
    expect(deletedContacts['_deletedContacts'][CAROL_PSEUDO]['_readFrom']).toEqual(CAROL_READ_FROM);
    expect(deletedContacts['_deletedContacts'][CAROL_PSEUDO]['_secretKey']).toEqual(CAROL_SECRET_KEY);
  });

  it("should provide an instance from a null object", () => {
    const object: any = null;
    const deletedContacts: DeletedContacts = DeletedContacts.fromObject(object);
    expect(deletedContacts).toBeDefined();
    expect(deletedContacts.toObject()).toBeDefined();
  });

  describe(", when adding a deleted contact,", () => {

    it("should add a deleted contact to an empty instance", () => {
      const deletedContacts: DeletedContacts = DeletedContacts.empty();

      deletedContacts
        .add(aContact.with().pseudo(CAROL_PSEUDO).readFrom(CAROL_READ_FROM).and.secretKey(CAROL_SECRET_KEY).noBlanks());

      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]['_readFrom']).toEqual(CAROL_READ_FROM);
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]['_secretKey']).toEqual(CAROL_SECRET_KEY);
    });

    it("should add a deleted contact to an instance having other deleted contacts", () => {
      const deletedContacts: DeletedContacts = DeletedContacts
        .fromObject({"carol.1": {readFrom: CAROL_READ_FROM, secretKey: CAROL_SECRET_KEY}});

      deletedContacts
        .add(aContact.with().pseudo(DAVID_PSEUDO).readFrom(DAVID_READ_FROM).and.secretKey(DAVID_SECRET_KEY).noBlanks());

      expect(Object.keys(deletedContacts['_deletedContacts']).length).toEqual(2);
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${DAVID_PSEUDO}.1`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${DAVID_PSEUDO}.1`]['_readFrom']).toEqual(DAVID_READ_FROM);
      expect(deletedContacts['_deletedContacts'][`${DAVID_PSEUDO}.1`]['_secretKey']).toEqual(DAVID_SECRET_KEY);
    });

    it("should add a deleted contact to an instance having an older version of that contact", () => {
      const deletedContacts: DeletedContacts = DeletedContacts
        .fromObject({"carol.1": {readFrom: CAROL_READ_FROM, secretKey: CAROL_SECRET_KEY}});

      deletedContacts
        .add(aContact.with().pseudo(CAROL_PSEUDO).readFrom(CAROL_READ_FROM).and.secretKey(CAROL_SECRET_KEY).noBlanks());

      expect(Object.keys(deletedContacts['_deletedContacts']).length).toEqual(2);
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.2`]).toBeDefined();
    });

    it("should shave off the earliest deleted contact if the amount of deleted contacts' pseudos is above a threshold", () => {
      const deletedContacts: DeletedContacts = DeletedContacts.fromObject({
        "carol.1": {readFrom: CAROL_READ_FROM, secretKey: CAROL_SECRET_KEY},
        "carol.2": {readFrom: CAROL_READ_FROM, secretKey: CAROL_SECRET_KEY}
      });

      deletedContacts
        .add(aContact.with().pseudo(CAROL_PSEUDO).readFrom(CAROL_READ_FROM).and.secretKey(CAROL_SECRET_KEY).noBlanks());
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.2`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.3`]).toBeDefined();

      deletedContacts
        .add(aContact.with().pseudo(CAROL_PSEUDO).readFrom(CAROL_READ_FROM).and.secretKey(CAROL_SECRET_KEY).noBlanks());
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.2`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.3`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.4`]).toBeDefined();
      expect(deletedContacts['_deletedContacts'][`${CAROL_PSEUDO}.1`]).not.toBeDefined();
    });
  });

  describe(", when fetching the deleted contacts as array", () => {

    it("should return all deleted contacts", () => {
      const carolDeletedContact: DeletedContact = DeletedContact.with(CAROL_READ_FROM, CAROL_SECRET_KEY);
      const davidDeletedContact: DeletedContact = DeletedContact.with(DAVID_READ_FROM, DAVID_SECRET_KEY);

      const deletedContacts: DeletedContacts = DeletedContacts
        .fromObject({'carol.1': carolDeletedContact.asObject(), 'david.1': davidDeletedContact.asObject()});
      const array: DeletedContact[] = deletedContacts.deletedContacts();

      expect(array.length).toEqual(2);
    });

    it("should return the deleted contacts matching the specified pseudo", () => {
      const carolDeletedContact: DeletedContact = DeletedContact.with(CAROL_READ_FROM, CAROL_SECRET_KEY);
      const davidDeletedContact: DeletedContact = DeletedContact.with(DAVID_READ_FROM, DAVID_SECRET_KEY);

      const deletedContacts: DeletedContacts = DeletedContacts
        .fromObject({'carol.1': carolDeletedContact.asObject(), 'david.1': davidDeletedContact.asObject()});
      const array: DeletedContact[] = deletedContacts.deletedContacts(["carol"]);

      expect(array.length).toEqual(1);
      expect(array[0].readFrom()).toEqual(CAROL_READ_FROM);
    });
  });

  it("should return the deleted contact with the specified \"read from\" box address", () => {

    const deletedContacts: DeletedContacts = DeletedContacts.fromObject({
      contactA: {readFrom: "readFromA", secretKey: "aSecretKey"},
      contactB: {readFrom: "readFromB", secretKey: "bSecretKey"}
    });

    const deletedContact: DeletedContact = deletedContacts.deletedContactWithReadFromBoxAddress("readFromB");

    expect(deletedContact.readFrom()).toEqual("readFromB");
  });

  describe(", when fetching contacts' hmac hashes by the \"read from\" box address,", () => {

    it("should provide all of the contacts' hashes by \"read from\" box addresses", (done: DoneFn) => {

      const deletedContacts: DeletedContacts = DeletedContacts.fromObject({
        contactA: {readFrom: "readFromA", secretKey: "aSecretKey"},
        contactB: {readFrom: "readFromB", secretKey: "bSecretKey"}
      });

      deletedContacts.hmacHashesByReadFromBoxAddresses().subscribe((hmacHashesByBoxAddress: HmacHashesByBoxAddress) => {
        const object = hmacHashesByBoxAddress.asObject();

        expect(object['readFromA'])
          .toEqual('usY9MMyN1RZAYlkDWIHZBa4zQT/CiU95H8KDM4goTuRcA7SXan12d/kYdY38DAiXu+0CQeamkCIB0HsnoXs9Cg==');
        expect(object['readFromB'])
          .toEqual('9v8BPjseNQxUFbBpHDx8wU/Pkfn6FnTh/WXBeGXFU05AKmddqxxC6l9ARIDdmaWqpKyZH+8zRvqvBaYZJZqZEQ==');

        done();
      });
    });

    it("should provide only the hashes for the specified \"read from\" box addresses", (done: DoneFn) => {

      const deletedContacts: DeletedContacts = DeletedContacts.fromObject({
        "contactA.1": {readFrom: "readFromA1", secretKey: "a1SecretKey"},
        "contactA.2": {readFrom: "readFromA2", secretKey: "a2SecretKey"},
        "contactB.1": {readFrom: "readFromB1", secretKey: "b1SecretKey"}
      });

      deletedContacts.hmacHashesByReadFromBoxAddresses(["contactA"])
        .subscribe((hmacHashesByBoxAddress: HmacHashesByBoxAddress) => {
          const object = hmacHashesByBoxAddress.asObject();
          expect(Object.keys(object).length).toEqual(2);
          expect(object['readFromA1'])
            .toEqual('mkaEw104IZjAEpfh6I7LTRm6tpHiXiSQvCuAjhX+fscNURJIeQb695LDlGVsXE+ziYooDDhx1TQh67jN4wQukQ==');
          expect(object['readFromA2'])
            .toEqual('0V5ZAkxA3UeKVc9ti5PNqdoFvx+rHz/Y0b3Yg06U6A1u2MJAu85YyGEFcDP3X/OvKwmihT8r3Xuo8fTkuy9iUA==');
          done();
        });
    });
  });
});
