import {Observable} from "rxjs/Observable";
import {forkJoin} from "rxjs/observable/forkJoin";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {map, mergeMap, toArray} from "rxjs/operators";
import {HmacHashesByBoxAddress} from "../../sessions/hmac-hashes-by-box-address";
import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";
import {Contact} from "./contact";
import {DeletedContact} from "./deleted-contact";

export class DeletedContacts implements Serializable {

  private static readonly INDEXED_PSEUDO_PREFIX: string = ".";
  private static readonly MAX_DELETED_CONTACTS_PER_PSEUDO: number = 3;

  static empty(): DeletedContacts {
    return new DeletedContacts({});
  }

  static fromObject(object: any): DeletedContacts {
    if (!object) {
      return new DeletedContacts({});
    }
    const deletedContacts: any = {};
    Object.keys(object).forEach((pseudo: string) => deletedContacts[pseudo] = DeletedContact.fromObject(object[pseudo]));
    return new DeletedContacts(deletedContacts);
  }

  /**
   * @param _deletedContacts A map of {@link DeletedContact} instances by pseudo
   */
  private constructor(private readonly _deletedContacts: any) {
  }

  add(contact: Contact) {

    const indexedPseudos: string[] = Object.keys(this._deletedContacts)
      .filter((indexedPseudo: string) => indexedPseudo.startsWith(contact.pseudo()))
      .sort();

    if (indexedPseudos.length >= DeletedContacts.MAX_DELETED_CONTACTS_PER_PSEUDO) {
      const pseudoToBeShavedOff: string = indexedPseudos.shift();
      delete this._deletedContacts[pseudoToBeShavedOff];
    }

    const latestIndex: number = indexedPseudos
      .reduce((index: number, indexedPseudo: string) => {
        const deletedContactIndex: number = Number
          .parseInt(indexedPseudo.substring(indexedPseudo.lastIndexOf(DeletedContacts.INDEXED_PSEUDO_PREFIX) + 1));
        return Math.max(index, deletedContactIndex);
      }, 0);

    const indexedPseudoToAdd: string = `${contact.pseudo()}.${latestIndex + 1}`;
    this._deletedContacts[indexedPseudoToAdd] = DeletedContact.with(contact.readFrom(), contact.secretKey());
  }

  asObject(): any {
    return this._deletedContacts;
  }

  deletedContact(pseudo: string): DeletedContact {
    return this._deletedContacts[pseudo];
  }

  deletedContacts(pseudos: string[] = null): DeletedContact[] {
    if (!pseudos) {
      return Object.keys(this._deletedContacts).map((pseudo: string) => this._deletedContacts[pseudo]);
    } else {
      return Object.keys(this._deletedContacts)
        .filter((indexedPseudo: string) => {
          const indexedPseudoRoot: string = indexedPseudo.substring(0, indexedPseudo.lastIndexOf("."))
          return pseudos.indexOf(indexedPseudoRoot) >= 0;
        })
        .map((indexedPseudo: string) => this._deletedContacts[indexedPseudo]);
    }
  }

  deletedContactWithReadFromBoxAddress(readFromBoxAddress: string): DeletedContact {
    return this.deletedContacts()
      .find((deletedContact: DeletedContact) => deletedContact.readFrom() === readFromBoxAddress);
  }

  hmacHashesByReadFromBoxAddresses(pseudos: string[] = null): Observable<HmacHashesByBoxAddress> {

    const deletedContacts: DeletedContact[] = pseudos ? this.deletedContacts(pseudos) : this._deletedContacts;
    if (deletedContacts.length === 0) {
      return of(HmacHashesByBoxAddress.empty());
    }

    const deletedContactValues = Object.keys(deletedContacts).map(key => deletedContacts[key]);

    return from(deletedContactValues)
      .pipe(
        mergeMap((deletedContact: DeletedContact) => forkJoin([
          of(deletedContact.readFrom()),
          deletedContact.hmacHash()
        ])),
        toArray(),
        map(arrayOfResultPairs => {
          const hmacHashesByBoxAddress: any = {};
          arrayOfResultPairs.forEach(resultPair => hmacHashesByBoxAddress[resultPair[0]] = resultPair[1]);
          return HmacHashesByBoxAddress.with(hmacHashesByBoxAddress);
        })
      );
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
