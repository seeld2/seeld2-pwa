import {KeyPair} from "./key-pair";

describe("KeyPair", () => {

  const PRIVATE_KEYS: string = "privateKeys";
  const PUBLIC_KEYS: string = "publicKeys";
  const TYPE: string = 'ecc.x25519';

  it("should provide an instance from its properties", () => {
    const keyPair: KeyPair = KeyPair.with(TYPE, PUBLIC_KEYS, PRIVATE_KEYS);
    expect(keyPair['_type']).toEqual(TYPE);
    expect(keyPair['_privateKeys']).toEqual(PRIVATE_KEYS);
    expect(keyPair['_publicKeys']).toEqual(PUBLIC_KEYS);
  });

  it("should provide a JSON representation of itself", () => {
    const keyPair: KeyPair = KeyPair.with(TYPE, PUBLIC_KEYS, PRIVATE_KEYS);
    const json: string = keyPair.toJson();
    expect(json).toEqual('{"type":"ecc.x25519","publicKeys":"publicKeys","privateKeys":"privateKeys"}');
  });

  it("should provide a serialized representation of itself", () => {
    const keyPair: KeyPair = KeyPair.with(TYPE, PUBLIC_KEYS, PRIVATE_KEYS);
    const object: any = keyPair.toObject();
    expect(object).toEqual({type: TYPE, publicKeys: PUBLIC_KEYS, privateKeys: PRIVATE_KEYS});
  });
});
