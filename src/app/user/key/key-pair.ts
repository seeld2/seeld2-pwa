import {Serializable} from "../../utils/serialization/serializable";
import {Serializer} from "../../utils/serialization/serializer";

export class KeyPair implements Serializable {

  static fromObject(object: any) {
    return KeyPair.with(object.type, object.publicKeys, object.privateKeys);
  }

  static with(type: string, publicKeys: string, privateKeys: string): KeyPair {
    return new KeyPair(type, publicKeys, privateKeys);
  }

  private constructor(private readonly _type: string,
                      private readonly _publicKeys: string,
                      private readonly _privateKeys: string) {
  }

  asObject(): any {
    return {
      type: this._type,
      publicKeys: this._publicKeys,
      privateKeys: this._privateKeys
    };
  }

  privateKeys(): string {
    return this._privateKeys;
  }

  publicKeys(): string {
    return this._publicKeys;
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
