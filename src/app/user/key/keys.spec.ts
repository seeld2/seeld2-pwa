import {KeyPair} from "./key-pair";
import {Keys} from "./keys";

describe("Keys", () => {

  const TYPE: string = 'ecc.x25519';

  it("should provide an instance based on a map of keys by key ID, with the keys as plain JavaScript objects", () => {
    const keysByKeyId: any = {
      "alice.ecc.x25519": aKeyPair("alice").asObject(),
      "bobby.ecc.x25519": aKeyPair("bobby").asObject()
    };
    const keys: Keys = Keys.fromObject(keysByKeyId);
    expect(keys['_keyPairsByKeyId']['alice.ecc.x25519']).toBeDefined();
    expect(keys['_keyPairsByKeyId']['alice.ecc.x25519']['_publicKeys']).toEqual("alicePublicKeys");
    expect(keys['_keyPairsByKeyId']['bobby.ecc.x25519']).toBeDefined();
    expect(keys['_keyPairsByKeyId']['bobby.ecc.x25519']['_publicKeys']).toEqual("bobbyPublicKeys");
  });

  it("should provide an instance based on a map of keys by key ID, with the keys as KeyPair types", () => {
    const keysByKeyId: any = {
      "alice.ecc.x25519": aKeyPair("alice"),
      "bobby.ecc.x25519": aKeyPair("bobby")
    };
    const keys: Keys = Keys.with(keysByKeyId);
    expect(keys['_keyPairsByKeyId']['alice.ecc.x25519']).toBeDefined();
    expect(keys['_keyPairsByKeyId']['alice.ecc.x25519']['_publicKeys']).toEqual("alicePublicKeys");
  });

  it("should provide a JSON representation of itself", () => {
    const aliceKeyPair: KeyPair = aKeyPair("alice");
    const bobbyKeyPair: KeyPair = aKeyPair("bobby");
    const keys: Keys = Keys.with({"alice.ecc.x25519": aliceKeyPair, "bobby.ecc.x25519": bobbyKeyPair});
    const json: string = keys.toJson();
    expect(json).toEqual('{' +
      '"alice.ecc.x25519":{"type":"ecc.x25519","publicKeys":"alicePublicKeys","privateKeys":"alicePrivateKeys"},' +
      '"bobby.ecc.x25519":{"type":"ecc.x25519","publicKeys":"bobbyPublicKeys","privateKeys":"bobbyPrivateKeys"}' +
      '}');
  });

  it("should provide serialized representation of itself", () => {
    const aliceKeyPair: KeyPair = aKeyPair("alice");
    const bobbyKeyPair: KeyPair = aKeyPair("bobby");
    const keys: Keys = Keys.with({"alice.ecc.x25519": aliceKeyPair, "bobby.ecc.x25519": bobbyKeyPair});
    const object: any = keys.toObject();
    expect(object).toEqual({"alice.ecc.x25519": aliceKeyPair.asObject(), "bobby.ecc.x25519": bobbyKeyPair.asObject()});
  });

  function aKeyPair(pseudo: string): KeyPair {
    return KeyPair.with(TYPE, `${pseudo}PublicKeys`, `${pseudo}PrivateKeys`);
  };
});
