import {KeyPair} from "./key-pair";
import {Keys} from "./keys";

export class KeysTestBuilder {

  static with(): KeysTestBuilder {
    return new KeysTestBuilder();
  }

  private readonly _keyPairsByKeyId: any = {};

  private constructor() {
  }

  asIs(): Keys {
    return Keys.with(this._keyPairsByKeyId);
  }

  keyPairByKeyId(keyId: string, keyPair: KeyPair): KeysTestBuilder {
    this._keyPairsByKeyId[keyId] = keyPair;
    return this;
  }
}
