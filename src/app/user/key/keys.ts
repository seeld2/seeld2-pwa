import {Serializable} from "../../utils/serialization/serializable";
import {KeyPair} from "./key-pair";
import {Serializer} from "../../utils/serialization/serializer";

export class Keys implements Serializable {

  static readonly SYSTEM_IDENTIFIER: string = 'seeld';
  static readonly SYSTEM_KEYS_TYPE: string = 'ecc.x25519';

  static fromObject(object: any): Keys {
    const keyPairsByKeyId = {};
    Object.keys(object).forEach((keyId: string) => {
      keyPairsByKeyId[keyId] = KeyPair.fromObject(object[keyId]);
    });
    return Keys.with(keyPairsByKeyId);
  }

  static systemKeysId(): string {
    return `${Keys.SYSTEM_IDENTIFIER}.${Keys.SYSTEM_KEYS_TYPE}`
  }

  static systemKeysWith(publicKeys: string, privateKeys: string): Keys {
    const keyPairsByKeyId: any = {};
    keyPairsByKeyId[Keys.systemKeysId()] = KeyPair.with(Keys.SYSTEM_KEYS_TYPE, publicKeys, privateKeys);
    return Keys.with(keyPairsByKeyId);
  }

  static with(keyPairsByKeyId: any): Keys {
    return new Keys(keyPairsByKeyId);
  }

  private constructor(private readonly _keyPairsByKeyId: any) {
  }

  asObject(): any {
    return this._keyPairsByKeyId;
  }

  keyPairsByKeyId(): any {
    return this._keyPairsByKeyId;
  }

  keyPairWithKeyId(keyId: string): KeyPair {
    return this._keyPairsByKeyId[keyId];
  }

  systemKeyPair(): KeyPair {
    return this._keyPairsByKeyId[Keys.systemKeysId()];
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  toObject(): any {
    return Serializer.toObject(this._keyPairsByKeyId);
  }
}
