import {DeletedContact} from "./contact/deleted-contact";
import {DeletedContacts} from "./contact/deleted-contacts";
import {UserPayload} from "./user-payload";
import {Contact} from "./contact/contact";
import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {Keys} from "./key/keys";
import {ContactKey} from "./contact/contact-key";
import {Contacts} from "./contact/contacts";
import {KeyPair} from "./key/key-pair";
import {UserPayloadTestBuilder as aUserPayload} from "./user-payload.test.builder";

describe("UserPayload", () => {

  const BOBBY_PSEUDO: string = 'bobby';
  const BOBBY_SECRET_KEY: string = "carolSecretKey";

  const CAROL_PSEUDO: string = "carol";
  const CAROL_READ_FROM: string = "carolReadFrom";
  const CAROL_SECRET_KEY: string = "carolSecretKey";

  const ARMORED_KEY_PAIR: ArmoredKeyPair = {privateKeys: 'privateKeys', publicKeys: 'publicKeys'};
  const CONTACTS: Contacts = Contacts.of({"bobby": aContact(BOBBY_PSEUDO)})
  const DELETED_CONTACTS: DeletedContacts = DeletedContacts
    .fromObject({carol: {readFrom: "carolReadFrom", secretKey: "carolSecretKey"}});
  const DISPLAY_NAME = "display name";
  const KEYS: Keys = Keys.fromObject({
    "seeld.ecc.x25519": {type: 'ecc.x25519', privateKeys: 'privateKeys', publicKeys: 'publicKeys'}
  });
  const PICTURE = "picture";
  const SECRET_KEY: string = "secretKey";
  const SYSTEM_KEYS: KeyPair = KeyPair.with('ecc.x25519', 'publicKeys', 'privateKeys');

  describe(", when building a new instance,", () => {

    it("should build a user payload from a JSON string", () => {
      const userPayload: UserPayload = UserPayload.fromJson('{' +
        '"contacts":{"bobby":{"pseudo":"bobby","displayName":null,"readFrom":null,"writeTo":null,"writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":"bobSecretKey","connectedSince":123456789}},"keys":{"seeld.ecc.x25519":{"type":"ecc.x25519","privateKeys":"privateKeys","publicKeys":"publicKeys"}},' +
        '"deletedContacts":{"carol":{"readFrom":"carolReadFrom","secretKey":"carolSecretKey"}},' +
        '"secretKey":"secretKey"}');
      expect(userPayload.contact(BOBBY_PSEUDO)).toBeDefined();
      expect(userPayload.contact(BOBBY_PSEUDO).secretKey()).toEqual("bobSecretKey")
      expect(userPayload.deletedContacts()).toEqual(DELETED_CONTACTS);
      expect(userPayload.secretKey()).toEqual(SECRET_KEY);
      expect(userPayload.systemKeys()).toEqual(SYSTEM_KEYS);
    });

    it("should build an instance from a plain JavaScript object representing a payload", () => {
      const contactsObject = CONTACTS.toObject();
      const userPayload: UserPayload = UserPayload.fromObject({
        contacts: contactsObject,
        deletedContacts: DELETED_CONTACTS.toObject(),
        keys: KEYS.toObject(),
        secretKey: SECRET_KEY
      });
      expect(userPayload.contact(BOBBY_PSEUDO)).toBeDefined();
      expect(userPayload.deletedContacts()).toEqual(DELETED_CONTACTS);
      expect(userPayload.secretKey()).toEqual(SECRET_KEY);
      expect(userPayload.systemKeys()).toEqual(SYSTEM_KEYS);
    });

    it("should build an instance with Contacts and Keys", () => {
      const userPayload: UserPayload = UserPayload.with(CONTACTS, DELETED_CONTACTS, DISPLAY_NAME, KEYS, PICTURE, SECRET_KEY);
      expect(userPayload.contact(BOBBY_PSEUDO)).toBeDefined();
      expect(userPayload.secretKey()).toEqual(SECRET_KEY);
      expect(userPayload.systemKeys()).toEqual(SYSTEM_KEYS);
    });

    it("should build an instance of a user payload without contacts using the specified system keys", () => {
      const userPayload: UserPayload = UserPayload.withoutContacts(DISPLAY_NAME, PICTURE, ARMORED_KEY_PAIR, SECRET_KEY);
      expect(userPayload).toBeDefined();
      expect(userPayload.secretKey()).toEqual(SECRET_KEY);
      expect(userPayload.systemKeys()).toEqual(SYSTEM_KEYS);
    });
  });

  it("should implement Serializable's asObject()", () => {
    const userPayload: UserPayload = aUserPayload.with()
      .contact(aContact(BOBBY_PSEUDO))
      .deletedContact(CAROL_PSEUDO, DeletedContact.with(CAROL_READ_FROM, CAROL_SECRET_KEY))
      .defaultSystemKeyPair()
      .secretKey(BOBBY_SECRET_KEY)
      .asIs();

    const object: any = userPayload.asObject();

    expect(object.contacts).toBeDefined();
    expect(object.deletedContacts).toBeDefined();
    expect(object.keys).toBeDefined();
    expect(object.secretKey).toBeDefined();
  });

  it("should return a contact with the specified pseudo", () => {
    const userPayload: UserPayload = UserPayload.with(CONTACTS, DELETED_CONTACTS, DISPLAY_NAME, KEYS, PICTURE, SECRET_KEY);
    expect(userPayload.contact(BOBBY_PSEUDO).pseudo()).toEqual(BOBBY_PSEUDO);
  });

  it("should return the contacts", () => {
    const userPayload: UserPayload = UserPayload.with(CONTACTS, DELETED_CONTACTS, DISPLAY_NAME, KEYS, PICTURE, SECRET_KEY);
    const contacts: Contacts = userPayload.contacts();
    expect(contacts.contact(BOBBY_PSEUDO)).toBeDefined();
  });

  it("should return the system keys", () => {
    const userPayload: UserPayload = UserPayload.withoutContacts(DISPLAY_NAME, PICTURE, ARMORED_KEY_PAIR, SECRET_KEY);
    const systemKeys = userPayload.systemKeys();
    expect(systemKeys).toEqual(SYSTEM_KEYS);
  });

  it("should be expressible as a JSON string", () => {
    const userPayload: UserPayload = UserPayload.with(CONTACTS, DELETED_CONTACTS, DISPLAY_NAME, KEYS, PICTURE, SECRET_KEY);

    const json: string = userPayload.toJson();

    expect(json).toEqual(`{"contacts":{"bobby":{"pseudo":"bobby","displayName":null,"readFrom":null,"writeTo":null,"writeWith":{"id":null,"type":null,"publicKeys":null},"secretKey":null,"connectedSince":123456789}},"deletedContacts":{"carol":{"readFrom":"carolReadFrom","secretKey":"carolSecretKey"}},"displayName":"${DISPLAY_NAME}","keys":{"seeld.ecc.x25519":{"type":"ecc.x25519","publicKeys":"publicKeys","privateKeys":"privateKeys"}},"picture":"${PICTURE}","secretKey":"secretKey"}`);
  });

  it("should be expressible as a plain JavaScript object", () => {
    const userPayload: UserPayload = UserPayload.with(CONTACTS, DELETED_CONTACTS, DISPLAY_NAME, KEYS, PICTURE, SECRET_KEY);

    const object: any = userPayload.toObject();

    const expected = {
      contacts: {
        bobby: {
          pseudo: "bobby",
          displayName: null,
          readFrom: null,
          writeTo: null,
          writeWith: {id: null, type: null, publicKeys: null},
          secretKey: null,
          connectedSince: 123456789
        }
      },
      deletedContacts: {
        carol: {
          readFrom: CAROL_READ_FROM,
          secretKey: CAROL_SECRET_KEY
        }
      },
      displayName: DISPLAY_NAME,
      keys: {
        "seeld.ecc.x25519": {
          type: "ecc.x25519",
          publicKeys: "publicKeys",
          privateKeys: "privateKeys"
        }
      },
      picture: PICTURE,
      secretKey: SECRET_KEY
    };
    expect(object).toEqual(expected);
  });

  function aContact(pseudo: string): Contact {
    return Contact.with(pseudo, null, null, null, ContactKey.with(null, null, null), null, 123456789);
  }
});
