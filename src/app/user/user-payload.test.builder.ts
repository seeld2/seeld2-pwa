import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {Contact} from "./contact/contact";
import {Contacts} from "./contact/contacts";
import {DeletedContact} from "./contact/deleted-contact";
import {DeletedContacts} from "./contact/deleted-contacts";
import {Keys} from "./key/keys";
import {UserPayload} from "./user-payload";

export class UserPayloadTestBuilder {

  public static readonly DISPLAY_NAME: string = "display name";
  public static readonly PICTURE: string = "picture";
  public static readonly SECRET_KEY: string = "secretKey";
  public static readonly SYSTEM_PRIVATE_KEYS: string = "systemPrivateKeys";
  public static readonly SYSTEM_PUBLIC_KEYS: string = "systemPublicKeys";

  static with(): UserPayloadTestBuilder {
    return new UserPayloadTestBuilder();
  }

  static withDefaultSystemKeyPair(): UserPayload {
    return UserPayload.withoutContacts(
      UserPayloadTestBuilder.DISPLAY_NAME,
      UserPayloadTestBuilder.PICTURE,
      <ArmoredKeyPair>{
        privateKeys: UserPayloadTestBuilder.SYSTEM_PRIVATE_KEYS,
        publicKeys: UserPayloadTestBuilder.SYSTEM_PUBLIC_KEYS
      },
      UserPayloadTestBuilder.SECRET_KEY
    );
  }

  and = this;

  private _contactsArray: Contact[] = [];
  private _deletedContacts: any = {};
  private _keys: Keys;
  private _secretKey: string

  private constructor() {
  }

  asIs(): UserPayload {
    return UserPayload.with(
      Contacts.fromArrayOfContacts(this._contactsArray),
      DeletedContacts.fromObject(this._deletedContacts),
      UserPayloadTestBuilder.DISPLAY_NAME,
      this._keys,
      UserPayloadTestBuilder.PICTURE,
      this._secretKey);
  }

  contact(contact: Contact): UserPayloadTestBuilder {
    this._contactsArray.push(contact);
    return this;
  }

  deletedContact(pseudo: string, deletedContact: DeletedContact): UserPayloadTestBuilder {
    this._deletedContacts[pseudo] = deletedContact;
    return this;
  }

  defaultSystemKeyPair(): UserPayloadTestBuilder {
    this._keys = Keys
      .systemKeysWith(UserPayloadTestBuilder.SYSTEM_PUBLIC_KEYS, UserPayloadTestBuilder.SYSTEM_PRIVATE_KEYS);
    return this;
  }

  secretKey(secretKey: string): UserPayloadTestBuilder {
    this._secretKey = secretKey;
    return this;
  }
}
