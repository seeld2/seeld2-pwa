import {ArmoredKeyPair} from "../crypto/openpgp/armored-key-pair";
import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";
import {Contact} from "./contact/contact";
import {Contacts} from "./contact/contacts";
import {DeletedContacts} from "./contact/deleted-contacts";
import {KeyPair} from "./key/key-pair";
import {Keys} from "./key/keys";

export class UserPayload implements Serializable {

  static fromJson(json: string): UserPayload {
    const object: any = JSON.parse(json);
    return UserPayload.fromObject(object);
  }

  static fromObject(object: any): UserPayload {
    return UserPayload.with(
      Contacts.fromObject(object.contacts),
      DeletedContacts.fromObject(object.deletedContacts),
      object.displayName,
      Keys.fromObject(object.keys),
      object.picture,
      object.secretKey,
    );
  }

  static with(contacts: Contacts,
              deletedContacts: DeletedContacts,
              displayName: string,
              keys: Keys,
              picture: string,
              secretKey: string): UserPayload {
    return new UserPayload(contacts, deletedContacts, displayName, keys, picture, secretKey);
  }

  static withoutContacts(displayName: string,
                         picture: string,
                         systemArmoredKeyPairs: ArmoredKeyPair,
                         secretKey: string): UserPayload {
    return UserPayload.with(
      Contacts.empty(),
      DeletedContacts.empty(),
      displayName,
      Keys.systemKeysWith(systemArmoredKeyPairs.publicKeys, systemArmoredKeyPairs.privateKeys),
      picture,
      secretKey);
  }

  private constructor(private readonly _contacts: Contacts,
                      private readonly _deletedContacts: DeletedContacts,
                      private readonly _displayName: string,
                      private readonly _keys: Keys,
                      private readonly _picture: string,
                      private readonly _secretKey: string) {
  }

  asObject() {
    return {
      contacts: this._contacts,
      deletedContacts: this._deletedContacts,
      displayName: this._displayName,
      keys: this._keys,
      picture: this._picture,
      secretKey: this._secretKey
    };
  }

  contact(pseudo: string): Contact {
    return this._contacts.contact(pseudo);
  }

  contacts(): Contacts {
    return this._contacts;
  }

  deletedContacts(): DeletedContacts {
    return this._deletedContacts;
  }

  displayName(): string {
    return this._displayName;
  }

  keys(): Keys {
    return this._keys;
  }

  picture(): string {
    return this._picture;
  }

  secretKey(): string {
    return this._secretKey;
  }

  systemKeys(): KeyPair {
    return this._keys.keyPairWithKeyId(Keys.systemKeysId());
  }

  toJson(): string {
    return Serializer.toJson(this);
  }

  toObject(): any {
    return Serializer.toObject(this);
  }
}
