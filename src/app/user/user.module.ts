import {NgModule} from "@angular/core";
import {CryptoModule} from "../crypto/crypto.module";
import {UtilsModule} from "../utils/utils.module";

@NgModule({
  imports: [
    CryptoModule,
    UtilsModule
  ]
})
export class UserModule {
}
