import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class NewUserEvent implements Serializable {

  static for(boxAddress: string, hmacHash: string): NewUserEvent {
    return new NewUserEvent(boxAddress, hmacHash);
  }

  private constructor(private readonly _boxAddress: string,
                      private readonly _hmacHash: string) {
  }

  asObject(): any {
    return {boxAddress: this._boxAddress, hmacHash: this._hmacHash};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
