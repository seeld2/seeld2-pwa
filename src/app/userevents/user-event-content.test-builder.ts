import {UserEventContent} from "./user-event-content";
import {UserEventReturn} from "./user-event-return";

export class UserEventContentTestBuilder {

  static return(): UserEventReturn {
    return <UserEventReturn>{
      toJson(): string {
        return "{\"type\":\"return\"}";
      }
    };
  }

  static toSend(): UserEventContent {
    return <UserEventContent>{
      toJson(): string {
        return "{\"type\":\"return\"}";
      }
    };
  }
}
