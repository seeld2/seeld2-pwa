/**
 * This interface indicates that the implementor incarnates a UserEvent's content.
 * As such it implements the capacity of the implementor to be expressed as a simple JavaScript object.
 */
export interface UserEventContent {
  toJson(): string;
}
