/**
 * An interface to exchange events between two users, such as a contact request.
 */
export interface UserEventRequest {
  boxAddressPayload: number[];
  payload: string;
  returnPayload: string;
}
