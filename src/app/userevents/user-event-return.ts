import {UserEventType} from "./user-event-type";
import {UserEventContent} from "./user-event-content";
import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";

export class UserEventReturn implements Serializable, UserEventContent {

  static for(userEventType: UserEventType, pseudo: string, sendDate: number) {
    return new UserEventReturn(userEventType, pseudo, sendDate);
  }

  private constructor(private readonly _userEventType: UserEventType,
                      private readonly _pseudo: string,
                      private readonly _sendDate: number) {
  }

  asObject(): any {
    return {
      sendDate: this._sendDate,
      type: this._userEventType,
      username: this._pseudo
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}
