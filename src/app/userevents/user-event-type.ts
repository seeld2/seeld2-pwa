/**
 * Describes the type of a user event.
 */
export enum UserEventType {
  CONTACT_REMOVE = 'CONTACT_REMOVE',
  CONTACT_REMOVE_RETURN = 'CONTACT_REMOVE_RETURN',
  CONTACT_REQUEST = 'CONTACT_REQUEST',
  CONTACT_REQUEST_RETURN = 'CONTACT_REQUEST_RETURN',
  CONTACT_REQUEST_RESPONSE = 'CONTACT_REQUEST_RESPONSE',
  CONTACT_REQUEST_RESPONSE_RETURN = 'CONTACT_REQUEST_RESPONSE_RETURN',
  CONTACT_SECRET_KEY = 'CONTACT_SECRET_KEY',
  CONTACT_SECRET_KEY_RETURN = 'CONTACT_SECRET_KEY_RETURN'
}
