import {UserEventType} from "./user-event-type";

export class UserEvent {

  private static readonly TOPIC_ROOT = 'UserEvent';

  static build(content: any, eventId: string, type: UserEventType): UserEvent {
    return new UserEvent(content, eventId, type);
  }

  static topicFor(userEventType: UserEventType) {
    return `${UserEvent.TOPIC_ROOT}:${userEventType}`;
  }

  private constructor(private readonly _content: any,
                      private readonly _eventId: string,
                      private readonly _type: UserEventType) {
  }

  content(): any {
    return this._content;
  }

  eventId(): string {
    return this._eventId;
  }

  isNotOfType(userEventType: UserEventType): boolean {
    return this._type !== userEventType;
  }

  isOfType(userEventType: UserEventType): boolean {
    return this._type === userEventType;
  }

  topic() {
    return UserEvent.topicFor(this._type);
  }
}
