import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {AngularJasmine} from "../../shared/angular-jasmine";
import {config} from "../app.config";
import {AES} from "../crypto/aes/aes";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {HmacHashesByBoxAddress} from "../sessions/hmac-hashes-by-box-address";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {Keys} from "../user/key/keys";
import {NewUserEvent} from "./new-user-event";
import {UserEvent} from "./user-event";
import {UserEventContentTestBuilder as aUserEventContent} from "./user-event-content.test-builder";
import {UserEventType} from "./user-event-type";
import {
  CreateEventRequest,
  DeleteEventRequest,
  FetchEventsRequest,
  UserEventResponse,
  UserEventsRemote
} from "./user-events-remote";

describe("UserEventsRemote", () => {

  const EVENT_ID: string = "eventId";
  const LOGGED_USER_BOX_ADDRESS: string = "loggedUserBoxAddress";

  const HMAC_HASH: string = "hmacHash";
  const NEW_USER_EVENT_AES: Uint8Array = new Uint8Array([1, 3, 4, 5, 6, 7, 9, 8, 1, 2, 0]);
  const RECEIVER_ARMORED_PUBLIC_KEYS: string = "receiverArmoredPublicKeys";
  const RECEIVER_BOX_ADDRESS: string = "receiverBoxAddress";
  const RETURN_USER_EVENT_CONTENT_PGP: string = "returnUserEventContentPgp"
  const SENDER_PUBLIC_KEYS: string = "senderPublicKeys";
  const SENT_USER_EVENT_CONTENT_PGP: string = "sentUserEventContentPgp";

  const USER_EVENT_1: UserEvent = UserEvent.build('content1', '1', UserEventType.CONTACT_REQUEST);
  const USER_EVENT_2: UserEvent = UserEvent.build('content2', '2', UserEventType.CONTACT_REQUEST);

  const USER_EVENT_RESPONSE_1: UserEventResponse = {eventId: '1', payload: 'payload1'};
  const USER_EVENT_RESPONSE_2: UserEventResponse = {eventId: '2', payload: 'payload2'};

  let httpClient: HttpClient;
  let sessionService: SessionService;

  let userEventsRemote: UserEventsRemote;

  let exchangeAes: AES;
  let openPgp: OpenPgp;
  let session: Session;

  beforeEach(() => {
    httpClient = AngularJasmine.httpClient();
    sessionService = SessionsJasmine.sessionService();

    userEventsRemote = new UserEventsRemote(httpClient, sessionService);

    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);
    exchangeAes = CryptoJasmine.exchangeAes();
    (<jasmine.Spy>sessionService.exchangeAes).and.returnValue(of(exchangeAes));
    openPgp = CryptoJasmine.openPgp();
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);
  });

  it("should delete an event with a given ID from the logged user's system box address", (done: DoneFn) => {

    (<jasmine.Spy>session.systemBoxAddress).and.returnValue(LOGGED_USER_BOX_ADDRESS);
    (<jasmine.Spy>httpClient.put).and.returnValue(of({}));

    userEventsRemote.deleteEventFromSystemBoxAddress(EVENT_ID).subscribe(() => {

      expect(httpClient.put).toHaveBeenCalledWith(
        `${config.api.events}/${EVENT_ID}/state/deleted`, DeleteEventRequest.for(LOGGED_USER_BOX_ADDRESS).toJson());

      done();
    });
  });

  it("should fetch events", (done: DoneFn) => {

    (<jasmine.Spy>sessionService.hmacHashesByReadFromBoxAddresses)
      .and.returnValue(of(HmacHashesByBoxAddress.with({boxAddress: HMAC_HASH})));
    (<jasmine.Spy>sessionService.hmacHashOfSystemBoxAddress).and.returnValue(of(HMAC_HASH));
    (<jasmine.Spy>sessionService.systemBoxAddress).and.returnValue(LOGGED_USER_BOX_ADDRESS);

    (<jasmine.Spy>httpClient.put).and.returnValue(of({events: [USER_EVENT_RESPONSE_1, USER_EVENT_RESPONSE_2]}));
    (openPgp.decryptNoVerify as jasmine.Spy)
      .and.returnValues(of(JSON.stringify(USER_EVENT_1)), of(JSON.stringify(USER_EVENT_2)));

    let results: UserEvent[] = [];
    userEventsRemote.fetchEvents(openPgp)
      .subscribe((userEvent: UserEvent) => {

        results.push(userEvent);
        if (results.length === 2) {

          expect(httpClient.put).toHaveBeenCalled();

          expect(openPgp.decryptNoVerify).toHaveBeenCalledTimes(2);
          expect((<jasmine.Spy>(openPgp.decryptNoVerify)).calls.argsFor(0)[0]).toEqual('payload1');
          expect((<jasmine.Spy>(openPgp.decryptNoVerify)).calls.argsFor(1)[0]).toEqual('payload2');

          expect(results[0].eventId()).toEqual('1');
          expect(results[1].eventId()).toEqual('2');

          done();
        }
      });
  });

  it("should post a new event", (done: DoneFn) => {
    const sentUserEventContent = aUserEventContent.toSend();
    const userEventReturnContent = aUserEventContent.return();

    (<jasmine.Spy>exchangeAes.encrypt).and.returnValue(of(NEW_USER_EVENT_AES));
    (<jasmine.Spy>sessionService.publicKeysForKeyId).and.returnValues(SENDER_PUBLIC_KEYS);
    (<jasmine.Spy>openPgp.encrypt).and.returnValues(
      of(SENT_USER_EVENT_CONTENT_PGP),
      of(RETURN_USER_EVENT_CONTENT_PGP)
    );

    (<jasmine.Spy>httpClient.post).and.returnValue(of({}));

    userEventsRemote.postEvent(
      NewUserEvent.for(RECEIVER_BOX_ADDRESS, HMAC_HASH),
      RECEIVER_ARMORED_PUBLIC_KEYS,
      sentUserEventContent,
      userEventReturnContent)
      .subscribe(() => {

        expect(exchangeAes.encrypt).toHaveBeenCalledWith(NewUserEvent.for(RECEIVER_BOX_ADDRESS, HMAC_HASH).toJson());
        expect(sessionService.publicKeysForKeyId).toHaveBeenCalledWith(Keys.systemKeysId());
        expect((<jasmine.Spy>(openPgp.encrypt)).calls.allArgs()[0])
          .toEqual([sentUserEventContent.toJson(), RECEIVER_ARMORED_PUBLIC_KEYS]);
        expect((<jasmine.Spy>(openPgp.encrypt)).calls.allArgs()[1])
          .toEqual([userEventReturnContent.toJson(), SENDER_PUBLIC_KEYS]);

        expect(httpClient.post).toHaveBeenCalledWith(`${config.api.events}`, CreateEventRequest
          .for(Array.from(NEW_USER_EVENT_AES), SENT_USER_EVENT_CONTENT_PGP, RETURN_USER_EVENT_CONTENT_PGP).toJson());

        done();
      });
  });
});

describe("FetchEventsRequest", () => {

  it("should provide a JSON of itself", () => {
    const request: FetchEventsRequest = FetchEventsRequest.for(HmacHashesByBoxAddress.with({boxAddress: 'hmacHash'}));
    const json: string = request.toJson();
    expect(json).toEqual("{\"hmacHashesByBoxAddress\":{\"boxAddress\":\"hmacHash\"}}");
  });
});
