import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {concat} from "rxjs/observable/concat";
import {forkJoin} from "rxjs/observable/forkJoin";
import {map, mergeMap} from "rxjs/operators";
import {config} from "../app.config";
import {AES} from "../crypto/aes/aes";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {HmacHashesByBoxAddress} from "../sessions/hmac-hashes-by-box-address";
import {SessionService} from "../sessions/session.service";
import {Keys} from "../user/key/keys";
import {Serializable} from "../utils/serialization/serializable";
import {Serializer} from "../utils/serialization/serializer";
import {NewUserEvent} from "./new-user-event";
import {UserEvent} from "./user-event";
import {UserEventContent} from "./user-event-content";

@Injectable()
export class UserEventsRemote {

  constructor(private readonly httpClient: HttpClient,
              private readonly sessionService: SessionService) {
  }

  // TODO Add box address (and hmac hash) to delete targeted events (that is, events that are not sent to the system box address)
  deleteEvent(eventId: string): Observable<any> {
    return this.httpClient.delete(`${config.api.events}/${eventId}`);
  }

  deleteEventFromSystemBoxAddress(eventId: string): Observable<string> {
    const request: DeleteEventRequest = DeleteEventRequest.for(this.sessionService.session().systemBoxAddress());
    return this.httpClient.put(`${config.api.events}/${eventId}/state/deleted`, request.toJson())
      .pipe(
        map(() => eventId)
      );
  }

  fetchEvents(openPgp: OpenPgp): Observable<UserEvent> { // TODO Get OpenPGP from sessionService
    return forkJoin(
      this.sessionService.hmacHashesByReadFromBoxAddresses(),
      this.sessionService.hmacHashOfSystemBoxAddress())
      .pipe(
        mergeMap(([hmacHashesByBoxAddress, hmacHashOfSystemBoxAddress]) => {
          hmacHashesByBoxAddress.set(this.sessionService.systemBoxAddress(), hmacHashOfSystemBoxAddress);
          return this.httpClient.put(`${config.api.events}`, FetchEventsRequest.for(hmacHashesByBoxAddress).toJson());
        }),
        mergeMap((userEventsResponse: UserEventsResponse) => {
          const observables: Observable<UserEvent>[] = userEventsResponse.events
            .map((userEventResponse: UserEventResponse) => {
              return this.decryptUserEventResponse(userEventResponse, openPgp);
            });

          return concat(observables);
        }),
        mergeMap((observable: Observable<UserEvent>) => observable) // This evaluates one by one each observable inside the concatenated observable of observables
      );
  }

  private decryptUserEventResponse(userEventResponse: UserEventResponse, openPgp: OpenPgp): Observable<UserEvent> {
    return openPgp.decryptNoVerify(userEventResponse.payload)
      .pipe(
        map((unencrypted: string) => {
          const content = JSON.parse(unencrypted);
          return UserEvent.build(content, userEventResponse.eventId, content['type']);
        })
      );
  }

  postEvent(newUserEvent: NewUserEvent,
            receiverArmoredPublicKeys: string,
            userEventContent: UserEventContent,
            userEventReturnContent: UserEventContent): Observable<any> {

    return this.sessionService.exchangeAes()
      .pipe(
        mergeMap((exchangeAes: AES) => {

          const newUserEventObs: Observable<number[]> = exchangeAes
            .encrypt(newUserEvent.toJson())
            .pipe(
              map((payload: Uint8Array) => Array.from(payload))
            );

          const userEventObs: Observable<string> = this.sessionService.openPgp()
            .encrypt(userEventContent.toJson(), receiverArmoredPublicKeys);

          const senderPublicKeys: string = this.sessionService.publicKeysForKeyId(Keys.systemKeysId());
          const userEventReturnObs: Observable<string> = this.sessionService.openPgp()
            .encrypt(userEventReturnContent.toJson(), senderPublicKeys);

          return forkJoin(newUserEventObs, userEventObs, userEventReturnObs);
        }),

        mergeMap(([newUserEventAesPayload, userEventPayload, userEventReturnPayload]: any[]) => {
          return this.httpClient.post(`${config.api.events}`, CreateEventRequest
            .for(newUserEventAesPayload, userEventPayload, userEventReturnPayload).toJson());
        })
      );
  }
}

export class CreateEventRequest implements Serializable {

  static for(aesPayload: number[], payload: string, returnPayload: string): CreateEventRequest {
    return new CreateEventRequest(aesPayload, payload, returnPayload);
  }

  private constructor(private readonly _aesPayload: number[],
                      private readonly _payload: string,
                      private readonly _returnPayload: string) {
  }

  asObject(): any {
    return {aesPayload: this._aesPayload, payload: this._payload, returnPayload: this._returnPayload};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export class DeleteEventRequest implements Serializable {

  static for(boxAddress: string): DeleteEventRequest {
    return new DeleteEventRequest(boxAddress, null);
  }

  private constructor(private readonly _boxAddress: string,
                      private readonly _hmacHash: string) {
  }

  asObject(): any {
    return {
      boxAddress: this._boxAddress,
      hmacHash: this._hmacHash
    };
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export class FetchEventsRequest implements Serializable {

  static for(hmacHashesByBoxAddress: HmacHashesByBoxAddress): FetchEventsRequest {
    return new FetchEventsRequest(hmacHashesByBoxAddress);
  }

  private constructor(private readonly _hmacHashesByBoxAddress: HmacHashesByBoxAddress) {
  }

  asObject(): any {
    return {hmacHashesByBoxAddress: this._hmacHashesByBoxAddress};
  }

  toJson(): string {
    return Serializer.toJson(this);
  }
}

export interface UserEventsResponse {
  events: UserEventResponse[]
}

export interface UserEventResponse {
  eventId: string,
  payload: string
}
