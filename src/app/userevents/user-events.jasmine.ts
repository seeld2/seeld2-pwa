import {Objects} from "../utils/objects";
import {UserEventsService} from "./user-events.service";
import {UserEventsRemote} from "./user-events-remote";

export class UserEventsJasmine {

  static userEventsService() {
    return jasmine
      .createSpyObj('userEventsService', Objects.propertyNamesOf(new UserEventsService(null, null, null)));
  }

  static userEventsRemote() {
    return jasmine.createSpyObj('userEventsRemote', Objects.propertyNamesOf(new UserEventsRemote(null, null)));
  }
}
