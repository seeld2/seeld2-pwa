import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {SessionsModule} from "../sessions/sessions.module";
import {UserEventsService} from "./user-events.service";
import {CryptoModule} from "../crypto/crypto.module";
import {UtilsModule} from "../utils/utils.module";
import {UserEventsRemote} from "./user-events-remote";

@NgModule({
  imports: [
    CryptoModule,
    HttpClientModule,
    SessionsModule,
    UtilsModule
  ],
  providers: [
    UserEventsRemote,
    UserEventsService
  ]
})
export class UserEventsModule {
}
