import {Events} from "ionic-angular";
import {from} from "rxjs/observable/from";
import {of} from "rxjs/observable/of";
import {IonicJasmine} from "../../shared/ionic-jasmine";
import {AES} from "../crypto/aes/aes";
import {CryptoJasmine} from "../crypto/crypto.jasmine";
import {OpenPgp} from "../crypto/openpgp/open-pgp";
import {ProfileTestBuilder as aProfile} from "../profiles/profile.test-builder";
import {Session} from "../sessions/session";
import {SessionService} from "../sessions/session.service";
import {SessionsJasmine} from "../sessions/sessions.jasmine";
import {Contact} from "../user/contact/contact";
import {ContactKey} from "../user/contact/contact-key";
import {ContactTestBuilder as aContact} from "../user/contact/contact.test-builder";
import {Keys} from "../user/key/keys";
import {NewUserEvent} from "./new-user-event";
import {UserEvent} from "./user-event";
import {UserEventContent} from "./user-event-content";
import {UserEventContentTestBuilder as aUserEventContent} from "./user-event-content.test-builder";

import {UserEventReturn} from "./user-event-return";
import {UserEventType} from "./user-event-type";
import {UserEventsRemote} from "./user-events-remote";
import {UserEventsJasmine} from "./user-events.jasmine";
import {UserEventsService} from "./user-events.service";

describe("UserEventsService", () => {

  const BOB_PSEUDO: string = "bob pseudo";
  const BOB_SECRET_KEY: string = "bobSecretKey";
  const BOB_SYSTEM_BOX_ADDRESS: string = "bobSystemBoxAddress";
  const BOB_SYSTEM_PUBLIC_KEYS: string = "bobSystemPublicKeys";
  const BOB_WRITE_TO_BOX_ADDRESS: string = "bobWriteToBoxAddress";

  const EVENT_ID: string = "eventId";
  const USER_EVENT_1: UserEvent = UserEvent.build('content1', '1', UserEventType.CONTACT_REQUEST);
  const USER_EVENT_2: UserEvent = UserEvent.build('content2', '2', UserEventType.CONTACT_REQUEST);

  let events: Events;
  let sessionService: SessionService;
  let userEventsRemote: UserEventsRemote;

  let userEventsService: UserEventsService;

  let exchangeAes: AES;
  let openPgp: OpenPgp;
  let session: Session;

  beforeEach(() => {
    events = IonicJasmine.events();
    sessionService = SessionsJasmine.sessionService();
    userEventsRemote = UserEventsJasmine.userEventsRemote();

    userEventsService = new UserEventsService(events, sessionService, userEventsRemote);

    exchangeAes = CryptoJasmine.exchangeAes();
    (<jasmine.Spy>sessionService.exchangeAes).and.returnValue(of(exchangeAes));
    openPgp = CryptoJasmine.openPgp();
    (<jasmine.Spy>sessionService.openPgp).and.returnValue(openPgp);
    session = SessionsJasmine.session();
    (<jasmine.Spy>sessionService.session).and.returnValue(session);
  });

  it("should publish new user events from the server", (done: DoneFn) => {

    (<jasmine.Spy>userEventsRemote.fetchEvents).and.returnValue(from([USER_EVENT_1, USER_EVENT_2]));

    const results: UserEvent[] = [];
    (<jasmine.Spy>events.publish).and.callFake(() => {

      expect((<jasmine.Spy>events.publish).arguments[0]).toEqual('UserEvent:CONTACT_REQUEST');

      results.push((<jasmine.Spy>events.publish).arguments[1]);
      if (results.length === 2) {

        expect(results[0].eventId()).toEqual('1');
        expect(results[1].eventId()).toEqual('2');

        done();
      }
    });

    userEventsService.fetchAndPublishNewUserEvents();
  });

  it("should post an event addressed to a contact", (done: DoneFn) => {

    const content: UserEventContent = aUserEventContent.toSend();
    const contentReturn: UserEventReturn = aUserEventContent.return();
    const contact: Contact = aContact.with()
      .pseudo(BOB_PSEUDO)
      .secretKey(BOB_SECRET_KEY)
      .writeTo(BOB_WRITE_TO_BOX_ADDRESS)
      .writeWith(ContactKey.with(Keys.SYSTEM_IDENTIFIER, Keys.SYSTEM_KEYS_TYPE, BOB_SYSTEM_PUBLIC_KEYS))
      .asIs();

    (<jasmine.Spy>userEventsRemote.postEvent).and.returnValue(of({}));

    userEventsService.postEventToContact(content, contentReturn, contact).subscribe(() => {

      const newUserEvent: NewUserEvent = (<jasmine.Spy>userEventsRemote.postEvent).calls.argsFor(0)[0];
      expect(newUserEvent.asObject()['boxAddress']).toEqual(BOB_WRITE_TO_BOX_ADDRESS);
      expect(userEventsRemote.postEvent)
        .toHaveBeenCalledWith(newUserEvent, BOB_SYSTEM_PUBLIC_KEYS, content, contentReturn);

      done();
    });
  });

  it("should post an event addressed to a contact, to a specified box address ", (done: DoneFn) => {

    const content: UserEventContent = aUserEventContent.toSend();
    const contentReturn: UserEventReturn = aUserEventContent.return();
    const contact: Contact = aContact.with()
      .pseudo(BOB_PSEUDO)
      .secretKey(BOB_SECRET_KEY)
      .writeWith(ContactKey.with(Keys.SYSTEM_IDENTIFIER, Keys.SYSTEM_KEYS_TYPE, BOB_SYSTEM_PUBLIC_KEYS))
      .asIs();

    (<jasmine.Spy>userEventsRemote.postEvent).and.returnValue(of({}));

    userEventsService.postEventToContact(content, contentReturn, contact, BOB_SYSTEM_BOX_ADDRESS).subscribe(() => {

      const newUserEvent: NewUserEvent = (<jasmine.Spy>userEventsRemote.postEvent).calls.argsFor(0)[0];
      expect(newUserEvent.asObject()['boxAddress']).toEqual(BOB_SYSTEM_BOX_ADDRESS);
      expect(userEventsRemote.postEvent)
        .toHaveBeenCalledWith(newUserEvent, BOB_SYSTEM_PUBLIC_KEYS, content, contentReturn);

      done();
    });
  });

  it("should post an event addressed to a given profile (not yet a contact)", (done: DoneFn) => {

    const content: UserEventContent = aUserEventContent.toSend();
    const contentReturn: UserEventReturn = aUserEventContent.return();
    const receiverProfile = aProfile.with().boxAddress(BOB_SYSTEM_BOX_ADDRESS).pseudo(BOB_PSEUDO).systemPublicKeys(BOB_SYSTEM_PUBLIC_KEYS).asIs();

    (<jasmine.Spy>userEventsRemote.postEvent).and.returnValue(of({}));

    userEventsService.postEventToProfile(content, contentReturn, receiverProfile).subscribe(() => {

      expect(userEventsRemote.postEvent(
        NewUserEvent.for(BOB_SYSTEM_BOX_ADDRESS, null),
        BOB_SYSTEM_PUBLIC_KEYS,
        content,
        contentReturn
      ));

      done();
    });
  });

  it("should remove a remote user event", (done: DoneFn) => {

    (<jasmine.Spy>userEventsRemote.deleteEvent).and.returnValue(of({}));

    userEventsService.removeEvent(EVENT_ID).subscribe(() => {
      expect(userEventsRemote.deleteEvent).toHaveBeenCalledWith(EVENT_ID);
      done();
    });
  });
});
