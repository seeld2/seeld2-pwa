import {Injectable} from "@angular/core";
import {Events} from "ionic-angular";
import {Observable} from "rxjs";
import {of} from "rxjs/observable/of";
import {mergeMap} from "rxjs/operators";
import {Profile} from "../profiles/profile";
import {SessionService} from "../sessions/session.service";
import {Contact} from "../user/contact/contact";
import {Logger} from "../utils/logging/logger";
import {NewUserEvent} from "./new-user-event";
import {UserEvent} from "./user-event";
import {UserEventContent} from "./user-event-content";
import {UserEventReturn} from "./user-event-return";
import {UserEventsRemote} from "./user-events-remote";

@Injectable()
export class UserEventsService {

  private readonly logger: Logger = Logger.for('UserEventsService');

  private fetchingUserEvents: boolean = false;

  constructor(private readonly events: Events,
              private readonly sessionService: SessionService,
              private readonly userEventsRemote: UserEventsRemote) {
  }

  deleteEventFromSystemBoxAddress(eventId: string): Observable<string> {
    return this.userEventsRemote.deleteEventFromSystemBoxAddress(eventId);
  }

  fetchAndPublishNewUserEvents() {

    if (!this.fetchingUserEvents) {
      this.fetchingUserEvents = true;

      this.userEventsRemote.fetchEvents(this.sessionService.openPgp())
        .subscribe(
          (userEvent: UserEvent) => {
            const userEventTopic: string = userEvent.topic();
            this.logger.debug(`Received user event ${userEventTopic} [${userEvent.eventId()}]`);
            this.events.publish(userEventTopic, userEvent);
          },
          error => {
            this.logger.error(error);
            this.fetchingUserEvents = false;
          },
          () => this.fetchingUserEvents = false
        );
    }
  }

  fetchUserEvents(): Observable<UserEvent> {
    return this.userEventsRemote.fetchEvents(this.sessionService.openPgp());
  }

  /**
   * Used to post an event to the specified contact's "write to" box address.<br>
   * This is the safest option compared to {@link postEventToProfile()}, since it sends the event to the unique box
   * address exchanged in the process of connecting. Also, it uses the HMAC secret key to ensure the event is only
   * accessible and deletable by the intended person.<br>
   * Currently, an optional "writeToBoxAddress" parameter allows overriding the contact's box address. This is permitted
   * for the moment because all events are still being sent to the system box address. So this method is currently
   * being called with the profile's system box address as a parameter. This also has per consequence that the event is
   * remotely stored WITHOUT a HMAC hash.
   * TODO This optional parameter will need to be removed as soon as the user event exchanges (post-connection) are effectively done on the contact's writeTo/readFrom box addresses
   */
  postEventToContact(content: UserEventContent,
                     contentReturn: UserEventReturn,
                     contact: Contact,
                     writeToBoxAddress: string = null): Observable<any> {
    const receiverBoxAddress: string = writeToBoxAddress ? writeToBoxAddress : contact.writeTo();
    const hmacHashObs: Observable<string> = writeToBoxAddress ? of(null) : contact.hmacHash(contact.writeTo());
    return hmacHashObs.pipe(
      mergeMap((hmacHash: string) => {
        const newUserEvent: NewUserEvent = NewUserEvent.for(receiverBoxAddress, hmacHash);
        return this.userEventsRemote.postEvent(newUserEvent, contact.writeWith().publicKeys(), content, contentReturn);
      })
    );
  }

  /**
   * Used to post an event to the system box address of a profile.<br>
   * Typically used to send contact requests and responses: all other events should be posted to the exchanged box addresses, once they are contacts. See {@link postEventToContact}.
   */
  postEventToProfile(content: UserEventContent,
                     contentReturn: UserEventContent,
                     receiverProfile: Profile): Observable<any> {
    const receiverBoxAddress: string = receiverProfile.boxAddress();
    const newUserEvent: NewUserEvent = NewUserEvent.for(receiverBoxAddress, null);
    return this.userEventsRemote.postEvent(newUserEvent, receiverProfile.systemPublicKeys(), content, contentReturn);
  }

  /**
   * @deprecated
   */
  removeEvent(eventId: string): Observable<any> {
    return this.userEventsRemote.deleteEvent(eventId);
  }
}
