import {AntPathMatcher} from "./ant-path-matcher";

describe("AntMatcher", () => {

  describe(", when matching a path against a normal ant path,", () => {

    it("match a path", () => {
      expect(AntPathMatcher.for(['/some/path']).match('/some/path')).toEqual('/some/path');
    });

    it("should exclude leading and trailing slashes when matching a path", () => {
      expect(AntPathMatcher.for(['some/path']).match('/some/path/')).toEqual('some/path');
      expect(AntPathMatcher.for(['/some/path/']).match('some/path')).toEqual('/some/path/');
      expect(AntPathMatcher.for(['/some/path/']).match('different/path')).toBeNull();
      expect(AntPathMatcher.for(['/some/path/']).match('different/path')).toBeNull();
    });

    it("should mismatch a path", () => {
      expect(AntPathMatcher.for(['/some/path']).match('/different/path')).toBeNull();
    });

    it("should mismatch a path that is shorter than the ant path", () => {
      expect(AntPathMatcher.for(['/some/path']).match('/some')).toBeNull();
    });

    it("should mismatch a path that is longer than the ant path", () => {
      expect(AntPathMatcher.for(['/some/path']).match('/some/path/too')).toBeNull();
    });
  });

  describe(", when matching a path against one-star ant paths,", () => {

    it("should match a path against an ant path with one star", () => {
      expect(AntPathMatcher.for(['/one/*/path']).match('/one/any/path')).toEqual('/one/*/path');
    });

    it("should mismatch a path against an ant path with one star", () => {
      expect(AntPathMatcher.for(['/one/*/path']).match('/one/path')).toBeNull()
      expect(AntPathMatcher.for(['/one/*/path']).match('/one/any/path/too/long')).toBeNull()
      expect(AntPathMatcher.for(['/one/*/path']).match('/one/any/different')).toBeNull()
    });

    it("should match a path against an ant path with multiple stars", () => {
      expect(AntPathMatcher.for(['/some/*/*/cool/*/path']).match('/some/a/b/cool/c/path/'))
        .toEqual('/some/*/*/cool/*/path');
    });

    it("should mismatch a path against an ant path with multiple stars", () => {
      expect(AntPathMatcher.for(['/some/*/*/cool/*/path']).match('/some/short/cool/c/path/')).toBeNull();
      expect(AntPathMatcher.for(['/some/*/*/cool/*/path']).match('/some/a/b/cool/too/long/path/')).toBeNull();
      expect(AntPathMatcher.for(['/some/*/*/cool/*/path']).match('/some/a/b/square/c/path/')).toBeNull();
    });
  });

  describe(", when matching a path against two-star ant paths,", () => {

    it("should match a path against an ant path with two-stars", () => {
      expect(AntPathMatcher.for(['one/**/four']).match('one/two/three/four')).toEqual('one/**/four');
    });

    it("should match a path against an ant path with two-stars at the end of the path", () => {
      expect(AntPathMatcher.for(['one/**']).match('one/two/three/four')).toEqual('one/**')
    });

    it("should match a path against an ant path with multiple two-stars", () => {
      expect(AntPathMatcher.for(['one/**/four/**/seven']).match('/one/two/three/four/five/six/seven'))
        .toEqual('one/**/four/**/seven');
    });

    it("should mismatch a path against an ant-path with two-stars", () => {
      expect(AntPathMatcher.for(["one/**/four"]).match('one/two/three')).toBeNull();
    });

    it("should mismatch a path against an ant path with multiple two-stars", () => {
      expect(AntPathMatcher.for(['one/**/four/**/seven']).match('/one/two/three/four/five/six/')).toBeNull();
    });
  });
});
