export class AntPathMatcher {

  public static for(antPaths: string[]): AntPathMatcher {
    return new AntPathMatcher(antPaths);
  }

  private constructor(private antPaths) {
  }

  match(path: string): string {

    for (let antPath of this.antPaths) {
      if (AntPathMatcher.matchOnePath(path, antPath)) {
        return antPath;
      }
    }

    return null;
  }

  private static matchOnePath(path: string, antPath: string): boolean {

    const pathComponents: PathComponents = PathComponents.of(path);
    const antPathComponents: PathComponents = PathComponents.of(antPath);

    while (pathComponents.hasNext()) {

      // Mismatch path components that are longer than ant path components
      if (!antPathComponents.current()) {
        return false;
      }

      // For two-star ant paths, move to the next component IF it matches the current path component
      if (antPathComponents.current() && antPathComponents.current().isTwoStar()) {
        const nextAntPathComponent: PathComponent = antPathComponents.peekNext();
        if (nextAntPathComponent && nextAntPathComponent.equals(pathComponents.current())) {
          antPathComponents.next();
        }
      }

      // Test path component against ant path component (only for "normal" ant path components)
      if (antPathComponents.current() && antPathComponents.current().isNormal() && !pathComponents.current().equals(antPathComponents.current())) {
        return false;
      }

      // Move indexes
      pathComponents.next();
      if (antPathComponents.current() && !antPathComponents.current().isTwoStar()) {
        antPathComponents.next();
      }
    }

    // Mismatch paths that have not completely fulfilled the ant path... Unless the last ant path section is a two-star
    if (antPathComponents.hasNext()) {
      return antPathComponents.last().isTwoStar();
    }

    return true;
  }
}

class PathComponents {

  static of(path: string) {
    const pathElements: string[] = path.split('/').filter((section: string) => section !== '');
    const pathComponents: PathComponent[] = pathElements.map((pathElement: string) => PathComponent.of(pathElement));
    return new PathComponents(pathComponents);
  }

  private index: number = 0;

  private constructor(private pathComponents: PathComponent[]) {
  }

  current(): PathComponent {
    return this.pathComponents[this.index];
  }

  hasNext(): boolean {
    return this.index < this.pathComponents.length;
  }

  next(): PathComponent {
    this.index++;
    return this.current();
  }

  peekNext(): PathComponent {
    const nextIndex: number = this.index + 1;
    if (nextIndex < this.pathComponents.length) {
      return this.pathComponents[nextIndex];
    }
    return undefined;
  }

  last(): PathComponent {
    return this.pathComponents[this.pathComponents.length - 1];
  }
}

class PathComponent {

  static of(pathComponent: string) {
    return new PathComponent(pathComponent);
  }

  private constructor(private pathComponent: string) {
  }

  equals(pathComponent: PathComponent): boolean {
    return this.pathComponent === pathComponent.pathComponent;
  }

  isOneStar(): boolean {
    return this.pathComponent === '*';
  }

  isTwoStar(): boolean {
    return this.pathComponent === '**';
  }

  isNormal(): boolean {
    return !this.isTwoStar() && !this.isOneStar();
  }
}

