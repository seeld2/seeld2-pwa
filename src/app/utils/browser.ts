export class Browser {

  static beforeBrowserCloses(handler) {
    window.addEventListener('beforeunload', handler);
  }

  static disableBackButton() {
    history.pushState({}, '', '');
    window.onpopstate = () => {
      history.forward();
    };
  }

  static enforceHttps() {
    if (location.protocol.toLowerCase().indexOf('https') == -1) {
      location.assign('https' + location.href.substring(location.href.indexOf(':')));
    }
  }
}
