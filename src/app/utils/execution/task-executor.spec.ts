import {Subscription} from "rxjs";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {tap} from "rxjs/operators";
import {Task} from "./task";
import {TaskExecutor} from "./task-executor";

describe("TaskExecutor", () => {

  let taskExecutor: TaskExecutor;

  let booleanResult: boolean;
  let executionResults: any[];

  beforeEach(() => {
    taskExecutor = TaskExecutor.create();

    booleanResult = false;
    executionResults = [];
  });

  it("should prevent attempts to execute an empty queue", () => {
    taskExecutor.executeWhileNext();
  });

  it("should execute a list of synchronous tasks in order of priority", () => {

    taskExecutor.queue(Task.sync(null, 2, syncFunction, [2]));
    taskExecutor.queue(Task.sync(null, 1, syncFunction, [1]));
    taskExecutor.queue(Task.sync(null, 3, syncFunction, [3]));

    taskExecutor.executeWhileNext();

    expect(executionResults).toEqual([1, 2, 3]);
  });

  it("should execute a list of asynchronous Observable tasks in order of priority", (done: DoneFn) => {

    taskExecutor.queue(Task.observable(null, 2, observableFunction, [2]));
    taskExecutor.queue(Task.observable(null, 1, observableFunction, [1]));

    taskExecutor.executeWhileNext();

    setTimeout(() => {
      expect(executionResults.length).toEqual(2);
      expect(executionResults).toEqual([1, 2]);
      done();
    }, 250);
  });

  it("should execute a list of asynchronous Subscription tasks in order of priority", (done: DoneFn) => {

    taskExecutor.queue(Task.subscription(null, 2, subscriptionFunction, [2]));
    taskExecutor.queue(Task.subscription(null, 1, subscriptionFunction, [1]));
    taskExecutor.queue(Task.subscription(null, 1, subscriptionFunction, [111]))
    taskExecutor.queue(Task.subscription(null, 1, subscriptionFunction, [11]))

    taskExecutor.executeWhileNext();

    setTimeout(() => {
      expect(executionResults.length).toEqual(4);
      expect(executionResults).toEqual([1, 111, 11, 2]);
      done();
    }, 250);
  });

  function observableFunction(...args): Observable<any> {
    return of(true)
      .pipe(
        tap(() => executionResults.push(args[0]))
      );
  }

  function subscriptionFunction(...args): Subscription {
    return of(true)
      .pipe(
        tap(() => executionResults.push(args[0]))
      )
      .subscribe();
  }

  function syncFunction(value) {
    executionResults.push(value);
  }
});
