import {Subscription} from "rxjs";
import {Task} from "./task";
import {TaskType} from "./task-type";

export class TaskExecutor {

  static create() {
    return new TaskExecutor();
  }

  private readonly tasks: Task[] = [];

  private executing: boolean = false;

  private constructor() {
  }

  executeWhileNext() {
    if (this.executing) {
      return;
    }
    this.executing = true;
    this.executeNext();
  }

  queue(task: Task): TaskExecutor {
    this.tasks.push(task);
    this.sortTasksOnPriority();
    return this;
  }

  private executeNext() {
    if (this.tasks.length === 0) {
      this.executing = false;
      return;
    }
    const task: Task = this.tasks.shift();

    switch (task.type()) {

      case TaskType.OBSERVABLE:
        return task.fnction().apply(task.context(), task.parameters()).subscribe(
          () => this.executeNext()
        );

      case TaskType.SUBSCRIPTION:
        const subscription: Subscription = task.fnction().apply(task.context(), task.parameters());
        subscription.add(() => this.executeNext());
        return;

      case TaskType.SYNC:
        task.fnction().apply(task.context(), task.parameters());
        this.executeNext();
        return;

      default:
        throw new Error("Unknown task type: " + task.type());
    }
  }

  private sortTasksOnPriority() {
    if (this.tasks.length === 0) {
      return;
    }

    this.tasks.sort((a: Task, b: Task) => {
      if (a.priority() < b.priority()) {
        return -1;
      }
      if (a.priority() > b.priority()) {
        return 1;
      }
      return 0;
    });
  }
}
