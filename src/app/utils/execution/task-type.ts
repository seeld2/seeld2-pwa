export enum TaskType {
  /**
   * Task types based on the asynchronous execution of an <em>>Observable</em>.<br>
   * The executor shall subscribe to the task' function and execute the next from the <em>subscribe</em> callbacks.
   */
  OBSERVABLE,
  /**
   * Task types based on the asynchronous execution of an RxJS <em>Subscription</em>.<br>
   * The executor shall call the task's function and execute the next task using the <em>add</em> method on the
   * <em>Subscription</em>.
   */
  SUBSCRIPTION,
  /**
   * Task types based on the synchronous execution of a simple function.<br>
   * The executor shall call the task's function and execute the next task after that.
   */
  SYNC
}
