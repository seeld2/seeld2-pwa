import {TaskType} from "./task-type";

export class Task {

  static observable(context: any, priority: number, fnction: any, parameters: any[] = []) {
    return new Task(context, fnction, parameters, priority, TaskType.OBSERVABLE);
  }

  static subscription(context: any, priority: number, fnction: any, parameters: any[] = []) {
    return new Task(context, fnction, parameters, priority, TaskType.SUBSCRIPTION);
  }

  static sync(context: any, priority: number, fnction: any, parameters: any[] = []) {
    return new Task(context, fnction, parameters, priority, TaskType.SYNC);
  }

  private constructor(private readonly _context: any,
                      private readonly _fnction: any,
                      private readonly _parameters: any[],
                      private readonly _priority: number,
                      private readonly _type: TaskType) {
  }

  context() {
    return this._context;
  }

  fnction(): any {
    return this._fnction
  }

  parameters(): any[] {
    return this._parameters;
  }

  priority(): number {
    return this._priority;
  }

  type(): TaskType {
    return this._type;
  }
}
