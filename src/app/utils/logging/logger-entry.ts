import {LoggingLevel} from "./logging-level";

import * as moment from "moment";

export class LoggerEntry {

  private static readonly LOGGING_LEVEL_DESCRIPTIONS: object = {
    [LoggingLevel.TRACE]: 'TRACE',
    [LoggingLevel.DEBUG]: 'DEBUG',
    [LoggingLevel.INFO]: 'INFO',
    [LoggingLevel.WARN]: 'WARN',
    [LoggingLevel.ERROR]: 'ERROR',
    [LoggingLevel.FATAL]: 'FATAL'
  };

  public static of(loggerName: string, level: LoggingLevel, entry: any) {
    return new LoggerEntry(loggerName, level, entry, moment().format('YYYY-MM-DD HH:mm:ss.SSS'));
  }

  private constructor(private loggerName: string,
                      private level: LoggingLevel,
                      private entry: any,
                      private timestamp: string) {
  }

  getEntry(): any {
    return this.entry;
  }

  getLevel(): LoggingLevel {
    return this.level;
  }

  isFromLogger(loggerName: string): boolean {
    return this.loggerName === loggerName;
  }

  meta(): string {
    return this.timestamp + ' ' + LoggerEntry.LOGGING_LEVEL_DESCRIPTIONS[this.level] + ' [' + this.loggerName + ']';
  }
}
