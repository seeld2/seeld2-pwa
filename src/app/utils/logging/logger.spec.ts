import {Logger} from "./logger";
import {LoggingLevel} from "./logging-level";
import {LoggerEntry} from "./logger-entry";

describe("Logger", () => {

  const LOGGER_NAME: string = "Test";
  const LOGGER_NAME_2: string = "Test 2";
  const MESSAGE: string = 'text';
  const OBJECT: Error = new Error('error');

  let originalConsole = {};

  let logger: Logger;

  beforeEach(() => {
    logger = Logger.for("Test");

    console.info('backing up console');
    originalConsole['trace'] = console.trace;
    originalConsole['debug'] = console.debug;
    originalConsole['info'] = console.info;
    originalConsole['warn'] = console.warn;
    originalConsole['error'] = console.error;

    console.trace = jasmine.createSpy('trace');
    console.debug = jasmine.createSpy('debug');
    console.info = jasmine.createSpy('info');
    console.warn = jasmine.createSpy('warn');
    console.error = jasmine.createSpy('error');
  });

  afterEach(() => {

    Logger.reset();

    console.trace = originalConsole['trace'];
    console.debug = originalConsole['debug'];
    console.info = originalConsole['info'];
    console.warn = originalConsole['warn'];
    console.error = originalConsole['error'];
    console.info('console restored!');
  });

  it("should list all currently stored entries", () => {

    logger.info(MESSAGE);
    logger.warn(MESSAGE);
    logger.error(MESSAGE);

    expect(Logger.lastEntries(3).length).toBe(3);
  });

  it("should display new log entries to console", () => {
    logger.trace(MESSAGE);
    expect(console.debug).toHaveBeenCalled();

    logger.debug(MESSAGE);
    expect(console.debug).toHaveBeenCalled();

    logger.info(MESSAGE);
    expect(console.info).toHaveBeenCalled();

    logger.warn(MESSAGE);
    expect(console.warn).toHaveBeenCalled();

    logger.error(MESSAGE);
    expect(console.error).toHaveBeenCalled();

    logger.fatal(MESSAGE);
    expect(console.error).toHaveBeenCalled();
  });

  it("should only store log entries without displaying them to console", () => {

    Logger.setConsoleOut(false);

    logger.trace(MESSAGE);
    expect(console.trace).not.toHaveBeenCalled();

    logger.debug(MESSAGE);
    expect(console.debug).not.toHaveBeenCalled();

    logger.info(MESSAGE);
    expect(console.info).not.toHaveBeenCalled();

    logger.warn(MESSAGE);
    expect(console.warn).not.toHaveBeenCalled();

    logger.error(MESSAGE);
    expect(console.error).not.toHaveBeenCalled();

    logger.fatal(MESSAGE);
    expect(console.error).not.toHaveBeenCalled();
  });

  it("should disable log entries storage completely", () => {

    Logger.setConsoleOut(false);
    Logger.setStorageLimit(0);

    const initialEntriesAmount = Logger.listEntries().length;
    logger.info(MESSAGE);

    expect(console.info).not.toHaveBeenCalled();
    expect(Logger.listEntries().length).toBe(initialEntriesAmount);
  });

  it("should change the configuration for all Logger instances", () => {
    const logger2: Logger = Logger.for(LOGGER_NAME_2);

    Logger.setLevel(LoggingLevel.TRACE);

    logger.trace(MESSAGE);
    logger2.trace(MESSAGE);

    Logger.setLevel(LoggingLevel.FATAL);

    logger.trace(MESSAGE);
    logger2.trace(MESSAGE);

    const lastEntries: LoggerEntry[] = Logger.lastEntries(2);
    expect(lastEntries.length).toBe(2);
    expect(lastEntries[0]['level']).toBe(LoggingLevel.TRACE);
    expect(lastEntries[1]['level']).toBe(LoggingLevel.TRACE);
  });

  describe(", when logging trace entries,", () => {

    it("should log a text entry", () => {

      logger.trace(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.TRACE);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an object entry", () => {

      logger.trace(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.TRACE);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.trace(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when logging debug entries,", () => {

    it("should log a text entry", () => {

      logger.debug(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.DEBUG);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an object entry", () => {

      logger.debug(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.DEBUG);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.debug(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when logging info entries,", () => {

    it("should log a text entry", () => {

      logger.info(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.INFO);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an object entry", () => {

      logger.info(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.INFO);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.info(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when logging warn entries,", () => {

    it("should log a text entry", () => {

      logger.warn(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.WARN);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an object entry", () => {

      logger.warn(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.WARN);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.warn(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when logging error entries,", () => {

    it("should log a text entry", () => {

      logger.error(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.ERROR);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an error entry object", () => {

      logger.error(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.ERROR);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.error(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when logging error entries,", () => {

    it("should log a text entry", () => {

      logger.fatal(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.FATAL);
      expect(Logger.lastEntry().getEntry()).toBe(MESSAGE);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log an object entry", () => {

      logger.fatal(OBJECT);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['loggerName']).toBe(LOGGER_NAME);
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.FATAL);
      expect(Logger.lastEntry().getEntry()).toBe(OBJECT);
      expect(Logger.lastEntry()['timestamp']).toBeDefined();
    });

    it("should log multiple entries", () => {
      logger.fatal(MESSAGE, OBJECT, MESSAGE);
      expect(Logger.listEntries().length).toBe(3);
    });
  });

  describe(", when applying logging level,", () => {

    it("should limit logging to trace and above", () => {

      Logger.setLevel(LoggingLevel.TRACE);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      const lastEntries: LoggerEntry[] = Logger.lastEntries(6);
      expect(lastEntries.length).toBeGreaterThanOrEqual(6);
      expect(lastEntries[0]['level']).toBe(LoggingLevel.FATAL);
      expect(lastEntries[1]['level']).toBe(LoggingLevel.ERROR);
      expect(lastEntries[2]['level']).toBe(LoggingLevel.WARN);
      expect(lastEntries[3]['level']).toBe(LoggingLevel.INFO);
      expect(lastEntries[4]['level']).toBe(LoggingLevel.DEBUG);
      expect(lastEntries[5]['level']).toBe(LoggingLevel.TRACE);
    });

    it("should limit logging to debug and above", () => {

      Logger.setLevel(LoggingLevel.DEBUG);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      const lastEntries: LoggerEntry[] = Logger.lastEntries(5);
      expect(lastEntries.length).toBe(5);
      expect(lastEntries[0]['level']).toBe(LoggingLevel.FATAL);
      expect(lastEntries[1]['level']).toBe(LoggingLevel.ERROR);
      expect(lastEntries[2]['level']).toBe(LoggingLevel.WARN);
      expect(lastEntries[3]['level']).toBe(LoggingLevel.INFO);
      expect(lastEntries[4]['level']).toBe(LoggingLevel.DEBUG);
    });

    it("should limit logging to info and above", () => {

      Logger.setLevel(LoggingLevel.INFO);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      const lastEntries: LoggerEntry[] = Logger.lastEntries(4);
      expect(lastEntries.length).toBe(4);
      expect(lastEntries[0]['level']).toBe(LoggingLevel.FATAL);
      expect(lastEntries[1]['level']).toBe(LoggingLevel.ERROR);
      expect(lastEntries[2]['level']).toBe(LoggingLevel.WARN);
      expect(lastEntries[3]['level']).toBe(LoggingLevel.INFO);
    });

    it("should limit logging to warn and above", () => {

      Logger.setLevel(LoggingLevel.WARN);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      const lastEntries: LoggerEntry[] = Logger.lastEntries(3);
      expect(lastEntries.length).toBe(3);
      expect(lastEntries[0]['level']).toBe(LoggingLevel.FATAL);
      expect(lastEntries[1]['level']).toBe(LoggingLevel.ERROR);
      expect(lastEntries[2]['level']).toBe(LoggingLevel.WARN);
    });

    it("should limit logging to error and above", () => {

      Logger.setLevel(LoggingLevel.ERROR);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      const lastEntries: LoggerEntry[] = Logger.lastEntries(2);
      expect(lastEntries.length).toBe(2);
      expect(lastEntries[0]['level']).toBe(LoggingLevel.FATAL);
      expect(lastEntries[1]['level']).toBe(LoggingLevel.ERROR);
    });

    it("should limit logging to fatal", () => {

      Logger.setLevel(LoggingLevel.FATAL);

      logger.trace(MESSAGE);
      logger.debug(MESSAGE);
      logger.info(MESSAGE);
      logger.warn(MESSAGE);
      logger.error(MESSAGE);
      logger.fatal(MESSAGE);

      expect(Logger.lastEntry()).toBeDefined();
      expect(Logger.lastEntry()['level']).toBe(LoggingLevel.FATAL);
    });

    it("should limit the amount of stored entries ", () => {

      Logger.setStorageLimit(2);

      logger.info('one');
      logger.info('two');
      logger.info('three');

      const lastEntries: LoggerEntry[] = Logger.lastEntries(2);
      expect(lastEntries.length).toBe(2);
      expect(lastEntries[0].getEntry()).toBe('three');
      expect(lastEntries[1].getEntry()).toBe('two');
    });

    it("should retrieve the last entry of a specific logger instance", () => {
      const logger2 = Logger.for(LOGGER_NAME_2);

      logger.info('nope');
      logger2.error('yep!');
      logger.warn('last nope');

      expect(logger.lastEntry().getEntry()).toEqual('last nope');
      expect(logger2.lastEntry().getEntry()).toEqual('yep!');
    });
  });
});
