import {LoggerEntry} from "./logger-entry";
import {LoggingLevel} from "./logging-level";

/**
 * The logger should be instantiated using the <strong>for()</strong> static method, and should be configured using the
 * adequate static methods.
 *
 * @example
 * const logger = Logger.for("MyClassName");
 *
 * @example
 * Logger.setConsoleOut(true);
 * Logger.setLevel(LoggingLevel.DEBUG);
 * Logger.setStorageLimit(1000);
 */
export class Logger {

  /**
   * Provides a Logger identified by the specified name.
   * The name can be anything, but it's better if it's unique as it will be used to identify the logs.
   *
   * @param name The logger's name.
   * @returns An instance of this logger.
   */
  static for(name: string): Logger {
    return new Logger(name);
  }

  /**
   * Returns the last "amount" of entries in the logger, last entry first.
   */
  static lastEntries(amount: number): LoggerEntry[] {
    return Logger.entries.slice(-amount).reverse();
  }

  /**
   * Returns the last entry in the logger.
   */
  static lastEntry(): LoggerEntry {
    const loggerEntries = Logger.lastEntries(1);
    return loggerEntries.length === 1 ? loggerEntries[0] : undefined;
  }

  static level(): LoggingLevel {
    return Logger.loggerConfiguration.level;
  }

  /**
   * Returns all the entries currently in the list.
   */
  static listEntries(): LoggerEntry[] {
    return Logger.entries.slice(0);
  }

  /**
   * Clears all entries and resets the logger's configuration to its default values.
   */
  static reset() {
    Logger.entries.splice(0);
    Logger.loggerConfiguration = {
      consoleOut: true,
      level: LoggingLevel.TRACE,
      storageLimit: 1000
    };
  }

  /**
   * If set to "true", it will output logged entries in the browser's console.
   */
  static setConsoleOut(consoleOut: boolean) {
    Logger.loggerConfiguration.consoleOut = consoleOut;
  }

  /**
   * Sets the logging level, which defines which logs will be stored.
   * @see LoggingLevel
   */
  static setLevel(loggingLevel: LoggingLevel) {
    Logger.loggerConfiguration.level = loggingLevel;
  }

  /**
   * Defines the amount of entries to be stored in memory before the oldest entries are deleted.
   */
  static setStorageLimit(storageLimit: number) {
    Logger.loggerConfiguration.storageLimit = storageLimit;
  }

  private static entries: LoggerEntry[] = [];

  private static loggerConfiguration: any = {
    consoleOut: true,
    level: LoggingLevel.TRACE,
    storageLimit: 1000
  };

  private constructor(private loggerName: string) {
  }

  /**
   * Logs one or more comma-separated entries at "debug" level.
   */
  debug(...entries: any[]) {
    if (Logger.loggerConfiguration.level <= LoggingLevel.DEBUG) {
      entries.forEach((entry: any) => {
        const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.DEBUG);
        if (Logger.loggerConfiguration.consoleOut) {
          console.debug(loggerEntry.meta(), loggerEntry.getEntry());
        }
      });
    }
  }

  /**
   * Logs one or more comma-separated entries at "error" level.
   */
  error(...entries: any[]) {
    if (Logger.loggerConfiguration.level <= LoggingLevel.ERROR) {
      entries.forEach((entry: any) => {
        const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.ERROR);
        if (Logger.loggerConfiguration.consoleOut) {
          console.error(loggerEntry.meta(), loggerEntry.getEntry());
        }
      });
    }
  }

  /**
   * Logs one or more comma-separated entries at "fatal" level.
   */
  fatal(...entries: any[]) {
    entries.forEach((entry: any) => {
      const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.FATAL);
      if (Logger.loggerConfiguration.consoleOut) {
        console.error(loggerEntry.meta(), loggerEntry.getEntry());
      }
    });
  }

  /**
   * Logs one or more comma-separated entries at "info" level.
   */
  info(...entries: any[]) {
    if (Logger.loggerConfiguration.level <= LoggingLevel.INFO) {
      entries.forEach((entry: any) => {
        const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.INFO);
        if (Logger.loggerConfiguration.consoleOut) {
          console.info(loggerEntry.meta(), loggerEntry.getEntry());
        }
      });
    }
  }

  /**
   * Returns the last entry of this logger instance.
   */
  lastEntry(): LoggerEntry {
    return Logger.entries
      .slice(0)
      .reverse()
      .find(entry => entry.isFromLogger(this.loggerName));
  }

  /**
   * Logs one or more comma-separated entries at "trace" level.
   */
  trace(...entries: any[]) {
    if (Logger.loggerConfiguration.level <= LoggingLevel.TRACE) {
      entries.forEach((entry: any) => {
        const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.TRACE);
        if (Logger.loggerConfiguration.consoleOut) {
          console.debug(loggerEntry.meta(), loggerEntry.getEntry());
        }
      });
    }
  }

  /**
   * Logs one or more comma-separated entries at "warn" level.
   */
  warn(...entries: any[]) {
    if (Logger.loggerConfiguration.level <= LoggingLevel.WARN) {
      entries.forEach((entry: any) => {
        const loggerEntry: LoggerEntry = this.addEntry(entry, LoggingLevel.WARN);
        if (Logger.loggerConfiguration.consoleOut) {
          console.warn(loggerEntry.meta(), loggerEntry.getEntry());
        }
      });
    }
  }

  private addEntry(entry: any, level: LoggingLevel): LoggerEntry {
    const loggerEntry: LoggerEntry = LoggerEntry.of(this.loggerName, level, entry);

    Logger.entries.push(loggerEntry);

    if (Logger.entries.length > Logger.loggerConfiguration.storageLimit) {
      Logger.entries.shift();
    }

    return loggerEntry;
  }
}
