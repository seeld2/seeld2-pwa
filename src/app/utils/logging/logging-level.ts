export enum LoggingLevel {
  /**
   * TRACE level should be used to trace the flow of events.
   */
  TRACE = 0,
  /**
   * DEBUG level should be used to display values, computation results, etc.
   */
  DEBUG = 1,
  /**
   * INFO level should be used to inform on a process that has been executed normally, such as the detected environment
   * (mobile, browser...), etc.
   */
  INFO = 2,
  /**
   * WARN level should be used to inform of something that has not gone as wished but that was handled as expected.
   */
  WARN = 3,
  /**
   * ERROR level should be used to inform of an error which the user must be informed of.
   */
  ERROR = 4,
  /**
   * FATAL should be used when there is no possibility of recovery.
   */
  FATAL = 5
}
