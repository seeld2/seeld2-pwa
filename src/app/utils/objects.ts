export class Objects {

  static propertyNamesOf(instance: any): string[] {
    return Object.keys(Object.getPrototypeOf(instance));
  }
}
