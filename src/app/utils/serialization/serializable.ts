export interface Serializable {

  /**
   * Converts the implementing type to an object.
   * The object's values that also implement the Serializable interface will be recurrently converted by the Serializer.
   * This method is used by {@Serializer}. It should never be called directly.
   * Use <code>Serializer.toObject()</code> instead.
   */
  asObject(): any;
}
