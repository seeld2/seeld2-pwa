import {Serializer} from "./serializer";
import {Serializable} from "./serializable";

describe("Serializer", () => {

  let child: Serializable;
  let parent: Serializable;
  let parentWithArray: Serializable;
  let serializable: Serializable;

  beforeEach(() => {
    child = {
      asObject(): any {
        return {value: 456};
      }
    };
    parent = {
      asObject(): any {
        return {text: "some text", child: child};
      }
    };
    parentWithArray = {
      asObject(): any {
        return {value: 123, children: [child, child]};
      }
    };
    serializable = {
      asObject(): any {
        return {text: "some text", value: 123};
      }
    };
  });

  it("should JSONify a simple Serializable", () => {

    const json: string = Serializer.toJson(serializable);

    expect(json).toEqual('{"text":"some text","value":123}');
  });

  it("should JSONify a Serializable having a Serializable property", () => {

    const json: any = Serializer.toJson(parent);

    expect(json).toEqual('{"text":"some text","child":{"value":456}}');
  });

  it("should serialize a simple Serializable to a plain JavaScript object", () => {

    const object: any = Serializer.toObject(serializable);

    expect(object['text']).toEqual("some text");
    expect(object['value']).toEqual(123);
  });

  it("should serialize a Serializable with a Serializable property to a plain JavaScript object", () => {

    const parentObject: any = Serializer.toObject(parent);

    expect(parentObject['text']).toEqual("some text");
    expect(Object.keys(parentObject)).toContain('child');
    const childObject: any = parentObject['child'];
    expect(childObject['value']).toEqual(456);
  });

  it("should serialize a Serializable containing an array of Serializables", () => {

    const parentObject: any = Serializer.toObject(parentWithArray);

    expect(parentObject['value']).toEqual(123);
    expect(parentObject['children'].length).toEqual(2);
    expect(parentObject['children'][0]['value']).toEqual(456);
    expect(parentObject['children'][1]['value']).toEqual(456);
  });
});
