import {Serializable} from "./serializable";
import * as _ from 'lodash';

export class Serializer {

  /**
   * Expresses the specified object as a JSON string, recurrently taking into account all Serializable properties.
   */
  static toJson(serializable: Serializable | any): string {
    const object: any = Serializer.toObject(serializable);
    return JSON.stringify(object);
  }

  /**
   * Serializes the specified object to transform it into a plain JavaScript object.
   * If the passed parameter is a {@link Serializable}, the object is serialized and its properties are recurrently
   * serialized (if they are themselves Serializable).
   * If the passed serializable is an object, its Serializable values will be recurrently serialized.
   */
  static toObject(serializable: Serializable | any): any {

    const serialized: any = _.cloneDeep(serializable.asObject ? serializable.asObject() : serializable);

    Object.keys(serialized).forEach((propertyName: string) => {
      const property: any = serialized[propertyName];
      if (!property) {
        return;
      }

      if (Array.isArray(property)) {
        property.forEach((item, index) => property[index] = Serializer.toObject(item as Serializable));

      } else if (property['asObject']) {
        serialized[propertyName] = Serializer.toObject(property as Serializable);
      }
    });

    return serialized;
  }
}
