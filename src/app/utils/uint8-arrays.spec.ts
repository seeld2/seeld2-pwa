import {Uint8Arrays} from "./uint8-arrays";

describe('Given Uint8Arrays', () => {

  const array1: Uint8Array = new Uint8Array([1, 2, 3]);
  const array2: Uint8Array = new Uint8Array([4, 5, 6]);

  it('should concatenate two arrays', () => {
    expect(Uint8Arrays.concat(array1, array2))
      .toEqual(new Uint8Array([1, 2, 3, 4, 5, 6]));
  });
});
