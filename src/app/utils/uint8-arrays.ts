export class Uint8Arrays {

  static concat(...arrays: Uint8Array[]): Uint8Array {

    let totalLength = 0;
    for (let arr of arrays) {
      totalLength += arr.length;
    }

    let concatenated: Uint8Array = new Uint8Array(totalLength);

    let offset = 0;
    for (let arr of arrays) {
      concatenated.set(arr, offset);
      offset += arr.length;
    }

    return concatenated;
  }
}
