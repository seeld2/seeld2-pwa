import {Versions} from "./versions";

describe("Versions", () => {

  describe(", when checking if is greater than another version", () => {

    it("should return false if version is lower than other version", () => {
      const versions: Versions = Versions.from({mobileVersion: '2.2.2'});
      expect(versions.isGreaterThan('3.2.2')).toEqual(false);
      expect(versions.isGreaterThan('2.3.2')).toEqual(false);
      expect(versions.isGreaterThan('2.2.3')).toEqual(false);
    });

    it("should return false if version is equal to other version", () => {
      const versions: Versions = Versions.from({mobileVersion: '2.2.2'});
      expect(versions.isGreaterThan('2.2.2')).toEqual(false);
    });

    it("should return true if version is greater than other version", () => {
      const versions: Versions = Versions.from({mobileVersion: '2.2.2'});
      expect(versions.isGreaterThan('1.2.2')).toEqual(true);
      expect(versions.isGreaterThan('2.1.2')).toEqual(true);
      expect(versions.isGreaterThan('2.2.1')).toEqual(true);
    });

    it("should return false version doesn't have the same amount of parts than other version", () => {
      expect(Versions.from({mobileVersion: '1.2.3'}).isGreaterThan('1.2')).toEqual(false);
      expect(Versions.from({mobileVersion: '1.2'}).isGreaterThan('1.2.3')).toEqual(false);
    });

    it("should return false if version is undefined", () => {
      expect(Versions.from({}).isGreaterThan('1.2.3')).toEqual(false);
    });

    it("should return false if other version is undefined", () => {
      expect(Versions.from({mobileVersion: '2.2.2'}).isGreaterThan(undefined)).toEqual(false);
    });
  });
});
