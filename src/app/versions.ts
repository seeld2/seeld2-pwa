import {Logger} from "./utils/logging/logger";

export class Versions {

  private readonly logger = Logger.for("Versions");

  static from(response): Versions {
    return new Versions(response.mobileVersion);
  }

  private constructor(private readonly _mobileVersion: string) {
  }

  isGreaterThan(otherVersion: string): boolean {
    if (!this._mobileVersion || !otherVersion) {
      return false;
    }
    const mobileVersionParts: string[] = this._mobileVersion.split('.');
    const otherVersionParts: string[] = otherVersion.split('.');
    if (mobileVersionParts.length !== otherVersionParts.length) {
      this.logger.error(`Cannot compare ${this._mobileVersion} with ${otherVersion}`);
      return false;
    }

    for (let i = 0; i < mobileVersionParts.length; i++) {
      const mobileVersionPart = Number.parseInt(mobileVersionParts[i]);
      const otherVersionPart = Number.parseInt(otherVersionParts[i]);
      if (mobileVersionPart > otherVersionPart) {
        return true;
      }
    }
    return false;
  }

  mobileVersion(): string {
    return this._mobileVersion;
  }
}
