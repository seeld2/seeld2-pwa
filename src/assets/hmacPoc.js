jQuery(document).ready(function() {

  var secretKey = "ddZQSrmcajDIMYCjueZcfAnxgWvTNjIcasqcdSZZjMLUPNmOfDZuHPpuQwGjULUd";

  var encodedSecretKey = new TextEncoder().encode(secretKey);
  console.log("Encoded secret key:", encodedSecretKey);

  window.crypto.subtle.importKey(
    "raw",
    encodedSecretKey,
    {name: "HMAC", hash: {name: "SHA-512"},},
    false, //whether the key is extractable
    ["sign"]
  )
    .then(function(key){
      const encodedData = new TextEncoder().encode("This is a test");
      console.log("Encoded data:", encodedData);

      return window.crypto.subtle.sign(
        "HMAC",
        key,
        encodedData //ArrayBuffer of data you want to sign
      );
    })
    .then(function(arraybuffer) {
      console.log("Encoded hashed data:", arraybuffer);

      const base64Hash = base64js.fromByteArray(new Uint8Array(arraybuffer));
      console.info("Base64 hashed data", base64Hash);
    })
    .catch(function(err){
      console.error(err);
    });
});
