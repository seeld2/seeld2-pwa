import {Injectable} from "@angular/core";
import {AlertController} from "ionic-angular";
import {File} from "@ionic-native/file";
import {SessionService} from "../../app/sessions/session.service";

@Injectable()
export class AccountKeys {

  constructor(private alertCtrl: AlertController,
              private file: File,
              private sessionService: SessionService) {
  }

  saveToFileAndroid(fileName: string) {
    this.file
      .checkFile(this.file.externalRootDirectory, fileName)
      .then(() => {
        this.alertCtrl.create({
          buttons: [{
            text: 'Yes, replace',
            handler: () => {
              this.doSaveToFileAndroid(fileName, true);
            }
          }, {
            text: 'No wait! Abort!!!',
            handler: () => {
            }
          }],
          enableBackdropDismiss: false,
          message: `The file ${fileName} is already present.<br/>Shall we overwrite that file?`,
          title: 'File already exists!'
        }).present();

      })
      .catch(() => {
        this.doSaveToFileAndroid(fileName);
      });
  }

  saveToFileBrowser(fileName: string) {
    const saveToFileLink = document.getElementById('saveToFileLink');
    saveToFileLink.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.accountKeysDataAsString()));
    saveToFileLink.setAttribute('download', fileName);
    saveToFileLink.click();

    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      this.alertCtrl.create({
        buttons: ['OK'],
        enableBackdropDismiss: false,
        message: `They should be located in your "Downloads" folder.<br/><strong>Do NOT lose your account keys: if you do, your account and messages will be lost!</strong>`,
        subTitle: 'Make sure you store that file in a safe place!',
        title: 'Account Keys saved!'
      }).present();
    }, 500);
  }

  private doSaveToFileAndroid(fileName: string, replace: boolean = false) {
    this.file
      .writeFile(this.file.externalRootDirectory, fileName, this.accountKeysDataAsString(), {replace: replace})
      .then(result => {
        this.alertCtrl.create({
          buttons: ['OK'],
          enableBackdropDismiss: false,
          message: `They should be located at "${result.fullPath}" on either your Internal storage or your SD Card.<br/><strong>Do NOT lose your account keys: if you do, your account and messages will be lost!</strong>`,
          subTitle: 'Make sure you store that file in a safe place!',
          title: 'Account Keys saved!'
        }).present();
      })
      .catch(error => {
        this.alertCtrl.create({
          buttons: ['OK'],
          message: `For some very strange reason your Account Keys could not be saved... We're so sorry!<br/>Please contact support with this information: ${JSON.stringify(error)}`,
          title: 'Unable to save your keys :('
        });
      });
  }

  private accountKeysDataAsString(): string {
    return JSON.stringify(this.sessionService.session().systemPublicKeys());
  }
}
