import {Conversation} from "../../../app/conversations/conversation";
import {MessageTestBuilder as aMessage} from "../../../app/conversations/messages/message.test-builder";

/**
 * @deprecated Use the ConversationTestBuilder approach
 */
export class ConversationFixture {

  static readonly CONVERSATION_ID: string = 'conversationId';
  static readonly READ: boolean = true;
  static readonly SEND_DATE: number = 1536309324475;
  static readonly USERNAME: string = 'username';

  static build(): Conversation {
    return this.withId(this.CONVERSATION_ID)
  }

  static withConversationIdAndSendDate(conversationId: string, sendDate: number): Conversation {
    return this.with(conversationId, true, sendDate);
  }

  static withId(id: string): Conversation {
    return this.with(id, this.READ, this.SEND_DATE);
  }

  static withRead(read: boolean) {
    return this.with(this.CONVERSATION_ID, read, this.SEND_DATE);
  }

  private static with(id: string, read: boolean, sendDate: number): Conversation {

    const participants: any = {};
    participants[this.USERNAME] = id;
    const message = aMessage.with().participants(participants).conversationId(id).and.sendDate(sendDate).noBlanks();
    // const message = MessageFixture.withParticipants(participants);
    // message.setConversationId(id);
    // message.sendDate = sendDate;

    return Conversation.with(id, message, read);
  }
}
