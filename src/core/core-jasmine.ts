import {MessagesService} from "../app/conversations/messages/messages.service";
import {RemoteNotificationsServiceProvider} from "../app/notifications/remote-notifications-service-provider.service";
import {ConversationsRemote} from "../app/conversations/conversations-remote";
import {UnreadMessagesPollerService} from "../app/conversations/messages/unread/unread-messages-poller.service";
import {MessagesStorageService} from "../app/conversations/messages/messages-storage.service";
import {LoaderService} from "../app/mobile/loader.service";
import {Objects} from "../app/utils/objects";
import {MessagesRemote} from "../app/conversations/messages/messages-remote";
import {MessagesCache} from "../app/conversations/messages/messages-cache";

export class CoreJasmine {

  static conversationsRemote() {
    return jasmine.createSpyObj('conversationsRemote', Objects.propertyNamesOf(new ConversationsRemote(null, null)));
  }

  static files() {
    return jasmine.createSpyObj('files', ['readHtmlFileInputAsBytes']);
  }

  static loaderService() {
    return jasmine.createSpyObj('loaderService', Objects.propertyNamesOf(new LoaderService(null)));
  }

  static messages() {
    return jasmine.createSpyObj('messages',
      Objects.propertyNamesOf(new MessagesService(null, null, null, null, null, null)));
  }

  static messagesCache() {
    return jasmine.createSpyObj('MessagesCache', Objects.propertyNamesOf(new MessagesCache(null, null)));
  }

  static messagesRemote() {
    return jasmine.createSpyObj('messagesRemote', Objects.propertyNamesOf(new MessagesRemote(null, null)));
  }

  static messagesStorage() {
    return jasmine.createSpyObj('messagesStorage',
      Objects.propertyNamesOf(new MessagesStorageService(null, null, null, null)));
  }

  static navController() {
    return jasmine.createSpyObj('NavController', ['getPrevious']);
  }

  static registrationsRemote() {
    return jasmine.createSpyObj('registrationsRemote', ['submitRegistration']);
  }

  static remoteNotificationsService() {
    return jasmine.createSpyObj('remoteNotificationsService', ['registerBoxAddresses']);
  }

  static remoteNotificationsServiceProvider() {
    return jasmine.createSpyObj('remoteNotificationsServiceProvider',
      Objects.propertyNamesOf(new RemoteNotificationsServiceProvider(null, null, null, null, null)));
  }

  static sessionStorageService() {
    return jasmine.createSpyObj('sessionStorageService',
      ['clearSession', 'isSessionStored', 'retrieveSession', 'storeSession', 'unlock', 'storageAes']);
  }

  static storageService() {
    return jasmine.createSpyObj('storageService', ['clear', 'get', 'has', 'set']);
  }

  static unreadMessagesPollerService() {
    return jasmine.createSpyObj('unreadMessagesPollerService',
      Objects.propertyNamesOf(new UnreadMessagesPollerService(null, null, null, null, null)));
  }
}
