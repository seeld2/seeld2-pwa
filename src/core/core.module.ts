import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FieldMatchesValidator} from "../shared/angular/validation/field-matches-validator";
import {SharedModule} from "../shared/shared.module";
import {AccountKeys} from "./account-keys/account-keys";

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SharedModule
  ],
  declarations: [],
  exports: [
    FieldMatchesValidator
  ],
  providers: [
    // Services
    AccountKeys,
  ]
})
export class CoreModule {
}
