import {Locks} from "./locks";
import {Lock} from "../shared/concurrency/lock";

describe("Locks", () => {

  const OWNER: string = 'owner';

  beforeEach(() => {
    Locks['messagesLock'].unlock();
  });

  it("should obtain a messages' lock", (done: DoneFn) => {

    Locks.getMessagesLock(OWNER)
      .subscribe((lock: Lock) => {
        expect(lock).toBeDefined();
        done();
      });
  });

  it("should indicate whether a lock is available or not", (done: DoneFn) => {

    expect(Locks.isMessagesLockAvailable()).toEqual(true);

    Locks['messagesLock'].lock(OWNER).then(() => {

      expect(Locks.isMessagesLockAvailable()).toEqual(false);

      done();
    });
  });

  afterEach(() => {
    Locks['messagesLock'].unlock();
  });
});
