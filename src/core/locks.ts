import {Observable} from "rxjs";
import {from} from "rxjs/observable/from";
import {Lock} from "../shared/concurrency/lock";
import {ExclusiveLock} from "../shared/concurrency/exclusive-lock";

/**
 * A singleton for managing context-specific locks so that they can be shared between processes.
 */
export class Locks {

  private static readonly messagesLock: Lock = new ExclusiveLock();

  /**
   * Returns the lock shared by messages-related tasks.
   */
  static getMessagesLock(owner: string): Observable<Lock> {
    return from(this.messagesLock.lock(owner));
  }

  /**
   * Returns whether the lock shared by messages-related tasks is locked or not.
   */
  static isMessagesLockAvailable(): boolean {
    return !this.messagesLock.isLocked();
  }
}
