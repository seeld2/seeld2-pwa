export class AngularJasmine {

  static httpClient() {
    return jasmine.createSpyObj('httpClient', ['get', 'post', 'put']);
  }

  static ngZone() {
    return jasmine.createSpyObj('ngZone', ['run']);
  }
}
