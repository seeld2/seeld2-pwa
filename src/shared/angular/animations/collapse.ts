import {animate, state, style, transition, trigger} from "@angular/animations";

export const collapse = {
  trigger: trigger('collapse', [
    state('hide', style({overflow: 'hidden', height: 0, opacity: 0.0})),
    state('show', style({overflow: 'hidden', height: "*", opacity: 1.0})),
    transition('hide => show', animate('300ms ease-in')),
    transition('show => hide', animate('300ms ease-out'))
  ])
};

