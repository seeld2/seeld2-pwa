import {AbstractControl} from "@angular/forms";

export class Validation {

  /**
   * Forces the specified AbstractControl to be marked as touched, dirty and to update its validity.
   */
  static force(control: AbstractControl) {
    control.markAsTouched();
    control.markAsDirty();
    control.updateValueAndValidity();
  }

  /**
   * Forces the update of parent <ion-item> elements' style, following the validation performed by Angular.<br/>
   * This is necessary at the moment because the change in classes ('ng-valid', 'ng-invalid', 'ng-touched', etc.)
   * performed by Angular in the input element is not always propagated to the upper elements for some reason,
   * resulting in, for example, a valid form that is still marked red by Ionic.
   *
   * @param {string} inputElementName The name of the <ion-input> element whose parent <ion-item> must be refreshed.
   * @param {number} delay (optional) An execution delay in milliseconds.
   */
  static refreshIonItem(inputElementName: string, delay ?: number) {
    if (delay) {
      const timeoutId = setTimeout(() => {
        Validation.doRefreshIonItem(inputElementName);
        clearTimeout(timeoutId);
      }, delay);
    } else {
      Validation.doRefreshIonItem(inputElementName);
    }
  }

  private static doRefreshIonItem(inputElementName: string) {

    let ionInputElement = document.querySelector('ion-input[name="' + inputElementName + '"]');
    if (ionInputElement == null) {
      ionInputElement = document.querySelector('ion-textarea[name="' + inputElementName + '"]');
    }
    if (ionInputElement == null) {
      console.warn('No element named ' + inputElementName + ' could be found in the DOM');
      return;
    }

    let ionItemElement = ionInputElement.parentElement;
    while (ionItemElement.nodeName.toLowerCase() !== 'ion-item') {
      ionItemElement = ionItemElement.parentElement;

      if (ionItemElement.nodeName.toLowerCase() === 'html') {
        throw new Error('Something went seriously wrong here...');
      }
    }
    ionItemElement.classList.toggle('ng-untouched', false);
    ionItemElement.classList.toggle('ng-touched', true);

    ionItemElement.classList.toggle('ng-valid', ionInputElement.classList.contains('ng-valid'));
    ionItemElement.classList.toggle('ng-invalid', ionInputElement.classList.contains('ng-invalid'));
  }
}
