import {Lock} from "./lock";
import {ExclusiveLock} from "./exclusive-lock";

describe("ExclusiveLock", () => {

  const OWNER_A = 'A';
  const OWNER_B = 'B';

  let lock: Lock = new ExclusiveLock();

  it("should acquire and release a lock", (done: DoneFn) => {

    lock.lock(OWNER_A)
      .then(() => {
        expect(lock.isLocked()).toBe(true);

        lock.unlock();
        expect(lock.isLocked()).toBe(false);

        done();
      });
  });

  it("should lock other acquirers until the lock is released by its first owner", (done: DoneFn) => {

    const lockOwners: string[] = [];

    lock.lock(OWNER_A)
      .then(() => {
        setTimeout(() => {
          lockOwners.push(OWNER_A);
          lock.unlock();
        }, 50);

        lock.lock(OWNER_B)
          .then(() => {
            lockOwners.push(OWNER_B);
            lock.unlock();

            expect(lockOwners.length).toBe(2);
            expect(lockOwners[0]).toEqual(OWNER_A);
            expect(lockOwners[1]).toEqual(OWNER_B);

            done();
          });
      });
  });

  it("should block same owner to re-acquire lock the lock is released", (done: DoneFn) => {

    const sequence: string[] = [];

    lock.lock(OWNER_A)
      .then(() => {
        setTimeout(() => {
          sequence.push('1');
          lock.unlock();
        }, 50);

        lock.lock(OWNER_A)
          .then(() => {
            sequence.push('2');
            lock.unlock();

            expect(sequence.length).toBe(2);
            expect(sequence[0]).toEqual('1');
            expect(sequence[1]).toEqual('2');

            done();
          });
      });
  });
});
