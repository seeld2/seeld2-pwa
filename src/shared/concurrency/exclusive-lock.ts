import {Lock} from "./lock";
import {Logger} from "../../app/utils/logging/logger";

export class ExclusiveLock implements Lock {

  private readonly logger: Logger = Logger.for('ExclusiveLock');

  private lock_: boolean = false;
  private lockOwner_: string;

  getOwner(): string {
    return this.lockOwner_;
  }

  isLocked(): boolean {
    return this.lock_;
  }

  lock(owner: string): Promise<Lock> {
    return new Promise(resolve => {

      const waitForLock = (waitTimeInMillis: number, owner: string) => {
        const timeoutId = setTimeout(() => {

          if (!this.lock_) {
            this.lock_ = true;
            this.lockOwner_ = owner;
            this.logger.debug(`Acquired lock for owner ${this.lockOwner_}`);
            clearTimeout(timeoutId);
            resolve(this);

          } else {
            waitForLock(100, owner)
          }

        }, waitTimeInMillis);
      };

      waitForLock(0, owner);
    });
  }

  unlock() {
    this.logger.debug(`Released lock for owner ${this.lockOwner_}`);
    this.lockOwner_ = undefined;
    this.lock_ = false;
  }
}
