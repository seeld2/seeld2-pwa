export interface Lock {

  /**
   * Returns the declared owner of this lock.
   */
  getOwner(): string;

  /**
   * Returns true if the lock is currently locked, false otherwise.
   */
  isLocked(): boolean;

  /**
   * Acquires the lock.
   * If the lock is not available then the process waits until the lock becomes available.
   */
  lock(owner: string): Promise<Lock>

  /**
   * Unlocks this instance of a lock.
   */
  unlock();
}
