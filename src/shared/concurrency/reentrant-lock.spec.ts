import {ReentrantLock} from "./reentrant-lock";

describe("ReentrantLock", () => {

  const OWNER_A = 'A';
  const OWNER_B = 'B';

  it("should mutually lock owners, waiting for the lock to be released", (done: DoneFn) => {

    const lockOwners: string[] = [];

    const lock: ReentrantLock = new ReentrantLock();

    lock.waitForLock(OWNER_A)
      .then(() => {
        setTimeout(() => {
          lockOwners.push(OWNER_A);
          lock.unlock();
        }, 50);

        return lock.waitForLock(OWNER_B)
      })
      .then(() => {
        lockOwners.push(OWNER_B);
        lock.unlock();

        expect(lockOwners.length).toEqual(2);
        expect(lockOwners[0]).toEqual(OWNER_A);
        expect(lockOwners[1]).toEqual(OWNER_B);

        done();
      });
  });

  it("should allow same owner to re-access the same lock (re-entrancy)", (done: DoneFn) => {

    const lock: ReentrantLock = new ReentrantLock();

    lock.waitForLock(OWNER_A);
    const promise2 = lock.waitForLock(OWNER_A);

    promise2.then(() => {
      lock.unlock();
      done();
    });
  });
});
