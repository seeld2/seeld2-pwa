/**
 * Simple implementation of a ReentrantLock.
 */
import {Logger} from "../../app/utils/logging/logger";

export class ReentrantLock {

  private readonly logger: Logger = Logger.for('ReentrantLock');

  private lock_: boolean = false;
  private lockOwner_: string = '';

  constructor() {
  }

  /**
   * Releases the acquired lock.
   */
  unlock() {
    this.logger.debug(`'Unlocking lock for owner ${this.lockOwner_}`);
    this.lockOwner_ = '';
    this.lock_ = false;
  }

  /**
   * Attempts to acquire a lock.
   *
   * @param lockOwner A string that identifies the owner requesting the lock.
   * @returns True if the lock was acquired, false otherwise.
   */
  waitForLock(lockOwner: string): Promise<boolean> {

    return new Promise((resolve) => {

      const waitAndAcquireLockWhenAvailable = (waitTimeInMillis: number, lockOwner) => {
        const timeoutId = setTimeout(() => {
          clearTimeout(timeoutId);

          if (this.lock_ === false) {
            this.doLock(lockOwner);
            resolve(true);

          } else if (lockOwner === this.lockOwner_) {
            this.doLock(lockOwner);
            resolve(true);

          } else {
            waitAndAcquireLockWhenAvailable(100, lockOwner);
          }

        }, waitTimeInMillis);
      };

      waitAndAcquireLockWhenAvailable(0, lockOwner);
    });
  }

  private doLock(lockOwner: string) {
    this.lock_ = true;
    this.lockOwner_ = lockOwner;
    this.logger.debug(`Locking lock for owner ${this.lockOwner_}`);
  }
}
