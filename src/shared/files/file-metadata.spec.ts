import {FileMetadata} from "./file-metadata";

describe("FileMetadata", () => {

  const FILE_CONTENT_TEXT = 'file content';
  const FILENAME: string = 'filename';
  const MIME_TYPE: string = 'mime/type';

  let file: File;
  let fileInput: HTMLInputElement;

  beforeEach(() => {
    file = new File([FILE_CONTENT_TEXT], FILENAME, {type: MIME_TYPE});
    fileInput = {files: new FileListFake(file)} as HTMLInputElement;
  });

  it("should build an instance from an HTMLInputElement", () => {

    const fileMetadata: FileMetadata = FileMetadata.of(fileInput);

    expect(fileMetadata).toBeDefined();
    expect(fileMetadata.filename).toBe(FILENAME);
    expect(fileMetadata.mime).toBe(MIME_TYPE);
  });
});

class FileListFake implements FileList {

  constructor(...files: File[]) {
    files.forEach((file: File, index: number) => {
      this[index] = file;
    });
    this.length = files.length;
  }

  [index: number]: File;

  readonly length: number;

  item(index: number): File {
    return undefined;
  }
}
