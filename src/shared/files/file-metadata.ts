export class FileMetadata {

  private constructor(public filename: string,
                      public mime: string) {
  }

  static of(fileInput: HTMLInputElement): FileMetadata {
    if (fileInput.files && fileInput.files.length > 0) {
      const file = fileInput.files[0];
      return new FileMetadata(file.name, file.type);
    }
    return null;
  }
}
