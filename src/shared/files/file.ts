import {FileMetadata} from "./file-metadata";

declare var base64js: any;

export class File {

  private constructor(public content: ArrayBuffer,
                      public metadata: FileMetadata) {
  }

  static of(content: ArrayBuffer, metadata: FileMetadata): File {
    return new File(content, metadata);
  }

  // TODO Test this
  dataUrl(): string {
    // const metadata = this.model.attachmentMetadata;
    let dataUrl = 'data:';
    if (this.metadata && this.metadata.mime) {
      dataUrl += this.metadata.mime;
    }
    dataUrl += ';base64,' + base64js.fromByteArray(new Uint8Array(this.content));
    return dataUrl;
  }
}
