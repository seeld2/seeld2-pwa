import {Files} from "./files";

describe("Files", () => {

  const FILE_CONTENT_BYTES = new Int8Array([1, 3, 5]);
  const FILE_CONTENT_TEXT = 'file content';
  const FILENAME = 'filename';
  const MIME_TYPE = 'mime/type';

  let file: File;
  let fileInput: HTMLInputElement;
  let reader: FileReader;

  let files: Files;

  beforeEach(() => {
    file = new File([FILE_CONTENT_TEXT], FILENAME, {type: MIME_TYPE});
    fileInput = {files: new FileListFake(file)} as HTMLInputElement;

    files = new Files();
    files['newFileReader'] = () => {
      return reader;
    }
  });

  describe("when reading an HTML file input as bytes", () => {

    it("does nothing if no file was selected", () => {
      fileInput = {files: new FileListFake()} as HTMLInputElement;
      reader = new FileReaderFake('load', new EventStub(FILE_CONTENT_TEXT));

      let success: boolean = false;
      files.readHtmlFileInputAsBytes(fileInput, () => {
        success = true;
      });

      expect(success).toBe(false);
    });

    it("does nothing if passed success function is null or undefined", () => {

      files.readHtmlFileInputAsBytes(fileInput, null);
    });

    it("reads bytes from an HTML file input", () => {
      reader = new FileReaderFake('load', new EventStub(FILE_CONTENT_BYTES));

      spyOn(reader, 'readAsArrayBuffer');
      (reader.readAsArrayBuffer as jasmine.Spy).and.callThrough();

      let result: string = '';
      files.readHtmlFileInputAsBytes(fileInput, (event: Event) => {
        result = event.target['result'];
      });

      expect(reader.readAsArrayBuffer).toHaveBeenCalled();

      expect(result).toBe(FILE_CONTENT_BYTES);
    });

    it("calls an error function if an error occurs", () => {
      reader = new FileReaderFake('error', null, new ErrorEvent(null, {message: 'error'}));

      let result: string = '';
      files.readHtmlFileInputAsBytes(fileInput, () => {}, (errorEvent: ErrorEvent) => {
        result = errorEvent.message;
      });

      expect(result).toBe('error');
    });

    it("calls an abort function if an abort occurs", () => {
      reader = new FileReaderFake('abort', new EventStub('abort'));

      let result: string = '';
      files.readHtmlFileInputAsBytes(fileInput, () => {}, () => {}, (event: Event) => {
        result = event.target['result'];
      });

      expect(result).toBe('abort');
    });
  });

  describe("when reading an HTML file input as text", () => {

    it("does nothing if no file was selected", () => {
      fileInput = {files: new FileListFake()} as HTMLInputElement;
      reader = new FileReaderFake('load', new EventStub(FILE_CONTENT_TEXT));

      let success: boolean = false;
      files.readHtmlFileInputAsText(fileInput, () => {
        success = true;
      });

      expect(success).toBe(false);
    });

    it("does nothing if passed success function is null or undefined", () => {

      files.readHtmlFileInputAsText(fileInput, null);
    });

    it("reads text from an HTML file input", () => {
      reader = new FileReaderFake('load', new EventStub(FILE_CONTENT_TEXT));

      spyOn(reader, 'readAsText');
      (reader.readAsText as jasmine.Spy).and.callThrough();

      let result: string = '';
      files.readHtmlFileInputAsText(fileInput, (event: Event) => {
        result = event.target['result'];
      });

      expect(reader.readAsText).toHaveBeenCalled();

      expect(result).toBe(FILE_CONTENT_TEXT);
    });

    it("calls an error function if an error occurs", () => {
      reader = new FileReaderFake('error', null, new ErrorEvent(null, {message: 'error'}));

      let result: string = '';
      files.readHtmlFileInputAsText(fileInput, () => {}, (errorEvent: ErrorEvent) => {
        result = errorEvent.message;
      });

      expect(result).toBe('error');
    });

    it("calls an abort function if an abort occurs", () => {
      reader = new FileReaderFake('abort', new EventStub('abort'));

      let result: string = '';
      files.readHtmlFileInputAsText(fileInput, () => {}, () => {}, (event: Event) => {
        result = event.target['result'];
      });

      expect(result).toBe('abort');
    });
  });
});

class FileListFake implements FileList {

  constructor(...files: File[]) {
    files.forEach((file: File, index: number) => {
      this[index] = file;
    });
    this.length = files.length;
  }

  [index: number]: File;

  readonly length: number;

  item(index: number): File {
    return undefined;
  }
}

class FileReaderFake implements FileReader {
  readonly DONE: number;
  readonly EMPTY: number;
  readonly LOADING: number;
  readonly error: DOMError;
  onabort: (this: MSBaseReader, ev: Event) => any;
  onerror: (this: MSBaseReader, ev: ErrorEvent) => any;
  onload: (ev: any) => any;
  onloadend: (this: MSBaseReader, ev: ProgressEvent) => any;
  onloadstart: (this: MSBaseReader, ev: Event) => any;
  onprogress: (this: MSBaseReader, ev: ProgressEvent) => any;
  readonly readyState: number;
  readonly result: any;

  constructor(private onCallResult: string, private event: Event = null, private errorEvent: ErrorEvent = null) {
  }

  abort(): void {
  }

  addEventListener<K extends keyof MSBaseReaderEventMap>(type: K, listener: (this: FileReader, ev: MSBaseReaderEventMap[K]) => any, useCapture?: boolean): void;
  addEventListener(type: string, listener: EventListenerOrEventListenerObject, useCapture?: boolean): void;
  addEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
  addEventListener<K extends keyof MSBaseReaderEventMap>(type: K, listener: (this: MSBaseReader, ev: MSBaseReaderEventMap[K]) => any, useCapture?: boolean): void;
  addEventListener(type, listener?, useCapture?: boolean | AddEventListenerOptions): void {
  }

  dispatchEvent(evt: Event): boolean {
    return false;
  }

  readAsArrayBuffer(blob: Blob): void {
    this.readAs();
  }

  readAsBinaryString(blob: Blob): void {
  }

  readAsDataURL(blob: Blob): void {
  }

  readAsText(blob: Blob, encoding?: string): void {
    this.readAs();
  }

  removeEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void {
  }

  private readAs() {
    switch (this.onCallResult) {
      case 'load':
        this.onload(this.event);
        break;
      case 'abort':
        this.onabort(this.event);
        break;
      case 'error':
        this.onerror(this.errorEvent);
        break;
    }
  }
}

class EventStub implements Event {
  readonly AT_TARGET: number;
  readonly BUBBLING_PHASE: number;
  readonly CAPTURING_PHASE: number;
  readonly bubbles: boolean;
  cancelBubble: boolean;
  readonly cancelable: boolean;
  readonly currentTarget: EventTarget;
  readonly defaultPrevented: boolean;
  readonly eventPhase: number;
  readonly isTrusted: boolean;
  returnValue: boolean;
  readonly scoped: boolean;
  readonly srcElement: Element | null;
  readonly target: any;
  readonly timeStamp: number;
  readonly type: string;

  constructor(result: any) {
    this.target = {result: result};
  }

  deepPath(): EventTarget[] {
    return [];
  }

  initEvent(eventTypeArg: string, canBubbleArg: boolean, cancelableArg: boolean): void {
  }

  preventDefault(): void {
  }

  stopImmediatePropagation(): void {
  }

  stopPropagation(): void {
  }
}
