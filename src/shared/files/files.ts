// TODO Move all these to File class and specs!

export class Files {

  // static metadata(fileInput: HTMLInputElement): FileMetadata {
  //   let metadata: FileMetadata = undefined;
  //   if (fileInput.files && fileInput.files.length > 0) {
  //     const file = fileInput.files[0];
  //     metadata = {
  //       filename: file.name,
  //       mime: file.type
  //     };
  //   }
  //   return metadata;
  // }

  readHtmlFileInputAsBytes(fileInput: HTMLInputElement, successFunction: Function, errorFunction = undefined, abortFunction = undefined) {

    if (fileInput.files && fileInput.files.length > 0) {

      // TODO Could we make this a static call?
      const reader: FileReader = this.newFileReader();

      reader.onload = (event: Event) => {
        if (successFunction) {
          successFunction(event);
        }
      };
      reader.onabort = (abort: Event) => {
        if (!abortFunction) {
          console.error('Files.readHtmlFileInputAsBytes()[onabort]', abort);
        } else {
          abortFunction(abort);
        }
      };
      reader.onerror = (error: ErrorEvent) => {
        if (!errorFunction) {
          console.error('Files.readHtmlFileInputAsBytes()[onerror]', error);
          throw new Error('Files.readHtmlFileInputAsBytes()[onerror]' + error.message);
        } else {
          errorFunction(error);
        }
      };

      reader.readAsArrayBuffer(fileInput.files[0]);
    }
  }

  readHtmlFileInputAsText(fileInput: HTMLInputElement, successFunction: Function, errorFunction = undefined, abortFunction = undefined) {

    if (fileInput.files && fileInput.files.length > 0) {

      const reader: FileReader = this.newFileReader();

      reader.onload = (event: Event) => {
        if (successFunction) {
          successFunction(event);
        }
      };
      reader.onabort = (abort: Event) => {
        if (!abortFunction) {
          console.error('Files.readHtmlFileInputAsText()[onabort]', abort);
        } else {
          abortFunction(abort);
        }
      };
      reader.onerror = (error: ErrorEvent) => {
        if (!errorFunction) {
          console.error('Files.readHtmlFileInputAsText()[onerror]', error);
          throw new Error('Files.readHtmlFileInputAsText()[onerror]' + error.message);
        } else {
          errorFunction(error);
        }
      };

      reader.readAsText(fileInput.files[0]);
    }
  }

  // noinspection JSMethodCanBeStatic
  private newFileReader(): FileReader {
    return new FileReader();
  }
}
