export class IonicJasmine {

  static alertController() {
    return jasmine.createSpyObj('alertController', ['nope']);
  }

  static events() {
    return jasmine.createSpyObj('events', ['publish']);
  }

  static loadingController() {
    return jasmine.createSpyObj('loadingCtrl', ['create']);
  }

  static platform() {
    return jasmine.createSpyObj('platform', ['is', 'platforms']);
  }

  static popoverController() {
    return jasmine.createSpyObj('popoverController', ['create']);
  }

  static storage() {
    return jasmine.createSpyObj('storage', ['clear', 'get', 'remove', 'set']);
  }

  static toast() {
    return jasmine.createSpyObj('Toast', ['present']);
  }

  static toastCtrl() {
    return jasmine.createSpyObj('toastCtrl', ['create']);
  }
}
