import {NgModule} from "@angular/core";
import {FieldMatchesValidator} from "./angular/validation/field-matches-validator";
import {Files} from "./files/files";
import {LoaderService} from "../app/mobile/loader.service";

@NgModule({
  imports: [],
  declarations: [
    FieldMatchesValidator
  ],
  exports: [
    FieldMatchesValidator
  ],
  providers: [
    Files,
    LoaderService
  ]
})
export class SharedModule {
}
