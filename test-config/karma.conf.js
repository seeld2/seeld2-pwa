var webpackConfig = require('./webpack.test.js');

module.exports = function(config) {
  var _config = {
    basePath: '../',

    frameworks: ['jasmine'],

    files: [
      {
        pattern: './test-config/karma-test-shim.js',
        watched: true
      },
      {
        pattern: './src/assets/js/base64js.min.js',
        watched: false,
      },
      {
        pattern: './src/assets/js/encoding.js',
        watched: false,
      },
      {
        pattern: './src/assets/js/openpgp.min.js',
        watched: false
      },
      {
        pattern: './src/assets/js/rfc5054-safe-prime-config.js',
        watched: false
      },
      {
        pattern: './src/assets/js/random.js',
        watched: false
      },
      {
        pattern: './src/assets/js/sha256.js',
        watched: false
      },
      {
        pattern: './src/assets/js/biginteger.js',
        watched: false
      },
      {
        pattern: './src/assets/js/thinbus-srp6client.js',
        watched: false
      },
      {
        pattern: './src/assets/js/thinbus-srp6client-sha256.js',
        watched: false
      },
      {
        pattern: './src/assets/**/*',
        watched: false,
        included: false,
        served: true,
        nocache: false
      }
    ],

    proxies: {
      '/assets/': '/base/src/assets/'
    },

    preprocessors: {
      './test-config/karma-test-shim.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      stats: 'errors-only'
    },

    webpackServer: {
      noInfo: true
    },

    browserConsoleLogOptions: {
      level: 'log',
      format: '%b %T: %m',
      terminal: true
    },

    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },

    reporters: config.coverage ? ['kjhtml', 'dots', 'coverage-istanbul'] : ['kjhtml', 'dots'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  };

  config.set(_config);
};
